<?php
namespace App\Controller;

class MaldoPay
{
	const CLIENT_ID = "32";
	const BRAND_ID = "611";
	const integrationId = "13189";
	const authentication_sandbox_query = 'https://api.maldopay.com/json/sandbox/auth/';
	const authentication_query = 'https://api.maldopay.com/json/auth/';
	const transaction_sandbox_query = 'https://api.maldopay.com/json/sandbox/transactions/';
	const transaction_query = 'https://api.maldopay.com/json/transactions/';

	public function authentication(
		$firstName,
		$lastName,
		$address,
		$birthDate,
		$passportNo,
		$countryCode,
		$city,
		$playerId,
		$postCode,
		$currencyCode,
		$languageCode,
		$emailAddress,
		$phone,
		$ipAddr
	) {
		$url = self::authentication_query;
		$auth_array = array(
			"auth" => array(
				"clientId" => "32",
				"brandId" => "611",
				"request" => array(
					"firstName" => $firstName,
					"lastName" => $lastName,
					"address" => $address,
					"birthDate" => $birthDate,
					"passportNo" => $passportNo,
					"countryCode" => $countryCode,
					"city" => $city,
					"playerId" => $playerId,
					"postCode" => $postCode,
					"currencyCode" => $currencyCode,
					"languageCode" => $languageCode,
					"emailAddress" => $emailAddress,
					"phone" => $phone,
					"ipAddr" => $ipAddr
				)
			)
		);
		$encode_auth_array = json_encode($auth_array);
		$data = array(
			'json'   => $encode_auth_array
		);
		$data['checksum'] = hash_hmac("sha256", $data['json'], 'e29c9c180c6279b0b02abd6a1801c7c04082cf486ec027aa13515e4f3884bb6b');
		$json_data = http_build_query($data);

		$ch = curl_init();
		$timeout = 10;
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_POST, true);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $json_data);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $timeout);
		$result = curl_exec($ch);
		curl_close($ch);
		$result_array = json_decode($result, true);
		return $result_array;
	}
	public function transaction_processing($currency, $token, $amount, $referenceOrderId, $chainMode, $national_id_number)
	{
		$url = self::transaction_query;
		$transaction_array = array(
			"transaction" => array(
				"clientId" => "32",
				"brandId" => "611",
				"integrationId" => "13189",
				"landingPages" => array(
					"landingSuccess" => "https://www.luxury-discounts.com/sales/success/$referenceOrderId",
					"landingPending" => "https://www.luxury-discounts.com/landing/pending",
					"landingDeclined" => "https://www.luxury-discounts.com/landing/declined",
					"landingFailed" => "https://www.luxury-discounts.com/landing/failed"
				),
				'request' => array(
					"serviceId" => "2031",
					"currencyCode" => "TRY",
					"type" => "DEPOSIT",
					"token" => $token,
					"ipAddr" => $_SERVER['REMOTE_ADDR'],
					"amount" => $amount,
					"reason" => "LuxuryDiscount transation",
					"referenceOrderId" => $referenceOrderId,
					"chainMode" => $chainMode,
					"serviceData" => array(
						"serviceData1" => 123

					)
				)
			)
		);
		$encode_auth_array = json_encode($transaction_array);
		$data = array(
			'json'   => $encode_auth_array
		);
		$data['checksum'] = hash_hmac("sha256", $data['json'], 'e29c9c180c6279b0b02abd6a1801c7c04082cf486ec027aa13515e4f3884bb6b');
		$json_data = http_build_query($data);

		$ch = curl_init();
		$timeout = 10;
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_POST, true);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $json_data);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $timeout);
		$result = curl_exec($ch);
		curl_close($ch);
		$result_array = json_decode($result, true);
		return $result_array;
	}
}
