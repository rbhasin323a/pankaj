# LANGUAGE translation of CakePHP Application
# Copyright YEAR NAME <EMAIL@ADDRESS>
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PROJECT VERSION\n"
"POT-Creation-Date: 2019-05-15 07:12+0000\n"
"PO-Revision-Date: YYYY-mm-DD HH:MM+ZZZZ\n"
"Last-Translator: NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <EMAIL@ADDRESS>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=utf-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=INTEGER; plural=EXPRESSION;\n"

#: Template/Plugin/CakeDC/Users/Users/add.ctp:2
#: Template/Plugin/CakeDC/Users/Users/edit.ctp:2
#: Template/Plugin/CakeDC/Users/Users/index.ctp:2;15
msgid "Actions"
msgstr ""

#: Template/Plugin/CakeDC/Users/Users/add.ctp:4
#: Template/Plugin/CakeDC/Users/Users/edit.ctp:10
msgid "List Users"
msgstr ""

#: Template/Plugin/CakeDC/Users/Users/add.ctp:5
#: Template/Plugin/CakeDC/Users/Users/edit.ctp:11
msgid "List Accounts"
msgstr ""

#: Template/Plugin/CakeDC/Users/Users/add.ctp:11
msgid "Add User"
msgstr ""

#: Template/Plugin/CakeDC/Users/Users/add.ctp:21
#: Template/Plugin/CakeDC/Users/Users/change_password.ctp:33
#: Template/Plugin/CakeDC/Users/Users/edit.ctp:31
#: Template/Plugin/CakeDC/Users/Users/request_reset_password.ctp:23
#: Template/Plugin/CakeDC/Users/Users/resend_token_validation.ctp:23
#: Template/Plugin/CakeDC/Users/Users/social_email.ctp:8
msgid "Submit"
msgstr ""

#: Template/Plugin/CakeDC/Users/Users/change_password.ctp:23
msgid "Current password"
msgstr ""

#: Template/Plugin/CakeDC/Users/Users/edit.ctp:5
#: Template/Plugin/CakeDC/Users/Users/index.ctp:29
msgid "Delete"
msgstr ""

#: Template/Plugin/CakeDC/Users/Users/edit.ctp:7
#: Template/Plugin/CakeDC/Users/Users/index.ctp:29
msgid "Are you sure you want to delete # {0}?"
msgstr ""

#: Template/Plugin/CakeDC/Users/Users/edit.ctp:17
msgid "Edit User"
msgstr ""

#: Template/Plugin/CakeDC/Users/Users/index.ctp:4
msgid "New {0}"
msgstr ""

#: Template/Plugin/CakeDC/Users/Users/index.ctp:26
msgid "View"
msgstr ""

#: Template/Plugin/CakeDC/Users/Users/index.ctp:27
msgid "Change password"
msgstr ""

#: Template/Plugin/CakeDC/Users/Users/index.ctp:28
msgid "Edit"
msgstr ""

#: Template/Plugin/CakeDC/Users/Users/index.ctp:38
msgid "previous"
msgstr ""

#: Template/Plugin/CakeDC/Users/Users/index.ctp:40
msgid "next"
msgstr ""

#: Template/Plugin/CakeDC/Users/Users/login.ctp:59
msgid "If you don't have an account you can <a href=\"/register\" class=\"fw800\">register here.</a>"
msgstr ""

#: Template/Plugin/CakeDC/Users/Users/profile.ctp:7
msgid "{0} {1}"
msgstr ""

#: Template/Plugin/CakeDC/Users/Users/profile.ctp:14
msgid "Change Password"
msgstr ""

#: Template/Plugin/CakeDC/Users/Users/profile.ctp:17
msgid "Username"
msgstr ""

#: Template/Plugin/CakeDC/Users/Users/profile.ctp:19
msgid "Email"
msgstr ""

#: Template/Plugin/CakeDC/Users/Users/profile.ctp:24
msgid "Social Accounts"
msgstr ""

#: Template/Plugin/CakeDC/Users/Users/profile.ctp:28
msgid "Avatar"
msgstr ""

#: Template/Plugin/CakeDC/Users/Users/profile.ctp:29
msgid "Provider"
msgstr ""

#: Template/Plugin/CakeDC/Users/Users/profile.ctp:30
msgid "Link"
msgstr ""

#: Template/Plugin/CakeDC/Users/Users/profile.ctp:37
msgid "Link to {0}"
msgstr ""

#: Template/Plugin/CakeDC/Users/Users/social_email.ctp:5
msgid "Please enter your email"
msgstr ""

