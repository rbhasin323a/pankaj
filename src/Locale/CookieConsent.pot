# LANGUAGE translation of CakePHP Application
# Copyright YEAR NAME <EMAIL@ADDRESS>
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PROJECT VERSION\n"
"POT-Creation-Date: 2019-05-15 07:12+0000\n"
"PO-Revision-Date: YYYY-mm-DD HH:MM+ZZZZ\n"
"Last-Translator: NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <EMAIL@ADDRESS>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=utf-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=INTEGER; plural=EXPRESSION;\n"

#: Template/Element/Layout/footer_main.ctp:128
msgid "We are using cookies to secure and improve your stay on our website."
msgstr ""

#: Template/Element/Layout/footer_main.ctp:129
msgid "Thank You"
msgstr ""

#: Template/Element/Layout/footer_main.ctp:130
msgid "Learn More"
msgstr ""

