<?php
$this->HtmlBetter->addCrumb(__('Home'), '/');
$this->HtmlBetter->addCrumb(__('GDPR Tools'), '/pages/gdpr-tools');
?>
<?php $this->assign('title', 'Delete request'); ?>
<section id="GdprRequests">
	<div class="container">
		<div class="row">
			<div class="col s12 l3 sidenav-left">
				<?= $this->element('Sidebar/nav-pages'); ?>
			</div>
			<div class="col s12 l9">
				<h4 class="page-heading"><?= __('Delete request'); ?></h4>
				<div class="card-panel p30">
					<?php if (!$data['error']) { ?>
						<p>Your e-mail address: <strong><?= $data['request']['email'] ?></strong></p>
						<p><?= __('You requested to delete your personal data. Keep in mind that this process will delete your account, so you will no longer be able to access or use it anymore.'); ?></p>
						<div class="row">
							<div class="col s12">
								<form action="/GdprRequests/delete" method="post">
									<input type="hidden" name="user_id" value="<?= !empty($data['user']->id) ? $data['user']->id : 0 ?>">
									<input type="hidden" name="h" value="<?= !empty($data['hash']) ? $data['hash'] : '' ?>">
									<input type="hidden" name="email" value="<?= !empty($data['request']['email']) ? $data['request']['email'] : '' ?>">
									<button type="submit" class="btn btn-lg btn-outline-light btn-block rounded-0">Yes, delete my account!</button>
								</form>
							</div>
						</div>
					<?php } else { ?>
						<h5><?= $data['error'] ?></h5>
					<?php } ?>
				</div>
			</div>
		</div>
	</div>
</section>
