<?php
$this->HtmlBetter->addCrumb(__('Home'), '/');
$this->HtmlBetter->addCrumb(__('GDPR Tools'), '/pages/gdpr-tools');
?>
<?php $this->assign('title', 'Bookings'); ?>
<section id="GdprRequests">
	<div class="container">
		<div class="row">
			<div class="col s12 l3 sidenav-left">
				<?= $this->element('Sidebar/nav-pages'); ?>
			</div>
			<div class="col s12 l9">
				<h4 class="page-heading"><?= __('Bookings'); ?></h4>
				<div class="card-panel p30">
					<?php if (!$data['error']) { ?>
						<p>Your e-mail address: <strong><?= $data['request']['email'] ?></strong></p>
						<p><?= __('You can download your bookings using the buttons below.'); ?></p>
						<div class="row">
							<div class="col s12">
								<button class="btn btn-lg btn-outline-light btn-block rounded-0" onclick="location.href = '<?= $data['download_csv'] ?>';">Download as CSV</button>
							</div>
						</div>
						<div class="row">
							<div class="col s12">
								<button class="btn btn-lg btn-outline-light btn-block rounded-0" onclick="location.href = '<?= $data['download_json'] ?>';">View as JSON</button>
							</div>
						</div>
					<?php } else { ?>
						<h5><?= $data['error'] ?></h5>
					<?php } ?>
				</div>
			</div>
		</div>
	</div>
</section>
