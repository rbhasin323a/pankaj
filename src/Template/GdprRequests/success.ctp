<?php $this->assign('title', 'Success'); ?>
<?php
    $this->HtmlBetter->addCrumb(__('Home'), '/');
    $this->HtmlBetter->addCrumb(__('GDPR Tools'), '/pages/gdpr-tools');
?>
<section id="GdprRequests">
    <div class="container">
        <div class="row">
            <div class="col s12 l3 sidenav-left">
                <?= $this->element('Sidebar/nav-pages'); ?>
            </div>
            <div class="col s12 l9 text-center">
                <div class="card-panel p30">
                    <h4 class="page-heading"><?= __('Success'); ?></h4>
                    <p class="lead">Your request has been submitted and will be processed in the next 72 hours.</p>
                </div>
            </div>
        </div>
    </div>
</section>