<?php
$this->assign('title', __('Maldopay Transaction Error'));
$this->HtmlBetter->addCrumb(__('Home'), '/');
$this->HtmlBetter->addCrumb(__('Maldopay Transaction Error'), '/');
?>
<style>
.time-left:hover {
	cursor: default;
}
.alert {
  padding: 20px;
  background-color: #f44336; /* Red */
  color: white;
  margin-bottom: 15px;

}
#back-sales-view {
  margin-bottom: 20px;

}
</style>
<section id="MladopayError">
  <div class="container">
    <div class="alert"><?= $codeMessage ?> </div>
    <a href="/sales/book/<?= $booking_id ?>" id="back-sales-view" class="btn waves-effect waves-light btn-large amber darken-3"> Back to booking </a>
  </div>
</section>
