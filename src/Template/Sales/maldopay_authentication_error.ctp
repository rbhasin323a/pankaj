<?php
$this->assign('title', __('Maldopay Error'));
$this->HtmlBetter->addCrumb(__('Home'), '/');
?>
<style>
.time-left:hover {
	cursor: default;
}
.alert {
  padding: 20px;
  background-color: #f44336; /* Red */
  color: white;
  margin-bottom: 15px;

}
#back-sales-view {
  margin-bottom: 20px;

}
</style>
<section id="MladopayError">
		<div class="container">
			<div class="row">
				<div class="authentication-error card-panel p30">
			    <h4 class="alert"><?= $codeMessage ?> </h4>
			    <a href="/sales/book/<?= $booking_id ?>" id="back-sales-view" class="btn waves-effect waves-light btn-large amber darken-3"> Back to booking </a>
		  </div>
		</div>
	</div>
</section>
