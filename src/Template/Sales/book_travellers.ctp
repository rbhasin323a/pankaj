<?php
$this->assign('title', h($sale->title));

$this->HtmlBetter->addCrumb(__('Home'), '/');
$this->HtmlBetter->addCrumb(__('Latest Sales'), '/latest-sales');
$this->HtmlBetter->addCrumb(h($sale->title), '/sale/' . $sale->slug);

?>
<section id="SalesView" class="page">
    <div class="container">
        <div class="row">
            <div class="col s12">
                <h4 class="title"><?= __('Travellers'); ?></h4>
            </div>
        </div>
        <?= $this->Form->create('BookingTravellers', ['url' => ['controller' => 'Sales', 'action' => 'book_travellers', $booking->id]]); ?>
        <div class="card-panel sale-book-travellers">

            <div class="row">
                <div class="col s12">
                    <?php $travellers = 0 ?>
                    <?php if(!empty($booking->adults)) { ?>
                    <?php for($i = 1; $i <= $booking->adults; $i++) { ?>
                    <div class="row">
                        <div class="col s8">
                            <h5><?= __('Adult'); ?> <?= $i; ?></h5>
                            <p><?= $this->Form->input(
                                    'travellers['.$travellers.'].title', [
                                        'type' => 'select',
                                        'options' => ['Mr' => __('Mr'), 'Ms' =>  __('Ms'), 'Mrs' => __('Mrs')],
                                        'default' => !empty($post_data['travellers'][$travellers]['title']) ? $post_data['travellers'][$travellers]['title'] : ''
                                    ]); ?></p>
                            <p><?= $this->Form->input(
                                    'travellers['.$travellers.'].first_name', [
                                        'type'  => 'text',
                                        'value' => !empty($post_data['travellers'][$travellers]['first_name']) ? $post_data['travellers'][$travellers]['first_name'] : ($travellers == 0 ? $booking->first_name : '')
                                    ]); ?></p>
                            <p><?= $this->Form->input(
                                    'travellers['.$travellers.'].last_name', [
                                        'type'  => 'text',
                                        'value' => !empty($post_data['travellers'][$travellers]['last_name']) ? $post_data['travellers'][$travellers]['last_name'] : ($travellers == 0 ? $booking->last_name : '')
                                    ]); ?></p>
                            <p><?= $this->Form->input(
                                    'travellers['.$travellers.'].birthday', [
                                        'type' => 'text',
                                        'class' => 'datepicker',
                                        'value' => !empty($post_data['travellers'][$travellers]['birthday']) ? $post_data['travellers'][$travellers]['birthday'] : ''
                                    ]); ?></p>
                            <p><?= $this->Form->input(
                                    'travellers['.$travellers.'].passport_number', [
                                        'type' => 'text',
                                        'value' => !empty($post_data['travellers'][$travellers]['passport_number']) ? $post_data['travellers'][$travellers]['passport_number'] : ''
                                    ]); ?></p>
                            <?= $this->Form->input(
                                    'travellers['.$travellers.'].type', [
                                        'type' => 'hidden',
                                        'value' => !empty($post_data['travellers'][$travellers]['type']) ? $post_data['travellers'][$travellers]['type'] : 'adult'
                                    ]); ?>
                            <?= $this->Form->input(
                                    'travellers['.$travellers.'].booking_id', [
                                        'type' => 'hidden',
                                        'value' => $booking->id
                                    ]); ?>
                        </div>
                    </div>
                    <?php $travellers++ ?>
                    <?php } ?>
                    <?php } ?>

                    <?php if(!empty($booking->children)) { ?>
                    <?php for($i = 1; $i <= $booking->children; $i++) { ?>
                    <div class="row">
                        <div class="col s8">
                            <h5><?= __('Child'); ?> <?= $i; ?></h5>
                            <p><?= $this->Form->input(
                                    'travellers['.$travellers.'].title', [
                                        'type' => 'select',
                                        'options' => ['Mr' => __('Mr'), 'Ms' =>  __('Ms'), 'Mrs' => __('Mrs')],
                                        'default' => !empty($post_data['travellers'][$travellers]['title']) ? $post_data['travellers'][$travellers]['title'] : ''
                                    ]); ?></p>
                            <p><?= $this->Form->input(
                                    'travellers['.$travellers.'].first_name', [
                                        'type'  => 'text',
                                        'value' => !empty($post_data['travellers'][$travellers]['first_name']) ? $post_data['travellers'][$travellers]['first_name'] : ($travellers == 0 ? $booking->first_name : '')
                                    ]); ?></p>
                            <p><?= $this->Form->input(
                                    'travellers['.$travellers.'].last_name', [
                                        'type'  => 'text',
                                        'value' => !empty($post_data['travellers'][$travellers]['last_name']) ? $post_data['travellers'][$travellers]['last_name'] : ($travellers == 0 ? $booking->last_name : '')
                                    ]); ?></p>
                            <p><?= $this->Form->input(
                                    'travellers['.$travellers.'].birthday', [
                                        'type' => 'text',
                                        'class' => 'datepicker',
                                        'value' => !empty($post_data['travellers'][$travellers]['birthday']) ? $post_data['travellers'][$travellers]['birthday'] : ''
                                    ]); ?></p>
                            <p><?= $this->Form->input(
                                    'travellers['.$travellers.'].passport_number', [
                                        'type' => 'text',
                                        'value' => !empty($post_data['travellers'][$travellers]['passport_number']) ? $post_data['travellers'][$travellers]['passport_number'] : ''
                                    ]); ?></p>
                            <?= $this->Form->input(
                                    'travellers['.$travellers.'].type', [
                                        'type' => 'hidden',
                                        'value' => !empty($post_data['travellers'][$travellers]['type']) ? $post_data['travellers'][$travellers]['type'] : 'child'
                                    ]); ?>
                            <?= $this->Form->input(
                                    'travellers['.$travellers.'].booking_id', [
                                        'type' => 'hidden',
                                        'value' => $booking->id
                                    ]); ?>
                        </div>
                    </div>
                    <?php $travellers++ ?>
                    <?php } ?>
                    <?php } ?>

                    <?php if(!empty($booking->infant)) { ?>
                    <?php for($i = 1; $i <= $booking->infant; $i++) { ?>
                    <div class="row">
                        <div class="col s8">
                            <h5><?= __('Infant'); ?> <?= $i; ?></h5>
                            <p><?= $this->Form->input(
                                    'travellers['.$travellers.'].title', [
                                        'type' => 'select',
                                        'options' => ['Mr' => __('Mr'), 'Ms' =>  __('Ms'), 'Mrs' => __('Mrs')],
                                        'default' => !empty($post_data['travellers'][$travellers]['title']) ? $post_data['travellers'][$travellers]['title'] : ''
                                    ]); ?></p>
                            <p><?= $this->Form->input(
                                    'travellers['.$travellers.'].first_name', [
                                        'type'  => 'text',
                                        'value' => !empty($post_data['travellers'][$travellers]['first_name']) ? $post_data['travellers'][$travellers]['first_name'] : ($travellers == 0 ? $booking->first_name : '')
                                    ]); ?></p>
                            <p><?= $this->Form->input(
                                    'travellers['.$travellers.'].last_name', [
                                        'type'  => 'text',
                                        'value' => !empty($post_data['travellers'][$travellers]['last_name']) ? $post_data['travellers'][$travellers]['last_name'] : ($travellers == 0 ? $booking->last_name : '')
                                    ]); ?></p>
                            <p><?= $this->Form->input(
                                    'travellers['.$travellers.'].birthday', [
                                        'type' => 'text',
                                        'class' => 'datepicker',
                                        'value' => !empty($post_data['travellers'][$travellers]['birthday']) ? $post_data['travellers'][$travellers]['birthday'] : ''
                                    ]); ?></p>
                            <p><?= $this->Form->input(
                                    'travellers['.$travellers.'].passport_number', [
                                        'type' => 'text',
                                        'value' => !empty($post_data['travellers'][$travellers]['passport_number']) ? $post_data['travellers'][$travellers]['passport_number'] : ''
                                    ]); ?></p>
                            <?= $this->Form->input(
                                    'travellers['.$travellers.'].type', [
                                        'type' => 'hidden',
                                        'value' => !empty($post_data['travellers'][$travellers]['type']) ? $post_data['travellers'][$travellers]['type'] : 'infant'
                                    ]); ?>
                            <?= $this->Form->input(
                                    'travellers['.$travellers.'].booking_id', [
                                        'type' => 'hidden',
                                        'value' => $booking->id
                                    ]); ?>
                        </div>
                    </div>
                    <?php $travellers++ ?>
                    <?php } ?>
                    <?php } ?>
                </div>
            </div>
            <button id="c" type="submit" class="btn waves-effect waves-light btn-large amber darken-3"><?= __('Confirm'); ?> <i class="material-icons right">send</i></button>
        </div>

        <?= $this->Form->end(); ?>
    </div>

</section>

<script>
    $(document).ready(function() {
        $('.datepicker').pickadate({
            selectMonths: true,
            selectYears: 200,
            format: 'yyyy-mm-dd'
        });
    });

</script>
