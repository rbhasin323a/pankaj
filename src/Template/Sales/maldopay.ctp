<?php
$this->assign('title', __('MaldoPay'));
$this->HtmlBetter->addCrumb(__('Home'), '/');
$this->HtmlBetter->addCrumb(__('Maldopay payment'), '/sale/success');
?>
<script>
	$(document).ready(function() {
		var MaldoPayScript = document.createElement("script");
		MaldoPayScript.type = 'text/javascript';
		MaldoPayScript.src = 'https://maldopay.com/api/maldopay_api.js';
		window.$ = window.jQuery;
		document.getElementsByTagName('head')[0].appendChild(MaldoPayScript);

	});
</script>
<div id="maldopay-frame-payment" user-firstName="<?= $form_data['post_data']['first_name'] ?>" user-lastName="<?= $form_data['post_data']['last_name'] ?>" user-address="<?= $form_data['user']['address_1'] ?>" user-birthDate_year="<?= $form_data['birth_date'][0] ?>" user-birthDate_month="<?= $form_data['birth_date'][1] ?>" user-birthDate_day="<?= $form_data['birth_date'][2] ?>" user-countryCode="<?= $form_data['country_id'] ?>" user-city="<?= $form_data['user']['city'] ?>" user-postCode="<?= $form_data['user']['postcode'] ?>" user-currencyCode="<?= $form_data['currency'] ?>" user-languageCode="<?= $form_data['language'] ?>" user-emailAddress="<?= $form_data['user']['email'] ?>" user-phone="<?= $form_data['post_data']['phone'] ?>" payment-referenceOrderId="<?= rand() /*$form_data['booking_id']*/ ?>" payment-type="DEPOSIT" user-playerId="123" payment-serviceId="2031" payment-amount="<?= $form_data['amount'] ?>" payment-currency="EUR">13189</div>
