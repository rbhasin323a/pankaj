<?php
$this->assign('title', h($sale->title));

$this->HtmlBetter->addCrumb(__('Home'), '/');
$this->HtmlBetter->addCrumb(__('Latest Sales'), '/latest-sales');
$this->HtmlBetter->addCrumb(h($sale->title), '/sale/' . $sale->slug);

function generateArray($min, $max)
{
	$array = [];

	for ($i = $min; $i <= $max; $i++) {
		$array[$i] = $i;
	}

	return $array;
}

?>
<section id="SalesView" class="page">
	<div class="container">
		<div class="row">
			<div class="col s12">
				<h4 class="title"><?= __('Book Now'); ?></h4>
			</div>
		</div>
		<?= $this->Form->create('Booking', ['url' => ['controller' => 'Sales', 'action' => 'book', $sale->id], 'id' => 'book']); ?>
		<div class="card-panel sale-book">
			<div class="row">
				<div class="col s12">
					<h5 class="mv20"><span class="step-number">1</span><?= __('Review and Book your trip'); ?></h5>
					<h4><?= !empty($sale->title) ? $sale->title : ''; ?></h4>
					<hr />
					<div class="row">
						<div class="col s12 m6 right">
							<img class="img-responsive sale-image" src="/files/images/sales/<?= $sale->main_image; ?>" />
						</div>
						<div class="col s12 m6">
							<div id="summary-description">
								<p><?= $sale_offer->summary_description; ?></p>
							</div>
							<p>
								<b><?= __('Check-in'); ?></b>: <?= (!empty($date_start) ? $date_start : 'Empty'); ?>
								<?= $this->Form->input('check_in', [
									'type' 	=> 'hidden',
									'value' => !empty($date_start) ? date('Y-m-d', strtotime($date_start)) : 'Empty'
								]); ?>
							</p>
							<p>
								<b><?= __('Check-out'); ?></b>: <?= (!empty($date_end) ? $date_end : 'Empty'); ?>
								<?= $this->Form->input('check_out', [
									'type' 	=> 'hidden',
									'value' => !empty($date_end) ? date('Y-m-d', strtotime($date_end)) : 'Empty'
								]); ?>
							</p>
							<p>
								<b><?= __('Rooms'); ?></b>: <?= $book['rooms'] ?>
								<?= $this->Form->input('rooms', [
									'type' 	=> 'hidden',
									'value' => !empty($book['rooms']) ? $book['rooms'] : '1'
								]); ?>
							</p>
						</div>
					</div>
					<div class="row">
						<div class="col s12 m6">
							<h6><?= __('Number of Travellers'); ?></h6>
							<p>
								<?= $this->Form->input('adults', [
									'type' 	=> 'select',
									'label' => __('Adults'),
									'class' => 'form-control',
									'default' => $sale_offer->max_adults,
									'options' => generateArray(1, $sale_offer->max_adults * $book['rooms'])
								]); ?>
							</p>
							<p>
								<?= $this->Form->input('children', [
									'type' 	=> 'select',
									'label' => __('Children') . ' (' . $sale_offer->child_age_from . '-' . $sale_offer->child_age_to . ')',
									'class' => 'form-control',
									'options' => generateArray(0, $sale_offer->max_children * $book['rooms'])
								]); ?>
							</p>
							<p>
								<?= $this->Form->input('infants', [
									'type' 	=> 'select',
									'label' => __('Infants') . ' (' . $sale_offer->infant_age_from . '-' . $sale_offer->infant_age_to . ')',
									'class' => 'form-control',
									'options' => generateArray(0, $sale_offer->max_children * $book['rooms'])
								]); ?>
							</p>
							<?php if (!empty($sale_offer)) { ?>
								<p><button class="waves-effect waves-light btn modal-trigger" data-target="child_policy_<?= !empty($sale_offer->id) ? $sale_offer->id : ''; ?>"><?= __('Child policy'); ?></button></p>
								<p><button class="waves-effect waves-light btn modal-trigger" data-target="single_travellers_<?= !empty($sale_offer->id) ? $sale_offer->id : ''; ?>"><?= __('Single travellers'); ?></button></p>
								<div id="child_policy_<?= !empty($sale_offer->id) ? $sale_offer->id : ''; ?>" class="modal">
									<div class="modal-content">
										<h4><?= __('Child Policy'); ?> (<?= $sale_offer->name ?>)</h4>
										<p><?= $sale_offer->child_policy ?></p>
									</div>
									<div class="modal-footer">
										<a href="#!" class=" modal-action modal-close waves-effect waves-green btn-flat">OK</a>
									</div>
								</div>
								<div id="single_travellers_<?= !empty($sale_offer->id) ? $sale_offer->id : ''; ?>" class="modal">
									<div class="modal-content">
										<h4><?= __('Single Policy'); ?> (<?= $sale_offer->name ?>)</h4>
										<p><?= $sale_offer->single_policy ?></p>
									</div>
									<div class="modal-footer">
										<a href="#!" class=" modal-action modal-close waves-effect waves-green btn-flat">OK</a>
									</div>
								</div>
							<?php } ?>
						</div>
					</div>


				</div>
			</div>
			<div class="row">
				<div class="col s12 m6">
					<h5 class="mv20"><span class="step-number">2</span><?= __('Enter Your Details'); ?></h5>
					<p>
						<?= $this->Form->input('first_name', [
							'type' 	=> 'text',
							'label' => __('First name'),
							'class' => 'form-control',
						]); ?>
					</p>
					<p>
						<?= $this->Form->input('last_name', [
							'type' 	=> 'text',
							'label' => __('Last name'),
							'class' => 'form-control',
						]); ?>
					</p>
					<p>
						<?= $this->Form->input('phone', [
							'type' 	=> 'text',
							'label' => __('Phone'),
							'class' => 'form-control',
							'required'
						]); ?>
					</p>
				</div>
			</div>
			<div class="row payment">
				<div class="col s12">
					<h5><?= __('Payment Details'); ?>:</h5>

					<?php
					$book_total = 0;
					if ($sale_offer->deposit) {
						foreach ($sale_allocations as $sale_allocation) {
							$book_total += $sale_allocation->total_deposit * $book['rooms'];
						}
					} else {
						foreach ($sale_allocations as $sale_allocation) {
							$book_total += $sale_allocation->rate * $book['rooms'];
						}
					}
					?>

					<div class="row pricing">
						<div class="col s12">
							<?= __('Price Total'); ?>: <span class="total"><?= $currency->convert($sale->currency_id, null, $book_total); ?></span>
							<?= $this->Form->input('sale_total', [
								'type' 	=> 'hidden',
								'value' => $book_total
							]); ?>
						</div>
					</div>
				</div>
			</div>
			<?php foreach ($sale_allocations as $sale_allocation) { ?>

				<?= $this->Form->input('booking_allocations[].allocation_id', [
					'type' 	=> 'hidden',
					'value' => $sale_allocation->id
				]); ?>
			<?php } ?>
			<?= $this->Form->input('transaction.title', [
				'type' 	=> 'hidden',
				'value' => 'Book - ' . $sale->title
			]); ?>
			<?= $this->Form->input('sale_id', [
				'type' 	=> 'hidden',
				'value' => $book['sale_id']
			]); ?>
			<?= $this->Form->input('offer_id', [
				'type' 	=> 'hidden',
				'value' => $book['offer_id']
			]); ?>
			<?= $this->Form->input('sale_allocation_id', [
				'type' 	=> 'hidden',
				'value' => $sale_allocations->first()->id
			]); ?>
			<?= $this->Form->input('type', [
				'type' => 'hidden',
				'value' => $book['type']
			]); ?>
			<button id="c" type="submit" class="btn waves-effect waves-light btn-large amber darken-3"><?= __('Next'); ?> <i class="material-icons right">send</i></button>
		</div>

		<?= $this->Form->end(); ?>
	</div>

</section>

<script>
	var sale_allocations = <?= json_encode($sale_allocations); ?>;
	var sale_offer = <?= json_encode($sale_offer); ?>;
	var booked_rooms = <?= $book['rooms'] ?>;

	var max_adults = <?= $sale_offer->max_adults ?>;
	var max_children = <?= $sale_offer->max_children ?>;
	max_children *= booked_rooms;

	function calculateBookTotal() {
		var adults_count = $('#adults').val();
		var children_count = $('#children').val();
		var infants_count = $('#infants').val();

		var deposit = 0;
		var adults_price = 0;
		var children_price = 0;
		var infants_price = 0;
		var book_total = 0;

		if (sale_offer.deposit) {
			for (var i = 0; i < sale_allocations.length; i++) {
				if (sale_allocations[i].total_deposit) {
					deposit += sale_allocations[i].total_deposit;
				}

				if (sale_allocations[i].child_deposit) {
					deposit += children_count * sale_allocations[i].child_deposit;
				}

				if (sale_allocations[i].infant_deposit) {
					deposit += infants_count * sale_allocations[i].infant_deposit;
				}
			}

			book_total = deposit;
		} else {
			for (var i = 0; i < sale_allocations.length; i++) {

				if (adults_count == 1) {
					adults_price += sale_allocations[i].single_rate * booked_rooms;
				} else {
					adults_price += sale_allocations[i].rate * booked_rooms;
				}

				children_price += (max_children > 0 ? Math.ceil(children_count / max_children) : 0) * sale_allocations[i].child_rate;
				infants_price += (max_children > 0 ? Math.ceil(infants_count / max_children) : 0) * sale_allocations[i].infant_rate;
			}

			book_total = adults_price + children_price + infants_price;
		}


		formatPrice('<?= $sale->currency_id ?>', book_total, true, function(result) {
			$('.payment .pricing span.total').html(result);
		});

		formatPrice('<?= $sale->currency_id ?>', book_total, false, function(result) {
			$('.payment .pricing input[name="sale_total"]').val(result);
		});
	}

	$(document).ready(function() {
		calculateBookTotal();
		$('.modal').modal();

		$('#adults, #children, #infants').on('change', function() {
			calculateBookTotal();
		});

		$('#children, #infants').on('change', function(event) {
			var choice = parseInt($(this).val());
			var newMax = max_children - choice; //minimum of this is zero
			var modifySelect;
			var modifyValue;
			var optionsHtml = '';
			var lis;

			if ($(this).is('#children')) {
				modifySelect = $('#infants');
			} else {
				modifySelect = $('#children');
			}

			for (var i = 0; i <= newMax; i++) {
				optionsHtml += '<option value="' + i + '">';
				optionsHtml += i;
				optionsHtml += '</option>';
			}

			modifyValue = modifySelect.val();
			modifySelect.html(optionsHtml);
			modifySelect.material_select();
			lis = modifySelect.siblings('ul[id^=\'select\']').find('li');

			lis.each(function(index, element) {
				if ($('span', element).html() == modifyValue) {
					$(element).addClass('active');
					$(element).parent().siblings('input[type=text]').prop('value', modifyValue);
					modifySelect.val(modifyValue);
				}
			});
		});
	});
</script>
