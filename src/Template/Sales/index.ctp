<?php
$this->assign('title', __('Luxury Discounts'));
?>
<section id="SalesIndex">
	<div class="container">
		<div class="row">
			<div class="col s12">
				<div class="sales-list">
					<h4 class="page-heading"><?= __('Latest Sales'); ?></h4>

					<div class="row">
						<?php foreach ($sales as $sale) : ?>
							<?php
								if (empty($sale->title)) {
									continue;
								}
							?>
							<div class="col s12 m10 offset-m1">
								<div class="card-wrap">
									<div class="card horizontal" data-sale-id="<?= $sale->id; ?>" data-created="<?= $sale->created; ?>">
										<?php if (!empty($sale->main_image)) { ?>
											<div data-sale-url="<?= $this->Url->build(['controller' => 'Sales', 'action' => 'view', !empty($sale->slug) ? $sale->slug : $sale->id]); ?>" class="l-l activator card-image waves-effect waves-block waves-light" data-bg="url(/files/images/sales/<?= $sale->main_image ?>), linear-gradient(#000, #fff)">
												<div class="card-label-container">
													<?php if ($deposits[$sale->id]) { ?>
														<div class="card-label"><?= __('Deposit Now'); ?></div>
													<?php
												} ?>
													<?php if ($sale->active == 2) { ?>
														<div class="card-label"><?= __('Expired'); ?></div>
													<?php
												} ?>
												</div>
												<span class="activator card-title"><?= (!empty($discounts[$sale->id]) ? __('Up to {0}% Discount', $discounts[$sale->id]) : ''); ?></span>
											</div>
										<?php
									} ?>
										<div class="card-stacked">
											<div class="card-content">
												<a href="<?= $this->Url->build(['controller' => 'Sales', 'action' => 'view', !empty($sale->slug) ? $sale->slug : $sale->id]); ?>" class="card-title black-text inline-full"><?= $sale->title; ?></a>
												<p class="blue-grey-text text-darken-3"><i class="material-icons va-t fs20">location_on</i> <?= $sale->destination; ?></p>
												<div class="divider mv10"></div>
												<p><?= $sale->sub_title; ?></p>
											</div>
											<div class="card-action">
												<div class="left">
													<?php if (!empty($prices[$sale->id])) { ?>
														<?= __('From'); ?> <a class="text-darken-4 from-price"><?= $prices[$sale->id]['min_rate'] ?></a><span style="text-decoration: line-through;"><?= $prices[$sale->id]['respective_rack_rate'] ?></span>

														<?php if ($prices[$sale->id]['fixed'] && $prices[$sale->id]['flexible']) { ?>
															<small>/ <?= __('per night'); ?></small>
														<?php
													} else if ($prices[$sale->id]['fixed'] && !$prices[$sale->id]['flexible']) { ?>
															<small>/ <?= __('per room per stay'); ?></small>
														<?php
													} else { ?>
															<small>/ <?= $prices[$sale->id]['text'] ?></small>
														<?php
													} ?>
														<br />
													<?php
												} ?>
													<?php if (!empty($time_lefts[$sale->id])) { ?>
														<span class="text-darken-4 time-left"><?= __('{0,plural,=0{0 days}=1{1 day} other{# days}}', $time_lefts[$sale->id]['days']) . " " . __('{0,plural,=0{0 hours}=1{1 hour} other{# hours}}', $time_lefts[$sale->id]['hours']) . " " . __('{0,plural,=0{0 minutes}=1{1 minute} other{# minutes}}', $time_lefts[$sale->id]['mins']); ?></span><br />
													<?php
												} elseif (!empty($time_lefts[$sale->id]) && is_string($time_lefts[$sale->id])) { ?>
														<span class="text-darken-4 time-left"><?= $time_lefts[$sale->id] ?></span><br />
													<?php
												} ?>
												</div>

												<div class="right mv10">
													<a class="waves-effect btn white blue-text text-darken-4 fw500" href="<?= $this->Url->build(['controller' => 'Sales', 'action' => 'view', !empty($sale->slug) ? $sale->slug : $sale->id]); ?>"><?= __('View Sale'); ?></a>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						<?php endforeach; ?>
					</div>

				</div>

				<script type="text/javascript">
					(function($) {
						function resizeSales() {
							var viewport_width = Math.max(document.documentElement.clientWidth, window.innerWidth || 0);
							var viewport_height = Math.max(document.documentElement.clientHeight, window.innerHeight || 0);

							if (viewport_width < 1200) {
								$('#SalesIndex .sales-list .card').removeClass('horizontal');
							} else {
								$('#SalesIndex .sales-list .card').addClass('horizontal');
							}
						}

						$(window).on('load', function(event) {
							resizeSales();
						});

						var resizeTimer;

						$(window).on('resize', function(event) {
							clearTimeout(resizeTimer);
							resizeTimer = setTimeout(resizeSales, 250);
						});

						$(document).on('click', '#SalesIndex .sales-list .card-image', function(event) {
							event.preventDefault();

							var sale_url = $(this).attr('data-sale-url');

							if (sale_url) {
								location.href = sale_url;
							}
						});
					})(jQuery);
				</script>
			</div>
		</div>
	</div>
</section>

<section id="PageHome">
	<?= $this->cell('HomeFeatured', [], ['categories_limit' => 4]); ?>

	<div class="inline-full va-t blue-grey lighten-5">
		<div class="container pv50 mv50">
			<div class="row mbn">
				<div class="col s12 m8">
					<h2 class="fs35 fw300 m-n mv5 purple-text text-darken-4"><?= __d('PageHome', 'OUR LUXURY NETWORK'); ?></h2>
					<h3 class="fs24 fw300 m-n"><?= __d('PageHome', 'JOIN OUR FAMILY OF EXCLUSIVE PARTNERS'); ?></h3>
				</div>
				<div class="col s12 m4">
					<a href="/contact" class="waves-effect btn-large btn-flat right mv15 white purple-text text-darken-4 fs20 fw500"><?= __d('PageHome', 'Get In Touch'); ?></a>
				</div>
			</div>
		</div>
	</div>
</section>
