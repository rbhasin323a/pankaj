<?php
$this->assign('title', h($sale->title));

$this->HtmlBetter->addCrumb(__('Home'), '/');
$this->HtmlBetter->addCrumb(__('Latest Sales'), '/latest-sales');
$this->HtmlBetter->addCrumb(h($sale->title), '/sale/' . $sale->slug);

$location_map = json_decode($sale->location_map, true);
echo $this->Html->css('/ui/css/isl-calendar.css');
echo $this->Html->css('/ui/css/flexslider.css');
echo $this->Html->script('/ui/js/jquery.flexslider-min.js');
?>
<script src="/assets/rev/js/<?= $this->Asset->getFileRev('website-components/sales-view/calendar.js', ROOT . '/webroot/assets/rev/js/rev-manifest.json'); ?>"></script>
<style>
	#SalesView .sale-info #offers .offer ul.actions li button {
		width: 100%;
	}

	#SalesView h4,
	#SalesView h5,
	#SalesView h6 {
		word-break: break-word;
	}

	@media only screen and (max-width: 640px) {
		#SalesView .sale-info #offers .offer ul.actions li button.modal-trigger {
			word-break: break-word;
			text-overflow: ellipsis;
			white-space: nowrap;
			overflow: hidden;
		}

		#SalesView .sale-info #offers .offer ul.actions li button {
			width: 100%;
		}

		#SalesView .sale-info .title {
			margin-left: 1rem;
		}

		#SalesView .sale-info .location {
			margin-left: 1rem;
		}

		#SalesView .sale-info {
			width: 97%;
		}

		#SalesView .sale-info #offers .offer ul.actions li button.modal-trigger img {
			display: none;
		}

	}

	.collapsible {
		border-color: #424242;
	}

	.collapsible-header {
		border-color: #424242;
	}

	.collapsible-body {
		border: none;
		padding: 1rem;
	}

	/* Popup container */
	.popup {
		position: relative;
		display: inline-block;
		cursor: pointer;
	}

	/* The actual popup (appears on top) */
	.popup .popuptext {
		visibility: hidden;
		width: 160px;
		background-color: #555;
		color: #fff;
		text-align: center;
		border-radius: 6px;
		padding: 8px 0;
		position: absolute;
		z-index: 1;
		bottom: 125%;
		left: 50%;
		margin-left: -80px;
	}

	/* Popup arrow */
	.popup .popuptext::after {
		content: "";
		position: absolute;
		top: 100%;
		left: 50%;
		margin-left: -5px;
		border-width: 5px;
		border-style: solid;
		border-color: #555 transparent transparent transparent;
	}

	/* Toggle this class when clicking on the popup container (hide and show the popup) */
	.popup .show {
		visibility: visible;
		-webkit-animation: fadeIn 1s;
		animation: fadeIn 1s
	}

	.circle {
		width: 61px;
		height: 61px;
		border-radius: 50%;
		font-size: 22px;
		color: black;
		text-align: center;
		border: solid #ff8f00 1px;
		padding: 12px;
		float: left;
		margin: 10px 20px 0 0;
		background: #ff8f00;
		font-weight: 500;
		letter-spacing: -1px;
	}

	.discount-text {
		margin: 20px;
		line-height: 20px;
	}

	.discount-btn {
		height: auto;
		border: 1px solid #424242;
	}

	.wrapper-dis {
		text-align: center;
		display: inline-block;
		margin: 0 auto 10px;
	}

	.clear:after {
		clear: both;
	}

	/* Add animation (fade in the popup) */
	@-webkit-keyframes fadeIn {
		from {
			opacity: 0;
		}

		to {
			opacity: 1;
		}
	}

	@keyframes fadeIn {
		from {
			opacity: 0;
		}

		to {
			opacity: 1;
		}
	}
</style>
<section id="SalesView">
	<div class="container sale-info">
		<div class="col s12 l11">
			<h1 class="title"><?= h($sale->title); ?></h1>
			<h2 class="location"><?= h($sale->sub_title); ?></h2>
			<div class="card-panel sales-view-page">
				<div class="custom-columns-row">
					<div class="custom-columns-row__custom-column width-60">
						<?php if (!empty($images)) { ?>
							<div class="flexslider">
								<ul class="slides">
									<?php if (!empty($sale->video_url)) { ?>
										<li>
											<div class="video-container">
												<div id="sale-video"></div>
											</div>
										</li>
									<?php } ?>
									<?php foreach ($images_with_translations as $image) { ?>
										<li>
											<div class="hotel-image" title="<?= $image['image_alt']; ?>" style="background-image: url('/files/images/sales/<?= rawurlencode($image['image_path']); ?>')"></div>
										</li>
									<?php } ?>
								</ul>
							</div>
						<?php } ?>
						<ul class="tabs tabs-fixed-width">
							<li class="tab"><a class="active" href="#main_details"><?= __('Offers'); ?></a></li>
							<li class="tab"><a href="#hotel_details"><?= __('About Hotel'); ?></a></li>
							<li class="tab"><a href="#travel_details"><?= __('Travel Details'); ?></a></li>
						</ul>
						<?php if (!empty($sale->sale_offers)) { ?>
							<div class="row">
								<div class="col s12 m12">
									<h4><?= __('Book now'); ?></h4>
								</div>
							</div>
						<?php } ?>
						<div class="row offer">
							<div class="col s12 <?= (!empty($sale->tripadvisor_widget)) ? 'm7 l8' : 'm12' ?>">
								<div id="main_details">
									<?php if (!empty($sale->sale_offers)) { ?>
										<h4 class="step-one">1. <?= __('Select offer'); ?></h4>
										<div id="offers">
											<?php foreach ($sale->sale_offers as $sale_offer) { ?>

												<?php if ($sale_offer->min_rate < PHP_INT_MAX) { ?>
													<div class="offer" data-offer-id="<?= $sale_offer->id ?>">
														<div class="card-panel" data-min-stay="<?= isset($sale_offer->minimum_stay) ? $sale_offer->minimum_stay : 0 ?>">
															<div class="row">
																<div class="col s12">
																	<div class="col s12 l6">
																		<h6><strong><?= $sale_offer->name; ?></strong></h6>
																	</div>
																	<div class="col s12 l6">
																		<?php if ($sale_offer->deposit) { ?>
																			<div class="price offer-deposit">
																				<span class="small"><?= __('Deposit from'); ?></span> <?= $currency->convert($sale->currency_id, null, $deposits[$sale_offer->id]); ?><br />
																				<span class="small">/ <?= __('per night'); ?></span>
																			</div>
																			<hr />
																		<?php } ?>
																		<div class="price">
																			<span class="small"><?= __('Total from'); ?></span>
																			<?= $currency->convert($sale->currency_id, null, $sale_offer->min_rate); ?>
																			<br />
																			<span class="small">/ <?= __('per night'); ?></span>
																		</div>
																	</div>
																</div>
															</div>
															<div class="row expanded-content" style="display:none;">
																<div class="col s12">
																	<div class="row">
																		<div class="col s12">
																			<span class="selected-offer"><i class="material-icons tiny va-t">done</i>&nbsp;<?= __('Selected offer'); ?></span>
																			<div class="description"><?= $sale_offer->summary_description; ?></div>
																		</div>
																	</div>
																	<div class="divider mv20"></div>
																	<div class="row">
																		<div class="col s12 l4">
																			<div class="input-field col s12">
																				<select name="rooms">
																					<option value="1">1</option>
																					<option value="2">2</option>
																					<option value="3">3</option>
																					<option value="4">4</option>
																					<option value="5">5</option>
																				</select>
																				<label><?= __('Rooms'); ?>:</label>
																			</div>
																		</div>
																		<?php if ($sale->sale_type == 1) { ?>
																			<div class="col s8">
																				<div class="input-field col s12">
																					<select name="departure_airport_code">
																						<?php foreach ($airports[$sale_offer->id] as $airport_code) { ?>
																							<option value="<?= $airport_code; ?>"><?= $airport_code; ?></option>
																						<?php } ?>
																					</select>
																					<label><?= __('Departure Airport'); ?>:</label>
																				</div>
																			</div>
																		<?php } ?>
																	</div>
																	<div class="divider mv20"></div>
																	<ul class="actions">
																		<li>
																			<p>
																				<?= __('You save the most on this date:'); ?>
																				<b><?= date('d.m.Y', strtotime($sale_offer->best_sale_offer_allocation->date_start)); ?></b><br />
																				<i><?= __("All other dates offer at least 20% off the hotel's rate"); ?></i>
																			</p>
																		</li>
																		<li>
																			<button class="darken-3 wrapper-dis discount-btn waves-effect waves-light btn modal-trigger btn-flat black">
																				<div class="circle"> <?= $sale_offer->best_sale_offer_allocation->discount; ?>% </div>
																				<div class="discount-text white-text">
																					<b><?= __('Discount'); ?>

																						<span class="amber-text text-darken-3"><?= $currency->convert($sale->currency_id, null, $sale_offer->best_sale_offer_allocation->rate); ?></span>
																					</b>
																					</br>
																					<span class="grey-text text-lighten-2">
																						<?= __('Hotel price'); ?> <strike><?= $currency->convert($sale->currency_id, null, $sale_offer->best_sale_offer_allocation->rack_rate); ?></strike>
																					</span>
																				</div>
																			</button>
																		</li>
																	</ul>
																	<ul class="collapsible expandable">
																		<li id="more_details_<?= $sale_offer->id ?>">
																			<div class="collapsible-body">
																				<?= $sale_offer->main_description; ?>
																			</div>
																		</li>
																		<li id="child_policy_<?= $sale_offer->id ?>">
																			<div class="collapsible-header grey darken-4">
																				<img src="/files/images/smiling-baby.svg" width="22" height="22" />
																				&nbsp;&nbsp;<?= __('Child Policy'); ?>
																			</div>
																			<div class="collapsible-body black white-text">
																				<p><?= $sale_offer->child_policy ?></p>
																			</div>
																		</li>
																		<li id="single_travellers_<?= $sale_offer->id ?>">
																			<div class="collapsible-header grey darken-4">
																				<img src="/files/images/boy-smiling.svg" width="22" height="22" />
																				&nbsp;&nbsp;<?= __('Single Travellers'); ?>
																			</div>
																			<div class="collapsible-body black white-text">
																				<p><?= $sale_offer->single_policy ?></p>
																			</div>
																		</li>
																	</ul>
																</div>
															</div>
														</div>
													</div>
												<?php } ?>
											<?php } ?>
										</div>
									<?php } ?>
								</div>
								<div id="hotel_details" class="col s12"><?= $sale->hotel_description; ?></div>
								<div id="travel_details" class="col s12">
									<?= $sale->travel_description; ?>
									<div class="divider mv20"></div>
									<h5 class="location"><i class="material-icons va-t">location_on</i><?= h($sale->destination); ?></h5>
									<div id="location_map" style="width: 100%; height: 420px;"></div>
									<div class="divider mv20"></div>
								</div>
							</div>

							<?php if (!empty($sale->tripadvisor_widget)) { ?>
								<div class="col s12 m5 l4">
									<?= $sale->tripadvisor_widget; ?>
									<style type="text/css">
										#CDSWIDSSP {
											width: 100% !important;
										}
									</style>
								</div>
							<?php } ?>
						</div>
					</div>
					<div class="custom-columns-row__custom-column width-40 <?= ($sale->active == 2 || $sale->active == 0) ? 'hidden' : '' ?>">
						<div class="fixed-aside">
							<h5>2. <?= __('Select travel dates'); ?></h5>
							<div class="card-panel booking-calendar">
								<div class="no-allocations" style="display:none;">Unfortunately, there are no allocations available matching your criteria!</div>
								<div class="isl-calendar-picker">
									<div class="calendar-header">
										<div class="col s7 calendar-switcher">
											<i id="isl-calendar-previous"></i>
											<span class="header-title"></span>
											<i id="isl-calendar-next"></i>
										</div>
										<button data-target="full-availability-popup" class="modal-trigger full-availability"><?= __('See full availability'); ?></button>
									</div>
									<div style="clear:both;"></div>
									<div class="calendar-body">
										<div class="calendar-months"></div>
										<div class="date-range"></div>
										<div class="summary">
											<div class="row" style="margin: 0;">
												<div class="summary-nights col s6">
													<div class="label"><?= __('Nights'); ?></div>
													<p class="nights"></p>
												</div>
												<div class="summary-price col s6">
													<div class="label"><?= __('Total Price:'); ?></div>
													<p class="total-price"></p>
												</div>
												<div class="summary-deposit col s4 hide">
													<div class="label"><?= __('Deposit:'); ?></div>
													<p class="total-deposit"></p>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div id="desktop-button-book">
								<h5>3. <?= __('Book now'); ?></h5>
								<?= $this->Form->create('BookSale', ['url' => ['controller' => 'Sales', 'action' => 'book', $sale->id], 'id' => 'book']); ?>
								<input type="hidden" name="book[sale_id]" value="<?= $sale->id ?>" />
								<input type="hidden" name="book[offer_id]" value="" />
								<input type="hidden" name="book[sale_allocation_id]" value="" />
								<input type="hidden" name="book[start_flexible_date]" value="" />
								<input type="hidden" name="book[end_flexible_date]" value="" />
								<input type="hidden" name="book[min_nights_validate]" value="1" />
								<input type="hidden" name="book[rooms]" value="1" />
								<input type="hidden" name="book[type]" value="<?= $sale->sale_type; ?>" />
								<button id="book-now" type="submit" class="btn waves-effect waves-light btn-large amber darken-3"><?= __('Book Now'); ?> <i class="material-icons right">send</i></button>
								<?= $this->Form->end(); ?>
							</div>
						</div>
					</div>
				</div>

				<script type="text/javascript">
					var old_calendar_height;
					var fixedAside = function() {
						if ($(window).width() >= 1200) {
							$(window).scroll(function(event) {
								var scrollLimit = $('.card-panel').outerHeight() - 35;
								if ($(window).scrollTop() >= $('.card-panel').offset().top) {
									if (($('.fixed-aside').height() + $(window).scrollTop() - $('.card-panel').offset().top) < scrollLimit) {
										$('.fixed-aside').css('bottom', 'inherit');
										$('.fixed-aside').css('top', $(window).scrollTop() - $('.card-panel').offset().top);
										old_calendar_height = $('.fixed-aside .booking-calendar').outerHeight();
									}
								} else {
									$('.fixed-aside').css('bottom', 'inherit');
									$('.fixed-aside').css('top', '0');
								}

								if ($(window).scrollTop() + $(window).height() > $(document).height()) {
									/* Near bottom of the page */
									$('.fixed-aside').css('top', 'inherit');
									$('.fixed-aside').css('bottom', '0');
								}

							});
						}
					}

					$(window).on('resize', function(event) {
						fixedAside();
					});

					$(window).trigger('resize');

					$(document).on('click', '.booking-calendar .calendar-days li', function() {
						setTimeout(function() {
							var new_calendar_height = $('.fixed-aside .booking-calendar').outerHeight();
							var close_to_bottom = $(window).scrollTop() + $(window).height() > $(document).height() - 100;
							if (new_calendar_height > old_calendar_height && close_to_bottom) {
								$('.fixed-aside').css('top', 'inherit');
								$('.fixed-aside').css('bottom', '0');
							}
						}, 100);
					});
				</script>
			</div>
		</div>
</section>
<div id="full-availability-popup" class="modal">
	<div class="modal-content">
	</div>
</div>

<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAAa2VYW1RAhvGkAwcnEK-5JNro89_XTME"></script>
<script type="text/javascript">
	var sale_location_map;
	var geocoder;
	var address = '<?= h(trim($sale->location_title)); ?>';
	var calendar;

	var sale = <?= json_encode($sale); ?>;
	var booking_dates;
	var first_selected_day;
	var last_selected_day
	var first_selected_month;
	var last_selected_month;
	var first_selected_year;
	var last_selected_year;
	var in_between_dates;

	function createCookie(name, value, days) {
		var expires;
		if (days) {
			var date = new Date();
			date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
			expires = "; expires=" + date.toGMTString();
		} else {
			expires = "";
		}
		document.cookie = name + "=" + value + expires + "; path=/";
	}


	function retrieveCookie(cname) {
		var cookiename = cname + "=";
		//Decode the document.cookie string
		var decodedCookieString = decodeURIComponent(document.cookie);
		//Array split the cookie string into individual cookies
		var cookieArray = decodedCookieString.split(';');
		//Iterate through the individual cookies
		for (var currentCookieIndex in cookieArray) {
			//Get the current cookie
			var currentCookie = cookieArray[currentCookieIndex];
			//Make sure it's not empty
			while (currentCookie.charAt(0) == ' ') {
				//And retrieve it
				currentCookie = currentCookie.substring(1);
			}
			//If the cookie's name matches the one we want
			if (currentCookie.indexOf(cookiename) === 0) {
				//Return it
				return currentCookie.substring(cookiename.length, currentCookie.length);
			}
		}
		//Otherwise return nothing
		return "";
	}

	function sale_map() {
		geocoder = new google.maps.Geocoder();

		geocoder.geocode({
			'address': address
		}, function(results, status) {
			if (status == google.maps.GeocoderStatus.OK) {
				sale_location_map = new google.maps.Map(document.getElementById('location_map'), {
					zoom: 10
				});

				if (status != google.maps.GeocoderStatus.ZERO_RESULTS) {
					sale_location_map.setCenter(results[0].geometry.location);

					var infowindow = new google.maps.InfoWindow({
						content: '<b>' + address + '</b>',
						size: new google.maps.Size(150, 50)
					});

					var marker = new google.maps.Marker({
						position: results[0].geometry.location,
						map: sale_location_map,
						title: address
					});

					google.maps.event.addListener(marker, 'click', function() {
						infowindow.open(sale_location_map, marker);
					});

				} else {
					alert("<?= __('No results found'); ?>");
				}
			}
		});
	}

	var sale_allocations;
	var g_calendar = null;

	(function($) {

		function arrangeCalendar(clickedOffer) {
			var sale_offer_id = $(clickedOffer).attr('data-offer-id');

			var sale_type = $('input[name="book[type]"]').val();
			var airport_code = '';

			var rooms = $('select[name=rooms]', clickedOffer).val();

			if (sale_type == 1) {
				airport_code = $('select[name=departure_airport_code]', clickedOffer).val();
			}

			$('input[name="book[sale_allocation_id]"]').val('');
			$('input[name="book[start_flexible_date]"]').val('');
			$('input[name="book[end_flexible_date]"]').val('');
			$('input[name="book[offer_id]"]').val(sale_offer_id);

			$.ajax({
				url: '/sales/getSaleAllocations',
				type: 'GET',
				data: {
					sale_offer_id: sale_offer_id,
					sale_type: sale_type,
					airport_code: airport_code,
					currency_id: <?= $sale->currency_id ?>
				},
				dataType: 'json',
				success: function(response) {
					if (response.status == "success") {
						sale_allocations = response.data.sale_allocations;

						var start_calendar_date = "";
						var end_calendar_date = "";

						for (var i = 0; i < sale_allocations.length; i++) {
							if (!start_calendar_date || sale_allocations[i].date_start < start_calendar_date) {
								start_calendar_date = sale_allocations[i].date_start;

							}

							if (!end_calendar_date || sale_allocations[i].date_end > end_calendar_date) {
								end_calendar_date = sale_allocations[i].date_end;
							}

							if (sale_allocations[i].outbound_overnight) {
								var end_date = new Date(end_calendar_date);
								end_date.setDate(end_date.getDate() + 1);
								end_calendar_date = end_date.getFullYear() + '-' + end_date.getMonth() + '-' + end_date.getDate();
							}

							if (sale_allocations[i].inbound_overnight) {
								var end_date = new Date(end_calendar_date);
								end_date.setDate(end_date.getDate() + 1);
								end_calendar_date = end_date.getFullYear() + '-' + end_date.getMonth() + '-' + end_date.getDate();
							}

							if (sale_allocations[i].available_rooms < 1 || sale_allocations[i].available_rooms < rooms) {
								sale_allocations.splice(i, 1);
								continue;
							}
						}

						if (start_calendar_date == "" || end_calendar_date == "") {
							$('.booking-calendar .no-allocations').show();
							$('.booking-calendar .isl-calendar-picker').hide();
						} else {
							$('.booking-calendar .no-allocations').hide();
							$('.booking-calendar .isl-calendar-picker').show();
							calendar = new Calendar(
								'.booking-calendar',
								start_calendar_date,
								end_calendar_date,
								response.data.sale_offer,
								response.data.deposit,
								response.data.currency,
								response.data.currency_rate,
								(g_calendar !== null && g_calendar.sale_offer_id == sale_offer_id) ? g_calendar.startDay : false,
								(g_calendar !== null && g_calendar.sale_offer_id == sale_offer_id) ? g_calendar.endDay : false,
								sale_offer_id
							);
							g_calendar = calendar;
							calendar.addPrices(sale_allocations, sale_type);
						}

						$(".offer").on("click", function() {
							var expanded_content = $('.expanded-content', this);
							if ($(this).attr('data-offer-id') !== $('input[name="book[offer_id]"]').val()) {
								createCookie('booking_dates', ' ', 1);
							}
						});
						if (document.cookie.match(/booking_dates=[^;\s]/)) {
							var getDatesObject = retrieveCookie('booking_dates');
							var datesRecreated = JSON.parse(getDatesObject);
							var html = '';
							var start_date_cookie = datesRecreated.date_start;
							var end_date_cookie = datesRecreated.date_end;


							$('input[name="book[start_flexible_date]"]').val(datesRecreated.date_start);
							$('input[name="book[end_flexible_date]"]').val(datesRecreated.date_end);
							$('input[name="book[sale_allocation_id]"]').val(datesRecreated.sale_allocation_ids);

							if ($('input[name="book[start_flexible_date]"]').val() != '' && $('input[name="book[end_flexible_date]"]').val() != '' && $('input[name="book[sale_allocation_id]"]').val() != '') {
								if ($('.calendar-months [data-full-date="' + datesRecreated.date_start + '"]').length &&
									$('.calendar-months [data-full-date="' + datesRecreated.date_end + '"]').length) {
									// calendar.dayClick($('.calendar-months [data-full-date="'+datesRecreated.date_start+'"]'));
									// calendar.dayClick($('.calendar-months [data-full-date="'+datesRecreated.date_end+'"]'));
									var first_day = $('.calendar-months [data-full-date="' + datesRecreated.date_start + '"]');
									var last_day = $('.calendar-months [data-full-date="' + datesRecreated.date_end + '"]');
									first_day.addClass('first-selected chosen');
									last_day.addClass('last-selected chosen');
									//if(!first_day.nextUntil(last_day).hasClass('inactive')) {
									first_day.nextUntil(last_day).addClass('chosen');
									//}
								}
								start_date = new Date(start_date_cookie);
								end_date = new Date(end_date_cookie);
								html += '<i class="material-icons">date_range</i>';
								html += '&nbsp;<span class="range-date-start">' + start_date.toDateString() + '</span>';
								html += '&nbsp;<i class="material-icons">trending_flat</i>';
								html += '&nbsp;<i class="material-icons">date_range</i>';
								html += '&nbsp;<span class="range-date-end">' + end_date.toDateString() + '</span>';
								$('.date-range').html(html);

								calendar.start_flexible_date = datesRecreated.date_start;
								calendar.end_flexible_date = datesRecreated.date_end;
								calendar.addSelectionSummary();
								$('#SalesView [name="book[min_nights_validate]"]').val(0);

								$('.card-panel').parent().each(function(index, value) {
									var expanded_content = $('.expanded-content', value);

									if ($(value).attr('data-offer-id') === datesRecreated.offer_id) {
										$(value).children().eq(0).addClass('opened');
										expanded_content.slideDown(300);
									} else {
										$(value).children().eq(0).removeClass('opened');
										expanded_content.slideUp(300);
									}
								});
							} else {
								createCookie('booking_dates', ' ', 1);
							}
						}

						$('.full-availability').click(function() {
							$('#full-availability-popup .modal-content').html($('.booking-calendar').html());
							$('#full-availability-popup .full-availability').hide(0);
							$('#full-availability-popup .calendar-header').hide(0);
							$('#full-availability-popup .date-range').hide(0);
							$('#full-availability-popup .clear-range').hide(0);
							$('#full-availability-popup .summary').hide(0);
							$('#full-availability-popup .calendar-months div.month').show(0);
							$('#full-availability-popup .calendar-months div.month .month-label').show(0);
						});

					}
				},
			});
		}

		$(document).ready(function() {
			<?php if (!empty($video_id)) { ?>
				var tag = document.createElement('script');

				tag.src = "https://www.youtube.com/iframe_api";
				var firstScriptTag = document.getElementsByTagName('script')[0];
				firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);

				window.onYouTubeIframeAPIReady = function() {
					player = new YT.Player('sale-video', {
						videoId: '<?= $video_id ?>',
						events: {
							'onStateChange': onPlayerStateChange
						}
					});
				}

				function onPlayerStateChange() {
					var state = player.getPlayerState();

					if (state == 1) {
						$('.flexslider').flexslider('pause');
					} else {
						$('.flexslider').flexslider('play');
					}
				}

			<?php } ?>

			$('.flexslider').flexslider({
				animation: "slide",
				pauseOnAction: false,
				slideshow: true,
				slideshowSpeed: 5000,
				video: true
			});

			$('a[href=\'#travel_details\']').on('click', function() {
				sale_map();
				$(this).off('click');
			});

			$('#book-now').on('click', function(e) {
				if (($('input[name=\'book[sale_allocation_id]\']').val() == "0" || $('input[name=\'book[sale_allocation_id]\']').val() == "") &&
					($('input[name=\'book[start_flexible_date]\']').val() == "" || $('input[name=\'book[end_flexible_date]\']').val() == "")) {

					e.preventDefault();
					alert("<?= __('Please, pick dates'); ?>");

				}

				if ($('input[name=\'book[min_nights_validate]\']').val() != "0") {
					e.preventDefault();
					alert('You should pick at least ' + $('input[name=\'book[min_nights_validate]\']').val() + ' nights!');
				}

			});
			var isMobile = false;
			if (/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|ipad|iris|kindle|Android|Silk|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i.test(navigator.userAgent) ||
				/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(navigator.userAgent.substr(0, 4))) {
				isMobile = true;
			}

			var calendar_html = $('.card-panel.booking-calendar').html();
			var button_html = $('#desktop-button-book').html();
			if ($(window).width() < 1200) {
				$('.fixed-aside .card-panel.booking-calendar').detach();
				if (isMobile) {
					$("#desktop-button-book").detach();
				}
				$('.fixed-aside h5').hide();
			}

			$('#offers .offer').on('click', function() {
				var expanded_content = $('.expanded-content', this);
				var offer_box = expanded_content.parents('.row.offer .card-panel');
				var sale_offer_id = $(this).attr('data-offer-id');

				$('input[name="book[rooms]"]').val($('select[name="rooms"]', expanded_content).val());
				$('input[name="book[offer_id]"]').val(sale_offer_id);

				if (!offer_box.hasClass('opened')) {
					$('#offers .offer .expanded-content').each(function() {
						$(this).slideUp(300);
						$(this).parents('.row.offer .card-panel').removeClass('opened');
						$(this).parents('.row.offer .card-panel').parents().find('.movable').remove();
					});

					expanded_content.slideDown(300);
					offer_box.addClass('opened');

					arrangeCalendar(this);

					if ($(window).width() < 1200) {

						offer_box.parent().append('<div class="card-panel booking-calendar movable">' + calendar_html + '</div>');
						if (isMobile) {
							offer_box.parent().append('<div id="desktop-button-book" class="s12 movable" style="background:#fff; color:#333; padding: 20px;">' + button_html + '</div>');
							$('input[name="book[offer_id]"]').val(sale_offer_id);

						}
					}
				}
			});

			$('select[name="departure_airport_code"], select[name="rooms"]').on('change', function() {
				var openedOffer = $('.card-panel.opened').parent();
				arrangeCalendar(openedOffer);
			});

			$('#offers .offer').first().click();

			$(document).on('change', '.expanded-content select[name="rooms"]', function() {
				$('input[name="book[rooms]"]').val($(this).val());
			});

			$('.modal').modal();
			$('.collapsible').collapsible();
		});
	})(jQuery);
</script>
