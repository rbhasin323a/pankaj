<?php $this->assign('title', __('Print Page')); ?>
<section id="SalesPrint">
	<div class="container">
		<div class="center">
			<div class="logo">
				<img class="logo-header" src="/ui/img/logo_header.png" data-rjs="/ui/img/logo_header@2x.png" />
				<h3><?= __('Luxury Discounts'); ?></h3>
			</div>
			<h5>Booking ID : <?= $booking->id ?></h4>
		</div>
		<div class="center" style="margin-top:40px">
			<h5><?= __('Place Information'); ?></h5>

			<table class="table">
				<thead>
					<tr>
						<th><?= __('Hotel Name'); ?></th>
						<th><?= __('Address'); ?></th>
						<th><?= __('Telephone'); ?></th>
						<th><?= __('Email'); ?></th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td><?= $sale->title ?></td>
						<td><?= $sale->location_title; ?></td>
						<td><?= $sale->company->support_phone ?></td>
						<td><?= $sale->company->support_email ?></td>
					</tr>
				</tbody>
			</table>
		</div>


		<div class="center">
			<h5><?= __('Travellers'); ?></h5>

			<table class="table">
				<thead>
					<tr>
						<th><?= __('Name'); ?></th>
						<th><?= __('Birthday'); ?></th>
						<th><?= __('Type'); ?></th>
					</tr>
				</thead>
				<tbody>
					<?php if ($travellers->isEmpty()) { ?>
						<tr>
							<td><?= $booking->first_name . ' ' . $booking->last_name ?></td>
							<td><?= $booking->my_user->birthday ?></td>
							<td>Adult</td>
						</tr>
					<?php } ?>
					<?php foreach ($travellers as $traveller) { ?>
						<tr>
							<td><?= $traveller->title . ' ' . $traveller->first_name . ' ' . $traveller->last_name ?></td>
							<td><?= $traveller->birthday ?></td>
							<td><?= $traveller->type ?></td>
						</tr>
					<?php } ?>
				</tbody>
			</table>
		</div>
		<hr />
		<div class="row">
			<div class="col s6 center">
				<h5><?= __('Check-in'); ?></h5>
				<h5><?= $check_in_time ?></h5>
			</div>

			<div class="col s6 center">
				<h5><?= __('Check-out'); ?></h5>
				<h5><?= $check_out_time ?></h5>
			</div>
		</div>
		<hr />
		<div class="row">
			<div class="col s3 center">
				<h6><?= __('Nights'); ?></h6>
				<h7><?= $nights; ?></h7>
			</div>

			<div class="col s3 center">
				<h6><?= __('Room(s)'); ?></h6>
				<h7><?= $booking->rooms ?></h7>
			</div>

			<div class="col s3 center">
				<h6><?= __('Adults'); ?></h6>
				<h7><?= $booking->adults ?></h7>
			</div>

			<div class="col s3 center">
				<h6><?= __('Children'); ?></h6>
				<h7><?= (int)$booking->children + (int)$booking->infants ?></h7>
			</div>
		</div>
		<div class="row">
			<div class="col s12 center">
				<h5><?= __('Notes'); ?></h5>
				<h6><?= !empty($booking->notes) ? $booking->notes : '' ?></h6>
				<h6>..........................................................................................................</h6>
				<h6>..........................................................................................................</h6>
			</div>
		</div>
		<div class="row">
			<div class="center">
				<h5><?= __('Company Information'); ?></h5>

				<table class="table">
					<thead>
						<tr>
							<th><?= __('Company Name'); ?></th>
							<th><?= __('Telephone'); ?></th>
							<th><?= __('Email'); ?></th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td><?= $sale->company->name ?></td>
							<td><?= $sale->company->support_phone ?></td>
							<td><?= $sale->company->support_email ?></td>
						</tr>
					</tbody>
				</table>
			</div>
		</div>
	</div>
</section>
<style>
	@media print {
		@page {
			margin: 0;
		}

		body {
			margin: .6cm;
		}

		html,
		body {
			height: auto;
		}
	}

	.page {
		margin: 0;
		min-height: inherit;
	}

	.print:last-child {
		page-break-after: auto;
	}
</style>
<script>
	$(document).ready(function() {
		$('header, footer, .drag-target, .common').remove();
		window.print();
	});
</script>
