<?php
$this->assign('title', __('Booking Successful'));

$this->HtmlBetter->addCrumb(__('Home'), '/');
$this->HtmlBetter->addCrumb(__('Latest Sales'), '/latest-sales');
$this->HtmlBetter->addCrumb(__('Booking Successful'), '/sale/success');
?>

<section id="SalesView">
	<div class="intro valign-wrapper">
		<div class="valign container">
			<?php if (!$mobile) { ?>
				<h3 style="display: inline-block"><?= __('Congratulations!'); ?></h3>
				<a href="<?= '/sales/success/' . $booking->id . '?print=true' ?>" style="position: relative; top: 30px; float: right;" target="blank" id="print-button" class="waves-effect waves-light btn"><?= __('Print'); ?></a>
			<?php } else {  ?>
				<a id="print-button" target="blank" style="float: right;" class="waves-effect waves-light btn mb20"><?= __('Print'); ?></a>
				<h3><?= __('Congratulations!'); ?></h3>
			<?php } ?>
			<h5><?= __('Your booking (ID: {0}) is now confirmed and your adventure is waiting for you.', [$booking->id]); ?></h5>
		</div>
	</div>
	<div class="container">
		<div class="card horizontal" style=" padding: 0 15px;">
			<div class="card-content text-left">
				<div class="row">
					<p><i class="material-icons pad green-text">&#xE876;</i><?= __('We have sent the confirmation e-mail to <a href="mailto:{0}">{0}</a>', [$booking->my_user->email]); ?></p>
					<p><i class="material-icons pad green-text">&#xE876;</i><?= __('Your booking for {0} is already confirmed', [$sale->title]); ?></p>
					<?php if ($max_discount != 0) { ?>
						<p><i class="material-icons pad green-text">&#xE876;</i><?= __('Our rate includes up to {0}% discount', [$max_discount]); ?></p>
					<?php } ?>
					<p><i class="material-icons pad green-text">&#xE876;</i><?= __('Got a query or request from the hotel? Send them a message at <a href="mailto:{0}">{0}</a> or call {1}', [$sale->company->support_email, $sale->company->support_phone]); ?></p>
				</div>
			</div>
		</div>
	</div>

	<div class="valing-wrapper">
		<div class="valign container">
			<h5 style="margin-top: 30px; margin-bottom: 10px;"><?= __('Check your details'); ?></h5>

			<div class="col m12 s12">
				<div class="card <?= $mobile ? 'vertical' : 'horizontal' ?>">
					<div class="card-content" style="width: 100%">
						<?php if (!$mobile) { ?>
							<div class="row">
								<div class="col m3 strong"><?= __('Booking ID'); ?></div>
								<div class="col m3 strong"><?= __('Name'); ?></div>
								<div class="col m3 strong"><?= __('Email'); ?></div>
								<div class="col m3 strong"><?= __('Phone'); ?></div>
								<div class="col m3"><?= $booking->id ?></div>
								<div class="col m3"><?= $booking->first_name . ' ' . $booking->last_name ?></div>
								<div class="col m3"><a href="mailto: <?= $booking->my_user->email ?>"><?= $booking->my_user->email ?></a></div>
								<div class="col m3"><?= $booking->phone ?></div>
							</div>

							<?php if (!$travellers->isEmpty()) { ?>
								<div id="travellers-details" style="display: none;">
									<?php foreach ($travellers as $traveller) { ?>
										<div class="row">
											<div class="col m3 strong"><?= __('Passport No.'); ?></div>
											<div class="col m3 strong"><?= __('Name'); ?></div>
											<div class="col m3 strong"><?= __('Birthday'); ?></div>
											<div class="col m3 strong"><?= __('Type'); ?></div>
											<div class="col m3"><?= $traveller->passport_number ?></div>
											<div class="col m3"><?= $traveller->title . ' ' . $traveller->first_name . ' ' . $traveller->last_name ?></div>
											<div class="col m3"><?= $traveller->birthday ?></div>
											<div class="col m3"><?= $traveller->type ?></div>
										</div>
									<?php } ?>
								</div>
							<?php } ?>
						<?php } else { ?>
							<div class="row" style="margin: 0">
								<div class="col s4 strong"><?= __('Booking ID'); ?></div>
								<div class="col s8"><?= $booking->id ?></div>
							</div>
							<div class="row" style="margin: 0">
								<div class="col s4 strong"><?= __('Name'); ?></div>
								<div class="col s8"><?= $booking->first_name . ' ' . $booking->last_name ?></div>
							</div>
							<div class="row" style="margin: 0">
								<div class="col s4 strong"><?= __('Email'); ?></div>
								<div class="col s8"><?= $booking->my_user->email ?></div>
							</div>
							<div class="row" style="margin: 0">
								<div class="col s4 strong"><?= __('Phone'); ?></div>
								<div class="col s8"><?= $booking->phone ?></div>
							</div>

							<?php if (!$travellers->isEmpty()) { ?>
								<div id="travellers-details" class="mt20" style="display: none;">
									<?php $first = true; ?>
									<?php $count = 1; ?>
									<?php foreach ($travellers as $traveller) { ?>
										<div class="traveller-info" style="<?= !$first ? 'padding-top: 20px;' : '' ?>">
											<span class="traveller-label">Traveller <?= $count ?></span>
											<div class="row" style="margin: 0">
												<div class="col s4 strong"><?= __('Passport No.'); ?></div>
												<div class="col s8"><?= $traveller->passport_number ?></div>
											</div>
											<div class="row" style="margin: 0">
												<div class="col s4 strong"><?= __('Name'); ?></div>
												<div class="col s8"><?= $traveller->title . ' ' . $traveller->first_name . ' ' . $traveller->last_name ?></div>
											</div>
											<div class="row" style="margin: 0">
												<div class="col s4 strong"><?= __('Birthday'); ?></div>
												<div class="col s8"><?= $traveller->birthday ?></div>
											</div>
											<div class="row" style="margin: 0">
												<div class="col s4 strong"><?= __('Type'); ?></div>
												<div class="col s8"><?= $traveller->type ?></div>
											</div>
										</div>
										<?php $first = false; ?>
										<?php $count++; ?>
									<?php } ?>
								</div>
							<?php } ?>
						<?php } ?>
						<?php if (!$travellers->isEmpty()) { ?>
							<div class="center">
								<button class="waves-effect waves-light btn details-trigger mt15" data-target="more_details_<?= $sale_offer->id ?>"><?= __('Travellers Details'); ?></button>
							</div>
						<?php } ?>
					</div>
				</div>
			</div>

			<div class="card-wrap">
				<div class="card <?= $mobile ? 'vertical' : 'horizontal' ?>" data-sale-id="<?= $sale->id; ?>" data-created="<?= $sale->created; ?>">
					<div class="activator card-image waves-effect waves-block waves-light">
						<a class="activator">
							<img class="" src="/files/images/sales/<?= $sale->main_image; ?>">
						</a>
					</div>
					<div class="card-stacked">
						<div class="card-content">
							<a href="<?= $this->Url->build(['controller' => 'Sales', 'action' => 'view', $sale->slug]); ?>" target="blank" class="card-title center black-text inline-full"><?= $sale->title; ?></a>
							<p class="blue-grey-text text-darken-3"><i class="material-icons va-t fs20">location_on</i> <?= $sale->destination; ?></p>
							<div class="divider mv10"></div>
							<div class="destination-info text-left">
								<span style="color: red">
									<p><?= __('Address:'); ?></p>
								</span>
								<p><?= __('Tel.:'); ?> <?= $sale->company->support_phone ?></p>
								<p><?= __('Email:'); ?> <a href="mailto: <?= $sale->company->support_email ?>"><?= $sale->company->support_email ?></a></p>
								<?php if (!empty($coordinates)) { ?>
									<p><?= __('GPS Coordinates:'); ?> <a href="<?= "http://www.google.com/maps/place/$coordinates[0],$coordinates[1]" ?>"><?= $sale->location_map ?></a></p>
								<?php } ?>
							</div>
						</div>
					</div>
				</div>
			</div>

			<div class="row">
				<div class="col m12 s12">
					<div class="card horizontal">
						<div class="card-content center" style="width: 100%">
							<div class="row">
								<div class="col m6 s12 card-left">
									<div class="valign-wrapper">
										<div class="container">
											<div class="center" style="font-size: 20px;"><?= __('Check-in'); ?></div>
											<div class="center"><?= $check_in_time ?></div>
										</div>
									</div>
									<div class="divider mv10"></div>
									<div class="row" style="margin-bottom: 0">
										<div class="col m6 s6">
											<?= $nights; ?><br />
											<?= __('Nights'); ?>
										</div>
										<div class="col m6 s6">
											<?= $booking->rooms ?><br />
											<?= __('Room(s)'); ?>
										</div>
									</div>
									<div class="divider mv10"></div>
									<div id="sale-offer-description" style="font-size: 14px; text-align: left;"><?= $sale_offer->main_description ?></div>
								</div>

								<div class="col m6 s12 card-right">
									<div class="valign-wrapper">
										<div class="container">
											<div class="center" style="font-size: 20px;"><?= __('Check-out'); ?></div>
											<div class="center"><?= $check_out_time ?></div>
										</div>
									</div>
									<div class="divider mv10"></div>
									<div class="row" style="margin-bottom: 0">
										<div class="col m6 s6">
											<?= $booking->adults ?><br />
											<?= __('Adults'); ?>
										</div>
										<div class="col m6 s6">
											<?= $booking->children ?>
											<?= __('Children'); ?><br />
											<?= $booking->infants ?>
											<?= __('Infants'); ?>
										</div>
									</div>
									<div class="divider mv10"></div>
									<div class="table">
										<div class="valign-table-cell" id="total-price">
											<span class="total-heading"><?= __('Total Price:'); ?></span><br />
											<span class="total-value"><?= $currency->convert($booking->currency_id, null, $booking->sale_total); ?></span>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>

			<h5 style="margin-bottom: 10px;"><?= __('Next steps'); ?></h5>
			<div class="card horizontal" style=" padding: 0 15px;">
				<div class="card-content text-left">
					<div class="row">
						<p><i class="material-icons tiny pad-tiny">label</i><?= __('Your booking at {0} has been already confirmed and the hotel will be expecting you.', [$sale->company->name]); ?></p>
						<p><i class="material-icons tiny pad-tiny">label</i><?= __('You will be asked for your name and booking reference ({0}) upon arrival.', [$booking->id]); ?></p><br />
						<p><?= __('You may be asked to provide a credit card to cover additional costs during your stay, like the minibar or room service.'); ?></p>
					</div>
				</div>
			</div>

			<h5 style="margin-top: 35px; margin-bottom: 10px;"><?= __('Need help?'); ?></h5>
			<div class="card horizontal" style=" padding: 0 15px;">
				<div class="card-content text-left">
					<div class="row">
						<p><?= __('Should you need any further information or details please get in contact directly with the hotel or contact us at: customersupport@luxury-discounts.com.'); ?></p><br />
						<p><?= __('For all kinds of queries please always quote your booking reference number – {0}.', [$booking->id]); ?></p>
					</div>
				</div>
			</div>

			<h5 class="mt35"><?= __('REFUND OF AMOUNTS AND CANCELLATION OF RESERVATIONS'); ?></h5>
			<div class="card horizontal" style=" padding: 0 15px;">
				<div class="card-content text-left">
					<p><b>7.1.</b> <?= __('If a user has paid a reservation for hotel accommodation and/or extra tourist services made via the website, and the provider of the travel offer refuses to execute the contract, then the latter should refund to the user the paid amounts in full.'); ?></p>
					<p><b>7.2.</b> <?= __('If the tour operator providing the respective organized travel significantly changes some of the main clauses of the contract or refuses to execute the contract, the user may cancel the contract without owing any penalty or compensation. In these cases the relations between the user and the tour operator are settled between them and <b>Luxury Discounts LTD</b>. may only provide assistance for such settlement.'); ?></p>
					<p><b>7.3.1.</b> <?= __('In case of cancellation by the user of booking  for hotel accommodation and other tourist services made via the website, the paid amounts are not subject to return by <b>Luxury Discounts LTD</b>. or the Provider of the purchased travel offer, unless the right for cancellation is not explicitly stated in the terms and conditions of the particular offer.'); ?></p>
					<p><b>7.3.2.</b> <?= __('If the user is granted a possibility to cancel a reservation, pursuant to Article 7.3.1 the provider of the purchased travel offer may charge penalties according to the terms and conditions of the offer.'); ?></p>
					<p><b>7.4.1.</b> <?= __('In case of a cancellation by the user of booking for organized travel made via the website, the paid amounts are not subject to return and in case they are, the refund is made according to the rules and conditions of the tour operator providing the respective organized travel.'); ?></p>
					<p><b>7.4.2.</b> <?= __('In such cases as those under the foregoing paragraph, the user should contact and negotiate directly with the tour operator providing the respective organized travel.'); ?></p>
				</div>
			</div>
		</div>
	</div>
</section>

<style>
	.strong {
		font-weight: 600;
	}

	.booking-info {
		font-size: 110%;
	}

	.booking-id {
		color: #FBD26B;
	}

	.card-title:after {
		content: "";
		border-bottom: 1px solid white;
	}

	.material-icons.pad {
		position: relative;
		top: 7px;
		padding-right: 5px;
	}

	.pad-tiny {
		position: relative;
		top: 2px;
		padding-right: 5px;
	}

	.green-text {
		color: #008000;
	}

	.table {
		display: table;
		width: 100%;
	}

	.valign-table-cell {
		display: table-cell;
		vertical-align: middle;
	}

	.total-heading {
		font-size: 24px;
		font-weight: 600;
		text-transform: uppercase;
	}

	.total-value {
		font-size: 40px;
		font-weight: 700;
	}

	.traveller-label {
		font-size: 18px;
		display: block;
		text-align: center;
	}
</style>

<script type="text/javascript">
	$('.details-trigger').on('click', function() {
		$('#travellers-details').slideToggle();
	});

	$('#print-button').on('click', function() {

	});
</script>

<?php if (!$mobile) { ?>
	<script type="text/javascript">
		$(document).ready(function() {
			var colHeight = $('#sale-offer-description').height();
			$('#total-price').height(colHeight);
		});
	</script>
<?php } else { ?>
	<script type="text/javascript">
		$(document).ready(function() {
			$('.card-right').css('margin-top', '4em');

			if ($('.intro .container').width() > $('.intro .container h3').width()) {
				var fontSize = parseFloat($('.intro .container h3').css('font-size'));
				var newFontSize = fontSize - ((20 / 100) * fontSize);
				$('.intro .container h3').css('font-size', newFontSize.toString() + 'px');
			}
		});
	</script>
<?php } ?>
