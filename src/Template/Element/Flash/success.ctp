<?php
$class = 'alert';

if (!empty($params['class'])) {
    $class .= ' alert-' . $params['class'];

	// Error
	if ($params['class'] == 'error') {
		$class .= ' red darken-2';
	}
} else {
	$class .= ' green';
}
?>
<div class="<?= $class; ?> alert-primary alert-dismissable brn z-depth-1">
	<div class="container">
		<div class="col s12">
			<a class="btn-floating btn-flat waves-effect waves-light right close"><i class="material-icons">close</i></a>
			<p class="white-text mbn pv15 text-center"><?= $message; ?></p>
		</div>
	</div>
</div>
