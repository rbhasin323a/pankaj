<?php
/**
 * Sales Filter Component
 */
$query = $this->request->query;

$this->loadHelper('Form', [
	'templates' => 'app_form',
]);

$c_id = substr(md5(uniqid(mt_rand(), true)), 0, 10);
$c_class = 'sales-filter';
?>
<?= $this->Form->create(null, ['url' => ['prefix' => false, 'plugin' => false, 'controller' => 'Sales', 'action' => 'index']]); ?>
<section id="SalesFilter-<?= $c_id; ?>" class="<?= $c_class; ?>">
	<div class="<?= $c_class; ?>__form__calendar-holder carousel white z-depth-1 br3" data-indicators="true">
		<div class="<?= $c_class; ?>__form__calendar-box carousel-item">
			<div class="<?= $c_class; ?>__form__calendar-label">
				<label class="inline-full fs15 fw400 p10 blue-grey-text text-darken-4" for="SalesFilter-<?= $c_id; ?>-checkin"><?= __d('SalesFilter', 'Check-In Date'); ?></label>
			</div>
			<input type="hidden" class="<?= $c_class; ?>__form__calendar-input" id="SalesFilter-<?= $c_id; ?>-checkin" name="checkin" data-value="<?= !empty($query['checkin']) ? h($query['checkin']) : ''; ?>" />
		</div>
		<div class="<?= $c_class; ?>__form__calendar-box carousel-item">
			<div class="<?= $c_class; ?>__form__calendar-label">
				<label class="inline-full fs15 fw400 p10 blue-grey-text text-darken-4" for="SalesFilter-<?= $c_id; ?>-checkout"><?= __d('SalesFilter', 'Check-Out Date'); ?></label>
			</div>
			<input type="hidden" class="<?= $c_class; ?>__form__calendar-input" id="SalesFilter-<?= $c_id; ?>-checkout" name="checkout" data-value="<?= !empty($query['checkout']) ? h($query['checkout']) : ''; ?>" />
		</div>
	</div>
	<script type="text/javascript">
		(function($) {
			function getSalesFilterDates() {
				var dates = {
					'checkin': {
						'day': null,
						'month': null,
						'year': null,
						'monthIndex': null,
						'timestamp': null,
						'date': null
					},
					'checkout': {
						'day': null,
						'month': null,
						'year': null,
						'monthIndex': null,
						'timestamp': null,
						'date': null
					}
				};

				var checkin_picker = $('#SalesFilter-<?= $c_id; ?>-checkin').pickadate('picker'),
					checkout_picker = $('#SalesFilter-<?= $c_id; ?>-checkout').pickadate('picker');

				if (checkin_picker.get('select')) {
					var checkin_pick = checkin_picker.get('select').pick,
						checkin_min = new Date(checkin_pick);

					dates.checkin.day = checkin_min.getDate(),
						dates.checkin.month = checkin_min.getMonth() + 1,
						dates.checkin.year = checkin_min.getFullYear();

					dates.checkin.monthIndex = checkin_min.getMonth(),
						dates.checkin.timestamp = checkin_min.getTime(),
						dates.checkin.date = checkin_min.getFullYear() + '-' + dates.checkin.month + '-' + checkin_min.getDate();
				}

				if (checkout_picker.get('select')) {
					var checkout_pick = checkout_picker.get('select').pick,
						checkout_min = new Date(checkout_pick);

					dates.checkout.day = checkout_min.getDate(),
						dates.checkout.month = checkout_min.getMonth() + 1,
						dates.checkout.year = checkout_min.getFullYear();

					dates.checkout.monthIndex = checkout_min.getMonth(),
						dates.checkout.timestamp = checkout_min.getTime(),
						dates.checkout.date = checkout_min.getFullYear() + '-' + dates.checkout.month + '-' + checkout_min.getDate();
				}

				return dates;
			}

			function updateSalesFilter() {
				var dates = getSalesFilterDates();

				var $checkin_root = $('#SalesFilter-<?= $c_id; ?>-checkin_root'),
					$checkin_picks = $('[data-pick]', $checkin_root);
				$checkin_day = $('[data-pick="' + dates.checkin.timestamp + '"]', $checkin_root);

				var $checkout_root = $('#SalesFilter-<?= $c_id; ?>-checkout_root'),
					$checkout_picks = $('[data-pick]', $checkout_root);
				$checkout_day = $('[data-pick="' + dates.checkout.timestamp + '"]', $checkout_root);

				$checkin_picks.add($checkout_picks).each(function(index, el) {
					var $pick = $(this),
						pick_timestamp = $pick.attr('data-pick');

					if (pick_timestamp == dates.checkin.timestamp || pick_timestamp == dates.checkout.timestamp) {
						$pick.addClass('picker__day--selected');
					}

					if (dates.checkin.timestamp !== null && dates.checkout.timestamp !== null) {
						if (pick_timestamp >= dates.checkin.timestamp && pick_timestamp <= dates.checkout.timestamp) {
							$pick.addClass('picker__day--ranged');
						}
					}
				});
			}

			function updateSalesList() {
				var request_data = {
					'checkin': null,
					'checkout': null
				};

				var dates = getSalesFilterDates();

				if (dates.checkin.date !== null) {
					request_data.checkin = dates.checkin.date;
				}

				if (dates.checkout.date !== null) {
					request_data.checkout = dates.checkout.date;
				}

				var $sales_list = $('.sales-list');
				$sales_list.addClass('sales-list--loading');

				$.ajax({
						url: '/latest-sales',
						type: 'GET',
						data: request_data,
					})
					.done(function(response) {
						var html = $.parseHTML(response);

						if (typeof html !== "undefined" && $sales_list.length > 0) {
							$response_sales_list = $('.sales-list', html);

							$sales_list.html($response_sales_list.html());

							$sales_list.removeClass('sales-list--loading');
						}
					});

				setTimeout(function() {
					$sales_list.removeClass('sales-list--loading')
				}, 5000);
			}

			var $sales_filter = $('#SalesFilter-<?= $c_id; ?> .<?= $c_class; ?>__form__calendar-holder');

			$(document).ready(function() {
				var today = new Date(),
					tomorrow = new Date();
				tomorrow.setDate(tomorrow.getDate() + 1);

				var $calendar_input_checkin = $('#SalesFilter-<?= $c_id; ?>-checkin').pickadate({
						selectMonths: true,
						min: today,
						formatSubmit: 'yyyy-mm-dd',
						closeOnSelect: false,
						closeOnClear: false,
						hiddenName: true
					}),
					checkin_picker = $calendar_input_checkin.pickadate('picker');

				var $calendar_input_checkout = $('#SalesFilter-<?= $c_id; ?>-checkout').pickadate({
						selectMonths: true,
						min: tomorrow,
						formatSubmit: 'yyyy-mm-dd',
						closeOnSelect: false,
						closeOnClear: false,
						hiddenName: true
					}),
					checkout_picker = $calendar_input_checkout.pickadate('picker');

				if (checkin_picker.get('value')) {
					checkout_picker.set('min', checkin_picker.get('select'));
				}

				if (checkout_picker.get('value')) {
					checkin_picker.set('max', checkout_picker.get('select'));
				}

				checkin_picker.on({
					set: function(event) {
						if (event.select) {
							var checkin_min = new Date(checkin_picker.get('select').pick);
							checkin_min.setDate(checkin_min.getDate() + 1);

							checkout_picker.set('min', checkin_min);

							if ($sales_filter.hasClass('carousel') && $sales_filter.hasClass('initialized')) {
								$sales_filter.sf_carousel('next');
							}

							updateSalesList();
							updateSalesFilter();
						} else if ('clear' in event) {
							checkout_picker.set('min', tomorrow);
						}
					},
					render: function(event) {
						updateSalesFilter();
					}
				});

				checkout_picker.on({
					set: function(event) {
						if (event.select) {
							var date = new Date(checkout_picker.get('select').pick);
							date.setDate(date.getDate() - 1);

							checkin_picker.set('max', date);

							updateSalesList();
							updateSalesFilter();
						} else if ('clear' in event) {
							checkin_picker.set('max', false);
						}
					},
					render: function(event) {
						updateSalesFilter();
					}
				});

				checkin_picker.open(false);
				checkout_picker.open(false);
			});

			$(window).on('load', function(e) {
				$sales_filter.sf_carousel({
					noWrap: true,
					duration: 300
				});
			});
		})(jQuery);
	</script>
</section>
<?= $this->Form->end(); ?>

<!-- Sales Filter Carousel -->
<script src="/assets/rev/js/<?= $this->Asset->getFileRev('website-components/sales-filter/carousel.js', ROOT . '/webroot/assets/rev/js/rev-manifest.json'); ?>"></script>
