<?php
$links = [
	$this->Url->build(['controller' => 'Pages', 'action' => 'display', 'about-us']) => __('About Us'),
	$this->Url->build(['controller' => 'Contact']) => __('Contact Us'),
	$this->Url->build(['controller' => 'Pages', 'action' => 'display', 'terms']) => __('Terms & Conditions'),
	$this->Url->build(['controller' => 'Pages', 'action' => 'display', 'privacy-policy']) => __('Privacy Policy')
];
?>
<div class="card-panel sidebar-nav">
	<div class="collection with-header">
		<div class="collection-header">
			<h5><?= __('Information'); ?></h5>
		</div>

		<?php foreach ($links as $link_url => $link_title) { ?>
			<a class="collection-item black-text <?= ($this->request->getAttribute('here') == $link_url) ? 'fw700' : ''; ?>" href="<?= $link_url; ?>"><?= $link_title; ?></a>
		<?php } ?>
	</div>
</div>
