<?php
$links = [
	$this->Url->build(['plugin' => false, 'controller' => 'Account', 'action' => 'index']) => __('Your Details'),
	$this->Url->build(['plugin' => false, 'controller' => 'Account', 'action' => 'bookings']) => __('Your Bookings'),
	$this->Url->build(['plugin' => false, 'controller' => 'Pages', 'action' => 'display', 'terms']) => __('Terms & Conditions'),
	$this->Url->build(['plugin' => false, 'controller' => 'Pages', 'action' => 'display', 'privacy-policy']) => __('Privacy Policy'),
	$this->Url->build(['plugin' => 'CakeDC/Users', 'controller' => 'Users', 'action' => 'changePassword']) => __('Change Password')
];
?>
<div class="card-panel sidebar-nav">
	<div class="collection with-header">
		<!-- Account -->
		<div class="collection-header">
			<h5><?= __('Account'); ?></h5>
		</div>

		<?php foreach ($links as $link_url => $link_title) { ?>
		    <?php if($link_title!="Change Password"){?>
			<a class="collection-item black-text <?= ($this->request->getAttribute('here') == $link_url) ? 'fw700' : ''; ?>" href="<?= $link_url; ?>"><?= $link_title; ?></a>
		<?php }} ?>

		<?php if (!empty($vd['user'])) { ?>
			<a class="collection-item red-text" href="/logout"><?= __('Logout'); ?></a>
		<?php } ?>
	</div>
</div>
