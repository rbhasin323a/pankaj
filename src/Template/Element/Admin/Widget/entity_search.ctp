<?= $this->Form->create('AdminEntitySearch', ['id' => 'form_search']); ?>

<span class="append-icon right" onclick="$('#form_search').submit();" style="cursor: pointer"><i class="fa fa-search"></i></span>
<input type="search" class="form-control" placeholder="Search..." name="q" value="<?= !empty($this->request->query['q']) ? $this->request->query['q'] : ''; ?>" />
<?= $this->Form->end(); ?>
