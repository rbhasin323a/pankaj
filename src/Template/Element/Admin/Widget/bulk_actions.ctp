<?= $this->Form->create('AdminBulkActions', ['id'=>'form_bulk_actions']); ?>
	<?php
		$this->Form->templates([
			// Select
			'select'				=> '<select name="{{name}}" {{attrs}} class="form-control select2-single">{{content}}</select>',
			'selectMultple'			=> '<select multiple name="{{name}}" {{attrs}} class="form-control select2-multiple">{{content}}</select>',
		]);
	?>
	<?= $this->Form->input('action', [
		'label'	=> false,
		'data-placeholder'	=> 'Bulk action',
		'options' => [
			'delete'	=> 'Delete Checked Items'
		],
		'empty' => true,
	]);	?>
	<script type="text/javascript">
	(function($) {
		$(document).ready(function() {
			// Init Select2 - Basic Single
			$("#form_bulk_actions .select2-single, #form_bulk_actions select.form-error").each(function(index, el) {
				$(this).addClass('form-control').select2({
					placeholder: 'Choose ' + $('label', $(this).parent()).text(),
					search: false,
				});
			});
		});
	})(jQuery);
	</script>
<?= $this->Form->end(); ?>