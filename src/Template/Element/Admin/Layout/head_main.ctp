<head>
	<meta charset="utf-8">
	<title><?= $this->fetch('title'); ?></title>
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<?= $this->fetch('meta'); ?>

	<!-- Favicon -->
	<link rel="apple-touch-icon" sizes="180x180" href="/assets/dist/img/favicon/apple-touch-icon.png?v=xd5war79q">
	<link rel="icon" type="image/png" sizes="32x32" href="/assets/dist/img/favicon/favicon-32x32.png?v=xd5war79q">
	<link rel="icon" type="image/png" sizes="16x16" href="/assets/dist/img/favicon/favicon-16x16.png?v=xd5war79q">
	<link rel="manifest" href="/assets/dist/img/favicon/site.webmanifest?v=xd5war79q">
	<link rel="shortcut icon" href="/assets/dist/img/favicon/favicon.ico?v=xd5war79q">
	<meta name="apple-mobile-web-app-title" content="Luxury Discounts">
	<meta name="application-name" content="Luxury Discounts">
	<meta name="msapplication-TileColor" content="#030408">
	<meta name="msapplication-TileImage" content="/assets/dist/img/favicon/mstile-144x144.png?v=xd5war79q">
	<meta name="msapplication-config" content="/assets/dist/img/favicon/browserconfig.xml?v=xd5war79q">
	<meta name="theme-color" content="#030408">

	<link href="https://code.jquery.com/ui/1.10.4/themes/ui-lightness/jquery-ui.css" rel="stylesheet">

	<!-- FullCalendar Plugin CSS -->
	<link rel="stylesheet" type="text/css" href="/backend/ui/vendor/admindesign/vendor/plugins/fullcalendar/fullcalendar.min.css">
	<!-- Theme CSS -->
	<link rel="stylesheet" type="text/css" href="/backend/ui/vendor/admindesign/assets/skin/default_skin/css/theme.css">
	<!-- Admin Forms CSS -->
	<link rel="stylesheet" type="text/css" href="/backend/ui/vendor/admindesign/assets/admin-tools/admin-forms/css/admin-forms.min.css">

	<!-- Icomoon CSS(font) -->
	<link rel="stylesheet" type="text/css" href="/backend/ui/vendor/admindesign/assets/fonts/icomoon/icomoon.css">
	<link href='//fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,700,300,600,400&subset=latin,cyrillic-ext,cyrillic' rel='stylesheet' type='text/css'>

	<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
	<!--[if lt IE 9]>
	<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
	<script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
	<![endif]-->

	<!-- Custom backend css -->
	<link type="text/css" rel="stylesheet" href="/backend/backend.css" />

	<?= $this->fetch('css'); ?>

	<!-- jQuery -->
	<script src="/backend/ui/vendor/admindesign/vendor/jquery/jquery-1.11.1.min.js"></script>
	<script src="/backend/ui/vendor/admindesign/vendor/jquery/jquery_ui/jquery-ui.min.js"></script>

	<!-- CKEditor -->
	<script src="/backend/ui/vendor/ckeditor/ckeditor.js"></script>

	<link rel="stylesheet" type="text/css" href="/backend/ui/vendor/admindesign/vendor/plugins/datepicker/css/bootstrap-datetimepicker.css">
	<link rel="stylesheet" type="text/css" href="/backend/ui/vendor/admindesign/vendor/plugins/select2/css/core.css">
	<script src="/backend/ui/vendor/admindesign/vendor/plugins/globalize/globalize.min.js"></script>
	<script src="/backend/ui/vendor/admindesign/vendor/plugins/moment/moment.min.js"></script>
	<script src="/backend/ui/vendor/admindesign/vendor/plugins/select2/select2.min.js"></script>
	<script src="/backend/ui/vendor/admindesign/vendor/plugins/datepicker/js/bootstrap-datetimepicker.min.js"></script>
	<script src="/backend/ui/js/common.js"></script>

	<?= $this->fetch('script'); ?>
</head>
