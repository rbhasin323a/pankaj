<footer id="content-footer" class="affix">
	<div class="row">
		<div class="col-md-6">
			<span class="footer-legal">© 2015 Luxury Discounts</span>
		</div>
		<div class="col-md-6 text-right">
			<div class="mr20">
				<a href="#content_wrapper" class="footer-return-top"><span class="fa fa-arrow-up"></span></a>
			</div>
		</div>
	</div>
</footer>
