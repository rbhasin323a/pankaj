<header class="navbar navbar-shadow bg-dark">
	<div class="navbar-branding">
		<a class="navbar-brand" href="/admin/">
			<b>Luxury</b>Discounts
		</a>
		<span id="toggle_sidemenu_l" class="ad ad-lines"></span>
	</div>
	<ul class="nav navbar-nav navbar-right">
		<li>
			<div class="navbar-btn btn-group">
				<a href="javascript:void(0);" class="topbar-menu-toggle btn btn-sm" data-toggle="button">
					<span class="fa fa-link"></span>
				</a>
			</div>
		</li>
		<?php if (!empty($vd['user']['first_name'])) { ?>
			<li class="dropdown menu-merge">
				<div class="navbar-btn btn-group">
					<button data-toggle="dropdown" class="btn btn-sm dropdown-toggle"><span class="hidden-xs"><?= $vd['user']['first_name']; ?></span> </button>
					<div class="dropdown-menu dropdown-persist w350 animated animated-shorter fadeIn" role="menu">
						<div class="panel mbn">
							<div class="panel-menu">
								<span class="panel-icon"><i class="fa fa-envelope"></i></span>
								<span class="panel-title fw600"> <?= $vd['user']['email']; ?></span>
							</div>
							<div class="panel-menu">
								<span class="panel-icon"><i class="fa fa-sign-out"></i></span>
								<span class="panel-title fw600"> <a href="/logout">Logout</a> </span>
							</div>
						</div>
					</div>
				</div>
			</li>
		<?php } ?>
	</ul>
</header>
