<aside id="sidebar_left" class="nano nano-light sidebar-light sidebar-light light">
	<!-- Start: Sidebar Left Content -->
	<div class="sidebar-left-content nano-content">
		<!-- Start: Sidebar Menu -->
		<ul class="nav sidebar-menu">
			<li class="sidebar-label pt20">Menu</li>
			<?php if (in_array('admin/index', $access)) { ?>
				<li>
					<a href="<?= $this->Url->build(['controller' => 'admin']); ?>">
						<span class="glyphicon glyphicon-home"></span>
						<span class="sidebar-title">Dashboard</span>
					</a>
				</li>
			<?php } ?>
			<?php if (in_array('sales/index', $access)) { ?>
				<li>
					<a href="<?= $this->Url->build(['controller' => 'sales']); ?>">
						<span class="fa fa-file"></span>
						<span class="sidebar-title">Sales</span>
					</a>
				</li>
			<?php } ?>
			<?php if (in_array('saleoffers/index', $access)) { ?>
				<li>
					<a href="<?= $this->Url->build(['controller' => 'saleOffers']); ?>">
						<span class="fa fa-file"></span>
						<span class="sidebar-title">Sale Offers</span>
					</a>
				</li>
			<?php } ?>
			<?php if (in_array('categories/index', $access)) { ?>
				<li>
					<a href="<?= $this->Url->build(['controller' => 'categories']); ?>">
						<span class="fa fa-file"></span>
						<span class="sidebar-title">Categories</span>
					</a>
				</li>
			<?php } ?>
			<?php if (in_array('bookings/index', $access)) { ?>
				<li>
					<a href="<?= $this->Url->build(['controller' => 'bookings']); ?>">
						<span class="fa fa-file"></span>
						<span class="sidebar-title">Bookings</span>
					</a>
				</li>
			<?php } ?>
			<?php if (in_array('companies/index', $access)) { ?>
				<li>
					<a href="<?= $this->Url->build(['controller' => 'companies']); ?>">
						<span class="fa fa-building-o"></span>
						<span class="sidebar-title">Companies</span>
					</a>
				</li>
			<?php } ?>
			<?php if (in_array('contractors/index', $access)) { ?>
				<li>
					<a href="<?= $this->Url->build(['controller' => 'contractors']); ?>">
						<span class="fa fa-briefcase"></span>
						<span class="sidebar-title">Contractor</span>
					</a>
				</li>
			<?php } ?>
			<?php if (in_array('suppliers/index', $access)) { ?>
				<li>
					<a href="<?= $this->Url->build(['controller' => 'suppliers']); ?>">
						<span class="fa fa-bank"></span>
						<span class="sidebar-title">Suppliers</span>
					</a>
				</li>
			<?php } ?>
			<?php if (in_array('contacts/index', $access)) { ?>
				<li>
					<a href="<?= $this->Url->build(['controller' => 'contacts']); ?>">
						<span class="fa fa-phone"></span>
						<span class="sidebar-title">Contacts</span>
					</a>
				</li>
			<?php } ?>
			<?php if (in_array('contacts/index', $access)) { ?>
				<li>
					<a href="<?= $this->Url->build(['controller' => 'users']); ?>">
						<span class="fa fa-users"></span>
						<span class="sidebar-title">Users</span>
					</a>
				</li>
			<?php } ?>
			<?php if (in_array('roles/index', $access)) { ?>
				<li>
					<a href="<?= $this->Url->build(['controller' => 'roles']); ?>">
						<span class="fa fa-users"></span>
						<span class="sidebar-title">User Roles</span>
					</a>
				</li>
			<?php } ?>
			<?php if (in_array('countries/index', $access)) { ?>
				<li>
					<a href="<?= $this->Url->build(['controller' => 'countries']); ?>">
						<span class="fa fa-flag"></span>
						<span class="sidebar-title">Countries</span>
					</a>
				</li>
			<?php } ?>
			<?php if (in_array('languages/index', $access)) { ?>
				<li>
					<a href="<?= $this->Url->build(['controller' => 'languages']); ?>">
						<span class="fa fa-globe"></span>
						<span class="sidebar-title">Languages</span>
					</a>
				</li>
			<?php } ?>
			<?php if (in_array('currencyrates/index', $access)) { ?>
				<li>
					<a href="<?= $this->Url->build(['controller' => 'currency_rates']); ?>">
						<span class="fa fa-eur"></span>
						<span class="sidebar-title">Currency Rates</span>
					</a>
				</li>
			<?php } ?>
			<?php if (in_array('taxes/index', $access)) { ?>
				<li>
					<a href="<?= $this->Url->build(['controller' => 'taxes']); ?>">
						<span class="fa fa-money"></span>
						<span class="sidebar-title">Taxes</span>
					</a>
				</li>
			<?php } ?>
			<?php if (in_array('transactions/index', $access)) { ?>
				<li>
					<a href="<?= $this->Url->build(['controller' => 'transactions']); ?>">
						<span class="fa fa-credit-card"></span>
						<span class="sidebar-title">Transactions</span>
					</a>
				</li>
			<?php } ?>
			<?php if (in_array('departures/index', $access)) { ?>
				<li>
					<a href="<?= $this->Url->build(['controller' => 'departures']); ?>">
						<span class="fa fa-plane"></span>
						<span class="sidebar-title">Departures</span>
					</a>
				</li>
			<?php } ?>
			<?php if (in_array('birthdays/index', $access)) { ?>
				<li>
					<a href="<?= $this->Url->build(['controller' => 'birthdays']); ?>">
						<span class="fa fa-birthday-cake"></span>
						<span class="sidebar-title">Birthdays</span>
					</a>
					<!-- TODO: Notifications -->
				</li>
			<?php } ?>
			<?php if (in_array('seo/index', $access)) { ?>
				<li>
					<a href="<?= $this->Url->build(['controller' => 'seo']); ?>">
						<span class="fa fa-file"></span>
						<span class="sidebar-title">SEO Data</span>
					</a>
				</li>
			<?php } ?>
			<?php if (in_array('footer/index', $access)) { ?>
				<li>
					<a href="<?= $this->Url->build(['controller' => 'footer']); ?>">
						<span class="fa fa-file"></span>
						<span class="sidebar-title">Footer Data</span>
					</a>
				</li>
			<?php } ?>
		</ul>

		<style type="text/css">
			.sidebar-menu .birthday-notifications {
				min-width: 24px;
				min-height: 24px;
				border-radius: 50%;
				font-size: 13px;
				color: #fff;
				line-height: 24px;
				text-align: center;
				background-color: #E9573F;

				<?php if (empty($_COOKIE['showSidebar']) || (!empty($_COOKIE['showSidebar']) && $_COOKIE['showSidebar'] == 'false')) {
					?>display: none;
					<?php
				} else {
					?>display: inline-block;
					<?php
				}

				?>
			}

			.sidebar-menu .birthday-notifications .notifications-count.pad {
				padding: 5px;
				font-weight: 600;
			}
		</style>

		<!-- Start: Sidebar Collapse Button -->
		<div class="sidebar-toggle-mini">
			<a href="javascript:void(0);">
				<span class="fa fa-sign-out"></span>
			</a>
		</div>
		<!-- End: Sidebar Collapse Button -->
	</div>
	<!-- End: Sidebar Left Content -->

	<script type="text/javascript">
		(function($) {
			$(document).ready(function() {
				if ($('#sidebar_left .sidebar-menu a[href^="' + window.location.pathname + '"]').length > 0) {
					$('#sidebar_left .sidebar-menu a[href^="' + window.location.pathname + '"]').parent('li').addClass('active');
				}
			});

		})(jQuery);
	</script>
</aside>
