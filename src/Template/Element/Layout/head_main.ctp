<?php
use Cake\Core\Configure;
?>

<head>
	<meta charset="utf-8">

	<title><?= !empty($seo_title) ? $seo_title : $this->fetch('title'); ?></title>
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<?= $this->fetch('meta'); ?>
	<?php if (empty($sale)) { ?>
		<meta name="title" content="<?= !empty($seo_title) ? $seo_title : $this->fetch('title'); ?>">
	<?php } ?>
	<?php if (!empty($sale)) { ?>
		<?php if (!empty($sale->meta_title)) { ?>
			<meta name="title" content="<?= $sale->meta_title; ?>">
		<?php } ?>

		<?php if (!empty($sale->meta_description)) { ?>
			<meta name="description" content="<?= $sale->meta_description; ?>">
		<?php } ?>
	<?php } ?>

	<meta name="description" content="<?= !empty($seo_description) ? $seo_description : ''; ?>">
	<!-- Favicon -->
	<link rel="apple-touch-icon" sizes="180x180" href="/assets/dist/img/favicon/apple-touch-icon.png?v=qt4sua6qn">
	<link rel="icon" type="image/png" sizes="32x32" href="/assets/dist/img/favicon/favicon-32x32.png?v=qt4sua6qn">
	<link rel="icon" type="image/png" sizes="16x16" href="/assets/dist/img/favicon/favicon-16x16.png?v=qt4sua6qn">
	<link rel="manifest" href="/assets/dist/img/favicon/site.webmanifest?v=qt4sua6qn">
	<link rel="shortcut icon" href="/assets/dist/img/favicon/favicon.ico?v=qt4sua6qn">
	<meta name="apple-mobile-web-app-title" content="Luxury Discounts">
	<meta name="application-name" content="Luxury Discounts">
	<meta name="msapplication-TileColor" content="#030408">
	<meta name="msapplication-TileImage" content="/assets/dist/img/favicon/mstile-144x144.png?v=qt4sua6qn">
	<meta name="msapplication-config" content="/assets/dist/img/favicon/browserconfig.xml?v=qt4sua6qn">
	<meta name="theme-color" content="#030408">

	<link href="https://fonts.googleapis.com/css?family=Roboto+Slab:400,700&amp;subset=cyrillic,cyrillic-ext,greek,greek-ext,latin-ext,vietnamese" rel="stylesheet">
	<link type="text/css" rel="stylesheet" href="/ui/js/jquery/jquery-ui.min.css" />

	<link type="text/css" rel="stylesheet" href="/assets/rev/css/<?= $this->Asset->getFileRev('bundle.min.css', ROOT . '/webroot/assets/rev/css/rev-manifest.json'); ?>" />

	<?= $this->fetch('css'); ?>

	<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
	<!--[if lt IE 9]>
	<script src="https://cdn.jsdelivr.net/g/html5shiv@3.7.3,respond@1.4.2"></script>
	<![endif]-->

	<!-- jQuery -->
	<script type="text/javascript" src="/assets/rev/js/<?= $this->Asset->getFileRev('jquery/jquery.min.js', ROOT . '/webroot/assets/rev/js/rev-manifest.json'); ?>"></script>

	<script type="text/javascript" src="/ui/js/jquery/jquery-ui.min.js"></script>
	<!-- Facebook Pixel Code -->

	<script>
		! function(f, b, e, v, n, t, s) {
			if (f.fbq) return;
			n = f.fbq = function() {
				n.callMethod ?
					n.callMethod.apply(n, arguments) : n.queue.push(arguments)
			};
			if (!f._fbq) f._fbq = n;
			n.push = n;
			n.loaded = !0;
			n.version = '2.0';
			n.queue = [];
			t = b.createElement(e);
			t.async = !0;
			t.src = v;
			s = b.getElementsByTagName(e)[0];
			s.parentNode.insertBefore(t, s)
		}(window, document, 'script', 'https://connect.facebook.net/en_US/fbevents.js');

		fbq('init', '2058849404231452');
		fbq('track', 'PageView');
	</script>
	<noscript>
		<img height="1" width="1" src="https://www.facebook.com/tr?id=2058849404231452&ev=PageView&noscript=1" />
	</noscript>
	<!-- End Facebook Pixel Code -->
	<?= $this->fetch('script'); ?>
</head>
