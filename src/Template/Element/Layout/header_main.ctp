<header id="header-main">
	<nav role="navigation">
		<div class="nav-wrapper container">
			<a href="/" id="logo-container" class="brand-logo">
				<div class="logo-header">
					<img src="/assets/dist/img/layout/header/logo_header_sun.png" alt="Luxury Discounts" data-rjs="/assets/dist/img/layout/header/logo_header_sun@2x.png" />
				</div>
				<div class="logo-header-text">
					<img src="/assets/dist/img/layout/header/logo_header_text.svg" alt="Luxury Discounts" height="40" />
				</div>
			</a>

			<ul class="main-nav right">
				<li><a href="/pages/about-us"><?= __('About Us'); ?></a></li>
				<li><a href="/contact"><?= __('Contact'); ?></a></li>
				<?php if (!empty($vd['user'])) { ?>
					<li><a href="/account"><?= __('Account'); ?></a></li>
				<?php } else { ?>
					<li><a href="/login"><?= __('Login'); ?></a></li>
				<?php } ?>
				<li>
					<?php if (!empty($vd['languages'])) { ?>
						<?= $this->Form->create('Language', ['url' => '/language', 'id' => 'language-form']); ?>
						<div class="input-field white-text">
							<select class="icons" id="language-select" name="lang">
								<?php foreach ($vd['languages'] as $language) { ?>
									<option <?= (!empty($vd['lang']) && $vd['lang'] == $language->code) ? 'selected' : ''; ?> value="<?= $language->code; ?>" data-icon="/assets/dist/img/component/country-flags/<?= $language->code; ?>.svg"><?= mb_strtoupper($language->name); ?></option>
								<?php } ?>
							</select>
							<script type="text/javascript">
								(function($) {
									$('#language-select').on('change', function() {
										lang = $(this).val();
										if (lang == 'gb') lang = 'en';
										$('[name="redirect"]').val(lang);
										$('#language-form').submit();
									});
								})(jQuery);
							</script>
						</div>
						<input type="hidden" name="referrer" value="<?= $this->request->getAttribute('here'); ?>" />
						<input type="hidden" name="redirect" value="<?= !empty($this->request->getAttribute('here')) ? $this->request->getAttribute('here') : '/' ?>" />
						<?= $this->Form->end(); ?>
					<?php } ?>
				</li>
				<li>
					<?= $this->Form->create('Currency', ['url' => '/currency', 'id' => 'currency-form']); ?>
					<div class="input-field white-text">
						<select class="icons" id="currency-select" name="currency">
							<option value="BGN" <?= (!empty($custom_currency) && $custom_currency == 'BGN') ? 'selected="selected"' : '' ?> class="left">BGN</option>
							<option value="EUR" <?= (!empty($custom_currency) && $custom_currency == 'EUR') ? 'selected="selected"' : '' ?> class="left">EUR</option>
							<option value="TRY" <?= (!empty($custom_currency) && $custom_currency == 'TRY') ? 'selected="selected"' : '' ?> class="left">TRY</option>
						</select>
						<script type="text/javascript">
							(function($) {
								$('#currency-select').on('change', function() {
									$('#currency-form').submit();
								});
							})(jQuery);
						</script>
					</div>
					<input type="hidden" name="redirect" value="<?= !empty($this->request->getAttribute('here')) ? $this->request->getAttribute('here') : '/'; ?>" />
					<?= $this->Form->end(); ?>
				</li>
			</ul>

			<a href="#" data-activates="mobile-nav" id="mobile-nav-button" class="button-collapse top-nav full hide-on-large-only right"><i class="material-icons">menu</i></a>
		</div>
	</nav>
	<ul class="side-nav" id="mobile-nav">
		<li><a href="javascript:void(0);" class="subheader"><?= __('Navigation'); ?></a></li>

		<li><a href="/latest-sales"><?= __('Latest Sales'); ?></a></li>
		<li><a href="/pages/about-us"><?= __('About Us'); ?></a></li>
		<li><a href="/contact"><?= __('Contact'); ?></a></li>
		<?php if (!empty($vd['user'])) { ?>
			<li><a href="/account"><?= __('Account'); ?></a></li>
		<?php } else { ?>
			<li><a href="/register"><?= __('Register'); ?></a></li>
			<li><a href="/login"><?= __('Login'); ?></a></li>
		<?php } ?>

		<li><a href="javascript:void(0);" class="subheader"><?= __('Language'); ?></a></li>

		<?php if (!empty($vd['languages'])) { ?>
			<?php foreach ($vd['languages'] as $language) { ?>
				<?php if (!empty($vd['lang']) && $vd['lang'] != $language->code) { ?>
					<li>
						<a href="/<?= ($language->code == 'gb') ? 'en' : $language->code ?>"><?= $language->name; ?></a>
					</li>
				<?php } ?>
			<?php } ?>
		<?php } ?>

		<li><a href="javascript:void(0);" class="subheader"><?= __('Currency'); ?></a></li>

		<li>
			<?= $this->Form->postLink(__('BGN'), ['prefix' => false, 'plugin' => null, 'controller' => 'Currency'], [
				'data' => ['currency' => 'BGN'],
				'class' => (!empty($custom_currency) && $custom_currency === 'BGN') ? 'blue darken-2 white-text' : ''
			]); ?>
		</li>

		<li>
			<?= $this->Form->postLink(__('EUR'), ['prefix' => false, 'plugin' => null, 'controller' => 'Currency'], [
				'data' => ['currency' => 'EUR'],
				'class' => (!empty($custom_currency) && $custom_currency === 'EUR') ? 'blue darken-2 white-text' : ''
			]); ?>
		</li>

		<li>
			<?= $this->Form->postLink(__('TRY'), ['prefix' => false, 'plugin' => null, 'controller' => 'Currency'], [
				'data' => ['currency' => 'TRY'],
				'class' => (!empty($custom_currency) && $custom_currency === 'TRY') ? 'blue darken-2 white-text' : ''
			]); ?>
		</li>
	</ul>
	<script type="text/javascript">
		(function($) {
			$(document).ready(function() {
				$("#mobile-nav-button").sideNav({
					edge: 'right'
				});
			});
		})(jQuery);
	</script>
</header>
