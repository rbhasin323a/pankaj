<footer id="footer-main" class="page-footer">
	<?= !empty($footer_menu) ? $footer_menu : ''; ?>
	<div class="footer-copyright">
		<div class="container">
			<div class="row">
				<div class="col s12 m4">
					<p><?= !empty($footer_copyright_text) ? $footer_copyright_text : ''; ?></p>
				</div>
				<div class="col s12 m8">
					<br class="hide-on-med-and-up" />
					<img src="/ui/img/footer_payment.png" data-rjs="/ui/img/footer_payment@2x.png" class="payment" />
				</div>
			</div>
		</div>
	</div>
</footer>

<?php if (empty($vd['user'])) { ?>
	<?= $this->cell('AccountCreatePopup'); ?>
<?php } ?>

<!--Google Analytics-->
<script type="text/javascript">
	(function(i, s, o, g, r, a, m) {
		i['GoogleAnalyticsObject'] = r;
		i[r] = i[r] || function() {
			(i[r].q = i[r].q || []).push(arguments)
		}, i[r].l = 1 * new Date();
		a = s.createElement(o),
			m = s.getElementsByTagName(o)[0];
		a.async = 1;
		a.src = g;
		m.parentNode.insertBefore(a, m)
	})(window, document, 'script', 'https://www.google-analytics.com/analytics.js', 'ga');

	ga('create', 'UA-82227865-1', 'auto');
	ga('send', 'pageview');
</script>

<!--Start of Zendesk Chat Script-->
<script type="text/javascript">
	window.$zopim || (function(d, s) {
		var z = $zopim = function(c) {
				z._.push(c)
			},
			$ = z.s =
			d.createElement(s),
			e = d.getElementsByTagName(s)[0];
		z.set = function(o) {
			z.set.
			_.push(o)
		};
		z._ = [];
		z.set._ = [];
		$.async = !0;
		$.setAttribute("charset", "utf-8");
		$.src = "https://v2.zopim.com/?4jMmemmB0BpqGu6aVv57BqsV86aUxrne";
		z.t = +new Date;
		$.
		type = "text/javascript";
		e.parentNode.insertBefore($, e)
	})(document, "script");
</script>
<!--End of Zendesk Chat Script-->

<!-- RetinaJS -->
<script type="text/javascript" src="/assets/rev/js/<?= $this->Asset->getFileRev('retina/retina.min.js', ROOT . '/webroot/assets/rev/js/rev-manifest.json'); ?>"></script>
<script type="text/javascript">
	$(window).on('load', function() {
		retinajs($('img[data-rjs]'));
	});
</script>

<div id="fb-root"></div>
<script>
	(function(d, s, id) {
		var js, fjs = d.getElementsByTagName(s)[0];
		if (d.getElementById(id)) return;
		js = d.createElement(s);
		js.id = id;
		js.src = 'https://connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.11&appId=1704694003129656';
		fjs.parentNode.insertBefore(js, fjs);
	}(document, 'script', 'facebook-jssdk'));
</script>

<!-- Materialize -->
<script type="text/javascript" src="/assets/rev/js/<?= $this->Asset->getFileRev('materialize/js/bin/materialize.min.js', ROOT . '/webroot/assets/rev/js/rev-manifest.json'); ?>"></script>

<!-- Pickadate Locale -->
<?php if (!empty($vd['lang'])) { ?>
	<?php if ($vd['lang'] == 'bg') { ?>
		<script src="/ui/js/pickadate/lib/translations/bg_BG.js"></script>
	<?php } else if ($vd['lang'] == 'tr') { ?>
		<script src="/ui/js/pickadate/lib/translations/tr_TR.js"></script>
	<?php } ?>
<?php } ?>

<!-- Lazyload -->
<script src="https://cdn.jsdelivr.net/npm/vanilla-lazyload@12.0.0/dist/lazyload.min.js"></script>
<script type="text/javascript">
	lazyLoadInstance = new LazyLoad({
		elements_selector: ".l-l"
	});
</script>

<!-- Website.js -->
<script src="/assets/rev/js/<?= $this->Asset->getFileRev('bundle.min.js', ROOT . '/webroot/assets/rev/js/rev-manifest.json'); ?>"></script>

<!-- CookieConsent -->
<link rel="stylesheet" type="text/css" href="//cdnjs.cloudflare.com/ajax/libs/cookieconsent2/3.1.0/cookieconsent.min.css" />
<script src="//cdnjs.cloudflare.com/ajax/libs/cookieconsent2/3.1.0/cookieconsent.min.js"></script>
<script type="text/javascript">
	window.addEventListener("load", function() {
		window.cookieconsent.initialise({
			palette: {
				popup: {
					background: "#237afc"
				},
				button: {
					background: "#fff",
					text: "#237afc"
				}
			},
			theme: "edgeless",
			position: "top",
			static: true,
			content: {
				message: '<?= __d('CookieConsent', 'We are using cookies to secure and improve your stay on our website.'); ?>',
				dismiss: '<?= __d('CookieConsent', 'Thank You'); ?>',
				link: '<?= __d('CookieConsent', 'Learn More'); ?>',
				href: "https://www.luxury-discounts.com/pages/privacy-policy"
			}
		})
	});
</script>
<style>
	.cc-window.cc-banner {
		align-items: flex-end;
	}

	.cc-banner.cc-theme-edgeless .cc-message {
		text-align: center;
		margin-left: 10%;
		align-self: center;
	}

	@media screen and (max-width: 736px) {
		.cc-banner.cc-theme-edgeless .cc-message {
			margin-left: 1em;
			margin-right: 1em;
		}
	}
</style>
