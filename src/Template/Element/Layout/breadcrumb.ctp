<?php
	if (!empty($this->request->params['controller'])) {
		$controller = $this->request->params['controller'];
		$this->Html->addCrumb(ucfirst($controller), $this->Url->build(['controller' => $controller, 'action' => 'index']));

		if (!empty($this->request->params['action'])) {
			$action = $this->request->params['action'];
			$this->Html->addCrumb(ucfirst($action), $this->Url->build(['controller' => $controller, 'action' => $action]));
		}
	}
?>
<section>
	<div class="row">
		<div class="col-xs-12">
			<?= $this->Html->getCrumbList([
					'firstClass' => false,
					'lastClass' => 'crumb-active',
					'class' => 'crumb-trail'
				], 'Home');
			?>
		</div>
	</div>
</section>
