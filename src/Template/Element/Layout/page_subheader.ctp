<?php
	$breadcrumbs = $this->HtmlBetter->getCrumbs();
?>
<?php if (!empty($breadcrumbs)) { ?>
<div class="page-subheader"><nav style="overflow: hidden;">
	<div class="nav-wrapper">
		<div class="container">
			<div class="col s12">
				<?php foreach ($breadcrumbs as $crumb) { ?>
				<a class="breadcrumb" href="<?= $crumb[1]; ?>"><?= $crumb[0]; ?></a>
				<?php } ?>
			</div>
		</div>
	</div>
</nav></div>
<?php } ?>
