<?php
	use Cake\Core\Configure;

	$this->loadHelper('Form', [
		'templates' => 'app_form',
	]);

	$this->HtmlBetter->addCrumb(__('Home'), '/');
	$this->HtmlBetter->addCrumb(__('Reset Password'), '/forgotten');
?>
<section id="UsersPasswordReset">
	<div class="container mv50 pv50">
		<div class="row mv50 pv50">
			<div class="col s12 m8 offset-m2 l6 offset-l3">
				<h4 class="page-heading"><?= __('Reset Password'); ?></h4>
				<div class="card-panel p30">
					<?= $this->Form->create('User'); ?>
					<div class="row mbn">
						<div class="col s12">
							<?= $this->Form->input('reference', ['class' => 'validate', 'required', 'label' => __('Email')]); ?>
						</div>
						<div class="col s12">
							<?= $this->Form->button(__d('Users', 'Submit') . '<i class="material-icons right">send</i>', ['class' => 'waves-effect waves-light btn-large amber darken-3 right']); ?>
						</div>
					</div>
					<?= $this->Form->end(); ?>
				</div>
			</div>
			<?php if (empty($vd['user'])) { ?>
			<div class="col s12">
				<div class="section text-center">
					<p><?= __('If you don\'t have an account you can <a href="/register" class="fw800">register here.</a>'); ?></p>
					<p><?= __('If you already have an account you can <a href="/login" class="fw800">login here.</a>'); ?></p>
				</div>
			</div>
			<?php } ?>
		</div>
	</div>
</section>
