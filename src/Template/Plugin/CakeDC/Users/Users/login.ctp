<?php
use Cake\Core\Configure;

$this->loadHelper('Form', [
	'templates' => 'app_form',
]);

$this->HtmlBetter->addCrumb(__('Home'), '/');
$this->HtmlBetter->addCrumb(__('Login'), '/login');
?>
<section id="UsersLogin">
	<div class="container mv50 pv50">
		<div class="row mv50 pv50">
			<div class="col s12 m8 offset-m2 l6 offset-l3">
				<h4 class="page-heading"><?= __('Login'); ?></h4>
				<div class="card-panel p30">
					<?= $this->Form->create(); ?>
					<?= $this->Form->unlockField('remember_me'); ?>
					<div class="row mbn">
						<div class="col s12">
							<?= $this->Form->input('email', ['class' => 'validate', 'required']); ?>
						</div>
						<div class="col s12">
							<?= $this->Form->input('password', ['class' => 'validate', 'required']); ?>
						</div>
						<?php if (Configure::read('Users.RememberMe.active')) { ?>
							<div class="col s12 m6 pv15">
								<p>
									<input type="hidden" name="remember_me" value="0" />
									<input type="checkbox" name="remember_me" value="1" id="remember-me" checked="checked">
									<label for="remember-me"><?= __d('CakeDC/Users', 'Remember me'); ?></label>
								</p>
							</div>
						<?php } ?>
						<div class="col s12 m6">
							<?= $this->Form->button(__('Login') . '<i class="material-icons right">lock</i>', ['class' => 'btn-large waves-effect waves-light amber darken-3 right']); ?>
						</div>
						<div class="col s12 mt20">
							<a href="/forgotten"><?= __('Forgot your password?'); ?></a>
						</div>
					</div>
					<?= $this->Form->end(); ?>
				</div>
			</div>
			<div class="col s12">
				<div class="section text-center">
					<?php if (empty($vd['user'])) { ?>
						<p><?= __d('Users', 'If you don\'t have an account you can <a href="/register" class="fw800">register here.</a>'); ?></p>
					<?php } ?>
					<p><?= __('If you haven\'t received your activation email <a href="/resend-validation" class="fw800">click here.</a>'); ?></p>
				</div>
			</div>
		</div>
	</div>
</section>
<script type="text/javascript">
	setTimeout(function() {
		if (document.querySelectorAll('input#email:-webkit-autofill').length > 0) {
			$("label[for='email']").addClass("active");
		}

		if (document.querySelectorAll('input#password:-webkit-autofill').length > 0) {
			$("label[for='password']").addClass("active");
		}
	}, 500);
</script>
