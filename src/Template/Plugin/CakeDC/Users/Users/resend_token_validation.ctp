<?php
	use Cake\Core\Configure;

	$this->loadHelper('Form', [
		'templates' => 'app_form',
	]);

	$this->HtmlBetter->addCrumb(__('Home'), '/');
	$this->HtmlBetter->addCrumb(__('Resend Validation'), '/resend-validation');
?>
<section id="UsersResendValidation">
	<div class="container mv50 pv50">
		<div class="row mv50 pv50">
			<div class="col s12 m8 offset-m2 l6 offset-l3">
				<h4 class="page-heading"><?= __('Resend Validation'); ?></h4>
				<div class="card-panel">
					<?= $this->Form->create($user); ?>
					<div class="row mbn">
						<div class="col s12">
							<?= $this->Form->input('reference', ['class' => 'validate', 'required', 'label' => __('Email')]); ?>
						</div>
						<div class="col s12">
							<?= $this->Form->button(__d('Users', 'Submit') . '<i class="material-icons right">lock</i>', ['class' => 'waves-effect waves-light btn-large amber darken-3 right']); ?>
						</div>
					</div>
					<?= $this->Form->end(); ?>
				</div>
			</div>
			<div class="col s12">
				<div class="section text-center">
					<?php if (empty($vd['user'])) { ?>
					<p><?= __('If you don\'t have an account you can <a href="/register" class="fw800">register here.</a>'); ?></p>
					<?php } ?>
					<p><?= __('If you have forgotten your password <a href="/forgotten" class="fw800">click here.</a>'); ?></p>
				</div>
			</div>
		</div>
	</div>
</section>
