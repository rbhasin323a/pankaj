<div class="users form">
	<?= $this->Flash->render(); ?>
	<?= $this->Form->create('User'); ?>
	<fieldset>
		<legend><?= __d('Users', 'Please enter your email'); ?></legend>
		<?= $this->Form->input('email'); ?>
	</fieldset>
	<?= $this->Form->button(__d('Users', 'Submit')); ?>
	<?= $this->Form->end(); ?>
</div>
