<?php
use Cake\Core\Configure;

$this->loadHelper('Form', [
	'templates' => 'app_form',
]);

$this->HtmlBetter->addCrumb(__('Home'), '/');
$this->HtmlBetter->addCrumb(__('Create Account'), '/register');
?>
<section id="UsersRegister">
	<div class="container mv50 pv50">
		<div class="row mv50 pv50">
			<div class="col s12 m8 offset-m2 l6 offset-l3">
				<h4 class="page-heading"><?= __('Create Account'); ?></h4>
				<div class="card-panel p30">
					<?= $this->Form->create($user, ['id' => 'UsersRegisterForm']); ?>
					<?= $this->Form->unlockField('tos'); ?>
					<?php $this->Form->templates([
						'dateWidget' => '{{year}}-{{month}}-{{day}}'
					]); ?>
					<div class="row mbn">
						<div class="col s12">
							<?= $this->Form->input('first_name', ['class' => 'validate']); ?>
						</div>
						<div class="col s12">
							<?= $this->Form->input('last_name', ['class' => 'validate']); ?>
						</div>
						<div class="col s12">
							<?= $this->Form->input('email', ['class' => 'validate']); ?>
						</div>
						<div class="col s12">
							<?= $this->Form->input(
								'birthday',
								[
									'label' => __('Date of Birth'),
									'type' => 'text',
									'class' => 'datepicker',
									'required' => 'required'
								]
							); ?>
						</div>
						<div class="col s12">
							<?= $this->Form->input('phone_mobile', ['class' => 'validate']); ?>
						</div>
						<div class="col s12">
							<?= $this->Form->input('password', ['class' => 'validate']); ?>
						</div>
						<div class="col s12">
							<?= $this->Form->input('password_confirm', ['class' => 'validate', 'type' => 'password']); ?>
						</div>
						<div class="col s12 pt15">
							<p>
								<input type="hidden" name="tos" value="0" />
								<input type="checkbox" name="tos" value="1" required="required" id="tos">
								<label for="tos"><?= sprintf(__('I agree to the <a href="%s" target="_blank">Terms & Conditions</a>'), '/pages/terms'); ?></label>
							</p>
						</div>
						<div class="col s12 pt25 center-align">
							<?= $this->Form->button(__('Create Account') . '<i class="material-icons right">lock</i>', ['class' => 'btn-large waves-effect waves-light amber darken-3']); ?>
						</div>
					</div>
					<?= $this->Form->end(); ?>
				</div>
			</div>
			<div class="col s12">
				<div class="section text-center">
					<?php if (empty($vd['user'])) { ?>
						<p><?= __('If you already have an account you can <a href="/login" class="fw800">login here.</a>'); ?></p>
					<?php } ?>
					<p><?= __('If you haven\'t received your activation email <a href="/resend-validation" class="fw800">click here.</a>'); ?></p>
				</div>
			</div>
		</div>
	</div>
</section>

<script type="text/javascript">
	(function($) {
		$(document).ready(function() {
			$('.datepicker').pickadate({
				selectMonths: true,
				selectYears: 200,
				format: 'yyyy-mm-dd'
			});

			$(document).on('submit', '#UsersRegisterForm', function(e) {
				var birthday = $('#birthday').prop('value');
				var error = false;

				if (birthday) {
					if (!birthday.match(/\d{4}-\d{2}-\d{2}/)) {
						e.preventDefault();
						error = true;
					}
				} else {
					e.preventDefault();
					error = true;
				}

				if (error) {
					var $div = '<div class="error-message fw600 red-text mb20 fs12"><?= __('Date of birth is required') ?></div>';
					var $container = $('#birthday').parent();

					$('.error-message', $container).remove();
					$container.append($div);
				}
			});
		});
	})(jQuery);
</script>
