<?php
	use Cake\Core\Configure;

	$this->assign('title', __('Change Password'));

	$this->loadHelper('Form', [
		'templates' => 'app_form',
	]);

	$this->HtmlBetter->addCrumb(__('Home'), '/');
	$this->HtmlBetter->addCrumb(__('Change Password'), '/change-password');
?>
<section id="UsersChangePassword">
	<div class="container mv50 pv50">
		<div class="row mv50 pv50">
			<div class="col s12 m8 offset-m2 l6 offset-l3">
				<h4 class="page-heading"><?= __('Change Password'); ?></h4>
				<div class="card-panel">
					<?= $this->Form->create($user); ?>
					<div class="row mbn">
						<?php if ($validatePassword) { ?>
						<div class="col s12">
							<?= $this->Form->input('current_password', ['class' => 'validate', 'required', 'type' => 'password', 'label' => __d('Users', 'Current password')]); ?>
						</div>
						<?php } ?>
						<div class="col s12">
							<?= $this->Form->input('password', ['class' => 'validate', 'required']); ?>
						</div>
						<div class="col s12">
							<?= $this->Form->input('password_confirm', ['class' => 'validate', 'required', 'type' => 'password']); ?>
						</div>
						<div class="col s12">
							<?= $this->Form->button(__d('Users', 'Submit') . '<i class="material-icons right">lock</i>', ['class' => 'waves-effect waves-light btn-large amber darken-3 right']); ?>
						</div>
					</div>
					<?= $this->Form->end(); ?>
				</div>
			</div>
		</div>
	</div>
</section>
