<p>Congratulations! Your adventure is waiting for you!</p>

<p>Dear <?= $booking->first_name . ' ' . $booking->last_name ?>, #<?= $booking->id ?></p>

<p>Thank you for booking with Luxury Discounts. We gladly confirm that your booking with <?= $sale->title ?> has been successful and we are delighted that you've chosen to book your stay with us. Please note that this booking is a direct contract between you and (#name of the hotel/Tour operator) for the provision of your hotel accommodation. Please see below the full details of your booking and also make sure to bring a copy of this email with you when you arrive.</p>

<p>
	Next steps:<br>
	<ul>
		<li>Your booking at <?= $sale->title ?> has been already confirmed and the hotel will be expecting you.</li>
		<li>You will be asked for your name and booking reference #<?= $booking->id ?> upon arrival.</li>
		<li>You may be asked to provide a credit card to cover additional costs during your stay, like the minibar or room service.</li>
	</ul>
	<p>

		<p><strong>BOOKING SUMMARY</strong></p>

		<p><strong><u>Customer details:</u></strong></p>

		<table style="width:100%; text-align: center;" border="1">
			<tr>
				<th>Booking ID</th>
				<th>Name</th>
				<th>Email</th>
				<th>Phone</th>
			</tr>
			<tr>
				<td><?= $booking->id ?></td>
				<td><?= $booking->first_name . ' ' . $booking->last_name ?></td>
				<td><?= $booking->my_user->email ?></td>
				<td><?= $booking->phone ?></td>
			</tr>
		</table>

		<br><br>

		<table style="width:100%; text-align: center;" border="1">
			<tr>
				<td colspan="3" rowspan="3"><img class="" src="/files/images/sales/<?= $sale->main_image; ?>"></td>
				<td colspan="2" rowspan="2">Check-in<br /><br /><?= $booking->check_in ?></td>
				<td colspan="2" rowspan="2">Check-out<br /><br /><?= $booking->check_out ?></td>
			</tr>
			<tr>
			</tr>
			<tr>
				<td><?= $nights; ?><br /> NIGHTS</td>
				<td><?= $booking->rooms ?><br /> ROOMS</td>
				<td><?= $booking->adults ?><br /> ADULTS</td>
				<td>
					<?= $booking->children ?> Children<br />
					<?= $booking->infants ?> Infants
				</td>
			</tr>
			<tr>
				<td colspan="3" rowspan="2">
					<strong><?= $sale->title; ?></strong><br /><br />
					Address: <?= $sale->location_title; ?><br />
					Tel.: <?= $sale->company->support_phone ?><br />
					Email: <?= $sale->company->support_email ?><br />
				</td>
				<td colspan="2" rowspan="2"><?= $sale_offer->main_description ?></td>
				<td colspan="2" rowspan="2">
					TOTAL PRICE:<br />
					<?= $total_price ?>
				</td>
			</tr>
			<tr>
			</tr>
		</table>

		<br /><br />

		<p><strong><u>Cancellation policy:</u></strong></p>

		<p>
			7.1. If a user has paid a reservation for hotel accommodation and/or extra tourist services made via the website, and the provider of the travel offer refuses to execute the contract, then the latter should refund to the user the paid amounts in full.<br />
			7.2. If the tour operator providing the respective organized travel significantly changes some of the main clauses of the contract or refuses to execute the contract, the user may cancel the contract without owing any penalty or compensation. In these cases the relations between the user and the tour operator are settled between them and Luxury Discounts LTD. may only provide assistance for such settlement.<br />
			7.3.1. In case of cancellation by the user of booking for hotel accommodation and other tourist services made via the website, the paid amounts are not subject to return by Luxury Discounts LTD. or the Provider of the purchased travel offer, unless the right for cancellation is not explicitly stated in the terms and conditions of the particular offer.<br />
			7.3.2. If the user is granted a possibility to cancel a reservation, pursuant to Article 7.3.1 the provider of the purchased travel offer may charge penalties according to the terms and conditions of the offer.<br />
			7.4.1. In case of a cancellation by the user of booking for organized travel made via the website, the paid amounts are not subject to return and in case they are, the refund is made according to the rules and conditions of the tour operator providing the respective organized travel.<br />
			7.4.2. In such cases as those under the foregoing paragraph, the user should contact and negotiate directly with the tour operator providing the respective organized travel.
		</p>

		<p>Should you need any further information or details please get in contact directly with the hotel or contact us at: customersupport@luxury-discounts.com.
			For all kind of queries please always quote your booking reference number – <?= $booking->id ?>.</p>

		<p><u>ADVENTURE IS WORTHWHILE</u></p>

		Best wishes,<br />
		<img src="/ui/img/logo_header.png" />
