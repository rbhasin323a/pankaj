<!DOCTYPE html>
<html lang="en">

<?= $this->element('Admin/Layout/head_main'); ?>

<?php if (empty($_COOKIE['showSidebar']) || (!empty($_COOKIE['showSidebar']) && $_COOKIE['showSidebar'] == 'false')) { ?>

	<body class="dashboard-page sb-l-m sb-l-disable-animation">
	<?php } else { ?>

		<body class="dashboard-page">
		<?php } ?>
		<!-- Start: Main -->
		<div id="main">
			<!-- Start: Header -->
			<?= $this->element('Admin/Layout/header_main'); ?>
			<!-- End: Header -->

			<!-- Start: Sidebar -->
			<?= $this->element('Admin/Sidebar/main_left'); ?>
			<!-- End: Sidebar Left -->

			<!-- Start: Content-Wrapper -->
			<section id="content_wrapper">

				<!-- Start: Topbar-Dropdown -->
				<div id="topbar-dropmenu" class="alt">
					<h4 class="mtn">Quick Links</h4>
					<div class="topbar-menu row">
						<div class="col-xs-4 col-sm-2">
							<a href="/admin/users/" class="metro-tile bg-info light">
								<span class="fa fa-user text-muted"></span>
								<span class="metro-title">Users</span>
							</a>
						</div>
						<div class="col-xs-4 col-sm-2">
							<a href="/admin/sales/" class="metro-tile bg-success light">
								<span class="fa fa-file text-muted"></span>
								<span class="metro-title">Sales</span>
							</a>
						</div>
						<div class="col-xs-4 col-sm-2">
							<a href="/admin/bookings/" class="metro-tile bg-primary light">
								<span class="fa fa-file text-muted"></span>
								<span class="metro-title">Bookings</span>
							</a>
						</div>
						<div class="col-xs-4 col-sm-2">
							<a href="/admin/booking_summaries/" class="metro-tile bg-primary light">
								<span class="fa fa-file text-muted"></span>
								<span class="metro-title">Booking Summaries</span>
							</a>
						</div>
					</div>
				</div>
				<!-- End: Topbar-Dropdown -->

				<section class="flash"><?= $this->Flash->render(); ?></section>
				<!-- Begin: Content -->
				<?= $this->fetch('content'); ?>
				<!-- End: Content -->
			</section>
			<!-- End: Content-Wrapper -->
		</div>
		<!-- End: Main -->

		<!-- BEGIN: PAGE SCRIPTS -->

		<!-- HighCharts Plugin -->
		<script src="/backend/ui/vendor/admindesign/vendor/plugins/highcharts/highcharts.js"></script>

		<!-- FullCalendar Plugin + moment Dependency -->
		<script src="/backend/ui/vendor/admindesign/vendor/plugins/fullcalendar/lib/moment.min.js"></script>
		<script src="/backend/ui/vendor/admindesign/vendor/plugins/fullcalendar/fullcalendar.min.js"></script>

		<!-- Theme Javascript -->
		<script src="/backend/ui/vendor/admindesign/assets/js/utility/utility.js"></script>
		<script src="/backend/ui/vendor/admindesign/assets/js/demo/demo.js"></script>
		<script src="/backend/ui/vendor/admindesign/assets/js/main.js"></script>

		<!-- Widget Javascript -->
		<script src="/backend/ui/vendor/admindesign/assets/js/demo/widgets.js"></script>
		<script type="text/javascript">
			jQuery(document).ready(function($) {

				"use strict";

				// Init Demo JS
				Demo.init();


				// Init Theme Core
				Core.init();


				// Init Widget Demo JS
				// demoHighCharts.init();

				// Because we are using Admin Panels we use the OnFinish
				// callback to activate the demoWidgets. It's smoother if
				// we let the panels be moved and organized before
				// filling them with content from various plugins

				// Init plugins used on this page
				// HighCharts, JvectorMap, Admin Panels

				// Init Admin Panels on widgets inside the ".admin-panels" container
				$('.admin-panels').adminpanel({
					grid: '.admin-grid',
					draggable: true,
					preserveGrid: true,
					// mobile: true,
					onStart: function() {
						// Do something before AdminPanels runs
					},
					onFinish: function() {
						$('.admin-panels').addClass('animated fadeIn').removeClass('fade-onload');

						// Init the rest of the plugins now that the panels
						// have had a chance to be moved and organized.
						// It's less taxing to organize empty panels
						demoHighCharts.init();
					},
					onSave: function() {
						$(window).trigger('resize');
					}
				});


				// Init plugins for ".calendar-widget"
				// plugins: FullCalendar
				//
				$('#calendar-widget').fullCalendar({
					// contentHeight: 397,
					editable: true,
					events: [{
						title: 'Sardinia beach holiday',
						start: '2015-05-1',
						end: '2015-05-3',
						className: 'fc-event-success',
					}, {
						title: 'Conference',
						start: '2015-05-11',
						end: '2015-05-13',
						className: 'fc-event-warning'
					}, {
						title: 'Lunch Testing',
						start: '2015-05-21',
						end: '2015-05-23',
						className: 'fc-event-primary'
					}, ],
					eventRender: function(event, element) {
						// create event tooltip using bootstrap tooltips
						$(element).attr("data-original-title", event.title);
						$(element).tooltip({
							container: 'body',
							delay: {
								"show": 100,
								"hide": 200
							}
						});
						// create a tooltip auto close timer
						$(element).on('show.bs.tooltip', function() {
							var autoClose = setTimeout(function() {
								$('.tooltip').fadeOut();
							}, 3500);
						});
					}
				});


				var highColors = [bgSystem, bgSuccess, bgWarning, bgPrimary];

				// Chart data
				var seriesData = [{
					name: 'Bookings',
					data: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
				}, {
					name: 'New Users',
					data: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
				}];

				var ecomChart = $('#ecommerce_chart1');
				if (ecomChart.length) {
					ecomChart.highcharts({
						credits: false,
						colors: highColors,
						chart: {
							backgroundColor: 'transparent',
							className: '',
							type: 'line',
							zoomType: 'x',
							panning: true,
							panKey: 'shift',
							marginTop: 45,
							marginRight: 1,
						},
						title: {
							text: null
						},
						xAxis: {
							gridLineColor: '#EEE',
							lineColor: '#EEE',
							tickColor: '#EEE',
							categories: ['Jan', 'Feb', 'Mar', 'Apr',
								'May', 'Jun', 'Jul', 'Aug',
								'Sep', 'Oct', 'Nov', 'Dec'
							]
						},
						yAxis: {
							min: 0,
							tickInterval: 5,
							gridLineColor: '#EEE',
							title: {
								text: null,
							}
						},
						plotOptions: {
							spline: {
								lineWidth: 3,
							},
							area: {
								fillOpacity: 0.2
							}
						},
						legend: {
							enabled: true,
							floating: false,
							align: 'right',
							verticalAlign: 'top',
							x: -15
						},
						series: seriesData
					});
				}
			});
		</script>
		<!-- END: PAGE SCRIPTS -->
	</body>

</html>
