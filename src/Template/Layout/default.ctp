<!DOCTYPE html>
<?php
$current_lang = 'en';
if (isset($_COOKIE['language'])) {
	$current_lang = $_COOKIE['language'] == 'gb' ? 'en' : $_COOKIE['language'];
} else {
	if (isset($_COOKIE['autolanguage'])) {
		$current_lang = $_COOKIE['autolanguage'] == 'gb' ? 'en' : $_COOKIE['autolanguage'];
	}
}
?>
<html lang="<?= $current_lang; ?>">
<?= $this->element('Layout/head_main'); ?>

<body>
   
    <?php echo $this->Facebook->initJsSDK(); ?>
	<?= $this->element('Layout/header_main'); ?>

	<section class="page">
		<?= $this->element('Layout/page_subheader'); ?>

		<div class="page-alerts"><?= $this->Flash->render(); ?><?= $this->Flash->render('auth'); ?></div>

		<?= $this->fetch('content'); ?>
	</section>

	<?= $this->element('Layout/footer_main'); ?>
</body>

</html>
