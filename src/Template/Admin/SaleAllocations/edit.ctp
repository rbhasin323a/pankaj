<?php use Cake\Utility\Inflector; ?>
<header id="topbar">
	<div class="row">
		<div class="col-xs-12">
			<ol class="breadcrumb">
				<li class="crumb-trail"><a href="/<?= $this->request->params['prefix']; ?>/"><?= Inflector::humanize($this->request->params['prefix']); ?></a></li>
				<li class="crumb-trail"><a href="/admin/sale-offers/edit/<?= $saleAllocation->sale_offer_id; ?>#allocations_tab">Sale Offers</a></li>
				<li class="crumb-active">Edit</li>
			</ol>
		</div>
	</div>
	<div class="row">
		<div class="col-xs-12">
			<div class="btn-toolbar mt5">
				<div class="btn-group btn-group-sm">
					<?= $this->Html->link(__('List Sale Allocations'), ['action' => 'index'], ['class' => 'btn btn-primary']); ?>
				</div>
				<div class="btn-group btn-group-sm">
					<?= $this->Html->link(__('List Sales'), ['controller' => 'Sales', 'action' => 'index'], ['class' => 'btn btn-default bg-white']); ?>
					<?= $this->Html->link(__('New Sale'), ['controller' => 'Sales', 'action' => 'add'], ['class' => 'btn btn-default bg-white']); ?>
				</div>
			</div>
		</div>
	</div>
</header>

<div id="content">
	<div class="tray tray-center">
		<div class="mw1000">
			<div class="saleAllocations form">
				<div class="panel panel-dark">
					<div class="panel-heading">
						<span class="panel-title fw700"><?= __('Edit Sale Allocation for'); ?> <?= $saleAllocation->sale_offer->name; ?></span>
					</div>
					<div class="panel-body">
						<?php if (!empty($saleAllocation->sale_offer->sale->id)) { ?>
							<div class="btn-group btn-group-sm mb10">
								<?= $this->Html->link(__('Preview Sale'), 'sales/view/' . $saleAllocation->sale_offer->sale->id, ['class' => 'btn btn-primary', 'target' => '_blank']); ?>
							</div>
						<?php } ?>
						<?= $this->Form->create($saleAllocation, ['class' => 'form', 'novalidate' => true]); ?>
						<div class="row">
							<?php
							$this->Form->templates([
								'inputContainer'        => '<div class="col-md-6 input-container">{{content}}</div>',
								'inputContainerError'   => '<div class="col-md-6 input-container">{{content}}</div>',

								'label'                 => '<label {{attrs}}>{{text}}</label>{{hidden}}{{input}}',

								'formGroup'             => '{{label}}{{input}}{{error}}',
								'nestingLabel'          => '<div class="radio-custom square radio-primary mt10">{{input}} <label {{attrs}}>{{text}}</label></div>',

								// Input
								'input'                 => '<input type="{{type}}" name="{{name}}" {{attrs}} class="form-control" />',

								// Textarea
								'textarea'              => '<textarea name="{{name}}" {{attrs}} class="form-control">{{value}}</textarea>',

								// Select
								'select'                => '<select name="{{name}}" {{attrs}} class="form-control select2-single">{{content}}</select>',
								'selectMultple'         => '<select multiple name="{{name}}" {{attrs}} class="form-control select2-multiple">{{content}}</select>',

								'dateWidget'            => '{{day}}{{month}}{{year}}{{hour}}{{minute}}{{second}}',

								// Error
								'error'                 => '<div class="text-danger">{{content}}</div>',
								'errorList'             => '{{content}}',
								'errorItem'             => '{{text}}',
							]);
							?>

							<?= $this->Form->input('sale_offer_id', ['type' => 'hidden', 'class' => 'form-control sale_offer_id']); ?>
							<?= $this->Form->input('rate', ['class' => 'form-control rate']); ?>
							<?= $this->Form->input('rack_rate', ['class' => 'form-control rack_rate']); ?>
							<?= $this->Form->input('single_rate', ['class' => 'form-control single_rate']); ?>
							<?= $this->Form->input('child_rate', ['class' => 'form-control child_rate']); ?>
							<?= $this->Form->input('infant_rate', ['class' => 'form-control infant_rate']); ?>

							<?php if ($saleAllocation->sale_offer->deposit) { ?>
								<?= $this->Form->input('total_deposit', ['class' => 'form-control total_deposit']); ?>
								<?= $this->Form->input('single_deposit', ['class' => 'form-control single_deposit']); ?>
								<?= $this->Form->input('child_deposit', ['class' => 'form-control child_deposit']); ?>
								<?= $this->Form->input('infant_deposit', ['class' => 'form-control infant_deposit']); ?>
							<?php } ?>

							<?= $this->Form->input('date_balance_due', ['class' => 'form-control date_balance_due']); ?>
							<?= $this->Form->input('date_start', ['class' => 'form-control date_start']); ?>
							<?= $this->Form->input('date_end', ['class' => 'form-control date_end']); ?>
							<?= $this->Form->input('departure_airport_code', ['class' => 'form-control departure_airport_code']); ?>
							<?= $this->Form->input('destination_airport_code', ['class' => 'form-control destination_airport_code']); ?>
							<?= $this->Form->input('discount', ['class' => 'form-control discount', 'readonly' => 'readonly']); ?>
							<?= $this->Form->input('booked_rooms', ['class' => 'form-control booked_rooms']); ?>
							<?= $this->Form->input('hold_rooms', ['class' => 'form-control hold_rooms']); ?>
							<?= $this->Form->input('locked_rooms', ['class' => 'form-control locked_rooms']); ?>
							<?= $this->Form->input('available_rooms', ['class' => 'form-control available_rooms']); ?>
							<?= $this->Form->input('outbound_overnight', ['class' => 'form-control outbound_overnight']); ?>
							<?= $this->Form->input('inbound_overnight', ['class' => 'form-control inbound_overnight']); ?>

						</div>
						<div class="row">
							<div class="col-xs-12">
								<?= $this->Form->button(__('Submit'), ['class' => 'btn btn-primary btn-lg']); ?>
							</div>
						</div>
						<?= $this->Form->end(); ?>

						<script type="text/javascript">
							(function($) {
								$(document).ready(function() {
									// Init Select2 - Basic Single
									$(".select2-single, select.form-error").each(function(index, el) {
										$(this).addClass('form-control').select2({
											placeholder: 'Choose ' + $('label', $(this).parent()).text(),
										});
									});

									// Init Select2 - Basic Multiple
									$(".select2-multiple, select.form-error").each(function(index, el) {
										$(this).addClass('form-control').select2({
											allowClear: true,
											placeholder: 'Choose ' + $('label', $(this).parent()).text(),
										});
									});

									$('.date_start').datetimepicker({
										format: 'YYYY-MM-DD'
									});

									$('.date_end').datetimepicker({
										format: 'YYYY-MM-DD'
									});

									$('.datetime').datetimepicker({
										format: 'YYYY-MM-DD HH:mm'
									});
								});
							})(jQuery);
						</script>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
