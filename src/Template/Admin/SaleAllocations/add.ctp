<nav class="large-3 medium-4 columns" id="actions-sidebar">
	<ul class="side-nav">
		<li class="heading"><?= __('Actions'); ?></li>
		<li><?= $this->Html->link(__('List Sale Allocations'), ['action' => 'index']); ?></li>
		<li><?= $this->Html->link(__('List Sale Offers'), ['controller' => 'SaleOffers', 'action' => 'index']); ?></li>
		<li><?= $this->Html->link(__('New Sale Offer'), ['controller' => 'SaleOffers', 'action' => 'add']); ?></li>
	</ul>
</nav>
<div class="saleAllocations form large-9 medium-8 columns content">
	<?= $this->Form->create($saleAllocation); ?>
	<fieldset>
		<legend><?= __('Add Sale Allocation'); ?></legend>
		<?php
		echo $this->Form->input('sale_offer_id', ['options' => $saleOffers]);
		echo $this->Form->input('rate');
		echo $this->Form->input('rack_rate');
		echo $this->Form->input('single_rate');
		echo $this->Form->input('child_rate');
		echo $this->Form->input('infant_rate');
		echo $this->Form->input('single_deposit');
		echo $this->Form->input('child_deposit');
		echo $this->Form->input('infant_deposit');
		echo $this->Form->input('date_balance_due');
		echo $this->Form->input('min_nights');
		echo $this->Form->input('date_start');
		echo $this->Form->input('date_end');
		echo $this->Form->input('discount', ['readonly' => 'readonly']);
		echo $this->Form->input('booked_rooms');
		echo $this->Form->input('hold_rooms');
		echo $this->Form->input('locked_rooms');
		echo $this->Form->input('available_rooms');
		echo $this->Form->input('outbound_overnight');
		echo $this->Form->input('inbound_overnight');
		?>
	</fieldset>
	<?= $this->Form->button(__('Submit')); ?>
	<?= $this->Form->end(); ?>
</div>
