<nav class="large-3 medium-4 columns" id="actions-sidebar">
	<ul class="side-nav">
		<li class="heading"><?= __('Actions'); ?></li>
		<li><?= $this->Html->link(__('New Sale Allocation'), ['action' => 'add']); ?></li>
		<li><?= $this->Html->link(__('List Sale Offers'), ['controller' => 'SaleOffers', 'action' => 'index']); ?></li>
		<li><?= $this->Html->link(__('New Sale Offer'), ['controller' => 'SaleOffers', 'action' => 'add']); ?></li>
	</ul>
</nav>
<div class="saleAllocations index large-9 medium-8 columns content">
	<h3><?= __('Sale Allocations'); ?></h3>
	<table cellpadding="0" cellspacing="0">
		<thead>
			<tr>
				<th><?= $this->Paginator->sort('id'); ?></th>
				<th><?= $this->Paginator->sort('sale_offer_id'); ?></th>
				<th><?= $this->Paginator->sort('rate'); ?></th>
				<th><?= $this->Paginator->sort('rack_rate'); ?></th>
				<th><?= $this->Paginator->sort('single_rate'); ?></th>
				<th><?= $this->Paginator->sort('child_rate'); ?></th>
				<th><?= $this->Paginator->sort('infant_rate'); ?></th>
				<th class="actions"><?= __('Actions'); ?></th>
			</tr>
		</thead>
		<tbody>
			<?php foreach ($saleAllocations as $saleAllocation) : ?>
				<tr>
					<td><?= h($saleAllocation->id); ?></td>
					<td><?= $saleAllocation->has('sale_offer') ? $this->Html->link($saleAllocation->sale_offer->name, ['controller' => 'SaleOffers', 'action' => 'view', $saleAllocation->sale_offer->id]) : '' ?></td>
					<td><?= $this->Number->format($saleAllocation->rate); ?></td>
					<td><?= $this->Number->format($saleAllocation->rack_rate); ?></td>
					<td><?= $this->Number->format($saleAllocation->single_rate); ?></td>
					<td><?= $this->Number->format($saleAllocation->child_rate); ?></td>
					<td><?= $this->Number->format($saleAllocation->infant_rate); ?></td>
					<td class="actions">
						<?= $this->Html->link(__('View'), ['action' => 'view', $saleAllocation->id]); ?>
						<?= $this->Html->link(__('Edit'), ['action' => 'edit', $saleAllocation->id]); ?>
						<?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $saleAllocation->id], ['confirm' => __('Are you sure you want to delete # {0}?', $saleAllocation->id)]); ?>
					</td>
				</tr>
			<?php endforeach; ?>
		</tbody>
	</table>
	<div class="paginator">
		<ul class="pagination">
			<?= $this->Paginator->prev('< ' . __('previous')); ?>
			<?= $this->Paginator->numbers(); ?>
			<?= $this->Paginator->next(__('next') . ' >'); ?>
		</ul>
		<p><?= $this->Paginator->counter(); ?></p>
	</div>
</div>
