<?php use Cake\Utility\Inflector; ?>
<header id="topbar">
	<div class="row">
		<div class="col-xs-12">
			<ol class="breadcrumb">
				<li class="crumb-trail"><a href="/<?= $this->request->params['prefix']; ?>/"><?= Inflector::humanize($this->request->params['prefix']); ?></a></li>
				<li class="crumb-trail"><a href="/<?= $this->request->params['prefix'] . '/' . $this->request->params['controller']; ?>">Sales</a></li>
				<li class="crumb-active">Add</li>
			</ol>
		</div>
	</div>
	<div class="row">
		<div class="col-xs-12">
			<div class="btn-toolbar mt5">
				<div class="btn-group btn-group-sm">
					<?= $this->Html->link(__('List Sales'), ['action' => 'index'], ['class' => 'btn btn-primary']); ?>
				</div>
			</div>
		</div>
	</div>
</header>

<div id="content">
	<div class="tray tray-center">
		<div class="mw1000">
			<div class="sales form">
				<div class="panel panel-dark">
					<div class="panel-heading">
						<span class="panel-title fw700"><?= __('Add Sale'); ?></span>
					</div>
					<div class="panel-body">
						<?= $this->Form->create($sale, ['class' => 'form', 'novalidate' => true]); ?>
						<div class="row">
							<?php
							$this->Form->templates([
								'inputContainer'        => '<div class="col-md-6 input-container">{{content}}</div>',
								'inputContainerError'   => '<div class="col-md-6 input-container">{{content}}</div>',

								'label'                 => '<label {{attrs}}>{{text}}</label>{{hidden}}{{input}}',

								'formGroup'             => '{{label}}{{input}}{{error}}',
								'nestingLabel'          => '<div class="radio-custom square radio-primary mt10">{{input}} <label {{attrs}}>{{text}}</label></div>',

								// Input
								'input'                 => '<input type="{{type}}" name="{{name}}" {{attrs}} class="form-control" />',

								// Textarea
								'textarea'              => '<textarea name="{{name}}" {{attrs}} class="form-control">{{value}}</textarea>',

								// Select
								'select'                => '<select name="{{name}}" {{attrs}} class="form-control select2-single">{{content}}</select>',
								'selectMultple'         => '<select multiple name="{{name}}" {{attrs}} class="form-control select2-multiple">{{content}}</select>',

								'dateWidget'            => '{{day}}{{month}}{{year}}{{hour}}{{minute}}{{second}}',

								// Error
								'error'                 => '<div class="text-danger">{{content}}</div>',
								'errorList'             => '{{content}}',
								'errorItem'             => '{{text}}',
							]);
							?>
							<?= $this->Form->input('title', ['class' => 'form-control title']); ?>
							<?= $this->Form->input('sub_title', ['class' => 'form-control sub_title']); ?>
							<?= $this->Form->input('destination', ['class' => 'form-control destination']); ?>
							<?= $this->Form->input('slug', ['class' => 'form-control slug']); ?>
							<?= $this->Form->input('short_description', ['class' => 'form-control short_description']); ?>
							<?= $this->Form->input('staff_description', ['class' => 'form-control staff_description']); ?>
							<?= $this->Form->input('hotel_description', ['class' => 'form-control hotel_description']); ?>
							<?= $this->Form->input('travel_description', ['class' => 'form-control travel_description']); ?>
							<?= $this->Form->input('review_description', ['class' => 'form-control review_description']); ?>
							<?= $this->Form->input('location_title', ['class' => 'form-control location_title']); ?>
							<?= $this->Form->input('location_map', ['class' => 'form-control location_map']); ?>
							<?= $this->Form->input('contractor_id', [
								'options' => $contractors,
								'empty' => true,
							]); ?>
							<?= $this->Form->input('supplier_id', [
								'options' => $suppliers,
								'empty' => true,
							]); ?>
							<?= $this->Form->input('company_id', [
								'options' => $companies,
								'empty' => true,
							]); ?>
							<?= $this->Form->input('currency_id', [
								'options' => $currencies,
							]); ?>
							<?= $this->Form->input('country_id', [
								'options' => $countries,
								'empty' => true,
							]); ?>
							<?= $this->Form->input('tax_id', [
								'options' => $taxes,
								'empty' => true,
							]); ?>
							<?= $this->Form->input('status', ['class' => 'form-control status']); ?>
							<?= $this->Form->input('active', ['class' => 'form-control active']); ?>
							<?= $this->Form->input('smart_stay', ['class' => 'form-control smart_stay']); ?>
							<?= $this->Form->input('date_open', ['type' => 'text', 'class' => 'form-control datetime']); ?>
							<?= $this->Form->input('date_close', ['type' => 'text', 'class' => 'form-control datetime']); ?>
							<?= $this->Form->input('sale_type', ['class' => 'form-control sale_type']); ?>
							<?= $this->Form->input('board_type', ['class' => 'form-control board_type']); ?>
							<?= $this->Form->input('promoted', ['class' => 'form-control promoted']); ?>
							<?= $this->Form->input('available_domains', ['class' => 'form-control available_domains']); ?>
							<?= $this->Form->input('exclude_summary', ['class' => 'form-control exclude_summary']); ?>
							<?= $this->Form->input('repeated', ['class' => 'form-control repeated']); ?>
							<?= $this->Form->input('enable_hold', ['class' => 'form-control enable_hold']); ?>
							<?= $this->Form->input('commission', ['class' => 'form-control commission']); ?>
							<?= $this->Form->input('top_discounts', ['class' => 'form-control top_discounts']); ?>
							<?= $this->Form->input('commission_type', ['class' => 'form-control commission_type']); ?>
							<?= $this->Form->input('show_price', ['class' => 'form-control show_price']); ?>
							<?= $this->Form->input('show_discount', ['class' => 'form-control show_discount']); ?>
							<?= $this->Form->input('visible', ['class' => 'form-control visible']); ?>
							<?= $this->Form->input('scyscanner_airport_id', [
								'options' => $scyscannerAirports,
								'empty' => true,
							]); ?>
						</div>
						<div class="row">
							<div class="col-xs-12">
								<?= $this->Form->button(__('Submit'), ['class' => 'btn btn-primary btn-lg']); ?>
							</div>
						</div>
						<?= $this->Form->end(); ?>

						<script type="text/javascript">
							(function($) {
								$(document).ready(function() {
									// Init Select2 - Basic Single
									$(".select2-single, select.form-error").each(function(index, el) {
										$(this).addClass('form-control').select2({
											placeholder: 'Choose ' + $('label', $(this).parent()).text(),
										});
									});

									// Init Select2 - Basic Multiple
									$(".select2-multiple, select.form-error").each(function(index, el) {
										$(this).addClass('form-control').select2({
											allowClear: true,
											placeholder: 'Choose ' + $('label', $(this).parent()).text(),
										});
									});

									$('.date').datepicker({
										format: 'YYYY-MM-DD'
									});

									$('.datetime').datetimepicker({
										format: 'YYYY-MM-DD HH:mm'
									});
								});
							})(jQuery);
						</script>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
