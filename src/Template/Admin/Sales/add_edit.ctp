<?php
use Cake\Utility\Inflector;

$this->loadHelper('Form', [
	'templates' => 'admin_form',
]);
?>
<header id="topbar">
	<div class="row">
		<div class="col-xs-12">
			<ol class="breadcrumb">
				<li class="crumb-trail"><a href="/<?= $this->request->params['prefix']; ?>/"><?= Inflector::humanize($this->request->params['prefix']); ?></a></li>
				<li class="crumb-trail"><a href="/<?= $this->request->params['prefix'] . '/' . $this->request->params['controller']; ?>">Sales</a></li>
				<li class="crumb-active"><?= Inflector::humanize($this->request->params['action']); ?></li>
			</ol>
		</div>
	</div>
	<div class="row">
		<div class="col-xs-12">
			<div class="btn-toolbar mt5">
				<div class="btn-group btn-group-sm">
					<?= $this->Html->link(__('List Sales'), ['action' => 'index'], ['class' => 'btn btn-primary']); ?>
				</div>
			</div>
		</div>
	</div>
</header>

<div id="content">
	<div class="tray tray-center">
		<div class="mw1000">
			<div class="sales form">
				<div class="panel panel-dark">
					<div class="panel-heading">
						<span class="panel-title fw700">
							<?php if (!empty($sale->title)) { ?>
								<em><?= $sale->title; ?></em> -
							<?php } ?>
							<?= Inflector::humanize($this->request->params['action']) . ' Sale'; ?>
						</span>
					</div>
					<div class="panel-body">
						<?php if (!empty($sale->id)) { ?>
							<div class="btn-group btn-group-sm">
								<?= $this->Html->link(__('Preview Sale'), 'sales/view/' . $sale->id, ['class' => 'btn btn-primary', 'target' => '_blank']); ?>
							</div>
							<ul class="nav nav-pills pull-right">
								<li class="active"><a href="#sale_details" data-toggle="tab" aria-expanded="false">Details</a></li>
								<li><a href="#sale_images" data-toggle="tab" aria-expanded="false">Image</a></li>
								<li><a href="#links" data-toggle="tab" aria-expanded="false">Links</a></li>
								<li><a href="#sale_offers" data-toggle="tab" aria-expanded="false">Offers</a></li>
							</ul>
							<div class="clearfix"></div>
						<?php } ?>
						<?= $this->Form->create($sale, ['class' => 'form', 'novalidate' => true, 'enctype' => 'multipart/form-data']); ?>
						<div class="tab-content pn br-n">
							<div id="sale_details" class="tab-pane active">
								<div class="row">
									<div class="col-xs-12">
										<h2 class="mb25">Sale Information</h2>
									</div>
									<?php if (!empty($vd['languages'])) { ?>
										<div class="col-xs-12">
											<div class="tab-block" style="margin:0 -16px 25px">
												<ul class="nav nav-tabs">
													<?php $i = 0;
													foreach ($vd['languages'] as $language) {
														$i++; ?>
														<li <?= ($i == 1) ? 'class="active"' : ''; ?>><a href="#tab_language_<?= $language['id']; ?>" data-toggle="tab">
																<img src="/ui/img/flags/24/<?= $language['code']; ?>.png" />&nbsp;<?= $language['name']; ?>
															</a></li>
													<?php } ?>
												</ul>
												<div class="tab-content">
													<?php $i = 0;
													foreach ($vd['languages'] as $language) {
														$i++; ?>
														<?php $locale = $language['locale']; ?>
														<div id="tab_language_<?= $language['id']; ?>" class="tab-pane <?= ($i == 1) ? 'active' : ''; ?>">
															<?= $this->Form->input('title', [
																'type'  => 'text',
																'label' => 'Title',
																'name'  => '_translations[' . $locale . '][title]',
																'class' => 'form-control title',
																'id'    => 'translation-title-' . $locale,
																'value' => !empty($_translations[$locale]['title']) ? $_translations[$locale]['title'] : ''
															]); ?>
															<?= $this->Form->input('sub_title', [
																'type'  => 'text',
																'label' => 'Sub Title',
																'name'  => '_translations[' . $locale . '][sub_title]',
																'class' => 'form-control sub_title',
																'id'    => 'translation-sub_title-' . $locale,
																'value' => !empty($_translations[$locale]['sub_title']) ? $_translations[$locale]['sub_title'] : ''
															]); ?>
															<?= $this->Form->input('destination', [
																'type'  => 'text',
																'label' => 'Destination',
																'name'  => '_translations[' . $locale . '][destination]',
																'class' => 'form-control destination',
																'id'    => 'translation-destination-' . $locale,
																'value' => !empty($_translations[$locale]['destination']) ? $_translations[$locale]['destination'] : ''
															]); ?>
															<?= $this->Form->input('slug', [
																'type'  => 'text',
																'label' => 'Slug',
																'name'  => '_translations[' . $locale . '][slug]',
																'class' => 'form-control slug',
																'id'    => 'translation-slug-' . $locale,
																'value' => !empty($_translations[$locale]['slug']) ? $_translations[$locale]['slug'] : ''
															]); ?>
															<?= $this->Form->input('meta_title', [
																'type'  => 'text',
																'label' => 'Meta Title',
																'name'  => '_translations[' . $locale . '][meta_title]',
																'class' => 'form-control meta_title',
																'id'    => 'translation-meta_title-' . $locale,
																'value' => !empty($_translations[$locale]['meta_title']) ? $_translations[$locale]['meta_title'] : ''
															]); ?>
															<?= $this->Form->input('hotel_description', [
																'type'  => 'textarea',
																'label' => 'Hotel Details',
																'name'  => '_translations[' . $locale . '][hotel_description]',
																'class' => 'form-control ckeditor hotel_description',
																'templateVars' => ['class' => 'inline-full'],
																'id'    => 'translation-hotel_description-' . $locale,
																'value' => !empty($_translations[$locale]['hotel_description']) ? $_translations[$locale]['hotel_description'] : ''
															]); ?>
															<?= $this->Form->input('travel_description', [
																'type'  => 'textarea',
																'label' => 'Travel Description',
																'name'  => '_translations[' . $locale . '][travel_description]',
																'class' => 'form-control ckeditor travel_description',
																'templateVars' => ['class' => 'inline-full'],
																'id'    => 'translation-travel_description-' . $locale,
																'value' => !empty($_translations[$locale]['travel_description']) ? $_translations[$locale]['travel_description'] : ''
															]); ?>
															<?= $this->Form->input('good_for', [
																'type'  => 'textarea',
																'label' => 'Good For',
																'name'  => '_translations[' . $locale . '][good_for]',
																'class' => 'form-control ckeditor good_for',
																'templateVars' => ['class' => 'inline-full'],
																'id'    => 'translation-good_for-' . $locale,
																'value' => !empty($_translations[$locale]['good_for']) ? $_translations[$locale]['good_for'] : ''
															]); ?>
															<?= $this->Form->input('meta_description', [
																'type'  => 'textarea',
																'label' => 'Meta Description',
																'name'  => '_translations[' . $locale . '][meta_description]',
																'class' => 'form-control ckeditor meta_description',
																'templateVars' => ['class' => 'inline-full'],
																'id'    => 'translation-meta_description-' . $locale,
																'value' => !empty($_translations[$locale]['meta_description']) ? $_translations[$locale]['meta_description'] : ''
															]); ?>
														</div>
													<?php } ?>
													<?= $this->Form->input('tripadvisor_widget', [
														'type'  => 'textarea',
														'name' => 'tripadvisor_widget',
														'label' => 'Tripadvisor Widget',
														'class' => 'form-control tripadvisor_widget',
														'templateVars' => ['class' => 'inline-full']
													]); ?>
												</div>
											</div>
										</div>
									<?php } ?>
									<?= $this->Form->input('location_map', ['class' => 'form-control location_map']); ?>
									<?= $this->Form->input('contractor_id', [
										'options' => $contractors,
										'empty' => true,
									]); ?>
									<?= $this->Form->input('supplier_id', [
										'options' => $suppliers,
										'empty' => true,
									]); ?>
									<?= $this->Form->input('company_id', [
										'options' => $companies,
										'empty' => true,
									]); ?>
									<?= $this->Form->input('currency_id', [
										'options' => $currencies,
									]); ?>
									<?= $this->Form->input('country_id', [
										'options' => $countries,
										'empty' => true,
									]); ?>
									<?= $this->Form->input('tax_id', [
										'options' => $taxes,
										'empty' => true,
									]); ?>

									<?= $this->Form->input('territory', ['type' => 'select', 'options'  => $languages, 'default' => !empty($language_to_sale) ? $language_to_sale : [], 'multiple' => 'checkbox', 'class' => 'form-control']); ?>

									<?= $this->Form->input('date_open', ['type' => 'text', 'class' => 'form-control datetime']); ?>
									<?= $this->Form->input('date_close', ['type' => 'text', 'class' => 'form-control datetime']); ?>

									<?= $this->Form->input('sale_type', ['class' => 'form-control sale_type', 'type' => 'select', 'options' => $sale_types]); ?>

									<?= $this->Form->input('board_type', ['class' => 'form-control board_type', 'type' => 'select', 'options' => $board_types]); ?>

									<?= $this->Form->input('commission', ['class' => 'form-control commission']); ?>
									<?= $this->Form->input('commission_type', ['class' => 'form-control commission_type', 'type' => 'select', 'options' => $commission_types]); ?>

									<?= $this->Form->input('promoted', ['class' => 'form-control promoted', 'min' => '0', 'max' => '100']); ?>
									<?= $this->Form->input('enable_hold', ['class' => 'form-control enable_hold']); ?>
									<?= $this->Form->input('active', [
										'options' => [
											0 => 'Disabled',
											1 => 'Enabled',
											2 => 'Expired'
										]
									]); ?>
								</div>
							</div>

							<div id="sale_images" class="tab-pane">
								<div class="row">
									<div class="col-xs-12">
										<h2 class="mb25">Video &amp; Images</h2>
									</div>
									<div class="col-xs-12">
										<?= $this->Form->input('video_url', ['label' => 'YouTube Video URL', 'class' => 'form-control video_url']); ?>
									</div>
									<div class="col-xs-12">
										<div class="table-responsive">
											<table cellpadding="0" cellspacing="0" class="table table-bordered themed-form table-hover theme-info tc-checkbox-1 fs13" id="table-images">
												<thead>
													<tr class="bg-light">
														<th>Main image</th>
														<th colspan="2">File</th>
														<th>Actions</th>
													</tr>
												</thead>
												<tbody id="sortable">
													<?php if (!empty($images)) { ?>
														<?php foreach ($images as $image_id => $image) { ?>
															<tr class="image-row">
																<td width="20"><input type="radio" name="main_image" value="<?= $image->image_path ?>" <?= $image['is_main'] ? 'checked' : '' ?> /></td>
																<td width="200"><img src="/files/images/sales/<?= rawurlencode($image->image_path); ?>" class="mn thumbnail img-responsive" /></td>
																<td><b><a href="/files/images/sales/<?= rawurlencode($image->image_path); ?>" class="image-link"><?= h($image->image_path); ?></a></b><br />
																	<input type="hidden" class="sort-order-image" name="img_to_sale[<?= $image->image_to_sale_id; ?>][sort_order]" value="<?= $image->sort_order ?>" />

																	<?php $i = 0;
																	foreach ($vd['languages'] as $language) {
																		$i++; ?>
																		<?php $locale = $language['locale']; ?>

																		<input type="hidden" name="_image_translations[<?= $locale; ?>][image_to_sale][<?= $image_id; ?>][image_path]" value="<?= $image->image_path ?>" />
																		<input type="hidden" name="_image_translations[<?= $locale; ?>][image_to_sale][<?= $image_id; ?>][sale_id]" value="<?= $image->sale_id ?>" />
																		<input type="hidden" name="_image_translations[<?= $locale; ?>][image_to_sale][<?= $image_id; ?>][image_to_sale_id]" value="<?= $image->image_to_sale_id ?>" />

																		<?= $this->Form->input('image_alt', [
																			'type'  => 'text',
																			'label' => 'Image Alt [' . $language['name'] . ']',
																			'name'  => '_image_translations[' . $locale . '][image_to_sale][' . $image_id . '][image_alt]',
																			'class' => 'form-control input-sm',
																			'id'    => 'translation-image-' . $image->image_to_sale_id . '-' . $locale,
																			'value' => !empty($_image_translations[$image_id]['_translations'][$locale]['image_alt']) ? $_image_translations[$image_id]['_translations'][$locale]['image_alt'] : ''
																		]); ?>
																		<br />
																		<div class="clearfix"></div>
																	<?php } ?>

																</td>
																<td>

																	<button class="btn btn-danger delete" data-image-name="<?= $image->image_path ?>" data-image-to-sale-id="<?= $image->image_to_sale_id ?>">Delete</button>
																</td>
															</tr>

															<script>
																$(function() {
																	$("#sortable").sortable({
																		revert: true,
																		update: function(event, ui) {
																			$('tr.image-row').each(function(i) {
																				$('.sort-order-image', this).val(i);
																				$('input[name*=image_to_sale]', this).each(function(n) {
																					var old_name = $(this).attr('name');
																					var new_name = old_name.replace(/sale\]\[(\d)\]\[/, 'sale][' + i + '][');

																					$(this).attr('name', new_name);
																				});
																			});
																		}
																	});
																	$("tr.image-row").disableSelection();
																});
															</script>
														<?php } ?>
													<?php } else { ?>
														<tr id="no-images-row">
															<td colspan="4">
																<p class="text-center fs16 lh50 pv20"><i class="icon-remove-sign"></i>&nbsp;&nbsp;No images found.</p>
															</td>
														</tr>
													<?php } ?>

												</tbody>
												<tfoot>
													<tr id="row-actions">
														<td colspan="4"><button id="add-row" class="btn btn-primary pull-right">Add Row</button></td>
													</tr>
												</tfoot>
											</table>
											<hr />
											<div class="buttons">
												<p><b>Upload an image to this sale.</b></p>

												<link rel="stylesheet" type="text/css" href="/backend/ui/vendor/admindesign/vendor/plugins/magnific/magnific-popup.css">

												<script type="text/javascript" src="/backend/ui/vendor/admindesign/vendor/plugins/magnific/jquery.magnific-popup.js"></script>

												<script type="text/javascript">
													(function($) {
														$(document).ready(function() {
															$('input[type=file]').trigger('change');

															$('#save-sale').on('click', function(e) {
																if ($('.image-row').length) {
																	var radios = $('input[name=main_image]');
																	var checked = false;

																	radios.each(function(index, element) {
																		if ($(element).is(':checked')) {
																			checked = true;
																		}
																	});

																	if (!checked) {
																		e.preventDefault();
																		alert('Please, choose a main image');
																	}
																}
															});

															$('#table-images').magnificPopup({
																delegate: '.image-link',
																type: 'image',
																tLoading: 'Loading image #%curr%...',
																mainClass: 'mfp-img-mobile',
																gallery: {
																	enabled: true,
																	navigateByImgClick: true,
																},
																image: {
																	tError: '<a href="%url%">The image #%curr%</a> could not be loaded.'
																}
															});

															$(document).on('click', '#table-images .delete', function(e) {
																e.preventDefault();
																var imageToSaleId = $(this).attr('data-image-to-sale-id');
																var filename = $(this).attr('data-image-name');

																if (imageToSaleId) {
																	if (confirm("Are you sure you want to delete the image?")) {
																		var that = this;

																		$.ajax({
																			url: '/admin/sales/deleteImage',
																			type: 'POST',
																			data: {
																				image_to_sale_id: imageToSaleId,
																				image_name: filename
																			},
																			success: function(resp) {
																				if (resp) {
																					$(that).closest('tr').remove();
																				}
																			}
																		});
																	}
																} else {
																	$(this).closest('tr').remove();
																}
															});

															$(document).on("change", "input[name=\'images[]\']", function(e) {
																var valueArray = $(this).val().split('\\');
																$(this).closest('tr').find('input[name=main_image]').val(valueArray[valueArray.length - 1]);
															});

															$('#add-row').on('click', function(e) {
																e.preventDefault();

																if ($('#no-images-row').length) {
																	$('#no-images-row').remove();
																}

																var tbody = $('#table-images tbody');
																var row = $('<tr class="image-row">');

																var html = '';
																html += '<td width="20">';
																html += '<input type="radio" name="main_image"/>';
																html += '</td>';
																html += '<td colspan="2">';
																html += '<label for="input-images"><b>Upload new image</b>&nbsp;&nbsp;</label>';
																html += '<input type="file" name="images[]" value="" style="display: inline-block;"/>';
																html += '</td>';
																html += '<td>';
																html += '<button class="btn btn-danger delete">Delete</button>';
																html += '</td>';

																row.html(html);
																tbody.append(row);
															});
														});
													})(jQuery);
												</script>
											</div>
										</div>
									</div>
								</div>
							</div>

							<div id="links" class="tab-pane">
								<div class="col-xs-12">
									<h2 class="mb25">Links</h2>
								</div>
								<div class="col-xs-6">
									<label for="category-autocomplete">Category Autocomplete</label>
									<input type="text" class="form-control" id="category-autocomplete">
									<div class="well" id="sale-categories">
										<?php if (!empty($sale->sale_to_category)) { ?>
											<?php foreach ($sale->sale_to_category as $sale_to_category) : ?>
												<div data-category-id="<?= $sale_to_category->category_id; ?>">
													<i class='fa fa-minus-circle remove-category'></i>&nbsp;<?= $sale_to_category->category->title; ?>
													<input type='hidden' name='sale_categories[]' value="<?= $sale_to_category->category_id; ?>" />
												</div>
											<?php endforeach ?>
										<?php } ?>
									</div>
								</div>
							</div>

							<div class="row">
								<div class="col-xs-12">
									<?= $this->Form->button(__('Save Sale'), ['class' => 'btn btn-primary btn-lg', 'id' => 'save-sale']); ?>
								</div>
							</div>
							<?= $this->Form->end(); ?>

							<div id="sale_offers" class="tab-pane">
								<div class="row">
									<div class="col-xs-12">
										<h2 class="mb25">Offers</h2>
									</div>
									<div class="col-xs-12">
										<div class="table-responsive">
											<table cellpadding="0" cellspacing="0" class="table table-bordered themed-form table-hover theme-info tc-checkbox-1 fs13">
												<thead>
													<tr class="bg-light">
														<th width="1"><?= ucfirst('id'); ?></th>
														<th><?= ucfirst('name'); ?></th>
														<th><?= ucfirst('active'); ?></th>
														<th><?= ucfirst('created'); ?></th>
														<th class="actions text-center"><?= __('Actions'); ?></th>
													</tr>
												</thead>
												<tbody>
													<?php if (!empty($sale->sale_offers)) { ?>
														<?php foreach ($sale->sale_offers as $saleOffer) : ?>
															<tr>
																<td><?= h($saleOffer->id); ?></td>
																<td><?= h($saleOffer->name); ?></td>
																<td><?= !empty($this->Number->format($saleOffer->active)) ? '<i class="fa fa-check fs18 text-success"></i>' : '<i class="fa fa-remove fs18 text-danger"></i>'; ?></td>
																<td><?= h($saleOffer->created); ?></td>
																<td class="actions text-center" width="200">
																	<div class="btn-group btn-group-sm ib">
																		<?= $this->Html->link(__('Edit'), ['controller' => 'sale_offers', 'action' => 'edit', $saleOffer->id], ['class' => 'btn btn-primary']); ?>
																		<?= $this->Html->link(__('Duplicate'), ['controller' => 'sale_offers', 'action' => 'duplicate', $saleOffer->id], ['class' => 'btn btn-primary']); ?>
																	</div>
																</td>
															</tr>
														<?php endforeach; ?>
													<?php } else { ?>
														<tr>
															<td colspan="6">
																<p class="text-center fs16 lh50 pv20"><i class="fa fa-ban text-danger"></i>&nbsp;&nbsp;No results found.</p>
															</td>
														</tr>
													<?php } ?>
												</tbody>
											</table>
										</div>
									</div>
								</div>

								<?= $this->Form->create($_sale_offer, ['class' => 'form', 'novalidate' => true]); ?>
								<div class="row">
									<div class="col-xs-12">
										<hr class="mbn" />
									</div>
									<div class="col-xs-12">
										<h2 class="mb25">Add Offer</h2>
									</div>
									<?php
									$this->Form->templates([
										'inputContainer'        => '<div class="col-md-6 {{class}} input-container">{{content}}</div>',
										'inputContainerError'   => '<div class="col-md-6 {{class}} input-container">{{content}}</div>',

										'label'                 => '<label {{attrs}}>{{text}}</label>{{hidden}}{{input}}',

										'formGroup'             => '{{label}} {{error}} {{input}}',
										'nestingLabel'          => '<div class="radio-custom square radio-primary mt10">{{input}} <label {{attrs}}>{{text}}</label></div>',

										// Input
										'input'                 => '<input type="{{type}}" name="{{name}}" {{attrs}} class="form-control" />',

										// Textarea
										'textarea'              => '<textarea name="{{name}}" {{attrs}} class="form-control">{{value}}</textarea>',

										// Select
										'select'                => '<select name="{{name}}" {{attrs}} class="form-control select2-single">{{content}}</select>',
										'selectMultple'         => '<select multiple name="{{name}}" {{attrs}} class="form-control select2-multiple">{{content}}</select>',

										'dateWidget'            => '{{day}}{{month}}{{year}}{{hour}}{{minute}}{{second}}',

										// Error
										'error'                 => '<span class="text-danger">* {{content}}</span>',
										'errorList'             => '{{content}}',
										'errorItem'             => '{{text}}',
									]);
									?>

									<?php if (!empty($vd['languages'])) { ?>
										<div class="col-xs-12">
											<div class="tab-block" style="margin:0 -16px 25px">
												<ul class="nav nav-tabs">
													<?php $i = 0;
													foreach ($vd['languages'] as $language) {
														$i++; ?>
														<li <?= ($i == 1) ? 'class="active"' : ''; ?>><a href="#sale_offer_tab_language_<?= $language['id']; ?>" data-toggle="tab">
																<img src="/ui/img/flags/24/<?= $language['code']; ?>.png" />&nbsp;<?= $language['name']; ?>
															</a></li>
													<?php } ?>
												</ul>
												<div class="tab-content">
													<?php $i = 0;
													foreach ($vd['languages'] as $language) {
														$i++; ?>
														<?php $locale = $language['locale']; ?>
														<div id="sale_offer_tab_language_<?= $language['id']; ?>" class="tab-pane <?= ($i == 1) ? 'active' : ''; ?>">
															<?= $this->Form->input('name', [
																'type'  => 'text',
																'label' => 'Name',
																'name'  => '_sale_offer[_translations][' . $locale . '][name]',
																'class' => 'form-control name',
																'templateVars'  => ['class' => 'inline-full'],
																'id'    => 'sale-offer-translation-name-' . $locale,
																'value' => !empty($_sale_offer_translations[$locale]['name']) ? $_sale_offer_translations[$locale]['name'] : ''
															]); ?>
															<?= $this->Form->input('main_description', [
																'type'  => 'textarea',
																'label' => 'Main Description',
																'name'  => '_sale_offer[_translations][' . $locale . '][main_description]',
																'class' => 'form-control ckeditor main_description',
																'templateVars'  => ['class' => 'inline-full'],
																'id'    => 'sale-offer-translation-main-description-' . $locale,
																'value' => !empty($_sale_offer_translations[$locale]['main_description']) ? $_sale_offer_translations[$locale]['main_description'] : ''
															]); ?>
															<?= $this->Form->input('summary_description', [
																'type'  => 'textarea',
																'label' => 'Summary Description',
																'name'  => '_sale_offer[_translations][' . $locale . '][summary_description]',
																'class' => 'form-control ckeditor summary_description',
																'templateVars'  => ['class' => 'inline-full'],
																'id'    => 'sale-offer-translation-summary-description-' . $locale,
																'value' => !empty($_sale_offer_translations[$locale]['summary_description']) ? $_sale_offer_translations[$locale]['summary_description'] : ''
															]); ?>
															<?= $this->Form->input('single_policy', [
																'type'  => 'textarea',
																'label' => 'Single Policy',
																'name'  => '_sale_offer[_translations][' . $locale . '][single_policy]',
																'class' => 'form-control ckeditor single_policy',
																'templateVars'  => ['class' => 'inline-full'],
																'id'    => 'sale-offer-translation-single-policy-' . $locale,
																'value' => !empty($_sale_offer_translations[$locale]['single_policy']) ? $_sale_offer_translations[$locale]['single_policy'] : ''
															]); ?>
															<?= $this->Form->input('child_policy', [
																'type'  => 'textarea',
																'label' => 'Child Policy',
																'name'  => '_sale_offer[_translations][' . $locale . '][child_policy]',
																'class' => 'form-control ckeditor child_policy',
																'templateVars'  => ['class' => 'inline-full'],
																'id'    => 'sale-offer-translation-child-policy-' . $locale,
																'value' => !empty($_sale_offer_translations[$locale]['child_policy']) ? $_sale_offer_translations[$locale]['child_policy'] : ''
															]); ?>
														</div>
													<?php } ?>
												</div>
											</div>
										</div>
									<?php } ?>


									<?= $this->Form->input('offer_type', [
										'name' => '_sale_offer[offer_type]',
										'class' => 'form-control offer_type',
										'type' => 'select',
										'options' => $offer_types
									]); ?>
									<?= $this->Form->input('max_adults', [
										'name'  => '_sale_offer[max_adults]',
										'id'    => 'sale_offer-max_adults',
										'class' => 'form-control max_adults'
									]); ?>
									<?= $this->Form->input('max_children', [
										'name'  => '_sale_offer[max_children]',
										'id'    => 'sale_offer-max_children',
										'class' => 'form-control max_children'
									]); ?>

									<div class="col-md-3 input-container">
										<label for="child-age-from">Child Age From</label>
										<input type="text" name="_sale_offer[child_age_from]" class="form-control child_age_from" maxlength="255" id="child-age-from" />
									</div>

									<div class="col-md-3 input-container">
										<label for="child-age-to">Child Age To</label>
										<input type="text" name="_sale_offer[child_age_to]" class="form-control child_age_to" maxlength="255" id="child-age-to" />
									</div>

									<?= $this->Form->input('child_free', [
										'name'  => '_sale_offer[child_free]',
										'id'    => 'sale_offer-child_free',
										'class' => 'form-control child_free'
									]); ?>

									<div class="col-md-3 input-container">
										<label for="infant-age-from">Infant Age From</label>
										<input type="text" name="_sale_offer[infant_age_from]" class="form-control" maxlength="255" id="infant-age-from" value="" />
									</div>

									<div class="col-md-3 input-container">
										<label for="infant-age-to">Infant Age To</label>
										<input type="text" name="_sale_offer[infant_age_to]" class="form-control" maxlength="255" id="infant-age-to" />
									</div>

									<?= $this->Form->input('infant_free', [
										'name'  => '_sale_offer[infant_free]',
										'id'    => 'sale_offer-infant_free',
										'class' => 'form-control infant_free'
									]); ?>
									<?= $this->Form->input('deposit', [
										'name'  => '_sale_offer[deposit]',
										'id'    => 'sale_offer-deposit',
										'class' => 'form-control deposit'
									]); ?>
									<?= $this->Form->input('active', [
										'name'  => '_sale_offer[active]',
										'id'    => 'sale_offer-active',
										'class' => 'form-control active'
									]); ?>
								</div>
								<div class="row">
									<div class="col-xs-12">
										<?= $this->Form->button(__('Add Sale Offer'), ['class' => 'btn btn-primary btn-lg']); ?>
									</div>
								</div>
								<?= $this->Form->end(); ?>
							</div>

						</div>
						<script type="text/javascript">
							(function($) {
								$(document).ready(function() {
									// Init Select2 - Basic Single
									$(".select2-single, select.form-error").each(function(index, el) {
										$(this).addClass('form-control').select2({
											placeholder: 'Choose ' + $('label', $(this).parent()).text(),
										});
									});

									// Init Select2 - Basic Multiple
									$(".select2-multiple, select.form-error").each(function(index, el) {
										$(this).addClass('form-control').select2({
											allowClear: true,
											placeholder: 'Choose ' + $('label', $(this).parent()).text(),
										});
									});

									// Click the tab if coming from the allocations page
									if (location.href.indexOf('#offers_tab') > -1) {
										$('a[href="#sale_offers"]').click();
									}

									// Adjust the sale allocation form as per the sale type
									$('select[name="sale_type"]').on('change', function(e) {
										if ($(this).val() == 0) {
											// Hotel
											$('input[name="_sale_offer[deposit]"').prop('checked', false);
											$('input[name="_sale_offer[deposit]"').parent().hide();
										} else {
											$('input[name="_sale_offer[deposit]"').parent().show();
										}
									});

									$('select[name="sale_type"]').trigger('change');

									$('.date').datepicker({
										format: 'YYYY-MM-DD'
									});

									$('.datetime').datetimepicker({
										format: 'YYYY-MM-DD HH:mm'
									});

									$('.ckeditor').each(function(index, el) {
										CKEDITOR.replace($(this).attr('id'));
									});

									$('[name*="slug"]').on('input', function(event) {
										var val = $(this).val();
										val = (typeof getSlug !== "undefined") ? getSlug(val, {
											separator: '-'
										}) : val;
										$(this).val(val);
									});

									$('#category-autocomplete').autocomplete({
										source: "/admin/categories/autocomplete",
										minLength: 2,
										select: function(event, choice) {
											// Reset autocomplete input
											event.preventDefault();
											$('#category-autocomplete').val('');

											var item = choice.item;

											if ($('[data-category-id="' + item.value + '"]').length == 0) {
												// Append category input
												var $element = $("<div data-category-id='" + item.value + "'><i class='fa fa-minus-circle remove-category'></i>&nbsp;" + item.label + "<input type='hidden' name='sale_categories[]' value='" + item.value + "' /></div>");

												$('#sale-categories').append($element);
											}
										}
									});

									$(document).on('click', '.remove-category', function(e) {
										$(this).parent().remove();
									});
								});
							})(jQuery);
						</script>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
