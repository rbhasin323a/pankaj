<?php use Cake\Utility\Inflector; ?>
<header id="topbar">
	<div class="row">
		<div class="col-xs-12">
			<ol class="breadcrumb">
				<li class="crumb-trail"><a href="/<?= $this->request->params['prefix']; ?>/"><?= Inflector::humanize($this->request->params['prefix']); ?></a></li>
				<li class="crumb-active">Birthdays</li>
			</ol>
		</div>
	</div>
	<div class="row">
		<div class="col-xs-12">
			<div class="btn-toolbar mt5">
			</div>
		</div>
	</div>
</header>

<div id="content">
	<div class="tray tray-center">
		<h2 class="text-dark mb15">Birthdays</h2>
		<div class="birthdays index panel">
			<?php if (!empty($users)) { ?>
				<div class="panel-body pn">
					<div class="table-responsive">
						<table cellpadding="0" cellspacing="0" class="table themed-form table-hover theme-info tc-checkbox-1 fs13">
							<thead>
								<tr class="bg-light">
									<th width="1"></th>
									<th><?= $this->Paginator->sort('id'); ?></th>
									<th><?= $this->Paginator->sort('first_name'); ?></th>
									<th><?= $this->Paginator->sort('last_name'); ?></th>
									<th><?= $this->Paginator->sort('birthday'); ?></th>
									<th>Turning</th>
									<th class="actions text-center"><?= __('Actions'); ?></th>
								</tr>
							</thead>
							<tbody>
								<?php foreach ($users as $user) : ?>
									<tr>
										<td width="20">
											<label class="option block mn hidden">
												<input type="checkbox" name="id" value="<?= $user['id'] ?>">
												<span class="checkbox mn"></span>
											</label>
										</td>
										<td width="1"><?= $this->Html->link($user['id'], ['controller' => 'Users', 'action' => 'edit', $user['id']]); ?></td>
										<td><?= h($user['first_name']); ?></td>
										<td><?= h($user['last_name']); ?></td>
										<td><?= h($user['birthday']); ?></td>
										<td><?= h($user['age_one_week_from_now']); ?></td>
										<td class="actions text-center" width="200">
											<div class="btn-group btn-group-sm ib">
												<?= $this->Html->link(__('Edit'), ['controller' => 'Users', 'action' => 'edit', $user['id']], ['class' => 'btn btn-primary']); ?>
												<?= $this->Form->postLink(__('Delete'), ['controller' => 'Users', 'action' => 'delete', $user['id']], ['confirm' => __('Are you sure you want to delete #{0}?', $user['id']), 'class' => 'btn btn-danger']); ?>
											</div>
										</td>
									</tr>
								<?php endforeach; ?>
							</tbody>
						</table>
					</div>
				</div>
			<?php } else { ?>
				<div class="panel-body text-center fs16 lh50 pv20"><i class="fa fa-ban text-danger"></i>&nbsp;&nbsp;No results found.</div>
			<?php } ?>
		</div>
	</div>
</div>
