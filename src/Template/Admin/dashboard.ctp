<section id="content" class="animated fadeIn">
	<div class="tray tray-center">
		<div class="row">
			<div class="col-sm-4 col-xl-3">
				<div class="panel panel-tile text-center br-a br-grey animated fadeInUp">
					<div class="panel-body">
						<h1 class="fs30 mt5 mbn">0</h1>
						<h6 class="text-system">NEW BOOKINGS</h6>
					</div>
					<div class="panel-footer br-t p12">
						<span class="fs11">
							<i class="fa fa-arrow-up pr5"></i> 0% INCREASE
						</span>
					</div>
				</div>
			</div>
			<div class="col-sm-4 col-xl-3">
				<div class="panel panel-tile text-center br-a br-grey animated fadeInUp">
					<div class="panel-body">
						<h1 class="fs30 mt5 mbn"><i class="fa fa-dollar fs28"></i>0</h1>
						<h6 class="text-success">TOTAL SALES GROSS</h6>
					</div>
					<div class="panel-footer br-t p12">
						<span class="fs11">
							<i class="fa fa-arrow-up pr5"></i> 0% INCREASE
						</span>
					</div>
				</div>
			</div>
			<div class="col-sm-3 col-xl-3 visible-xl">
				<div class="panel panel-tile text-center br-a br-grey animated fadeInUp">
					<div class="panel-body">
						<h1 class="fs30 mt5 mbn">0</h1>
						<h6 class="text-danger">UNIQUE VISITS</h6>
					</div>
					<div class="panel-footer br-t p12">
						<span class="fs11">
							<i class="fa fa-arrow-down pr5 text-danger"></i> 0% DECREASE
						</span>
					</div>
				</div>
			</div>
			<div class="col-sm-4 col-xl-3">
				<div class="panel panel-tile text-center br-a br-grey animated fadeInUp">
					<div class="panel-body">
						<h1 class="fs30 mt5 mbn">0</h1>
						<h6 class="text-warning">PENDING PAYMENTS</h6>
					</div>
					<div class="panel-footer br-t p12">
						<span class="fs11">
							<i class="fa fa-arrow-up pr5 text-success"></i> 0% INCREASE
						</span>
					</div>
				</div>
			</div>
		</div>
		<div class="admin-panels fade-onload">
			<div class="row">
				<div class="col-md-6 admin-grid">
					<div class="panel panel-dark" id="p01" data-panel-title="false">
						<div class="panel-heading">
							<span class="panel-title hidden-xs hidden"> Recent Activity</span>
							<ul class="nav panel-tabs panel-tabs-left">
								<li class="active">
									<a href="#tab1_1" data-toggle="tab"> User Activity</a>
								</li>
							</ul>
						</div>
						<div class="panel-body pn">
							<div class="tab-content">
								<div class="tab-pane active p15" id="tab1_1" role="tabpanel" >
									<div class="row">
										<div class="col-md-7 pln br-r mvn15">
											<h5 class="ml5 mt20 ph10 pb5 br-b fw700 hidden">Visitors
												<small class="pull-right fw700">0
													<span class="text-primary">(0 unique)</span>
												</small>
											</h5>
											<div id="ecommerce_chart1" style="height: 300px;"></div>
										</div>
										<div class="col-md-5">
											<h5 class="mt5 mbn ph10 pb5 br-b fw700">Top Referrals
											</h5>
											<table class="table mbn tc-med-1 tc-bold-last tc-fs13-last">
												<thead>
													<tr class="hidden">
														<th>Source</th>
														<th>Count</th>
													</tr>
												</thead>
												<tbody>
													<tr>
														<td>
															<i class="fa fa-circle text-warning fs8 pr10"></i>
															<span>google.com</span>
														</td>
														<td>0</td>
													</tr>
													<tr>
														<td>
															<i class="fa fa-circle text-warning fs8 pr10"></i>
															<span>facebook.com</span>
														</td>
														<td>0</td>
													</tr>
												</tbody>
											</table>
										</div>
										<div class="col-md-4 hidden">
											<h5 class="mt5 ph10 pb5 br-b fw700">Traffic Sources
											</h5>
											<table class="table mbn">
												<thead>
													<tr class="hidden">
														<th>#</th>
														<th>First Name</th>
													</tr>
												</thead>
												<tbody>
													<tr>
														<td class="va-m fw700 text-muted"><span class="flag-xs flag-us mr5 va-b"></span> United States</td>
														<td class="fs15 fw700 text-right">28%</td>
													</tr>
													<tr>
														<td class="va-m fw700 text-muted"><span class="flag-xs flag-tr mr5 va-b"></span> Turkey</td>
														<td class="fs15 fw700 text-right">25%</td>
													</tr>
													<tr>
														<td class="va-m fw700 text-muted"><span class="flag-xs flag-fr mr5 va-b"></span> France</td>
														<td class="fs15 fw700 text-right">22%</td>
													</tr>
													<tr>
														<td class="va-m fw700 text-muted"><span class="flag-xs flag-in mr5 va-b"></span> India</td>
														<td class="fs15 fw700 text-right">18%</td>
													</tr>
													<tr>
														<td class="va-m fw700 text-muted"><span class="flag-xs flag-es mr5 va-b"></span> Spain</td>
														<td class="fs15 fw700 text-right">15%</td>
													</tr>
													<tr>
														<td class="va-m fw700 text-muted"><span class="flag-xs flag-de mr5 va-b"></span> Germany</td>
														<td class="fs15 fw700 text-right">12%</td>
													</tr>
												</tbody>
											</table>
										</div>
									</div>
								</div>
								<div role="tabpanel" class="tab-pane" id="tab1_2">
									<div class="table-responsive">
										<table class="table themed-form theme-warning tc-checkbox-1 fs13">
											<thead>
												<tr class="bg-light">
													<th class="">Image</th>
													<th class="">Product Title</th>
													<th class="">SKU</th>
													<th class="">Price</th>
													<th class="">Stock</th>
													<th class="text-right">Status</th>
												</tr>
											</thead>
											<tbody>
												<tr>
													<td class="w100">
														<img class="img-responsive mw20 ib mr10" title="user" src="/backend/ui/vendor/admindesign/assets/img/stock/products/thumb_1.jpg">
													</td>
													<td class="">Apple Ipod 4G - Silver</td>
													<td class="">#21362</td>
													<td class="">$215</td>
													<td class="">0</td>
													<td class="text-right">
														<div class="btn-group text-right">
															<button type="button" class="btn btn-success br2 btn-xs fs12 dropdown-toggle" data-toggle="dropdown" aria-expanded="false"> Active
																<span class="caret ml5"></span>
															</button>
															<ul class="dropdown-menu" role="menu">
																<li>
																	<a href="javascript:void(0);">Edit</a>
																</li>
																<li>
																	<a href="javascript:void(0);">Delete</a>
																</li>
																<li>
																	<a href="javascript:void(0);">Archive</a>
																</li>
																<li class="divider"></li>
																<li>
																	<a href="javascript:void(0);">Complete</a>
																</li>
																<li class="active">
																	<a href="javascript:void(0);">Pending</a>
																</li>
																<li>
																	<a href="javascript:void(0);">Canceled</a>
																</li>
															</ul>
														</div>
													</td>
												</tr>
												<tr>
													<td class="w100">
														<img class="img-responsive mw20 ib mr10" title="user" src="/backend/ui/vendor/admindesign/assets/img/stock/products/thumb_2.jpg">
													</td>
													<td class="">Apple Smart Watch - 1G</td>
													<td class="">#15262</td>
													<td class="">$455</td>
													<td class="">2,100</td>
													<td class="text-right">
														<div class="btn-group text-right">
															<button type="button" class="btn btn-success br2 btn-xs fs12 dropdown-toggle" data-toggle="dropdown" aria-expanded="false"> Active
																<span class="caret ml5"></span>
															</button>
															<ul class="dropdown-menu" role="menu">
																<li>
																	<a href="javascript:void(0);">Edit</a>
																</li>
																<li>
																	<a href="javascript:void(0);">Delete</a>
																</li>
																<li>
																	<a href="javascript:void(0);">Archive</a>
																</li>
																<li class="divider"></li>
																<li>
																	<a href="javascript:void(0);">Complete</a>
																</li>
																<li class="active">
																	<a href="javascript:void(0);">Pending</a>
																</li>
																<li>
																	<a href="javascript:void(0);">Canceled</a>
																</li>
															</ul>
														</div>
													</td>
												</tr>
												<tr>
													<td class="w100">
														<img class="img-responsive mw20 ib mr10" title="user" src="/backend/ui/vendor/admindesign/assets/img/stock/products/thumb_6.jpg">
													</td>
													<td class="">Apple Macbook 4th Gen - Silver</td>
													<td class="">#66362</td>
													<td class="">$1699</td>
													<td class="">6,100</td>
													<td class="text-right">
														<div class="btn-group text-right">
															<button type="button" class="btn btn-success br2 btn-xs fs12 dropdown-toggle" data-toggle="dropdown" aria-expanded="false"> Active
																<span class="caret ml5"></span>
															</button>
															<ul class="dropdown-menu" role="menu">
																<li>
																	<a href="javascript:void(0);">Edit</a>
																</li>
																<li>
																	<a href="javascript:void(0);">Delete</a>
																</li>
																<li>
																	<a href="javascript:void(0);">Archive</a>
																</li>
																<li class="divider"></li>
																<li>
																	<a href="javascript:void(0);">Complete</a>
																</li>
																<li class="active">
																	<a href="javascript:void(0);">Pending</a>
																</li>
																<li>
																	<a href="javascript:void(0);">Canceled</a>
																</li>
															</ul>
														</div>
													</td>
												</tr>
												<tr>
													<td class="w100">
														<img class="img-responsive mw20 ib mr10" title="user" src="/backend/ui/vendor/admindesign/assets/img/stock/products/thumb_7.jpg">
													</td>
													<td class="">Apple Iphone 16GB - Silver</td>
													<td class="">#51362</td>
													<td class="">$1299</td>
													<td class="">5,200</td>
													<td class="text-right">
														<div class="btn-group text-right">
															<button type="button" class="btn btn-success br2 btn-xs fs12 dropdown-toggle" data-toggle="dropdown" aria-expanded="false"> Active
																<span class="caret ml5"></span>
															</button>
															<ul class="dropdown-menu" role="menu">
																<li>
																	<a href="javascript:void(0);">Edit</a>
																</li>
																<li>
																	<a href="javascript:void(0);">Delete</a>
																</li>
																<li>
																	<a href="javascript:void(0);">Archive</a>
																</li>
																<li class="divider"></li>
																<li>
																	<a href="javascript:void(0);">Complete</a>
																</li>
																<li class="active">
																	<a href="javascript:void(0);">Pending</a>
																</li>
																<li>
																	<a href="javascript:void(0);">Canceled</a>
																</li>
															</ul>
														</div>
													</td>
												</tr>
											</tbody>
										</table>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="panel panel-dark" id="p18">
						<div class="panel-heading">
							<span class="panel-title">Browser List</span>
						</div>
						<div class="panel-body pn">
							<table class="table mbn tc-med-1 tc-bold-2">
								<tbody>
									<tr>
										<td><span class="favicons chrome va-t mr10"></span>Chrome</td>
										<td>0%</td>
									</tr>
									<tr>
										<td><span class="favicons firefox va-t mr10"></span>Firefox</td>
										<td>0%</td>
									</tr>
									<tr>
										<td><span class="favicons ie va-t mr10"></span>Internet Explorer</td>
										<td>0%</td>
									</tr>
									<tr>
										<td><span class="favicons safari va-t mr10"></span>Safari</td>
										<td>0%</td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
				</div>
				<div class="col-md-6 admin-grid">
					<div class="panel panel-dark" id="p6">
						<div class="panel-heading">
							<span class="panel-title">Social network logins</span>
						</div>
						<div class="panel-body pn">
							<div class="row table-layout">
								<div class="col-xs-5 va-m">
									<div style="width: 100%; height: 197px; margin: 0 auto"></div>
								</div>
								<div class="col-xs-7 br-l pn">
									<table class="table mbn tc-med-1 tc-bold-last">
										<tbody>
											<tr>
												<td><span class="fa fa-circle text-danger fs14 mr10"></span>Google</td>
												<td>0%</td>
											</tr>
											<tr>
												<td><span class="fa fa-circle text-primary fs14 mr10"></span>Facebook</td>
												<td>0%</td>
											</tr>
										</tbody>
									</table>
								</div>
							</div>
						</div>
					</div>
					<div class="panel panel-dark" id="p16">
						<div class="panel-heading">
							<span class="panel-title">Country List</span>
						</div>
						<div class="panel-body pn">
							<table class="table mbn tc-med-1 tc-bold-last">
								<thead>
									<tr class="hidden">
										<th>#</th>
										<th>First Name</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td><span class="flag-xs flag-us mr5 va-b"></span>United States</td>
										<td>0%</td>
									</tr>
									<tr>
										<td><span class="flag-xs flag-de mr5 va-b"></span>Germany</td>
										<td>0%</td>
									</tr>
									<tr>
										<td><span class="flag-xs flag-fr mr5 va-b"></span>France</td>
										<td>0%</td>
									</tr>
									<tr>
										<td><span class="flag-xs flag-tr mr5 va-b"></span>Turkey</td>
										<td>0%</td>
									</tr>
									<tr>
										<td><span class="flag-xs flag-es mr5 va-b"></span>Spain</td>
										<td>0%</td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
