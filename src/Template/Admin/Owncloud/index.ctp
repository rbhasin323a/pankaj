<div id="content">
	<div class="tray tray-center">
		<div class="index panel">
			<div class="panel-body">
				<div class="row">
					<div class="col-xs-12"><div style="margin: -14px;">
						<iframe src="/admin/owncloud/files/" width="100%" height="100%" frameborder="none" id="owncloud-wrap"></iframe>
					</div></div>
					<script type="text/javascript">
						(function($) {
							$(window).load(function() {
								$('#owncloud-wrap').height($('body').height() - 200);
							});
							$(window).resize(function() {
								$('#owncloud-wrap').height($('body').height() - 200);
							});
						})(jQuery);
					</script>
				</div>
			</div>
		</div>
	</div>
</div>
