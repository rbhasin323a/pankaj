<?php use Cake\Utility\Inflector; ?>
<header id="topbar">
	<div class="row">
		<div class="col-xs-12">
			<ol class="breadcrumb">
				<li class="crumb-trail"><a href="/<?= $this->request->params['prefix']; ?>/"><?= Inflector::humanize($this->request->params['prefix']); ?></a></li>
				<li class="crumb-trail"><a href="/<?= $this->request->params['prefix'] . '/' . $this->request->params['controller']; ?>">Roles</a></li>
				<li class="crumb-active">Edit</li>
			</ol>
		</div>
	</div>
	<div class="row">
		<div class="col-xs-12">
			<div class="btn-toolbar mt5">
				<div class="btn-group btn-group-sm">
					<?= $this->Html->link(__('List Roles'), ['action' => 'index'], ['class' => 'btn btn-primary']); ?>
				</div>
			</div>
		</div>
	</div>
</header>

<div id="content">
	<div class="tray tray-center">
		<div class="mw1000">
			<div class="roles form">
				<div class="panel panel-dark">
					<div class="panel-heading">
						<span class="panel-title fw700"><?= __('Edit role'); ?></span>
					</div>
					<div class="panel-body">
						<div class="row">
							<div class="col-xs-12">
								<button class="btn btn-m btn-warning pull-right m10" id="unselect-all">Unselect all</button>
								<button class="btn btn-m btn-primary pull-right m10" id="select-all">Select all</button>
							</div>
						</div>
						<?= $this->Form->create($role, ['class' => 'form', 'novalidate' => true]); ?>
						<div class="row">
							<div class="well permission">
								<?php if ($permissions) { ?>
									<?php foreach ($permissions as $permission) { ?>
										<div class="checkbox">
											<label for="">
												<?php if (in_array($permission, (array)$role_permissions['access'])) { ?>
													<input type="checkbox" name="permissions[access][]" value="<?= $permission; ?>" checked="checked">

												<?php } else { ?>
													<input type="checkbox" name="permissions[access][]" value="<?= $permission; ?>">
												<?php } ?>
												<?= $permission; ?>
											</label>
										</div>
									<?php } ?>
								<?php } ?>
							</div>
						</div>
						<div class="row">
							<div class="col-xs-12">
								<?= $this->Form->button(__('Submit'), ['class' => 'btn btn-primary btn-lg']); ?>
							</div>
						</div>
						<?= $this->Form->end(); ?>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<script>
	$(document).ready(function() {
		$('#select-all').on('click', function() {
			$('.permission :checkbox').prop('checked', true);
		});

		$('#unselect-all').on('click', function() {
			$('.permission :checkbox').prop('checked', false);
		});
	});
</script>
