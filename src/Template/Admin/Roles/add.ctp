<?php use Cake\Utility\Inflector; ?>
<header id="topbar">
	<div class="row">
		<div class="col-xs-12">
			<ol class="breadcrumb">
				<li class="crumb-trail"><a href="/<?= $this->request->params['prefix']; ?>/"><?= Inflector::humanize($this->request->params['prefix']); ?></a></li>
				<li class="crumb-trail"><a href="/<?= $this->request->params['prefix'] . '/' . $this->request->params['controller']; ?>">roles</a></li>
				<li class="crumb-active">Add</li>
			</ol>
		</div>
	</div>
	<div class="row">
		<div class="col-xs-12">
			<div class="btn-toolbar mt5">
				<div class="btn-group btn-group-sm">
					<?= $this->Html->link(__('List roles'), ['action' => 'index'], ['class' => 'btn btn-primary']); ?>
				</div>
			</div>
		</div>
	</div>
</header>

<div id="content">
	<div class="tray tray-center">
		<div class="mw1000">
			<div class="roles form">
				<div class="panel panel-dark">
					<div class="panel-heading">
						<span class="panel-title fw700"><?= __('Add role'); ?></span>
					</div>
					<div class="panel-body">
						<?= $this->Form->create($role, ['class' => 'form', 'novalidate' => true]); ?>
						<div class="row">
							<div class="col-xs-12">
								<?= $this->Form->input('name', ['class' => 'form-control name']); ?>
							</div>
						</div>
						<div class="row">
							<div class="col-xs-12">
								<?= $this->Form->button(__('Submit'), ['class' => 'btn btn-primary btn-lg mt10']); ?>
							</div>
						</div>
						<?= $this->Form->end(); ?>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
