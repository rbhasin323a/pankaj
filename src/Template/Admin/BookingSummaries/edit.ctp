<?php use Cake\Utility\Inflector; ?>
<header id="topbar">
	<div class="row">
		<div class="col-xs-12">
			<ol class="breadcrumb">
				<li class="crumb-trail"><a href="/<?= $this->request->params['prefix']; ?>/"><?= Inflector::humanize($this->request->params['prefix']); ?></a></li>
				<li class="crumb-trail"><a href="/<?= $this->request->params['prefix'] . '/' . $this->request->params['controller']; ?>">Booking Summaries</a></li>
				<li class="crumb-active">Edit</li>
			</ol>
		</div>
	</div>
	<div class="row">
		<div class="col-xs-12">
			<div class="btn-toolbar mt5">
				<div class="btn-group btn-group-sm">
					<?= $this->Html->link(__('List Booking Summaries'), ['action' => 'index'], ['class' => 'btn btn-primary']); ?>
				</div>
				<div class="btn-group btn-group-sm">
					<?= $this->Html->link(__('List Bookings'), ['controller' => 'Bookings', 'action' => 'index'], ['class' => 'btn btn-default bg-white']); ?>
					<?= $this->Html->link(__('New Booking'), ['controller' => 'Bookings', 'action' => 'add'], ['class' => 'btn btn-default bg-white']); ?>
				</div>
				<div class="btn-group btn-group-sm">
					<?= $this->Html->link(__('List Sales'), ['controller' => 'Sales', 'action' => 'index'], ['class' => 'btn btn-default bg-white']); ?>
					<?= $this->Html->link(__('New Sale'), ['controller' => 'Sales', 'action' => 'add'], ['class' => 'btn btn-default bg-white']); ?>
				</div>
			</div>
		</div>
	</div>
</header>

<div id="content">
	<div class="tray tray-center">
		<div class="mw1000">
			<div class="bookingSummaries form">
				<div class="panel panel-dark">
					<div class="panel-heading">
						<?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $bookingSummary->id], ['confirm' => __('Are you sure you want to delete #{0}?', $bookingSummary->id), 'class' => 'btn btn-sm btn-danger pull-right m10']); ?>
						<span class="panel-title fw700"><?= __('Edit Booking Summary'); ?></span>
					</div>
					<div class="panel-body">
						<?= $this->Form->create($bookingSummary, ['class' => 'form', 'novalidate' => true]); ?>
						<div class="row">
							<?php
							$this->Form->templates([
								'inputContainer'        => '<div class="col-md-6 input-container">{{content}}</div>',
								'inputContainerError'   => '<div class="col-md-6 input-container">{{content}}</div>',

								'label'                 => '<label {{attrs}}>{{text}}</label>{{hidden}}{{input}}',

								'formGroup'             => '{{label}}{{input}}{{error}}',
								'nestingLabel'          => '<div class="radio-custom square radio-primary mt10">{{input}} <label {{attrs}}>{{text}}</label></div>',

								// Input
								'input'                 => '<input type="{{type}}" name="{{name}}" {{attrs}} class="form-control" />',

								// Textarea
								'textarea'              => '<textarea name="{{name}}" {{attrs}} class="form-control">{{value}}</textarea>',

								// Select
								'select'                => '<select name="{{name}}" {{attrs}} class="form-control select2-single">{{content}}</select>',
								'selectMultple'         => '<select multiple name="{{name}}" {{attrs}} class="form-control select2-multiple">{{content}}</select>',

								'dateWidget'            => '{{day}}{{month}}{{year}}{{hour}}{{minute}}{{second}}',

								// Error
								'error'                 => '<div class="text-danger">{{content}}</div>',
								'errorList'             => '{{content}}',
								'errorItem'             => '{{text}}',
							]);
							?>
							<?= $this->Form->input('booking_id', [
								'options' => $bookings,
							]); ?>
							<?= $this->Form->input('sale_id', [
								'options' => $sales,
								'empty' => true,
							]); ?>
							<?= $this->Form->input('contractor_id', [
								'options' => $contractors,
								'empty' => true,
							]); ?>
							<?= $this->Form->input('sale_offer_id', [
								'options' => $saleOffers,
								'empty' => true,
							]); ?>
							<?= $this->Form->input('departure_id', [
								'options' => $departures,
								'empty' => true,
							]); ?>
							<?= $this->Form->input('user_id', [
								'options' => $users,
								'empty' => true,
							]); ?>
							<?= $this->Form->input('transaction_id', [
								'options' => $transactions,
								'empty' => true,
							]); ?>
							<?= $this->Form->input('currency_id', [
								'options' => $currencies,
								'empty' => true,
							]); ?>
							<?= $this->Form->input('company_id', [
								'options' => $companies,
								'empty' => true,
							]); ?>
							<?= $this->Form->input('supplier_id', [
								'options' => $suppliers,
								'empty' => true,
							]); ?>
							<?= $this->Form->input('adults', ['class' => 'form-control adults']); ?>
							<?= $this->Form->input('children', ['class' => 'form-control children']); ?>
							<?= $this->Form->input('infants', ['class' => 'form-control infants']); ?>
							<?= $this->Form->input('county', ['class' => 'form-control county']); ?>
							<?= $this->Form->input('pax', ['class' => 'form-control pax']); ?>
							<?= $this->Form->input('date_booked', ['class' => 'form-control date_booked']); ?>
							<?= $this->Form->input('check_in', ['class' => 'form-control check_in']); ?>
							<?= $this->Form->input('check_out', ['class' => 'form-control check_out']); ?>
							<?= $this->Form->input('no_nights', ['class' => 'form-control no_nights']); ?>
							<?= $this->Form->input('rooms', ['class' => 'form-control rooms']); ?>
							<?= $this->Form->input('territory', ['class' => 'form-control territory']); ?>
							<?= $this->Form->input('total_sell_rate_in_currency', ['class' => 'form-control total_sell_rate_in_currency']); ?>
							<?= $this->Form->input('rate_to_gbp', ['class' => 'form-control rate_to_gbp']); ?>
							<?= $this->Form->input('total_sell_rate', ['class' => 'form-control total_sell_rate']); ?>
							<?= $this->Form->input('commission_ex_vat', ['class' => 'form-control commission_ex_vat']); ?>
							<?= $this->Form->input('vat_on_commission', ['class' => 'form-control vat_on_commission']); ?>
							<?= $this->Form->input('gross_commission', ['class' => 'form-control gross_commission']); ?>
							<?= $this->Form->input('total_net_rate', ['class' => 'form-control total_net_rate']); ?>
							<?= $this->Form->input('customer_total_price', ['class' => 'form-control customer_total_price']); ?>
							<?= $this->Form->input('customer_payment', ['class' => 'form-control customer_payment']); ?>
							<?= $this->Form->input('credits_used', ['class' => 'form-control credits_used']); ?>
							<?= $this->Form->input('credit_amount_deductible_from_commission', ['class' => 'form-control credit_amount_deductible_from_commission']); ?>
							<?= $this->Form->input('booking_fee_net_rate', ['class' => 'form-control booking_fee_net_rate']); ?>
							<?= $this->Form->input('vat_on_booking_fee', ['class' => 'form-control vat_on_booking_fee']); ?>
							<?= $this->Form->input('booking_fee', ['class' => 'form-control booking_fee']); ?>
							<?= $this->Form->input('payment_type', ['class' => 'form-control payment_type']); ?>
							<?= $this->Form->input('payment_surcharge_net_rate', ['class' => 'form-control payment_surcharge_net_rate']); ?>
							<?= $this->Form->input('vat_on_payment_surcharge', ['class' => 'form-control vat_on_payment_surcharge']); ?>
							<?= $this->Form->input('payment_surcharge', ['class' => 'form-control payment_surcharge']); ?>
							<?= $this->Form->input('type', ['class' => 'form-control type']); ?>
							<?= $this->Form->input('top_discount', ['class' => 'form-control top_discount']); ?>
							<?= $this->Form->input('total_room_nights', ['class' => 'form-control total_room_nights']); ?>
							<?= $this->Form->input('impulse', ['class' => 'form-control impulse']); ?>
							<?= $this->Form->input('notes', ['class' => 'form-control notes']); ?>
							<?= $this->Form->input('gross_profit', ['class' => 'form-control gross_profit']); ?>
							<?= $this->Form->input('destination_name', ['class' => 'form-control destination_name']); ?>
							<?= $this->Form->input('destination_type', ['class' => 'form-control destination_type']); ?>
							<?= $this->Form->input('post_code', ['class' => 'form-control post_code']); ?>
							<?= $this->Form->input('country', ['class' => 'form-control country']); ?>
							<?= $this->Form->input('division', ['class' => 'form-control division']); ?>
							<?= $this->Form->input('city', ['class' => 'form-control city']); ?>
							<?= $this->Form->input('city_district', ['class' => 'form-control city_district']); ?>
							<?= $this->Form->input('total_custom_tax', ['class' => 'form-control total_custom_tax']); ?>
						</div>
						<div class="row">
							<div class="col-xs-12">
								<?= $this->Form->button(__('Submit'), ['class' => 'btn btn-primary btn-lg']); ?>
							</div>
						</div>
						<?= $this->Form->end(); ?>

						<script type="text/javascript">
							(function($) {
								$(document).ready(function() {
									// Init Select2 - Basic Single
									$(".select2-single, select.form-error").each(function(index, el) {
										$(this).addClass('form-control').select2({
											placeholder: 'Choose ' + $('label', $(this).parent()).text(),
										});
									});

									// Init Select2 - Basic Multiple
									$(".select2-multiple, select.form-error").each(function(index, el) {
										$(this).addClass('form-control').select2({
											allowClear: true,
											placeholder: 'Choose ' + $('label', $(this).parent()).text(),
										});
									});

									$('.date').datepicker({
										format: 'YYYY-MM-DD'
									});

									$('.datetime').datetimepicker({
										format: 'YYYY-MM-DD HH:mm'
									});
								});
							})(jQuery);
						</script>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
