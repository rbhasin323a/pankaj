<?php use Cake\Utility\Inflector; ?>
<header id="topbar">
    <div class="row">
        <div class="col-xs-12">
            <ol class="breadcrumb">
                <li class="crumb-trail"><a href="/<?= $this->request->params['prefix']; ?>/"><?= Inflector::humanize($this->request->params['prefix']); ?></a></li>
                <li class="crumb-active">Categories</li>
            </ol>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12">
            <div class="btn-toolbar mt5">
                <div class="btn-group btn-group-sm">
                    <?= $this->Html->link(__('New Category'), ['action' => 'add'], ['class' => 'btn btn-primary']); ?>
                </div>
            </div>
        </div>
    </div>
</header>

<div id="content">
    <div class="tray tray-center">
        <h2 class="text-dark mb15">Category Links</h2>
        <div class="categories index panel">
            <div class="panel-body">
                <?php foreach ($categories as $category) { ?>
                    <div class="col-xs-6">
                        <label for="category-autocomplete"><?= $category->title; ?></label>
                        <div class="well" style="height: 200px; overflow: auto;">
                        <?php if (!empty($category->sale_to_category)) { ?>
                            <ul>
                            <?php foreach ($category->sale_to_category as $sale_to_category) { ?>
                                <li><a href="/admin/sales/edit/<?= $sale_to_category->sale->id; ?>"><?= $sale_to_category->sale->title; ?></a></li>
                            <?php } ?>
                            </ul>
                        <?php } ?>
                        </div>
                    </div>
                <?php } ?>
            </div>
        </div>
    </div>
</div>