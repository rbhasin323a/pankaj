<?php use Cake\Utility\Inflector; ?>
<header id="topbar">
	<div class="row">
		<div class="col-xs-12">
			<ol class="breadcrumb">
				<li class="crumb-trail"><a href="/<?= $this->request->params['prefix']; ?>/"><?= Inflector::humanize($this->request->params['prefix']); ?></a></li>
				<li class="crumb-trail"><a href="/<?= $this->request->params['prefix'] . '/' . $this->request->params['controller']; ?>">Categories</a></li>
				<li class="crumb-active">Edit</li>
			</ol>
		</div>
	</div>
	<div class="row">
		<div class="col-xs-12">
			<div class="btn-toolbar mt5">
				<div class="btn-group btn-group-sm">
					<?= $this->Html->link(__('List Linked Sales'), ['action' => 'links'], ['class' => 'btn btn-primary']); ?>
				</div>
			</div>
		</div>
	</div>
</header>

<div id="content">
	<div class="tray tray-center">
		<div class="mw1000">
			<div class="bookings form">
				<div class="panel panel-dark">
					<div class="panel-heading">
						<?php if (!empty($category->id)) { ?>
							<?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $category->id], ['confirm' => __('Are you sure you want to delete #{0}?', $category->id), 'class' => 'btn btn-sm btn-danger pull-right m10']); ?>
							<span class="panel-title fw700"><?= __('Edit Category'); ?></span>
						<?php } ?>
					</div>
					<div class="panel-body">
						<div id="category_details" class="tab-pane active">
							<?= $this->Form->create($category, ['class' => 'form', 'novalidate' => true, 'enctype' => 'multipart/form-data']); ?>
							<div class="row">
								<?php $this->Form->templates([
									'inputContainer'        => '<div class="col-xs-6 input-container">{{content}}</div>',
									'inputContainerError'   => '<div class="col-xs-6 input-container">{{content}}</div>',

									'label'                 => '<label {{attrs}}>{{text}}</label>{{hidden}}{{input}}',

									'formGroup'             => '{{label}}{{input}}{{error}}',
									'nestingLabel'          => '<div class="radio-custom square radio-primary mt10">{{input}} <label {{attrs}}>{{text}}</label></div>',

									// Input
									'input'                 => '<input type="{{type}}" name="{{name}}" {{attrs}} class="form-control" />',

									// Textarea
									'textarea'              => '<textarea name="{{name}}" {{attrs}} class="form-control">{{value}}</textarea>',

									// Select
									'select'                => '<select name="{{name}}" {{attrs}} class="form-control select2-single">{{content}}</select>',
									'selectMultple'         => '<select multiple name="{{name}}" {{attrs}} class="form-control select2-multiple">{{content}}</select>',

									'dateWidget'            => '{{day}}{{month}}{{year}}{{hour}}{{minute}}{{second}}',

									// Error
									'error'                 => '<div class="text-danger">{{content}}</div>',
									'errorList'             => '{{content}}',
									'errorItem'             => '{{text}}',
								]);
								?>

								<?php if (!empty($vd['languages'])) { ?>
									<div class="col-xs-12">
										<div class="tab-block" style="margin:0 -16px 25px">
											<ul class="nav nav-tabs">
												<?php $i = 0;
												foreach ($vd['languages'] as $language) {
													$i++; ?>
													<li <?= ($i == 1) ? 'class="active"' : ''; ?>><a href="#category_tab_language_<?= $language['id']; ?>" data-toggle="tab">
															<img src="/ui/img/flags/24/<?= $language['code']; ?>.png" />&nbsp;<?= $language['name']; ?>
														</a></li>
												<?php } ?>
											</ul>
											<div class="tab-content">
												<?php $i = 0;
												foreach ($vd['languages'] as $language) {
													$i++; ?>
													<?php $locale = $language['locale']; ?>
													<div id="category_tab_language_<?= $language['id']; ?>" class="tab-pane <?= ($i == 1) ? 'active' : ''; ?>">
														<?= $this->Form->input('title', [
															'type'  => 'text',
															'label' => 'Title',
															'name'  => '_translations[' . $locale . '][title]',
															'class' => 'form-control title',
															'id'    => 'translation-title-' . $locale,
															'value' => !empty($_translations[$locale]['title']) ? $_translations[$locale]['title'] : ''
														]); ?>
													</div>
												<?php } ?>
											</div>
										</div>
									</div>
								<?php } ?>
								<div class="col-xs-12">
									<?= $this->Form->input('columns', ['type' => 'text', 'class' => 'form-control']); ?>

									<?= $this->Form->input('priority', ['type' => 'text', 'class' => 'form-control']); ?>
								</div>
								<div class="col-xs-12">
									<?= $this->Form->input('image', ['type' => 'file', 'label' => 'Upload Image']); ?>
								</div>
								<div class="col-xs-12">
									<?php if (!empty($category->image_path)) { ?>
										<img src="/files/images/<?= rawurlencode($category->image_path); ?>" class="mn thumbnail img-responsive" />
									<?php } ?>
								</div>
							</div>
							<div class="row mt10">
								<div class="col-xs-12">
									<?= $this->Form->button(__('Submit'), ['class' => 'btn btn-primary btn-lg']); ?>
								</div>
							</div>
							<?= $this->Form->end(); ?>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
