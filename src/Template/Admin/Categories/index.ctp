<?php use Cake\Utility\Inflector; ?>
<header id="topbar">
	<div class="row">
		<div class="col-xs-12">
			<ol class="breadcrumb">
				<li class="crumb-trail"><a href="/<?= $this->request->params['prefix']; ?>/"><?= Inflector::humanize($this->request->params['prefix']); ?></a></li>
				<li class="crumb-active">Categories</li>
			</ol>
		</div>
	</div>
	<div class="row">
		<div class="col-xs-12">
			<div class="btn-toolbar mt5">
				<div class="btn-group btn-group-sm">
					<?= $this->Html->link(__('New Category'), ['action' => 'add'], ['class' => 'btn btn-primary']); ?>
				</div>
			</div>
		</div>
	</div>
</header>

<div id="content">
	<div class="tray tray-center">
		<h2 class="text-dark mb15">Categories</h2>
		<div class="categories index panel">
			<?php if (!empty($categories->count())) { ?>
				<div class="panel-body pn">
					<div class="table-responsive">
						<table cellpadding="0" cellspacing="0" class="table themed-form table-hover theme-info tc-checkbox-1 fs13">
							<thead>
								<tr class="bg-light">
									<th width="1"></th>
									<th width="1"><?= $this->Paginator->sort('id'); ?></th>
									<th><?= $this->Paginator->sort('title'); ?></th>
									<th><?= __('Columns'); ?></th>
									<th><?= $this->Paginator->sort('priority'); ?></th>
									<th class="actions text-center"><?= __('Actions'); ?></th>
								</tr>
							</thead>
							<tbody>
								<?php foreach ($categories as $category) : ?>
									<tr>
										<td width="20">
											<label class="option block mn hidden">
												<input type="checkbox" name="id" value="<?= $category->id ?>">
												<span class="checkbox mn"></span>
											</label>
										</td>
										<td><b><?= $this->Html->link($category->id, ['action' => 'edit', $category->id]); ?></b></td>
										<td><?= h($category->title); ?></td>
										<td><?= h($category->columns); ?></td>
										<td><?= h($category->priority); ?></td>
										<td class="actions text-center" width="250">
											<div class="btn-group btn-group-sm ib">
												<?= $this->Html->link(__('Edit'), ['action' => 'edit', $category->id], ['class' => 'btn btn-primary']); ?>
												<?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $category->id], ['confirm' => __('Are you sure you want to delete #{0}?', $category->id), 'class' => 'btn btn-danger']); ?>
											</div>
										</td>
									</tr>
								<?php endforeach; ?>
							</tbody>
						</table>
					</div>
				</div>
				<div class="panel-footer">
					<div class="paginator">
						<ul class="pagination mn va-t">
							<?= $this->Paginator->prev(__('previous')); ?>
							<?= $this->Paginator->numbers(); ?>
							<?= $this->Paginator->next(__('next')); ?>
						</ul>
						<div class="ib m5 mh20 va-t"><?= $this->Paginator->counter(); ?></div>
					</div>
				</div>
			<?php } else { ?>
				<div class="panel-body text-center fs16 lh50 pv20"><i class="fa fa-ban text-danger"></i>&nbsp;&nbsp;No results found.</div>
			<?php } ?>
			<script type="text/javascript">
				(function($) {
					$(document).ready(function() {
						if ($("#filter-period").length) {
							$('#filter-period').multiselect({
								buttonClass: 'btn btn-default btn-sm ph15',
								dropRight: true
							});
						}
					});
				})(jQuery);
			</script>
		</div>
	</div>
</div>
