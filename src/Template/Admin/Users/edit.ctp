<?php use Cake\Utility\Inflector; ?>
<header id="topbar">
	<div class="row">
		<div class="col-xs-12">
			<ol class="breadcrumb">
				<li class="crumb-trail"><a href="/<?= $this->request->params['prefix']; ?>/"><?= Inflector::humanize($this->request->params['prefix']); ?></a></li>
				<li class="crumb-trail"><a href="/<?= $this->request->params['prefix'] . '/' . $this->request->params['controller']; ?>">Users</a></li>
				<li class="crumb-active">Edit</li>
			</ol>
		</div>
	</div>
	<div class="row">
		<div class="col-xs-12">
			<div class="btn-toolbar mt5">
				<div class="btn-group btn-group-sm">
					<?= $this->Html->link(__('List Users'), ['action' => 'index'], ['class' => 'btn btn-primary']); ?>
				</div>
			</div>
		</div>
	</div>
</header>

<div id="content">
	<div class="tray tray-center">
		<div class="mw1000">
			<div class="users form">
				<div class="panel panel-dark">
					<div class="panel-heading">
						<?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $user->id], ['confirm' => __('Are you sure you want to delete #{0}?', $user->id), 'class' => 'btn btn-sm btn-danger pull-right m10']); ?>
						<span class="panel-title fw700"><?= __('Edit User'); ?></span>
					</div>
					<div class="panel-body">
						<?= $this->Form->create($user, ['class' => 'form', 'novalidate' => true]); ?>
						<div class="row">
							<?php
							$this->Form->templates([
								'inputContainer'        => '<div class="col-md-6 input-container">{{content}}</div>',
								'inputContainerError'   => '<div class="col-md-6 input-container">{{content}}</div>',

								'label'                 => '<label {{attrs}}>{{text}}</label>{{hidden}}{{input}}',

								'formGroup'             => '{{label}}{{input}}{{error}}',
								'nestingLabel'          => '<div class="radio-custom square radio-primary mt10">{{input}} <label {{attrs}}>{{text}}</label></div>',

								// Input
								'input'                 => '<input type="{{type}}" name="{{name}}" {{attrs}} class="form-control" />',

								// Textarea
								'textarea'              => '<textarea name="{{name}}" {{attrs}} class="form-control">{{value}}</textarea>',

								// Select
								'select'                => '<select name="{{name}}" {{attrs}} class="form-control select2-single">{{content}}</select>',
								'selectMultple'         => '<select multiple name="{{name}}" {{attrs}} class="form-control select2-multiple">{{content}}</select>',

								'dateWidget'            => '{{day}}{{month}}{{year}}{{hour}}{{minute}}{{second}}',

								// Error
								'error'                 => '<div class="text-danger">{{content}}</div>',
								'errorList'             => '{{content}}',
								'errorItem'             => '{{text}}',
							]);
							?>
							<?= $this->Form->input('first_name', ['class' => 'form-control first_name']); ?>
							<?= $this->Form->input('last_name', ['class' => 'form-control last_name']); ?>
							<?= $this->Form->input('email', ['class' => 'form-control email']); ?>
							<?= $this->Form->input('birthday', ['label' => 'Date of Birth', 'class' => 'form-control date']); ?>
							<?= $this->Form->input('address_1', ['class' => 'form-control address_1']); ?>
							<?= $this->Form->input('address_2', ['class' => 'form-control address_2']); ?>
							<?= $this->Form->input('postcode', ['class' => 'form-control postcode']); ?>
							<?= $this->Form->input('city', ['class' => 'form-control city']); ?>
							<?= $this->Form->input('country', ['class' => 'form-control country_id']); ?>
							<?= $this->Form->input('active', ['type' => 'checkbox']); ?>
							<?= $this->Form->input('newsletter', ['type' => 'checkbox']); ?>
							<div class="col-md-6 input-container">
								<label for="role">User Role</label>
								<?= $this->Form->select('role', $user_roles, ['class' => 'form-control role']); ?>
							</div>
							<?= $this->Form->input('phone_home', ['class' => 'form-control phone_home']); ?>
							<?= $this->Form->input('phone_mobile', ['class' => 'form-control phone_mobile']); ?>
						</div>
						<div class="row">
							<div class="col-xs-12">
								<?= $this->Form->button(__('Submit'), ['class' => 'btn btn-primary btn-lg']); ?>
							</div>
						</div>
						<?= $this->Form->end(); ?>

						<script type="text/javascript">
							(function($) {
								$(document).ready(function() {
									// Init Select2 - Basic Single
									$(".select2-single, select.form-error").each(function(index, el) {
										$(this).addClass('form-control').select2({
											placeholder: 'Choose ' + $('label', $(this).parent()).text(),
										});
									});

									// Init Select2 - Basic Multiple
									$(".select2-multiple, select.form-error").each(function(index, el) {
										$(this).addClass('form-control').select2({
											allowClear: true,
											placeholder: 'Choose ' + $('label', $(this).parent()).text(),
										});
									});

									$('.date').datetimepicker({
										format: 'YYYY-MM-DD'
									});

									$('.datetime').datetimepicker({
										format: 'YYYY-MM-DD HH:mm'
									});
								});
							})(jQuery);
						</script>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
