<?php use Cake\Utility\Inflector; ?>
<header id="topbar">
	<div class="row">
		<div class="col-xs-12">
			<ol class="breadcrumb">
				<li class="crumb-trail"><a href="/<?= $this->request->params['prefix']; ?>/"><?= Inflector::humanize($this->request->params['prefix']); ?></a></li>
				<li class="crumb-trail"><a href="/<?= $this->request->params['prefix'] . '/' . $this->request->params['controller']; ?>">Users</a></li>
				<li class="crumb-active">Add</li>
			</ol>
		</div>
	</div>
	<div class="row">
		<div class="col-xs-12">
			<div class="btn-toolbar mt5">
				<div class="btn-group btn-group-sm">
					<?= $this->Html->link(__('List Users'), ['action' => 'index'], ['class' => 'btn btn-primary']); ?>
				</div>
			</div>
		</div>
	</div>
</header>

<div id="content">
	<div class="tray tray-center">
		<div class="mw1000">
			<div class="users form">
				<div class="panel panel-dark">
					<div class="panel-heading">
						<span class="panel-title fw700"><?= __('Add User'); ?></span>
					</div>
					<div class="panel-body">
						<?= $this->Form->create($user, ['class' => 'form', 'novalidate' => true]); ?>
						<div class="row">
							<?php
							$this->Form->templates([
								'inputContainer'        => '<div class="col-md-6 input-container">{{content}}</div>',
								'inputContainerError'   => '<div class="col-md-6 input-container">{{content}}</div>',

								'label'                 => '<label {{attrs}}>{{text}}</label>{{hidden}}{{input}}',

								'formGroup'             => '{{label}}{{input}}{{error}}',
								'nestingLabel'          => '<div class="radio-custom square radio-primary mt10">{{input}} <label {{attrs}}>{{text}}</label></div>',

								// Input
								'input'                 => '<input type="{{type}}" name="{{name}}" {{attrs}} class="form-control" />',

								// Textarea
								'textarea'              => '<textarea name="{{name}}" {{attrs}} class="form-control">{{value}}</textarea>',

								// Select
								'select'                => '<select name="{{name}}" {{attrs}} class="form-control select2-single">{{content}}</select>',
								'selectMultple'         => '<select multiple name="{{name}}" {{attrs}} class="form-control select2-multiple">{{content}}</select>',

								'dateWidget'            => '{{day}}{{month}}{{year}}{{hour}}{{minute}}{{second}}',

								// Error
								'error'                 => '<div class="text-danger">{{content}}</div>',
								'errorList'             => '{{content}}',
								'errorItem'             => '{{text}}',
							]);
							?>
							<?= $this->Form->input('first_name', ['class' => 'form-control first_name required']); ?>
							<?= $this->Form->input('last_name', ['class' => 'form-control last_name required']); ?>
							<?= $this->Form->input('email', ['class' => 'form-control email required']); ?>
							<?= $this->Form->input('password', ['class' => 'form-control password required']); ?>
							<?= $this->Form->input('address_1', ['class' => 'form-control address_1']); ?>
							<?= $this->Form->input('address_2', ['class' => 'form-control address_2']); ?>
							<?= $this->Form->input('postcode', ['class' => 'form-control postcode']); ?>
							<?= $this->Form->input('city', ['class' => 'form-control city']); ?>
							<?= $this->Form->input('country', ['class' => 'form-control country']); ?>
							<?= $this->Form->input('birthday', ['label' => 'Date of Birth', 'class' => 'form-control date required']); ?>
							<div class="col-md-6 input-container">
								<label for="role">User Role</label>
								<?= $this->Form->select('role', $user_roles, ['class' => 'form-control role']); ?>
							</div>
							<?= $this->Form->input('phone_home', ['class' => 'form-control phone_home']); ?>
							<?= $this->Form->input('phone_mobile', ['class' => 'form-control phone_mobile required']); ?>
							<?= $this->Form->input('active', ['type' => 'checkbox', 'class' => 'required']); ?>
						</div>
						<div class="row">
							<div class="col-xs-12">
								<?= $this->Form->button(__('Submit'), ['class' => 'btn btn-primary btn-lg']); ?>
							</div>
						</div>
						<?= $this->Form->end(); ?>
						<style>
							.red-text {
								color: red;
							}
						</style>
						<script type="text/javascript">
							(function($) {
								$(document).ready(function() {
									// Init Select2 - Basic Single
									$(".select2-single, select.form-error").each(function(index, el) {
										$(this).addClass('form-control').select2({
											placeholder: 'Choose ' + $('label', $(this).parent()).text(),
										});
									});

									// Init Select2 - Basic Multiple
									$(".select2-multiple, select.form-error").each(function(index, el) {
										$(this).addClass('form-control').select2({
											allowClear: true,
											placeholder: 'Choose ' + $('label', $(this).parent()).text(),
										});
									});

									$('.date').datetimepicker({
										format: 'YYYY-MM-DD'
									});

									$('.datetime').datetimepicker({
										format: 'YYYY-MM-DD HH:mm'
									});

									$(document).on('submit', '.users', function(e) {
										var birthday = $('#birthday').prop('value');
										var req = $('.required').prop('value');
										var error = false;

										if (birthday) {
											if (!birthday.match(/\d{4}-\d{2}-\d{2}/)) {
												e.preventDefault();
												error = true;
											}
										} else {
											e.preventDefault();
											error = true;
										}

										if (req) {
											if (req.lenght === 0) {
												e.preventDefault();
												error = true;
											}
										} else {
											e.preventDefault();
											error = true;
										}

										if (error) {
											var $div = '<div class="error-message fw600 red-text mb20 fs12"><?= __('This field is required'); ?></div>';
											var $containerBirthday = $('#birthday').parent();
											var $containerReq = $('.required').parent();
											$('.error-message', $containerBirthday).remove();
											$containerReq.append($div);
										}
									});
								});
							})(jQuery);
						</script>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
