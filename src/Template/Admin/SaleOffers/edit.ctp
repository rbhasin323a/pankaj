<?php use Cake\Utility\Inflector; ?>
<header id="topbar">
	<div class="row">
		<div class="col-xs-12">
			<ol class="breadcrumb">
				<li class="crumb-trail"><a href="/<?= $this->request->params['prefix']; ?>/"><?= Inflector::humanize($this->request->params['prefix']); ?></a></li>
				<li class="crumb-trail"><a href="/<?= $this->request->params['prefix'] . '/' . $this->request->params['controller']; ?>">Sale Offers</a></li>
				<li class="crumb-active">Edit</li>
			</ol>
		</div>
	</div>
	<div class="row">
		<div class="col-xs-12">
			<div class="btn-toolbar mt5">
				<div class="btn-group btn-group-sm">
					<?= $this->Html->link(__('List Sale Offers'), ['action' => 'index'], ['class' => 'btn btn-primary']); ?>
				</div>

				<div class="btn-group btn-group-sm">
					<?= $this->Html->link(__('List Sales'), ['controller' => 'Sales', 'action' => 'index'], ['class' => 'btn btn-default bg-white']); ?>
					<?= $this->Html->link(__('New Sale'), ['controller' => 'Sales', 'action' => 'add'], ['class' => 'btn btn-default bg-white']); ?>
				</div>
				<div class="btn-group btn-group-sm">
					<?= $this->Html->link(__('List Sale Allocations'), ['controller' => 'SaleAllocations', 'action' => 'index'], ['class' => 'btn btn-default bg-white']); ?>
					<?= $this->Html->link(__('New Sale Allocation'), ['controller' => 'SaleAllocations', 'action' => 'add'], ['class' => 'btn btn-default bg-white']); ?>
				</div>
			</div>
		</div>
	</div>
</header>

<div id="content">
	<div class="tray tray-center">
		<div class="mw1000">
			<div class="saleOffers form">
				<div class="panel panel-dark">
					<div class="panel-heading">
						<?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $saleOffer->id], ['confirm' => __('Are you sure you want to delete #{0}?', $saleOffer->id), 'class' => 'btn btn-sm btn-danger pull-right m10']); ?>
						<span class="panel-title fw700"><?= __('Edit Sale Offer'); ?></span>
					</div>
					<div class="panel-body">
						<?= $this->Form->create($saleOffer, ['class' => 'form', 'novalidate' => true]); ?>
						<div class="row">
							<?php
							$this->Form->templates([
								'inputContainer'        => '<div class="col-md-6 input-container">{{content}}</div>',
								'inputContainerError'   => '<div class="col-md-6 input-container">{{content}}</div>',

								'label'                 => '<label {{attrs}}>{{text}}</label>{{hidden}}{{input}}',

								'formGroup'             => '{{label}}{{input}}{{error}}',
								'nestingLabel'          => '<div class="radio-custom square radio-primary mt10">{{input}} <label {{attrs}}>{{text}}</label></div>',

								// Input
								'input'                 => '<input type="{{type}}" name="{{name}}" {{attrs}} class="form-control" />',

								// Textarea
								'textarea'              => '<textarea name="{{name}}" {{attrs}} class="form-control">{{value}}</textarea>',

								// Select
								'select'                => '<select name="{{name}}" {{attrs}} class="form-control select2-single">{{content}}</select>',
								'selectMultple'         => '<select multiple name="{{name}}" {{attrs}} class="form-control select2-multiple">{{content}}</select>',

								'dateWidget'            => '{{day}}{{month}}{{year}}{{hour}}{{minute}}{{second}}',

								// Error
								'error'                 => '<div class="text-danger">{{content}}</div>',
								'errorList'             => '{{content}}',
								'errorItem'             => '{{text}}',
							]);
							?>
							<?= $this->Form->input('sale_id', [
								'options' => $sales,
							]); ?>
							<?= $this->Form->input('name', ['class' => 'form-control name']); ?>
							<?= $this->Form->input('active', ['class' => 'form-control active']); ?>
							<?= $this->Form->input('travel_date', ['class' => 'form-control travel_date']); ?>
							<?= $this->Form->input('minimum_stay', ['class' => 'form-control minimum_stay']); ?>
							<?= $this->Form->input('max_adults', ['class' => 'form-control max_adults']); ?>
							<?= $this->Form->input('max_children', ['class' => 'form-control max_children']); ?>
							<?= $this->Form->input('child_rate', ['class' => 'form-control child_rate']); ?>
							<?= $this->Form->input('child_free', ['class' => 'form-control child_free']); ?>
							<?= $this->Form->input('child_age_description', ['class' => 'form-control child_age_description']); ?>
							<?= $this->Form->input('infant_rate', ['class' => 'form-control infant_rate']); ?>
							<?= $this->Form->input('infant_free', ['class' => 'form-control infant_free']); ?>
							<?= $this->Form->input('infant_age_description', ['class' => 'form-control infant_age_description']); ?>
							<?= $this->Form->input('deposit', ['class' => 'form-control deposit']); ?>
							<?= $this->Form->input('single_policy', ['class' => 'form-control single_policy']); ?>
							<?= $this->Form->input('child_policy', ['class' => 'form-control child_policy']); ?>
							<?= $this->Form->input('summary_description', ['class' => 'form-control summary_description']); ?>
							<?= $this->Form->input('main_description', ['class' => 'form-control main_description']); ?>
							<?= $this->Form->input('allocations', ['class' => 'form-control allocations']); ?>
						</div>
						<div class="row">
							<div class="col-xs-12">
								<?= $this->Form->button(__('Submit'), ['class' => 'btn btn-primary btn-lg']); ?>
							</div>
						</div>
						<?= $this->Form->end(); ?>

						<link rel="stylesheet" type="text/css" href="/backend/ui/vendor/admindesign/vendor/plugins/datepicker/css/bootstrap-datetimepicker.css">
						<link rel="stylesheet" type="text/css" href="/backend/ui/vendor/admindesign/vendor/plugins/select2/css/core.css">
						<script src="/backend/ui/vendor/admindesign/vendor/plugins/globalize/globalize.min.js"></script>
						<script src="/backend/ui/vendor/admindesign/vendor/plugins/moment/moment.min.js"></script>
						<script src="/backend/ui/vendor/admindesign/vendor/plugins/select2/select2.min.js"></script>
						<script src="/backend/ui/vendor/admindesign/vendor/plugins/datepicker/js/bootstrap-datetimepicker.min.js"></script>
						<script type="text/javascript">
							(function($) {
								$(document).ready(function() {
									// Init Select2 - Basic Single
									$(".select2-single, select.form-error").each(function(index, el) {
										$(this).addClass('form-control').select2({
											placeholder: 'Choose ' + $('label', $(this).parent()).text(),
										});
									});

									// Init Select2 - Basic Multiple
									$(".select2-multiple, select.form-error").each(function(index, el) {
										$(this).addClass('form-control').select2({
											allowClear: true,
											placeholder: 'Choose ' + $('label', $(this).parent()).text(),
										});
									});

									$('.date').datepicker({
										format: 'YYYY-MM-DD'
									});

									$('.datetime').datetimepicker({
										format: 'YYYY-MM-DD HH:mm'
									});
								});
							})(jQuery);
						</script>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
