<?php
    use Cake\Utility\Inflector;
?>
<header id="topbar">
    <div class="row">
        <div class="col-xs-12">
            <ol class="breadcrumb">
                <li class="crumb-trail"><a href="/<?= $this->request->params['prefix']; ?>/"><?= Inflector::humanize($this->request->params['prefix']); ?></a></li>
                <li class="crumb-trail"><a href="/admin/sales/edit/<?= $saleOffer->sale_id; ?>#offers_tab">Sale Offers</a></li>
                <li class="crumb-active"><?= Inflector::humanize($this->request->params['action']); ?></li>
            </ol>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12">
            <div class="btn-toolbar mt5">
                <div class="btn-group btn-group-sm">
                    <?= $this->Html->link(__('List Sale Offers'), ['action' => 'index'], ['class' => 'btn btn-primary']); ?>
                </div>

                <div class="btn-group btn-group-sm">
                    <?= $this->Html->link(__('List Sales'), ['controller' => 'Sales', 'action' => 'index'], ['class' => 'btn btn-default bg-white']); ?>
                    <?= $this->Html->link(__('New Sale'), ['controller' => 'Sales', 'action' => 'add'], ['class' => 'btn btn-default bg-white']); ?>
                </div>
                <div class="btn-group btn-group-sm">
                    <?= $this->Html->link(__('List Sale Allocations'), ['controller' => 'SaleAllocations', 'action' => 'index'], ['class' => 'btn btn-default bg-white']); ?>
                    <?= $this->Html->link(__('New Sale Allocation'), ['controller' => 'SaleAllocations', 'action' => 'add'], ['class' => 'btn btn-default bg-white']); ?>
                </div>
            </div>
        </div>
    </div>
</header>

<div id="content">
    <div class="tray tray-center">
        <div class="">
            <div class="sales form">
                <div class="panel panel-dark">
                    <div class="panel-heading">
                        <span class="panel-title fw700">
                            <?php if (!empty($saleOffer->name)) { ?>
                            <em><?= $saleOffer->name; ?></em> -
                            <?php } ?>
                            <?= Inflector::humanize($this->request->params['action']) . ' Sale Offer'; ?>
                        </span>
                    </div>
                    <div class="panel-body">
                        <?php if (!empty($saleOffer->sale_id)) { ?>
                            <div class="btn-group btn-group-sm">
                                <?= $this->Html->link(__('Preview Sale'), 'sales/view/' . $saleOffer->sale_id, ['class' => 'btn btn-primary', 'target' => '_blank']); ?>
                            </div>
                        <?php } ?>
                        <?php if (!empty($saleOffer->id)) { ?>
                        <ul class="nav nav-pills pull-right">
                            <li class="active"><a href="#sale_offer_details" data-toggle="tab" aria-expanded="false">Details</a></li>
                            <li><a href="#sale_allocations" data-toggle="tab" aria-expanded="false">Sale Offer Allocations</a></li>
                        </ul>
                        <div class="clearfix"></div>
                        <?php } ?>
                        <div class="tab-content pn br-n">
                            <div id="sale_offer_details" class="tab-pane active">
                                <?= $this->Form->create($saleOffer, ['class' => 'form', 'novalidate' => true]); ?>
                                    <div class="row">
                                        <?php
                                            $this->Form->templates([
                                                'inputContainer'        => '<div class="col-md-6 {{class}} input-container">{{content}}</div>',
                                                'inputContainerError'   => '<div class="col-md-6 {{class}} input-container">{{content}}</div>',

                                                'label'                 => '<label {{attrs}}>{{text}}</label>{{hidden}}{{input}}',

                                                'formGroup'             => '{{label}} {{error}} {{input}}',
                                                'nestingLabel'          => '<div class="radio-custom square radio-primary mt10">{{input}} <label {{attrs}}>{{text}}</label></div>',

                                                // Input
                                                'input'                 => '<input type="{{type}}" name="{{name}}" {{attrs}} class="form-control" />',

                                                // Textarea
                                                'textarea'              => '<textarea name="{{name}}" {{attrs}} class="form-control">{{value}}</textarea>',

                                                // Select
                                                'select'                => '<select name="{{name}}" {{attrs}} class="form-control select2-single">{{content}}</select>',
                                                'selectMultple'         => '<select multiple name="{{name}}" {{attrs}} class="form-control select2-multiple">{{content}}</select>',

                                                'dateWidget'            => '{{day}}{{month}}{{year}}{{hour}}{{minute}}{{second}}',

                                                // Error
                                                'error'                 => '<span class="text-danger">* {{content}}</span>',
                                                'errorList'             => '{{content}}',
                                                'errorItem'             => '{{text}}'
                                            ]);
                                        ?>

                                        <div class="col-xs-12"><h2 class="mb25">Sale Offer Information</h2></div>

                                        <?php if (!empty($vd['languages'])) { ?>
                                        <div class="col-xs-12">
                                            <div class="tab-block" style="margin:0 -16px 25px">
                                                <ul class="nav nav-tabs">
                                                    <?php $i = 0; foreach ($vd['languages'] as $language) { $i++; ?>
                                                    <li <?= ($i == 1) ? 'class="active"' : ''; ?>><a href="#tab_language_<?= $language['id']; ?>" data-toggle="tab">
                                                        <img src="/ui/img/flags/24/<?= $language['code']; ?>.png" />&nbsp;<?= $language['name']; ?>
                                                    </a></li>
                                                    <?php } ?>
                                                </ul>
                                                <div class="tab-content">
                                                    <?php $i = 0; foreach ($vd['languages'] as $language) { $i++; ?>
                                                        <?php $locale = $language['locale']; ?>
                                                        <div id="tab_language_<?= $language['id']; ?>" class="tab-pane <?= ($i == 1) ? 'active' : ''; ?>">
                                                            <?= $this->Form->input('name', [
                                                                'type'  => 'text',
                                                                'label' => 'Name',
                                                                'name'  => '_translations[' . $locale . '][name]',
                                                                'class' => 'form-control name',
                                                                'templateVars'  => ['class' => 'inline-full'],
                                                                'id'    => 'translation-name-' . $locale,
                                                                'value' => !empty($_translations[$locale]['name']) ? $_translations[$locale]['name'] : ''
                                                            ]); ?>
                                                            <?= $this->Form->input('main_description', [
                                                                'type'  => 'textarea',
                                                                'label' => 'Main Description',
                                                                'name'  => '_translations[' . $locale . '][main_description]',
                                                                'class' => 'form-control ckeditor main_description',
                                                                'templateVars'  => ['class' => 'inline-full'],
                                                                'id'    => 'translation-main-description-' . $locale,
                                                                'value' => !empty($_translations[$locale]['main_description']) ? $_translations[$locale]['main_description'] : ''
                                                            ]); ?>
                                                            <?= $this->Form->input('summary_description', [
                                                                'type'  => 'textarea',
                                                                'label' => 'Summary Description',
                                                                'name'  => '_translations[' . $locale . '][summary_description]',
                                                                'class' => 'form-control ckeditor summary_description',
                                                                'templateVars'  => ['class' => 'inline-full'],
                                                                'id'    => 'translation-summary-description-' . $locale,
                                                                'value' => !empty($_translations[$locale]['summary_description']) ? $_translations[$locale]['summary_description'] : ''
                                                            ]); ?>
                                                            <?= $this->Form->input('single_policy', [
                                                                'type'  => 'textarea',
                                                                'label' => 'Single Policy',
                                                                'name'  => '_translations[' . $locale . '][single_policy]',
                                                                'class' => 'form-control ckeditor single_policy',
                                                                'templateVars'  => ['class' => 'inline-full'],
                                                                'id'    => 'translation-single-policy-' . $locale,
                                                                'value' => !empty($_translations[$locale]['single_policy']) ? $_translations[$locale]['single_policy'] : ''
                                                            ]); ?>
                                                            <?= $this->Form->input('child_policy', [
                                                                'type'  => 'textarea',
                                                                'label' => 'Child Policy',
                                                                'name'  => '_translations[' . $locale . '][child_policy]',
                                                                'class' => 'form-control ckeditor child_policy',
                                                                'templateVars'  => ['class' => 'inline-full'],
                                                                'id'    => 'translation-child-policy-' . $locale,
                                                                'value' => !empty($_translations[$locale]['child_policy']) ? $_translations[$locale]['child_policy'] : ''
                                                            ]); ?>
                                                        </div>
                                                    <?php } ?>
                                                </div>
                                            </div>
                                        </div>
                                        <?php } ?>

                                        <?= $this->Form->input('offer_type', [
                                            'class' => 'form-control offer_type',
                                            'type' => 'select',
                                            'options' => $offer_types
                                        ]); ?>
                                        <?= $this->Form->input('max_adults', ['class' => 'form-control max_adults']); ?>
                                        <?= $this->Form->input('max_children', ['class' => 'form-control max_children']); ?>



                                        <div class="col-md-3 input-container">
                                            <label for="child-age-from">Child Age From</label>
                                            <input type="text" name="child_age_from" value="<?= !empty($saleOffer->child_age_from) ? $saleOffer->child_age_from : 0; ?>" class="form-control child_age_from" maxlength="255" id="child-age-from"/>
                                        </div>

                                        <div class="col-md-3 input-container">
                                            <label for="child-age-to">Child Age To</label>
                                            <input type="text" name="child_age_to" value="<?= !empty($saleOffer->child_age_to) ? $saleOffer->child_age_to : 0; ?>"  class="form-control child_age_to" maxlength="255" id="child-age-to"/>
                                        </div>

                                        <?= $this->Form->input('child_free', ['class' => 'form-control child_free']); ?>

                                        <div class="col-md-3 input-container">
                                            <label for="infant-age-from">Infant Age From</label>
                                            <input type="text" name="infant_age_from" value="<?= !empty($saleOffer->infant_age_from) ? $saleOffer->infant_age_from : 0; ?>"  class="form-control" maxlength="255" id="infant-age-from" value=""/>
                                        </div>

                                        <div class="col-md-3 input-container">
                                            <label for="infant-age-to">Infant Age To</label>
                                            <input type="text" name="infant_age_to" value="<?= !empty($saleOffer->infant_age_to) ? $saleOffer->infant_age_to : 0; ?>"  class="form-control" maxlength="255" id="infant-age-to"/>
                                        </div>

                                        <?= $this->Form->input('infant_free', ['class' => 'form-control infant_free']); ?>
                                        <?= $this->Form->input('active', ['class' => 'form-control active']); ?>
                                        <?= $this->Form->input('deposit', ['class' => 'form-control deposit']); ?>
                                    </div>
                                    <div class="row">
                                        <div class="col-xs-12">
                                            <?= $this->Form->button(__('Save Sale Offer'), ['class' => 'btn btn-primary btn-lg']); ?>

                                            <?php if (!empty($saleOffer->id)) { ?>
                                            <?php /*echo $this->Form->postLink(
                                                    __('Delete'),
                                                    ['action' => 'delete', $saleOffer->id],
                                                    ['confirm' => __('Are you sure you want to delete #{0}?', $saleOffer->id),'class' => 'btn btn-sm btn-danger pull-right m10']
                                                );*/
                                            ?>
                                            <?php } ?>
                                        </div>
                                    </div>
                                <?= $this->Form->end(); ?>
                            </div>

                            <div id="sale_allocations" class="tab-pane">
                                <div class="row">
                                    <div class="col-xs-12"><h2 class="mb25">Allocations</h2></div>
                                    <div class="col-xs-12"><div class="table-responsive" style="overflow: auto;">
                                        <table cellpadding="0" cellspacing="0" class="table table-bordered themed-form table-hover theme-info tc-checkbox-1 big-table">
                                            <thead>
                                                <tr class="bg-light">
                                                    <th>Rate</th>
                                                    <th>Rack rate</th>
                                                    <th>Single rate</th>
                                                    <th>Child rate</th>
                                                    <th>Infant rate</th>
                                                    <th>Date start</th>
                                                    <th>Date end</th>
                                                    <th>Available Rooms</th>
                                                    <th>Departure Airport</th>
                                                    <th>Destination Airport</th>
                                                    <th>Total deposit</th>
                                                    <th>Child deposit</th>
                                                    <th>Infant deposit</th>
                                                    <th>Date balance due</th>
                                                    <th>Min nights</th>
                                                    <th>Hold Rooms</th>
                                                    <th>Booked Rooms</th>
                                                    <th>Locked Rooms</th>
                                                    <th>Discount</th>
                                                    <th class="actions text-center"><?= __('Actions'); ?></th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php if (!empty($saleOffer->sale_allocations)) { ?>
                                                    <?php foreach ($saleOffer->sale_allocations as $saleAllocation): ?>
                                                        <tr>
                                                            <td><?= h($saleAllocation->rate); ?></td>
                                                            <td><?= h($saleAllocation->rack_rate); ?></td>
                                                            <td><?= h($saleAllocation->single_rate); ?></td>
                                                            <td><?= h($saleAllocation->child_rate); ?></td>
                                                            <td><?= h($saleAllocation->infant_rate); ?></td>
                                                            <td><?= h($saleAllocation->date_start); ?></td>
                                                            <td><?= h($saleAllocation->date_end); ?></td>
                                                            <td><?= h($saleAllocation->available_rooms); ?></td>
                                                            <td><?= h($saleAllocation->departure_airport_code); ?></td>
                                                            <td><?= h($saleAllocation->destination_airport_code); ?></td>
                                                            <td><?= h($saleAllocation->total_deposit); ?></td>
                                                            <td><?= h($saleAllocation->child_deposit); ?></td>
                                                            <td><?= h($saleAllocation->infant_deposit); ?></td>
                                                            <td><?= h($saleAllocation->date_balance_due); ?></td>
                                                            <td><?= h($saleAllocation->min_nights); ?></td>
                                                            <td><?= h($saleAllocation->hold_rooms); ?></td>
                                                            <td><?= h($saleAllocation->booked_rooms); ?></td>
                                                            <td><?= h($saleAllocation->locked_rooms); ?></td>
                                                            <td><?= h($saleAllocation->discount); ?></td>
                                                            <td class="actions text-center" width="200">
                                                                <div class="btn-group btn-group-sm ib">
                                                                    <?= $this->Html->link(__('Edit'), ['controller'=>'sale_allocations','action' => 'edit', $saleAllocation->id], ['class' => 'btn btn-primary']); ?>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                    <?php endforeach; ?>
                                                <?php } else { ?>
                                                    <tr>
                                                        <td colspan="18">
                                                            <p class="text-center fs16 lh50 pv20"><i class="fa fa-ban text-danger"></i>&nbsp;&nbsp;No results found.</p>
                                                        </td>
                                                    </tr>
                                                <?php } ?>
                                            </tbody>
                                        </table>
                                    </div></div>
                                </div>

                                <hr />

                                <div class="row">
                                    <div class="col-xs-12"><h2>Upload Allocation Spreadsheet</h2></div>
                                    <div class="col-xs-12">
                                        <p><b>Upload an allocations file to add new allocations to the offer.</b></p>
                                    </div>
                                    <div class="col-xs-12">
                                        <form method="post" id="allocation-upload" enctype="multipart/form-data" class="form" action="/admin/sale-offers/uploadAllocations/<?= $saleOffer->id; ?>">
                                            <div class="row" style="margin: 0;">
                                                <div class="col-xs-12 input-container">
                                                    <div class="label">
                                                        <div class="form-control">
                                                            <input type="file" name="allocations_csv"/>
                                                        </div>
                                                    </div>


                                                </div>
                                                <button type="submit" class="btn btn-primary btn-lg" form="allocation-upload">Upload Allocations</button>
                                            </div>
                                        </form>
                                    </div>
                                </div>

                                <hr />

                                <?= $this->Form->create($_sale_allocation, ['class' => 'form', 'novalidate' => true]); ?>
                                <div class="row">
                                    <div class="col-xs-12"><h2 class="mb25">Add Single Sale Allocation</h2></div>
                                    <?php
                                        $this->Form->templates([
                                            'inputContainer'        => '<div class="col-md-6 {{class}} input-container">{{content}}</div>',
                                            'inputContainerError'   => '<div class="col-md-6 {{class}} input-container">{{content}}</div>',

                                            'label'                 => '<label {{attrs}}>{{text}}</label>{{hidden}}{{input}}',

                                            'formGroup'             => '{{label}} {{error}} {{input}}',
                                            'nestingLabel'          => '<div class="radio-custom square radio-primary mt10">{{input}} <label {{attrs}}>{{text}}</label></div>',

                                            // Input
                                            'input'                 => '<input type="{{type}}" name="{{name}}" {{attrs}} class="form-control" />',

                                            // Textarea
                                            'textarea'              => '<textarea name="{{name}}" {{attrs}} class="form-control">{{value}}</textarea>',

                                            // Select
                                            'select'                => '<select name="{{name}}" {{attrs}} class="form-control select2-single">{{content}}</select>',
                                            'selectMultple'         => '<select multiple name="{{name}}" {{attrs}} class="form-control select2-multiple">{{content}}</select>',

                                            'dateWidget'            => '{{day}}{{month}}{{year}}{{hour}}{{minute}}{{second}}',

                                            // Error
                                            'error'                 => '<span class="text-danger">* {{content}}</span>',
                                            'errorList'             => '{{content}}',
                                            'errorItem'             => '{{text}}',
                                        ]);
                                    ?>
                                    <?= $this->Form->input('rate', [
                                        'name'  => '_sale_allocation[rate]',
                                        'id'    => 'sale_allocation-rate',
                                        'class' => 'form-control rate',
                                    ]); ?>
                                    <?= $this->Form->input('rack_rate', [
                                        'name'  => '_sale_allocation[rack_rate]',
                                        'id'    => 'sale_allocation-rack_rate',
                                        'class' => 'form-control rack_rate',
                                    ]); ?>
                                    <?= $this->Form->input('single_rate', [
                                        'name'  => '_sale_allocation[single_rate]',
                                        'id'    => 'sale_allocation-single_rate',
                                        'class' => 'form-control single_rate',
                                    ]); ?>
                                    <?= $this->Form->input('child_rate', [
                                        'name'  => '_sale_allocation[child_rate]',
                                        'id'    => 'sale_allocation-child_rate',
                                        'class' => 'form-control child_rate',
                                    ]); ?>
                                    <?= $this->Form->input('infant_rate', [
                                        'name'  => '_sale_allocation[infant_rate]',
                                        'id'    => 'sale_allocation-infant_rate',
                                        'class' => 'form-control infant_rate',
                                    ]); ?>
                                    <?= $this->Form->input('total_deposit', [
                                        'name'  => '_sale_allocation[total_deposit]',
                                        'id'    => 'sale_allocation-total_deposit',
                                        'class' => 'form-control total_deposit',
                                    ]); ?>
                                    <?= $this->Form->input('single_deposit', [
                                        'name'  => '_sale_allocation[single_deposit]',
                                        'id'    => 'sale_allocation-single_deposit',
                                        'class' => 'form-control single_deposit',
                                    ]); ?>
                                    <?= $this->Form->input('child_deposit', [
                                        'name'  => '_sale_allocation[child_deposit]',
                                        'id'    => 'sale_allocation-child_deposit',
                                        'class' => 'form-control child_deposit',
                                    ]); ?>
                                    <?= $this->Form->input('infant_deposit', [
                                        'name'  => '_sale_allocation[infant_deposit]',
                                        'id'    => 'sale_allocation-infant_deposit',
                                        'class' => 'form-control infant_deposit',
                                    ]); ?>
                                    <?= $this->Form->input('date_balance_due', [
                                        'type'  => 'text',
                                        'name'  => '_sale_allocation[date_balance_due]',
                                        'id'    => 'sale_allocation-date_balance_due',
                                        'class' => 'form-control date_balance_due',
                                    ]); ?>
                                    <?= $this->Form->input('date_start', [
                                        'type'  => 'text',
                                        'name'  => '_sale_allocation[date_start]',
                                        'id'    => 'sale_allocation-date_start',
                                        'class' => 'form-control date_start',
                                    ]); ?>
                                    <?= $this->Form->input('date_end', [
                                        'type'  => 'text',
                                        'name'  => '_sale_allocation[date_end]',
                                        'id'    => 'sale_allocation-date_end',
                                        'class' => 'form-control date_end greyed-out',
                                        'disabled' => 'disabled'
                                    ]); ?>
                                    <?= $this->Form->input('departure_airport_code', [
                                        'type'  => 'text',
                                        'name'  => '_sale_allocation[departure_airport_code]',
                                        'id'    => 'sale_allocation-departure_airport_code',
                                        'class' => 'form-control departure_airport_code',
                                    ]); ?>
                                    <?= $this->Form->input('destination_airport_code', [
                                        'type'  => 'text',
                                        'name'  => '_sale_allocation[destination_airport_code]',
                                        'id'    => 'sale_allocation-destination_airport_code',
                                        'class' => 'form-control destination_airport_code',
                                    ]); ?>
                                    <?= $this->Form->input('discount', [
                                        'name'  => '_sale_allocation[discount]',
                                        'id'    => 'sale_allocation-discount',
                                        'class' => 'form-control discount',
                                        'readonly' => 'readonly'
                                    ]); ?>
                                    <?= $this->Form->input('booked_rooms', [
                                        'name'  => '_sale_allocation[booked_rooms]',
                                        'id'    => 'sale_allocation-booked_rooms',
                                        'class' => 'form-control booked_rooms',
                                    ]); ?>
                                    <?= $this->Form->input('hold_rooms', [
                                        'name'  => '_sale_allocation[hold_rooms]',
                                        'id'    => 'sale_allocation-hold_rooms',
                                        'class' => 'form-control hold_rooms',
                                    ]); ?>
                                    <?= $this->Form->input('locked_rooms', [
                                        'name'  => '_sale_allocation[locked_rooms]',
                                        'id'    => 'sale_allocation-locked_rooms',
                                        'class' => 'form-control locked_rooms',
                                    ]); ?>
                                    <?= $this->Form->input('available_rooms', [
                                        'name'  => '_sale_allocation[available_rooms]',
                                        'id'    => 'sale_allocation-available_rooms',
                                        'class' => 'form-control available_rooms',
                                    ]); ?>
                                    <?= $this->Form->input('outbound_overnight', [
                                        'name'  => '_sale_allocation[outbound_overnight]',
                                        'id'    => 'sale_allocation-outbound_overnight',
                                        'class' => 'form-control outbound_overnight',
                                    ]); ?>
                                    <?= $this->Form->input('inbound_overnight', [
                                        'name'  => '_sale_allocation[inbound_overnight]',
                                        'id'    => 'sale_allocation-inbound_overnight',
                                        'class' => 'form-control inbound_overnight',
                                    ]); ?>
                                </div>
                                <div class="row">
                                    <div class="col-xs-12">
                                        <?= $this->Form->button(__('Add Sale Allocation'), ['class' => 'btn btn-primary btn-lg']); ?>
                                    </div>
                                </div>
                                <?= $this->Form->end(); ?>
                            </div>
                        </div>
                        <script type="text/javascript">
                        (function($) {
                            $(document).ready(function() {
                                // Init Select2 - Basic Single
                                $(".select2-single, select.form-error").each(function(index, el) {
                                    $(this).addClass('form-control').select2({
                                        placeholder: 'Choose ' + $('label', $(this).parent()).text(),
                                    });
                                });

                                // Init Select2 - Basic Multiple
                                $(".select2-multiple, select.form-error").each(function(index, el) {
                                    $(this).addClass('form-control').select2({
                                        allowClear: true,
                                        placeholder: 'Choose ' + $('label', $(this).parent()).text(),
                                    });
                                });

                                // Click the tab if coming from the allocations page
                                if (location.href.indexOf('#allocations_tab') > -1) {
                                    $('a[href="#sale_allocations"]').click();
                                }

                                // Set starting date_end to be corresponding to the date of date_start
                                $('.date_start').on('change', function(e) {
                                    var dateEndInput = $('.date_end');

                                    dateEndInput.removeClass('greyed-out');
                                    dateEndInput.removeAttr('disabled');
                                    dateEndInput.prop('value', $(this).prop('value'));
                                    dateEndInput.datetimepicker({
                                        format: 'YYYY-MM-DD',
                                        useCurrent: false,
                                        defaultDate: new Date($(this).prop('value'))
                                    });
                                    $(this).off('change');
                                });

                                $('.date').datetimepicker({
                                    format: 'YYYY-MM-DD'
                                });

                                $('.date_start').datetimepicker({
                                    format: 'YYYY-MM-DD',
                                    useCurrent: false
                                });

                                $('.date_end').val('Please, select a start date first');

                                $('.date_balance_due').datetimepicker({
                                    format: 'YYYY-MM-DD'
                                });

                                $('.datetime').datetimepicker({
                                    format: 'YYYY-MM-DD HH:mm'
                                });

                                $('.ckeditor').each(function(index, el) {
                                    CKEDITOR.replace($(this).attr('id'));
                                });
                            });
                        })(jQuery);
                        </script>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
