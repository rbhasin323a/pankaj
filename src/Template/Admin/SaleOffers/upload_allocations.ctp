<?php
    use Cake\Utility\Inflector;
    use Cake\Routing\Router;
?>
<header id="topbar">
    <div class="row">
        <div class="col-xs-12">
            <ol class="breadcrumb">
                <li class="crumb-trail"><a href="/<?= $this->request->params['prefix']; ?>/"><?= Inflector::humanize($this->request->params['prefix']); ?></a></li>
                <li class="crumb-trail"><a href="/admin/sale-offers/edit/<?= $sale_offer_id ?>#allocations_tab">Sale Offers</a></li>
            </ol>
        </div>
    </div>
</header>
<div id="content">
    <div class="panel panel-dark">
        <div class="panel-heading">Upload Allocations Preview</div>
        <div class="panel-body">
            <form action="/admin/sale-offers/saveUploadedAllocations/<?= $sale_offer_id ?>#allocations_tab" method="post" id="upload-form">
                <input type="hidden" name="sale_offer_id" value="<?= $sale_offer_id; ?>" />
                <div class="table-responsive">
                    <table cellpadding="0" cellspacing="0" class="table table-bordered themed-form theme-info tc-checkbox-1 big-table">
                        <thead>
                            <tr>
                                <th colspan="18" class="text-center"><h2>New Allocations</h2></th> 
                            </tr>
                            <tr>
                                <th>Sale Offer ID</th>
                                <th>Rate</th>
                                <th>Rack Rate</th>
                                <th>Single Rate</th>
                                <th>Child Rate</th>
                                <th>Infant Rate</th>
                                <th>Date Start</th>
                                <th>Date End</th>
                                <th>Available Rooms</th>
                                <th>Departure Airport Code</th>
                                <th>Destination Airport Code</th>
                                <th>Total Deposit</th>
                                <th>Child Deposit</th>
                                <th>Infant Deposit</th>
                                <th>Date Balance Due</th>
                                <th>Outbound Overnight</th>
                                <th>Inbound Overnight</th>
                                <th>Min Nights</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($allocations_preview['new'] as $key => $new_allocation) { ?>
                                <tr>
                                    <td>
                                        <?= $new_allocation['sale_offer_id']; ?>
                                        <input type="hidden" name="data[new][<?= $key; ?>][sale_offer_id]" value="<?= $new_allocation['sale_offer_id']; ?>"/>
                                    </td>
                                    <td>
                                        <?= $new_allocation['rate']; ?>
                                        <input type="hidden" name="data[new][<?= $key; ?>][rate]" value="<?= $new_allocation['rate']; ?>"/>
                                    </td>
                                    <td>
                                        <?= $new_allocation['rack_rate']; ?>
                                        <input type="hidden" name="data[new][<?= $key; ?>][rack_rate]" value="<?= $new_allocation['rack_rate']; ?>"/>
                                            
                                            
                                    </td>
                                    <td>
                                        <?= $new_allocation['single_rate']; ?>
                                        <input type="hidden" name="data[new][<?= $key; ?>][single_rate]" value="<?= $new_allocation['single_rate']; ?>"/>
                                        
                                    </td>
                                    <td>
                                        <?= $new_allocation['child_rate']; ?>
                                        <input type="hidden" name="data[new][<?= $key; ?>][child_rate]" value="<?= $new_allocation['child_rate']; ?>"/>
                                            
                                    </td>
                                    <td>
                                        <?= $new_allocation['infant_rate']; ?>
                                        <input type="hidden" name="data[new][<?= $key; ?>][infant_rate]" value="<?= $new_allocation['infant_rate']; ?>"/>
                                    </td>
                                    <td>
                                        <?= $new_allocation['date_start']; ?>
                                        <input type="hidden" name="data[new][<?= $key; ?>][date_start]" value="<?= $new_allocation['date_start']; ?>"/>
                                    </td>
                                    <td>
                                        <?= $new_allocation['date_end']; ?>
                                        <input type="hidden" name="data[new][<?= $key; ?>][date_end]" value="<?= $new_allocation['date_end']; ?>"/>
                                    </td>
                                    <td>
                                        <?= $new_allocation['available_rooms']; ?>
                                        <input type="hidden" name="data[new][<?= $key; ?>][available_rooms]" value="<?= $new_allocation['available_rooms']; ?>"/>
                                    </td>
                                    <td>
                                        <?= $new_allocation['departure_airport_code']; ?>
                                        <input type="hidden" name="data[new][<?= $key; ?>][departure_airport_code]" value="<?= $new_allocation['departure_airport_code']; ?>"/>
                                    </td>
                                    <td>
                                        <?= $new_allocation['destination_airport_code']; ?>
                                        <input type="hidden" name="data[new][<?= $key; ?>][destination_airport_code]" value="<?= $new_allocation['destination_airport_code']; ?>"/>
                                    </td>
                                    <td>
                                        <?= $new_allocation['total_deposit']; ?>
                                        <input type="hidden" name="data[new][<?= $key; ?>][total_deposit]" value="<?= $new_allocation['total_deposit']; ?>"/>
                                    </td>
                                    <td>
                                        <?= $new_allocation['child_deposit']; ?>
                                        <input type="hidden" name="data[new][<?= $key; ?>][child_deposit]" value="<?= $new_allocation['child_deposit']; ?>"/>
                                    </td>
                                    <td>
                                        <?= $new_allocation['infant_deposit']; ?>
                                        <input type="hidden" name="data[new][<?= $key; ?>][infant_deposit]" value="<?= $new_allocation['infant_deposit']; ?>"/>
                                    </td>
                                    <td>
                                        <?= $new_allocation['date_balance_due']; ?>
                                        <input type="hidden" name="data[new][<?= $key; ?>][date_balance_due]" value="<?= $new_allocation['date_balance_due']; ?>"/>
                                    </td>
                                    <td>
                                        <?= $new_allocation['outbound_overnight']; ?>
                                        <input type="hidden" name="data[new][<?= $key; ?>][outbound_overnight]" value="<?= $new_allocation['outbound_overnight']; ?>"/>
                                    </td>
                                    <td>
                                        <?= $new_allocation['inbound_overnight']; ?>
                                        <input type="hidden" name="data[new][<?= $key; ?>][inbound_overnight]" value="<?= $new_allocation['inbound_overnight']; ?>"/>
                                    </td>
                                    <td>
                                        <?= $new_allocation['min_nights']; ?>
                                        <input type="hidden" name="data[new][<?= $key; ?>][min_nights]" value="<?= $new_allocation['min_nights']; ?>"/>
                                    </td>
                                </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                </div>
                <div class="table-responsive mt25">
                    <table cellpadding="0" cellspacing="0" class="table table-bordered themed-form theme-info tc-checkbox-1 big-table">
                        <thead>
                            <tr>
                                <th colspan="18" class="text-center"><h2>Updated Allocations</h2></th> 
                            </tr>
                            <tr class="legend">
                                <td colspan="3">
                                    <h4>Legend</h4>
                                </td>
                                <td colspan="3">
                                    <div class="preview-old"></div> => Old
                                </td>
                                <td colspan="11">
                                    <div class="preview-new"></div> => New
                                </td>
                            </tr>
                            <tr>
                                <th>Sale Offer ID</th>
                                <th>Rate</th>
                                <th>Rack Rate</th>
                                <th>Single Rate</th>
                                <th>Child Rate</th>
                                <th>Infant Rate</th>
                                <th>Date Start</th>
                                <th>Date End</th>
                                <th>Available Rooms</th>
                                <th>Departure Airport Code</th>
                                <th>Destination Airport Code</th>
                                <th>Total Deposit</th>
                                <th>Child Deposit</th>
                                <th>Infant Deposit</th>
                                <th>Date Balance Due</th>
                                <th>Outbound Overnight</th>
                                <th>Inbound Overnight</th>
                                <th>Min Nights</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr class="row-separator"><td colspan="18"></td></tr>
                            <?php foreach($allocations_preview['existing'] as $key => $existing_allocation) { ?>
                                <tr class="old-data">
                                    <td><?= $existing_allocation['old']['sale_offer_id']; ?></td>
                                    <td><?= $existing_allocation['old']['rate']; ?></td>
                                    <td><?= $existing_allocation['old']['rack_rate']; ?></td>
                                    <td><?= $existing_allocation['old']['single_rate']; ?></td>
                                    <td><?= $existing_allocation['old']['child_rate']; ?></td>
                                    <td><?= $existing_allocation['old']['infant_rate']; ?></td>
                                    <td><?= $existing_allocation['old']['date_start']; ?></td>
                                    <td><?= $existing_allocation['old']['date_end']; ?></td>
                                    <td><?= $existing_allocation['old']['available_rooms']; ?></td>
                                    <td><?= $existing_allocation['old']['departure_airport_code']; ?></td>
                                    <td><?= $existing_allocation['old']['destination_airport_code']; ?></td>
                                    <td><?= $existing_allocation['old']['total_deposit']; ?></td>
                                    <td><?= $existing_allocation['old']['child_deposit']; ?></td>
                                    <td><?= $existing_allocation['old']['infant_deposit']; ?></td>
                                    <td><?= $existing_allocation['old']['date_balance_due']; ?></td>
                                    <td><?= $existing_allocation['old']['outbound_overnight']; ?></td>
                                    <td><?= $existing_allocation['old']['inbound_overnight']; ?></td>
                                    <td><?= $existing_allocation['old']['min_nights']; ?></td>
                                </tr>
                                <tr>
                                    <td class="hide">
                                        <input type="hidden" name="data[existing][<?= $key; ?>][id]" value="<?= $existing_allocation['new']['id']; ?>">
                                    </td>
                                    <td>
                                        <?= $existing_allocation['new']['sale_offer_id']; ?>
                                        <input type="hidden" name="data[existing][<?= $key; ?>][sale_offer_id]" value="<?= $existing_allocation['new']['sale_offer_id']; ?>">
                                    </td>
                                    <td>
                                        <?= $existing_allocation['new']['rate']; ?>
                                        <input type="hidden" name="data[existing][<?= $key; ?>][rate]" value="<?= $existing_allocation['new']['rate']; ?>">
                                    </td>
                                    <td>
                                        <?= $existing_allocation['new']['rack_rate']; ?>
                                        <input type="hidden" name="data[existing][<?= $key; ?>][rack_rate]" value="<?= $existing_allocation['new']['rack_rate']; ?>">
                                    </td>
                                    <td>
                                        <?= $existing_allocation['new']['single_rate']; ?>
                                        <input type="hidden" name="data[existing][<?= $key; ?>][single_rate]" value="<?= $existing_allocation['new']['single_rate']; ?>">
                                    </td>
                                    <td>
                                        <?= $existing_allocation['new']['child_rate']; ?>
                                        <input type="hidden" name="data[existing][<?= $key; ?>][child_rate]" value="<?= $existing_allocation['new']['child_rate']; ?>">
                                    </td>
                                    <td>
                                        <?= $existing_allocation['new']['infant_rate']; ?>
                                        <input type="hidden" name="data[existing][<?= $key; ?>][infant_rate]" value="<?= $existing_allocation['new']['infant_rate']; ?>">
                                    </td>
                                    <td>
                                        <?= $existing_allocation['new']['date_start']; ?>
                                        <input type="hidden" name="data[existing][<?= $key; ?>][date_start]" value="<?= $existing_allocation['new']['date_start']; ?>">
                                    </td>
                                    <td>
                                        <?= $existing_allocation['new']['date_end']; ?>
                                        <input type="hidden" name="data[existing][<?= $key; ?>][date_end]" value="<?= $existing_allocation['new']['date_end']; ?>">
                                    </td>
                                    <td>
                                        <?= $existing_allocation['new']['available_rooms']; ?>
                                        <input type="hidden" name="data[existing][<?= $key; ?>][available_rooms]" value="<?= $existing_allocation['new']['available_rooms']; ?>">
                                    </td>
                                    <td>
                                        <?= $existing_allocation['new']['departure_airport_code']; ?>
                                        <input type="hidden" name="data[existing][<?= $key; ?>][departure_airport_code]" value="<?= $existing_allocation['new']['departure_airport_code']; ?>">
                                    </td>
                                    <td>
                                        <?= $existing_allocation['new']['destination_airport_code']; ?>
                                        <input type="hidden" name="data[existing][<?= $key; ?>][destination_airport_code]" value="<?= $existing_allocation['new']['destination_airport_code']; ?>">
                                    </td>
                                    <td>
                                        <?= $existing_allocation['new']['total_deposit']; ?>
                                        <input type="hidden" name="data[existing][<?= $key; ?>][total_deposit]" value="<?= $existing_allocation['new']['total_deposit']; ?>">
                                    </td>
                                    <td>
                                        <?= $existing_allocation['new']['child_deposit']; ?>
                                        <input type="hidden" name="data[existing][<?= $key; ?>][child_deposit]" value="<?= $existing_allocation['new']['child_deposit']; ?>">
                                    </td>
                                    <td>
                                        <?= $existing_allocation['new']['infant_deposit']; ?>
                                        <input type="hidden" name="data[existing][<?= $key; ?>][infant_deposit]" value="<?= $existing_allocation['new']['infant_deposit']; ?>">
                                    </td>
                                    <td>
                                        <?= $existing_allocation['new']['date_balance_due']; ?>
                                        <input type="hidden" name="data[existing][<?= $key; ?>][date_balance_due]" value="<?= $existing_allocation['new']['date_balance_due']; ?>">
                                    </td>
                                    <td>
                                        <?= $existing_allocation['new']['outbound_overnight']; ?>
                                        <input type="hidden" name="data[existing][<?= $key; ?>][outbound_overnight]" value="<?= $existing_allocation['new']['outbound_overnight']; ?>">
                                    </td>
                                    <td>
                                        <?= $existing_allocation['new']['inbound_overnight']; ?>
                                        <input type="hidden" name="data[existing][<?= $key; ?>][inbound_overnight]" value="<?= $existing_allocation['new']['inbound_overnight']; ?>">
                                    </td>
                                    <td>
                                        <?= $existing_allocation['new']['min_nights']; ?>
                                        <input type="hidden" name="data[existing][<?= $key; ?>][min_nights]" value="<?= $existing_allocation['new']['min_nights']; ?>">
                                    </td>
                                </tr>
                                <tr class="row-separator"><td colspan="18"></td></tr>
                            <?php } ?>
                        </tbody>
                    </table>
                </div>
            </form>
        </div>
        <div class="panel-footer text-center">
            <button type="submit" form="upload-form" class="btn btn-lg btn-primary">Confirm</button>
            <button type="button" onclick="location.href = location.href.replace('uploadAllocations/', 'edit/') + '#allocations_tab'" class="btn btn-lg btn-primary">Cancel</button>
        </div>
    </div>
</div>
<style type="text/css">
    tr.row-separator {
        background-color: grey;
        opacity: 0.5;
    }
    .legend .preview-old {
        background-color: rgba(255, 143, 0, 0.4);
        display: inline-block;
        width: 25px;
        height: 25px;
        border: 1px solid grey;
        vertical-align: middle;
    }
    .legend .preview-new {
        display: inline-block;
        width: 25px;
        height: 25px;
        border: 1px solid grey;
        vertical-align: middle;
    }
    tr.old-data {
        background-color: rgba(255, 143, 0, 0.4);
    }
</style>