<?php use Cake\Utility\Inflector; ?>
<header id="topbar">
	<div class="row">
		<div class="col-xs-12">
			<ol class="breadcrumb">
				<li class="crumb-trail"><a href="/<?= $this->request->params['prefix']; ?>/"><?= Inflector::humanize($this->request->params['prefix']); ?></a></li>
				<li class="crumb-active">Languages</li>
			</ol>
		</div>
	</div>
	<div class="row">
		<div class="col-xs-12">
			<div class="btn-toolbar mt5">
				<div class="btn-group btn-group-sm">
					<?= $this->Html->link(__('New Language'), ['action' => 'add'], ['class' => 'btn btn-primary']); ?>
				</div>
			</div>
		</div>
	</div>
</header>

<div id="content">
	<div class="tray tray-center">
		<h2 class="text-dark mb15">Languages</h2>
		<div class="languages index panel">
			<?php if (!empty($languages->count())) { ?>
				<div class="panel-body">
					<div class="row">
						<?php if ($this->request->params['prefix'] == 'admin') { ?>
							<div class="col-md-4 has-success"><?= $this->element('Admin/Widget/entity_search'); ?></div>
						<?php } ?>
						<div class="col-md-4 pull-right text-right hidden">
							<div class="ib topbar-dropdown mt5">
								<label for="filter-period" class="control-label pr15 text-dark">Filter Period</label>
								<select id="filter-period" class="hidden" name="filter_days">
									<optgroup label="Filter By:">
										<option value="30" selected="selected">Last 30 Days</option>
										<option value="60">Last 60 Days</option>
										<option value="365">Last Year</option>
									</optgroup>
								</select>
							</div>
						</div>
					</div>
				</div>
				<div class="panel-body pn">
					<div class="table-responsive">
						<table cellpadding="0" cellspacing="0" class="table themed-form table-hover theme-info tc-checkbox-1 fs13">
							<thead>
								<tr class="bg-light">
									<th width="1"></th>
									<th><?= $this->Paginator->sort('id'); ?></th>
									<th><?= $this->Paginator->sort('name'); ?></th>
									<th><?= $this->Paginator->sort('code'); ?></th>
									<th><?= $this->Paginator->sort('locale'); ?></th>
									<th><?= $this->Paginator->sort('sort_order'); ?></th>
									<th><?= $this->Paginator->sort('status'); ?></th>
									<th><?= $this->Paginator->sort('created'); ?></th>
									<th class="actions text-center"><?= __('Actions'); ?></th>
								</tr>
							</thead>
							<tbody>
								<?php foreach ($languages as $language) : ?>
									<tr>
										<td width="20">
											<label class="option block mn hidden">
												<input type="checkbox" name="id" value="<?= $language->id ?>">
												<span class="checkbox mn"></span>
											</label>
										</td>
										<td width="1"><?= $this->Html->link($language->id, ['action' => 'edit', $language->id]); ?></td>
										<td><?= h($language->name); ?></td>
										<td><?= h($language->code); ?></td>
										<td><?= h($language->locale); ?></td>
										<td><?= $this->Number->format($language->sort_order); ?></td>
										<td><?= h($language->status); ?></td>
										<td><?= h($language->created); ?></td>
										<td class="actions text-center" width="200">
											<div class="btn-group btn-group-sm ib">
												<?= $this->Html->link(__('Edit'), ['action' => 'edit', $language->id], ['class' => 'btn btn-primary']); ?>
												<?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $language->id], ['confirm' => __('Are you sure you want to delete #{0}?', $language->id), 'class' => 'btn btn-danger']); ?>
											</div>
										</td>
									</tr>
								<?php endforeach; ?>
							</tbody>
						</table>
					</div>
				</div>
				<div class="panel-footer">
					<div class="paginator">
						<ul class="pagination mn va-t">
							<?= $this->Paginator->prev(__('previous')); ?>
							<?= $this->Paginator->numbers(); ?>
							<?= $this->Paginator->next(__('next')); ?>
						</ul>
						<div class="ib m5 mh20 va-t"><?= $this->Paginator->counter(); ?></div>
					</div>
				</div>
			<?php } else { ?>
				<div class="panel-body text-center fs16 lh50 pv20"><i class="fa fa-ban text-danger"></i>&nbsp;&nbsp;No results found.</div>
			<?php } ?>
			<script type="text/javascript">
				(function($) {
					$(document).ready(function() {
						if ($("#filter-period").length) {
							$('#filter-period').multiselect({
								buttonClass: 'btn btn-default btn-sm ph15',
								dropRight: true
							});
						}
					});
				})(jQuery);
			</script>
		</div>
	</div>
</div>
