<?php use Cake\Utility\Inflector; ?>
<header id="topbar">
	<div class="row">
		<div class="col-xs-12">
			<ol class="breadcrumb">
				<li class="crumb-trail"><a href="/<?= $this->request->params['prefix']; ?>/"><?= Inflector::humanize($this->request->params['prefix']); ?></a></li>
				<li class="crumb-trail"><a href="/<?= $this->request->params['prefix'] . '/' . $this->request->params['controller']; ?>">SEO Data</a></li>
				<li class="crumb-active">Edit</li>
			</ol>
		</div>
	</div>
	<div class="row">
		<div class="col-xs-12">
			<div class="btn-toolbar mt5">
				<div class="btn-group btn-group-sm">
					<?= $this->Html->link(__('View All SEO Data'), ['action' => 'index'], ['class' => 'btn btn-primary']); ?>
				</div>

			</div>
		</div>
	</div>
</header>

<div id="content">
	<div class="tray tray-center">
		<div class="mw1000">
			<div class="seo_data form">
				<div class="panel panel-dark">
					<div class="panel-heading">
						<span class="panel-title fw700"><?= __('Add SEO Data'); ?></span>
					</div>
					<div class="panel-body">
						<?= $this->Form->create($seo, ['class' => 'form', 'novalidate' => true]); ?>
						<div class="row">
							<?php if (!empty($vd['languages'])) { ?>
								<div class="col-xs-12">
									<div class="tab-block" style="margin:0 -16px 25px">
										<ul class="nav nav-tabs">
											<?php $i = 0;
											foreach ($vd['languages'] as $language) {
												$i++; ?>
												<li <?= ($i == 1) ? 'class="active"' : ''; ?>><a href="#tab_language_<?= $language['id']; ?>" data-toggle="tab">
														<img src="/ui/img/flags/24/<?= $language['code']; ?>.png" />&nbsp;<?= $language['name']; ?>
													</a></li>
											<?php } ?>
										</ul>
										<div class="tab-content">
											<?php $i = 0;
											foreach ($vd['languages'] as $language) {
												$i++; ?>
												<?php $locale = $language['locale']; ?>
												<div id="tab_language_<?= $language['id']; ?>" class="tab-pane <?= ($i == 1) ? 'active' : ''; ?>">
													<?= $this->Form->input('seo_title', [
														'type'  => 'text',
														'label' => 'SEO Title',
														'name'  => '_translations[' . $locale . '][seo_title]',
														'class' => 'form-control seo-title',
														'id'    => 'translation-seo-title-' . $locale,
														'value' => !empty($_translations[$locale]['seo_title']) ? $_translations[$locale]['seo_title'] : ''
													]); ?>
													<?= $this->Form->input('seo_description', [
														'type'  => 'textarea',
														'label' => 'SEO Description',
														'name'  => '_translations[' . $locale . '][seo_description]',
														'class' => 'form-control seo-description',
														'id'    => 'translation-seo-description-' . $locale,
														'value' => !empty($_translations[$locale]['seo_description']) ? $_translations[$locale]['seo_description'] : ''
													]); ?>
												</div>
											<?php } ?>
											<br />
											<?= $this->Form->input('page', [
												'type'  => 'select',
												'label' => 'Select a page for which to apply the SEO Title',
												'name'  => 'page',
												'class' => 'form-control page',
												'options' => $pages,
											]); ?>
										</div>
									</div>
								</div>
							<?php } ?>
						</div>
						<div class="row">
							<div class="col-xs-12">
								<?= $this->Form->button(__('Submit'), ['class' => 'btn btn-primary btn-lg']); ?>
							</div>
						</div>
						<?= $this->Form->end(); ?>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
