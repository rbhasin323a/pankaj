<?php use Cake\Utility\Inflector; ?>
<header id="topbar">
	<div class="row">
		<div class="col-xs-12">
			<ol class="breadcrumb">
				<li class="crumb-trail"><a href="/<?= $this->request->params['prefix']; ?>/"><?= Inflector::humanize($this->request->params['prefix']); ?></a></li>
				<li class="crumb-active">SEO Data</li>
			</ol>
		</div>
	</div>
	<div class="row">
		<div class="col-xs-12">
			<div class="btn-toolbar mt5">
				<div class="btn-group btn-group-sm">
					<?= $this->Html->link(__('New SEO data'), ['action' => 'add'], ['class' => 'btn btn-primary']); ?>
				</div>

			</div>
		</div>
	</div>
</header>

<div id="content">
	<div class="tray tray-center">
		<h2 class="text-dark mb15">SEO Data</h2>
		<div class="contacts index panel">
			<?php if (!empty($seos->count())) { ?>
				<div class="panel-body">
					<div class="row">
						<?php if ($this->request->params['prefix'] == 'admin') { ?>
							<div class="col-md-4 has-success"><?= $this->element('Admin/Widget/entity_search'); ?></div>
						<?php } ?>
					</div>
				</div>
				<div class="panel-body pn">
					<div class="table-responsive">
						<table cellpadding="0" cellspacing="0" class="table themed-form table-hover theme-info tc-checkbox-1 fs13">
							<thead>
								<tr class="bg-light">
									<th width="1"></th>
									<th><?= $this->Paginator->sort('id'); ?></th>
									<th><?= $this->Paginator->sort('seo_title'); ?></th>
									<th><?= $this->Paginator->sort('seo_description'); ?></th>
									<th><?= $this->Paginator->sort('page'); ?></th>
									<th class="actions text-center"><?= __('Actions'); ?></th>
								</tr>
							</thead>
							<tbody>
								<?php foreach ($seos as $seo) : ?>
									<tr>
										<td width="20">
											<label class="option block mn hidden">
												<input type="checkbox" name="id" value="<?= $seo->id ?>">
												<span class="checkbox mn"></span>
											</label>
										</td>
										<td width="1"><?= $this->Html->link($seo->id, ['action' => 'edit', $seo->id]); ?></td>
										<td><?= h($seo->seo_title); ?></td>
										<td><?= h($seo->seo_description); ?></td>
										<td><?= h($seo->page); ?></td>
										<td class="actions text-center" width="200">
											<div class="btn-group btn-group-sm ib">
												<?= $this->Html->link(__('Edit'), ['action' => 'edit', $seo->id], ['class' => 'btn btn-primary']); ?>
												<?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $seo->id], ['confirm' => __('Are you sure you want to delete #{0}?', $seo->id), 'class' => 'btn btn-danger']); ?>
											</div>
										</td>
									</tr>
								<?php endforeach; ?>
							</tbody>
						</table>
					</div>
				</div>
				<div class="panel-footer">
					<div class="paginator">
						<ul class="pagination mn va-t">
							<?= $this->Paginator->prev(__('previous')); ?>
							<?= $this->Paginator->numbers(); ?>
							<?= $this->Paginator->next(__('next')); ?>
						</ul>
						<div class="ib m5 mh20 va-t"><?= $this->Paginator->counter(); ?></div>
					</div>
				</div>
			<?php } else { ?>
				<div class="panel-body text-center fs16 lh50 pv20"><i class="fa fa-ban text-danger"></i>&nbsp;&nbsp;No results found.</div>
			<?php } ?>
			<script type="text/javascript">
				(function($) {
					$(document).ready(function() {
						if ($("#filter-period").length) {
							$('#filter-period').multiselect({
								buttonClass: 'btn btn-default btn-sm ph15',
								dropRight: true
							});
						}
					});
				})(jQuery);
			</script>
		</div>
	</div>
</div>
