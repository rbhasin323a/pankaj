<?php use Cake\Utility\Inflector; ?>
<header id="topbar">
	<div class="row">
		<div class="col-xs-12">
			<ol class="breadcrumb">
				<li class="crumb-trail"><a href="/<?= $this->request->params['prefix']; ?>/"><?= Inflector::humanize($this->request->params['prefix']); ?></a></li>
				<li class="crumb-trail"><a href="/<?= $this->request->params['prefix'] . '/' . $this->request->params['controller']; ?>">Footer Data</a></li>
			</ol>
		</div>
	</div>
</header>

<div id="content">
	<div class="tray tray-center">
		<div class="mw1000">
			<div class="footer_data form">
				<div class="panel panel-dark">
					<div class="panel-heading">
						<span class="panel-title fw700"><?= __('Footer Data'); ?></span>
					</div>
					<div class="panel-body">
						<?= $this->Form->create($footer, ['class' => 'form', 'novalidate' => true]); ?>
						<div class="row">
							<?php if (!empty($vd['languages'])) { ?>
								<div class="col-xs-12">
									<div class="tab-block" style="margin:0 -16px 25px">
										<ul class="nav nav-tabs">
											<?php $i = 0;
											foreach ($vd['languages'] as $language) {
												$i++; ?>
												<li <?= ($i == 1) ? 'class="active"' : ''; ?>><a href="#tab_language_<?= $language['id']; ?>" data-toggle="tab">
														<img src="/ui/img/flags/24/<?= $language['code']; ?>.png" />&nbsp;<?= $language['name']; ?>
													</a></li>
											<?php } ?>
										</ul>
										<div class="tab-content">
											<?php $i = 0;
											foreach ($vd['languages'] as $language) {
												$i++; ?>
												<?php $locale = $language['locale']; ?>
												<div id="tab_language_<?= $language['id']; ?>" class="tab-pane <?= ($i == 1) ? 'active' : ''; ?>">
													<?= $this->Form->input('copyright_text', [
														'type'  => 'text',
														'label' => 'Content for the copy-right text',
														'name'  => '_translations[' . $locale . '][copyright_text]',
														'class' => 'form-control copyright-text',
														'id'    => 'translation-copyright-text-' . $locale,
														'value' => !empty($_translations[$locale]['copyright_text']) ? $_translations[$locale]['copyright_text'] : ''
													]); ?>
													<br />
													<?= $this->Form->input('menu_content', [
														'type'  => 'textarea',
														'label' => 'Menu content',
														'name'  => '_translations[' . $locale . '][menu_content]',
														'class' => 'form-control menu-conent',
														'id'    => 'translation-menu-conent-' . $locale,
														'value' => !empty($_translations[$locale]['menu_content']) ? $_translations[$locale]['menu_content'] : ''
													]); ?>
												</div>
											<?php } ?>
											<br />
										</div>
									</div>
								</div>
							<?php } ?>
						</div>
						<div class="row">
							<div class="col-xs-12">
								<?= $this->Form->button(__('Submit'), ['class' => 'btn btn-primary btn-lg']); ?>
							</div>
						</div>
						<?= $this->Form->end(); ?>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
