<?php use Cake\Utility\Inflector; ?>
<header id="topbar">
	<div class="row">
		<div class="col-xs-12">
			<ol class="breadcrumb">
				<li class="crumb-trail"><a href="/<?= $this->request->params['prefix']; ?>/"><?= Inflector::humanize($this->request->params['prefix']); ?></a></li>
				<li class="crumb-active">Bookings</li>
			</ol>
		</div>
	</div>
	<div class="row">
		<div class="col-xs-12">
			<div class="btn-toolbar mt5">
				<div class="btn-group btn-group-sm">
					<?= $this->Html->link(__('New Booking'), ['action' => 'add'], ['class' => 'btn btn-primary']); ?>
				</div>
			</div>
		</div>
	</div>
</header>
<div id="content">
	<div class="tray tray-center">
		<h2 class="text-dark mb15">Bookings</h2>
		<div class="bookings index panel">
			<form action="/admin/bookings" id="form-filter">
				<div class="panel-body">
					<div class="row">
						<div class="col-md-4">
							<div class="ib topbar-dropdown mt5">
								<label for="filter-note" class="control-label pr15 text-dark">Special occasions</label>
								<select id="filter-note" class="filter-select" name="filter_notes">
									<?php if (
										!isset($this->request->query['filter_notes']) || (isset($this->request->query['filter_notes']) && $this->request->query['filter_notes'] == 'all')
									) { ?>
										<option value="all" selected='selected'>Show all</option>
									<?php } else { ?>
										<option value="all">Show all</option>
									<?php } ?>

									<?php if (isset($this->request->query['filter_notes']) && $this->request->query['filter_notes'] == 1) { ?>
										<option value="1" selected='selected'>With special occasion</option>
									<?php } else { ?>
										<option value="1">With special occasion</option>
									<?php } ?>

									<?php if (isset($this->request->query['filter_notes']) && $this->request->query['filter_notes'] == 0 && $this->request->query['filter_notes'] != 'all') { ?>
										<option value="0" selected='selected'>Without special occasion</option>
									<?php } else { ?>
										<option value="0">Without special occasion</option>
									<?php } ?>
								</select>
							</div>
						</div>
						<div class="col-md-4">
							<label for="filter-supplier" class="control-label pr15 text-dark">Supplier</label>
							<select id="filter-supplier" class="filter-select" name="filter_supplier">
								<option value="">Show all</option>
								<?php foreach ($suppliers as $supplier) { ?>
									<?php if (isset($this->request->query['filter_supplier'])) { ?>
										<?php if ($this->request->query['filter_supplier'] == $supplier->id) { ?>
											<option value="<?= $supplier->id; ?>" selected="selected"><?= $supplier->name; ?></option>
										<?php } else { ?>
											<option value="<?= $supplier->id; ?>"><?= $supplier->name; ?></option>
										<?php } ?>
									<?php } else {  ?>
										<option value="<?= $supplier->id; ?>"><?= $supplier->name; ?></option>
									<?php } ?>
								<?php } ?>
							</select>
						</div>
						<div class="col-md-4">
							<label for="filter-sales" class="control-label pr15 text-dark">Sales</label>
							<select id="filter-sale" class="filter-select" name="filter_sale">
								<option value="">Show all</option>
								<?php foreach ($sales as $sale) { ?>
									<?php if (isset($this->request->query['filter_sale'])) { ?>
										<?php if ($this->request->query['filter_sale'] == $sale->id) { ?>
											<option value="<?= $sale->id; ?>" selected="selected"><?= $sale->title; ?></option>
										<?php } else { ?>
											<option value="<?= $sale->id; ?>"><?= $sale->title; ?></option>
										<?php } ?>
									<?php } else {  ?>
										<option value="<?= $sale->id; ?>"><?= $sale->title; ?></option>
									<?php } ?>
								<?php } ?>
							</select>
						</div>
					</div>
					<div class="row mt10">
						<div class="col-md-4">
							<label for="filter-date-from" class="control-label pr15 text-dark">Date From</label>
							<input type="text" class="form-control datepicker" id="filter-date-from" name="filter_date_from" value="<?= !empty($this->request->query['filter_date_from']) ? $this->request->query['filter_date_from'] : ''; ?>">
						</div>
						<div class="col-md-4">
							<label for="filter-date-to" class="control-label pr15 text-dark">Date To</label>
							<input type="text" class="form-control datepicker" id="filter-date-to" name="filter_date_to" value="<?= !empty($this->request->query['filter_date_to']) ? $this->request->query['filter_date_to'] : ''; ?>">
						</div>
						<div class="col-md-4">
						</div>
					</div>
					<div class="row">
						<div class="col-md-4 col-md-offset-8">
							<button type="submit" form="form-filter" class="btn btn-info pull-right text-center">Filter</button>
						</div>
					</div>
				</div>
			</form>
			<?php if (!empty($bookings->count())) { ?>
				<div class="panel-body pn">
					<form action="<?= '/admin/bookings/csvExport?' . http_build_query($this->request->query); ?>" target="_blank" method="POST" id="form-csv">
						<div class="table-responsive">
							<table cellpadding="0" cellspacing="0" class="table themed-form table-hover theme-info tc-checkbox-1 fs13">
								<thead>
									<tr class="bg-light">
										<th width="60" class="text-left">
											<label class="option mn">
												<input type="checkbox" id="check-all">
												<span class="checkbox mn"></span>
											</label>
										</th>
										<th><?= $this->Paginator->sort('id'); ?></th>
										<th><?= $this->Paginator->sort('status'); ?></th>
										<th><?= $this->Paginator->sort('transaction_id'); ?></th>
										<th><?= $this->Paginator->sort('sale_total'); ?></th>
										<th><?= $this->Paginator->sort('user_id'); ?></th>
										<th><?= $this->Paginator->sort('sale_id'); ?></th>
										<th><?= $this->Paginator->sort('created'); ?></th>
										<th class="actions text-center"><?= __('Actions'); ?></th>
									</tr>
								</thead>
								<tbody>
									<?php foreach ($bookings as $booking) : ?>
										<tr <?= ($booking->transaction_status != '0') ? 'class="failed-transaction"' : '' ?>>
											<td width="60" class="text-left">
												<label class="option mn">
													<input type="checkbox" name="selected[]" value="<?= $booking->id ?>">
													<span class="checkbox mn"></span>
												</label>
												<?php if (!empty($booking->notes)) { ?>
													<i class="fa fa-exclamation-triangle fa-posfix icon-danger"></i>
												<?php } ?>
											</td>
											<td width="1"><?= $this->Html->link($booking->id, ['action' => 'edit', $booking->id]); ?></td>
											<td><?= h($booking->status); ?></td>
											<td>
												<?= $booking->has('transaction') ? $this->Html->link($booking->transaction->title, ['controller' => 'Transactions', 'action' => 'edit', $booking->transaction->id]) : '' ?>
											</td>
											<td><?= $booking->sale_total ?></td>
											<td>
												<?= $booking->has('my_user') ? $this->Html->link($booking->first_name . ' ' . $booking->last_name, ['controller' => 'Users', 'action' => 'edit', $booking->my_user->id]) : '' ?>
											</td>
											<td>
												<?= $booking->has('sale') ? $this->Html->link($booking->sale->id, ['controller' => 'Sales', 'action' => 'edit', $booking->sale->id]) : '' ?>
											</td>
											<td><?= h($booking->created); ?></td>
											<td class="actions text-center" width="200">
												<div class="btn-group btn-group-sm ib">
													<?= $this->Html->link(__('Edit'), ['action' => 'edit', $booking->id], ['class' => 'btn btn-primary']); ?>
												</div>
											</td>
										</tr>
									<?php endforeach; ?>
								</tbody>
							</table>
						</div>
					</form>
				</div>
				<div class="panel-footer">
					<div class="paginator">
						<div class="btn-group btn-group-sm pull-right">
							<button type="submit" class="btn btn-info" form="form-csv">Export to CSV</button>
							<button type="submit" name="csv-eu-format" value="1" class="btn btn-warning" form="form-csv">Export to CSV (EU date)</button>
						</div>
						<ul class="pagination mn va-t">
							<?= $this->Paginator->prev(__('previous')); ?>
							<?= $this->Paginator->numbers(); ?>
							<?= $this->Paginator->next(__('next')); ?>
						</ul>
						<div class="ib m5 mh20 va-t"><?= $this->Paginator->counter(); ?></div>
					</div>
				</div>
			<?php } else { ?>
				<div class="panel-body text-center fs16 lh50 pv20"><i class="fa fa-ban text-danger"></i>&nbsp;&nbsp;No results found.</div>
			<?php } ?>
			<script type="text/javascript">
				(function($) {
					$('body').addClass('admin-validation-page');

					$(document).ready(function() {
						if ($(".filter-select").length) {
							$('.filter-select').multiselect({
								buttonClass: 'btn btn-default btn-sm ph15',
								dropRight: true
							});
						}

						$('.datepicker').datetimepicker({
							format: 'YYYY-MM-DD'
						});

						$('#check-all').on('click', function() {
							if ($(this).is(':checked')) {
								$('input[name="selected[]"]').prop('checked', true);
							} else {
								$('input[name="selected[]"]').prop('checked', false);
							}
						});
					});
				})(jQuery);
			</script>
		</div>
	</div>
</div>
