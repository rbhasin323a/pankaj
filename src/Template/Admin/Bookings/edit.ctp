<?php use Cake\Utility\Inflector; ?>
<header id="topbar">
	<div class="row">
		<div class="col-xs-12">
			<ol class="breadcrumb">
				<li class="crumb-trail"><a href="/<?= $this->request->params['prefix']; ?>/"><?= Inflector::humanize($this->request->params['prefix']); ?></a></li>
				<li class="crumb-trail"><a href="/<?= $this->request->params['prefix'] . '/' . $this->request->params['controller']; ?>">Bookings</a></li>
				<li class="crumb-active">Edit</li>
			</ol>
		</div>
	</div>
	<div class="row">
		<div class="col-xs-12">
			<div class="btn-toolbar mt5">
				<div class="btn-group btn-group-sm">
					<?= $this->Html->link(__('List Bookings'), ['action' => 'index'], ['class' => 'btn btn-primary']); ?>
				</div>
			</div>
		</div>
	</div>
</header>

<div id="content">
	<div class="tray tray-center">
		<div class="mw1000">
			<div class="bookings form">
				<div class="panel panel-dark">
					<div class="panel-heading">
						<span class="panel-title fw700"><?= __('Edit Booking'); ?></span>
					</div>
					<div class="panel-body">
						<ul class="nav nav-pills pull-right">
							<li class="active"><a href="#booking_details" data-toggle="tab" aria-expanded="false">Booking Details</a></li>
							<?php if (!empty($booking->booking_travellers)) { ?>
								<li><a href="#travellers" data-toggle="tab" aria-expanded="false">Travellers</a></li>
							<?php } ?>
						</ul>
						<div class="clearfix"></div>

						<div class="tab-content pn br-n">
							<div id="booking_details" class="tab-pane active">
								<?= $this->Form->create($booking, ['class' => 'form', 'novalidate' => true]); ?>
								<div class="row">
									<?php $this->Form->templates([
										'inputContainer'        => '<div class="col-md-6 input-container">{{content}}</div>',
										'inputContainerError'   => '<div class="col-md-6 input-container">{{content}}</div>',

										'label'                 => '<label {{attrs}}>{{text}}</label>{{hidden}}{{input}}',

										'formGroup'             => '{{label}}{{input}}{{error}}',
										'nestingLabel'          => '<div class="radio-custom square radio-primary mt10">{{input}} <label {{attrs}}>{{text}}</label></div>',

										// Input
										'input'                 => '<input type="{{type}}" name="{{name}}" {{attrs}} class="form-control" />',

										// Textarea
										'textarea'              => '<textarea name="{{name}}" {{attrs}} class="form-control">{{value}}</textarea>',

										// Select
										'select'                => '<select name="{{name}}" {{attrs}} class="form-control select2-single">{{content}}</select>',
										'selectMultple'         => '<select multiple name="{{name}}" {{attrs}} class="form-control select2-multiple">{{content}}</select>',

										'dateWidget'            => '{{day}}{{month}}{{year}}{{hour}}{{minute}}{{second}}',

										// Error
										'error'                 => '<div class="text-danger">{{content}}</div>',
										'errorList'             => '{{content}}',
										'errorItem'             => '{{text}}',
									]);
									?>
									<?= $this->Form->input('first_name', ['type' => 'text', 'class' => 'form-control']); ?>
									<?= $this->Form->input('last_name', ['type' => 'text', 'class' => 'form-control']); ?>
									<?= $this->Form->input('phone', ['type' => 'text', 'class' => 'form-control']); ?>
									<?= $this->Form->input('rooms', ['class' => 'form-control']); ?>
									<?= $this->Form->input('adults', ['class' => 'form-control']); ?>
									<?= $this->Form->input('children', ['class' => 'form-control']); ?>
									<?= $this->Form->input('infants', ['class' => 'form-control']); ?>
									<?= $this->Form->input('status', ['class' => 'form-control status']); ?>
									<?= $this->Form->input('check_in', ['type' => 'text', 'class' => 'form-control']); ?>
									<?= $this->Form->input('check_out', ['type' => 'text', 'class' => 'form-control']); ?>
									<?= $this->Form->input('type', ['class' => 'form-control type']); ?>
									<?= $this->Form->input('provider', ['class' => 'form-control provider']); ?>

									<?= $this->Form->input('transaction_id', [
										'options' => $transactions,
									]); ?>
									<?= $this->Form->input('user_id', [
										'options' => $users,
									]); ?>
									<?= $this->Form->input('sale_id', [
										'options' => $sales,
									]); ?>
									<?= $this->Form->input('details', ['class' => 'form-control details']); ?>
									<?= $this->Form->input('notes', ['class' => 'form-control notes', 'label' => 'Special occasion']); ?>
								</div>
								<div class="row">
									<div class="col-xs-12">
										<?= $this->Form->button(__('Submit'), ['class' => 'btn btn-primary btn-lg']); ?>
									</div>
								</div>
								<?= $this->Form->end(); ?>
							</div>
							<?php if (!empty($booking->booking_travellers)) { ?>
								<div id="travellers" class="tab-pane">
									<?= $this->Form->create($booking->booking_travellers, ['class' => 'form', 'novalidate' => true, 'url' => '/admin/booking_travellers/edit/' . $booking->id]); ?>
									<div class="row">
										<?php $this->Form->templates([
											'inputContainer'        => '<div class="col-md-6 input-container">{{content}}</div>',
											'inputContainerError'   => '<div class="col-md-6 input-container">{{content}}</div>',

											'label'                 => '<label {{attrs}}>{{text}}</label>{{hidden}}{{input}}',

											'formGroup'             => '{{label}}{{input}}{{error}}',
											'nestingLabel'          => '<div class="radio-custom square radio-primary mt10">{{input}} <label {{attrs}}>{{text}}</label></div>',

											// Input
											'input'                 => '<input type="{{type}}" name="{{name}}" {{attrs}} class="form-control" />',

											// Textarea
											'textarea'              => '<textarea name="{{name}}" {{attrs}} class="form-control">{{value}}</textarea>',

											// Select
											'select'                => '<select name="{{name}}" {{attrs}} class="form-control select2-single">{{content}}</select>',
											'selectMultple'         => '<select multiple name="{{name}}" {{attrs}} class="form-control select2-multiple">{{content}}</select>',

											'dateWidget'            => '{{day}}{{month}}{{year}}{{hour}}{{minute}}{{second}}',

											// Error
											'error'                 => '<div class="text-danger">{{content}}</div>',
											'errorList'             => '{{content}}',
											'errorItem'             => '{{text}}',
										]); ?>
										<?php $key = 0; ?>
										<?php $i = 0; ?>
										<?php foreach ($booking->booking_travellers as $traveller) { ?>
											<?php $i++; ?>
											<h2 class="mh10 mb15"> Traveller <?= $i ?></h1>
												<?= $this->Form->input('traveller[' . $key . '][type]', [
													'type' => 'select',
													'options' => ['adult' => 'adult', 'infant' => 'infant', 'child' => 'child'],
													'class' => 'form-control',
													'value' => $traveller['type'],
													'label' => 'Type'
												]); ?>
												<?= $this->Form->input('traveller[' . $key . '][title]', [
													'type' => 'select',
													'options' => ['Mr' => 'Mr', 'Ms' => 'Ms', 'Mrs' => 'Mrs'],
													'class' => 'form-control',
													'value' => $traveller['title'],
													'label' => 'Title'
												]); ?>
												<?= $this->Form->input('traveller[' . $key . '][first_name]', [
													'type' => 'text',
													'class' => 'form-control',
													'value' => $traveller['first_name'],
													'label' => 'First Name'
												]); ?>
												<?= $this->Form->input('traveller[' . $key . '][last_name]', [
													'type' => 'text',
													'class' => 'form-control',
													'value' => $traveller['last_name'],
													'label' => 'Last Name'
												]); ?>
												<?= $this->Form->input('traveller[' . $key . '][passport_number]', [
													'type' => 'text',
													'class' => 'form-control',
													'value' => $traveller['passport_number'],
													'label' => 'Passport Number'
												]); ?>
												<?= $this->Form->input('traveller[' . $key . '][birthday]', [
													'type' => 'text',
													'class' => 'date form-control',
													'value' => date('Y-m-d', strtotime($traveller['birthday'])),
													'label' => 'Birthday'
												]); ?>
												<?= $this->Form->input('traveller[' . $key . '][booking_id]', [
													'type' => 'hidden',
													'class' => 'form-control',
													'value' => $traveller['booking_id']
												]); ?>
												<?= $this->Form->input('traveller[' . $key . '][id]', [
													'type' => 'hidden',
													'class' => 'form-control',
													'value' => $traveller['id']
												]); ?>
												<?php $key++; ?>
												<div class="clearfix"></div>
											<?php } ?>

									</div>
									<div class="row">
										<div class="col-xs-12">
											<?= $this->Form->button(__('Submit'), ['class' => 'btn btn-primary btn-lg']); ?>
										</div>
									</div>
									<?= $this->Form->end(); ?>
								</div>
							<?php } ?>
						</div>

						<script type="text/javascript">
							(function($) {
								$(document).ready(function() {
									// Init Select2 - Basic Single
									$(".select2-single, select.form-error").each(function(index, el) {
										$(this).addClass('form-control').select2({
											placeholder: 'Choose ' + $('label', $(this).parent()).text(),
										});
									});

									// Init Select2 - Basic Multiple
									$(".select2-multiple, select.form-error").each(function(index, el) {
										$(this).addClass('form-control').select2({
											allowClear: true,
											placeholder: 'Choose ' + $('label', $(this).parent()).text(),
										});
									});

									$('.date').datepicker({
										format: 'YYYY-MM-DD',
										onSelect: function(dateText, inst) {
											var date = $(this).val();

											dateText = inst.selectedYear.toString() + '-' + inst.selectedMonth.toString() + '-' + inst.selectedDay;

											$(this).val(dateText);
										}
									});

									$('.datetime').datetimepicker({
										format: 'YYYY-MM-DD HH:mm'
									});



								});
							})(jQuery);
						</script>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
