<?php
$this->assign('title', __('Contact Us'));

$this->loadHelper('Form', [
	'templates' => 'app_form',
]);

$this->HtmlBetter->addCrumb(__('Home'), '/');
$this->HtmlBetter->addCrumb(__('Contact Us'), '/contact');

$name = !empty($vd['user']['first_name']) ? $vd['user']['first_name'] : '';
$name = !empty($vd['user']['last_name']) ? $name . ' ' . $vd['user']['last_name'] : '';

$email = !empty($vd['user']['email']) ? $vd['user']['email'] : '';
?>
<section id="PageContact">
	<div class="container">
		<div class="row">
			<div class="col s12 l3 sidenav-left">
				<?= $this->element('Sidebar/nav-pages'); ?>
			</div>
			<div class="col s12 l9">
				<h4 class="page-heading"><?= __('Contact Us'); ?></h4>
				<div class="card-panel p30">
					<div class="row mbn">
						<div class="luxury-contact-details">
							<div class="col s12 m6">
								<div class="luxury-contact-details__email">
									<p><strong>Email адрес</strong></p>
									<span><a href="mailto:support@luxury-discounts.com">support@luxury-discounts.com</a></span>
								</div>
							</div>
							<div class="col s12 m6">
								<div class="luxury-contact-details__phone">
									<p><strong>Телефон</strong></p>
									<span>+359 2 4917 959</span>
								</div>
							</div>
						</div>
					</div>
					<?= $this->Form->create($contact); ?>
					<div class="row mt20 mbn">
						<div class="col s12 m6">
							<?= $this->Form->input('name', ['class' => 'validate', 'value' => $name]); ?>
						</div>
						<div class="col s12 m6">
							<?= $this->Form->input('email', ['class' => 'validate', 'value' => $email]); ?>
						</div>
						<div class="col s12">
							<?= $this->Form->input('message', ['class' => 'materialize-textarea validate', 'rows' => '4']); ?>
						</div>
						<div class="col s12">
							<?= $this->Form->button('Send <i class="material-icons right">send</i>', ['class' => 'waves-effect waves-light btn-large amber darken-3 right']); ?>
						</div>
					</div>
					<?= $this->Form->end(); ?>
				</div>
			</div>
		</div>
	</div>
</section>
