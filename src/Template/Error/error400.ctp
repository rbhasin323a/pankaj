<?php
	$this->assign('title', __('Not Found'));

	$this->HtmlBetter->addCrumb(__('Home'), '/');
	$this->HtmlBetter->addCrumb(__('Not Found'), $this->request->getAttribute('here'));
?>
<section id="Error400">
	<div class="container">
		<div class="row mv50 pv50">
			<div class="col s12 m8 offset-m2 l6 offset-l3">
				<h4 class="page-heading"><?= __d('cake', 'The requested address was not found.'); ?></h4>
				<div class="card-panel">
					<p class="text-danger mbn"><?= $url; ?></p>
				</div>
			</div>
		</div>
	</div>
</section>
