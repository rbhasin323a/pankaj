<?php
use Cake\Core\Configure;
use Cake\Error\Debugger;

$this->assign('title', __('Internal Error'));

$this->HtmlBetter->addCrumb(__('Home'), '/');
$this->HtmlBetter->addCrumb(__('Internal Error'), $this->request->getAttribute('here'));

if (Configure::read('debug')) :
	$this->viewBuilder()->setLayout('dev_error');

	$this->assign('templateName', 'error500.ctp');

	$this->start('file');
	?>
	<?php if (!empty($error->queryString)) : ?>
		<p class="notice">
			<strong>SQL Query: </strong>
			<?= h($error->queryString); ?>
		</p>
	<?php endif; ?>
	<?php if (!empty($error->params)) : ?>
		<strong>SQL Query Params: </strong>
		<?= Debugger::dump($error->params); ?>
	<?php endif; ?>
	<?php
	echo $this->element('auto_table_warning');

	if (extension_loaded('xdebug')) :
		xdebug_print_function_stack();
	endif;

	$this->end();
endif;
?>
<section id="Error500">
	<div class="container">
		<div class="row mv50 pv50">
			<div class="col s12 m8 offset-m2 l6 offset-l3">
				<h4 class="page-heading"><?= __d('cake', 'An internal error has occurred.'); ?></h4>
				<div class="card-panel">
					<p class="text-danger mbn"><?= $message; ?></p>
				</div>
			</div>
		</div>
	</div>
</section>
