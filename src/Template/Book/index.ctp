<?php
$this->assign('title', __('Book'));

$this->loadHelper('Form', [
	'templates' => 'app_form',
]);
?>
<section id="Book" class="page">
	<div class="page-subheader"><nav>
		<div class="nav-wrapper">
			<div class="container">
				<div class="col s12">
					<a class="breadcrumb" href="/"><?= __('Home'); ?></a>
					<a class="breadcrumb" href="<?= $this->Url->build(['controller' => 'Book', 'action' => 'index']); ?>"><?= __('Book'); ?></a>
				</div>
			</div>
		</div>
	</nav></div>
	<div class="page-alerts"><?= $this->Flash->render(); ?></div>
	<div class="container">
		<div class="row">
			<div class="col s12 l3 sidenav-left">
				<?= $this->element('Sidebar/nav-pages'); ?>
			</div>
			<div class="col s12 l9">
				<h4 class="page-heading"><?= __('Book'); ?></h4>
				<div class="card-panel">
					<form action="" method="POST">
						<div id="credit-card-fields">

						</div>
					</form>
					<script type="text/javascript" src="https://bridge.paymill.com/"></script>
					<script type="text/javascript">
					paymill.embedFrame('credit-card-fields', {
						resize: false,
						lang: 'en'
					}, function(error) {
						if (error) {
							// Frame could not be loaded, check error object for reason.
							console.log(error.apierror, error.message);
							// Example: "container_not_found"
						} else {
							// Frame was loaded successfully and is ready to be used.
						}
					});
					</script>
				</div>
			</div>
		</div>
	</div>
</section>
