<?php
/**
 * Home Search Component Cell
 */
$this->loadHelper('Form', [
	'templates' => 'app_form',
]);

$c_id = substr(md5(uniqid(mt_rand(), true)), 0, 10);
$c_class = 'home-search';
?>
<div id="HomeSearchCell-<?= $c_id; ?>" class="<?= $c_class; ?> <?= $c_class; ?>--winter inline-full va-t">
	<div class="container">
		<div class="pv25 mv25 hide-on-med-and-down"></div>
		<div class="row">
			<?php if (!empty($special_sale_offers_count) || !empty($special_sales)) { ?>
			<div class="col s12 m6 push-m3 l6 push-l3 xl4">
				<h3 class="fs24 fw300 mv25 pv25 text-center"><?= __d('HomeSearchCell', '{0,plural,=0{NO SPECIAL OFFERS} =1{{1} SPECIAL OFFER} other{{1} SPECIAL OFFERS}}', $special_sale_offers_count, '<span class="blue-text text-darken-4 fw400">' . $special_sale_offers_count . '</span>'); ?></h3>
				<div class="<?= $c_class; ?>__sales" data-indicators="true">
					<?php foreach ($special_sales as $sale) { ?>
					<?php $url = $this->Url->build(['controller' => 'Sales', 'action' => 'view', !empty($sale->slug) ? $sale->slug : $sale->id]); ?>
					<div class="<?= $c_class; ?>__sales-item">
						<div class="card hoverable grey lighten-5">
							<div class="card-content">
								<a href="<?= $url; ?>" class="card-title blue-grey-text text-darken-4 fs28 mv20"><?= h($sale->title); ?></a>
								<p class="grey-text text-darken-2 fs18 mb50"><?= h($sale->sub_title); ?></p>
							</div>
							<div class="card-action br-n text-right">
								<table class="table">
									<tr>
										<td class="pn va-m pr10">
											<?php if (!empty($sale->sale_offers)) { ?>
											<span><?= __d('HomeSearchCell', '{0,plural,=0{No special offers} =1{1 special offer} other{# special offers}}', count($sale->sale_offers)); ?></span>
											<?php } ?>
										</td>
										<td class="pn va-t" width="1%"><a href="<?= $url; ?>" class="waves-effect btn-large btn-flat blue-grey lighten-5 blue-text text-darken-4 fw500"><?= __d('HomeSearchCell', 'VIEW'); ?></a></td>
									</tr>
								</table>
							</div>
						</div>
					</div>
					<?php } ?>
				</div>
				<script type="text/javascript">
					(function($) {
						var $sales = $('#HomeSearchCell-<?= $c_id; ?> .<?= $c_class; ?>__sales'),
						$sale_items = $('.<?= $c_class; ?>__sales-item', $sales),
						sale_offset_top = 0,
						sale_rotation = 0;

						$sale_items.each(function(index, el) {
							var $sale = $(this);

							if (index > 0) {
								$sale.addClass('<?= $c_class; ?>__sales-item--shuffled');
								sale_offset_top += 10;
								sale_rotation += Math.ceil(index / 2) * 2;
							}

							$sale.css({
								'top': sale_offset_top + 'px',
								'transform': 'rotateZ(' + sale_rotation * ((index % 2 != 0) ? -1 : 1) + 'deg)'
							});

							$sale.prependTo($sales);
						});
					})(jQuery);
				</script>
			</div>
			<?php } ?>
			<div class="col s12 <?= (!empty($special_sale_offers_count) || !empty($special_sales)) ? 'xl8' : 'xl10 push-xl1'; ?> mv50 pv50 hide-on-med-and-down">
				<div class="row">
					<div class="col s12 m10 push-m1 mv25 pv25">
						<h3 class="fs24 fw300"><?= __d('HomeSearchCell', '{0} TOTAL OFFERS', '<span class="amber-text text-darken-4 fw400">' . $total_offers_count . '</span>'); ?></h3>
						<div class="<?= $c_class; ?>__form white z-depth-1 br3">
							<?= $this->Form->create(null, ['url' => ['prefix'=> false, 'plugin' => false, 'controller' => 'Sales', 'action' => 'index']]); ?>
							<table class="table">
								<tr>
									<td class="pn va-t" width="1%">
										<div class="<?= $c_class; ?>__form__calendar">
											<div class="<?= $c_class; ?>__form__calendar-toggle">
												<a href="javascript:void(0);" title="Pick a date" class="waves-effect btn-large btn-block btn-flat white blue-grey-text text-darken-4">
													<img src="/assets/dist/img/component/home-search-cell/calendar_button.svg" width="66" />
												</a>
											</div>
											<div class="<?= $c_class; ?>__form__calendar-holder white z-depth-1 br3">
												<table class="table">
													<tr>
														<td class="pn va-t" width="50%">
															<div class="<?= $c_class; ?>__form__calendar-box">
																<div class="<?= $c_class; ?>__form__calendar-label">
																	<label class="inline-full p10 blue-grey-text text-darken-4" for="HomeSearchCell-<?= $c_id; ?>-checkin"><?= __d('HomeSearchCell', 'Check-In Date'); ?></label>
																</div>
																<input type="hidden" class="<?= $c_class; ?>__form__calendar-input" id="HomeSearchCell-<?= $c_id; ?>-checkin" name="checkin" />
															</div>
														</td>
														<td class="pn va-t" width="50%">
															<div class="<?= $c_class; ?>__form__calendar-box">
																<div class="<?= $c_class; ?>__form__calendar-label">
																	<label class="inline-full p10 blue-grey-text text-darken-4" for="HomeSearchCell-<?= $c_id; ?>-checkout"><?= __d('HomeSearchCell', 'Check-Out Date'); ?></label>
																</div>
																<input type="hidden" class="<?= $c_class; ?>__form__calendar-input" id="HomeSearchCell-<?= $c_id; ?>-checkout" name="checkout" />
															</div>
														</td>
													</tr>
												</table>
											</div>
										</div>
									</td>
									<td class="pn va-m" width="100%">
										<?= $this->Form->input('q', ['label' => false, 'placeholder' => __d('HomeSearchCell', 'Destination or Hotel')]); ?>
									</td>
									<td class="pn va-t" width="1%">
										<div class="<?= $c_class; ?>__form__action">
											<?= $this->Form->button(__d('HomeSearchCell', 'Search'), ['class' => 'waves-effect btn-large btn-block btn-flat white blue-grey-text text-darken-4']); ?>
										</div>
									</td>
								</tr>
							</table>
							<?= $this->Form->end(); ?>
						</div>
						<script type="text/javascript">
							(function($) {
								$(document).ready(function($) {
									var $search_form = $('#HomeSearchCell-<?= $c_id; ?> .<?= $c_class; ?>__form'),
									$calendar = $('.<?= $c_class; ?>__form__calendar', $search_form),
									$calendar_toggle = $('.<?= $c_class; ?>__form__calendar-toggle', $calendar),
									$calendar_holder = $('.<?= $c_class; ?>__form__calendar-holder', $calendar);

									$calendar_toggle.on('click', function(event) {
										event.preventDefault();
										$calendar_holder.fadeToggle(600);
									});

									var today = new Date(), tomorrow = new Date();
									tomorrow.setDate(tomorrow.getDate() + 1);

									var $calendar_input_checkin = $('#HomeSearchCell-<?= $c_id; ?>-checkin').pickadate({
										selectMonths: true,
										min: today,
										formatSubmit: 'yyyy-mm-dd',
										closeOnSelect: true,
										closeOnClear: true,
										hiddenName: true
									}),
									checkin_picker = $calendar_input_checkin.pickadate('picker');

									var $calendar_input_checkout = $('#HomeSearchCell-<?= $c_id; ?>-checkout').pickadate({
										selectMonths: true,
										min: tomorrow,
										formatSubmit: 'yyyy-mm-dd',
										closeOnSelect: true,
										closeOnClear: true,
										hiddenName: true
									}),
									checkout_picker = $calendar_input_checkout.pickadate('picker');

									if (checkin_picker.get('value')) {
										checkout_picker.set('min', checkin_picker.get('select'));
									}

									if (checkout_picker.get('value')) {
										checkin_picker.set('max', checkout_picker.get('select'));
									}

									checkin_picker.on('set', function(event) {
										if (event.select) {
											var checkin_min = new Date(checkin_picker.get('select').pick);
											checkin_min.setDate(checkin_min.getDate() + 1);

											checkout_picker.set('min', checkin_min);
										} else if ('clear' in event) {
											checkout_picker.set('min', tomorrow);
										}
									});

									checkout_picker.on('set', function(event) {
										if (event.select) {
											var date = new Date(checkout_picker.get('select').pick);
											date.setDate(date.getDate() - 1);

											checkin_picker.set('max', date);
										} else if ('clear' in event) {
											checkin_picker.set('max', false);
										}
									});

									checkin_picker.open(false);
									checkout_picker.open(false);
								});
							})(jQuery);
						</script>
					</div>
				</div>
				<div class="row">
					<div class="col s12 m6 text-center pv25">
						<h5 class="fs22 fw300"><?= __d('HomeSearchCell', 'Browse Our Collection'); ?></h5>
						<a href="/sales" class="waves-effect btn mv10 white amber-text text-darken-4 fw500"><?= __d('HomeSearchCell', 'VIEW ALL'); ?></a>
					</div>
					<div class="col s12 m6 text-center pv25">
						<h5 class="fs22 fw300"><?= __d('HomeSearchCell', 'Create Your Account'); ?></h5>
						<a href="/register" class="waves-effect btn mv10 white blue-text text-darken-4 fw500"><?= __d('HomeSearchCell', 'SIGN UP'); ?></a>
					</div>
				</div>
			</div>
		</div>
		<div class="pv25 mv25 hide-on-med-and-down"></div>
	</div>
</div>