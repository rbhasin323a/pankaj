<?php
$this->loadHelper('Form', [
	'templates' => 'app_form',
]);

$this->HtmlBetter->addCrumb(__('Home'), '/');
$this->HtmlBetter->addCrumb(__('Account'), '/account');
?>
<section id="UsersAccount">
	<div class="container">
		<div class="row">
			<div class="col s12 l3 sidenav-left">
				<?= $this->element('Sidebar/nav-account'); ?>
			</div>
			<div class="col s12 l9">
				<h4 class="page-heading"><?= __('Your Details'); ?></h4>
				<div class="card-panel p30">
					<?= $this->Form->create($user, ['id' => 'UsersAccountForm','novalidate' => true]); ?>
					<div class="row mbn">
						<div class="col s12">
							<div class="row">
								<div class="col s12 m6">
									<?= $this->Form->input('first_name', ['class' => 'validate']); ?>
								</div>
								<div class="col s12 m6">
									<?= $this->Form->input('last_name', ['class' => 'validate']); ?>
								</div>
							</div>
						</div>
						<div class="col s12">
							<?= $this->Form->input('email', ['class' => 'validate']); ?>
						</div>
						<div class="col s12">
							<?= $this->Form->input('birthday', [
								'label' => __('Date of Birth'),
								'type' => 'text',
								'class' => 'datepicker',
								'required' => 'required'
							]); ?>
						</div>
						<div class="col s12">
							<div class="row mbn">
								<div class="col s12 m6">
									<?= $this->Form->input('phone_mobile', ['class' => 'validate', 'label' => __('Mobile Phone')]); ?>
								</div>
								<div class="col s12 m6">
									<?= $this->Form->input('phone_home', ['class' => 'validate', 'label' => __('Home Phone')]); ?>
								</div>
							</div>
						</div>
						<div class="col s12">
							<div class="row mbn">
								<div class="col s12 m5">
									<?= $this->Form->input('country_id', ['type' => 'select', 'class' => 'validate', 'options' => $countries, 'label' => false]); ?>
								</div>
								<div class="col s12 m5">
									<?= $this->Form->input('city', ['class' => 'validate']); ?>
								</div>
								<div class="col s12 m2">
									<?= $this->Form->input('postcode', ['class' => 'validate']); ?>
								</div>
							</div>
						</div>
						<div class="col s12">
							<?= $this->Form->input('address_1', ['type' => 'textarea', 'rows' => 3, 'class' => 'materialize-textarea validate']); ?>
						</div>
						<div class="col s12">
							<?= $this->Form->input('address_2', ['type' => 'textarea', 'rows' => 3, 'class' => 'materialize-textarea validate']); ?>
						</div>
						
						<div class="row">
								<div class="col s12 m6">
								<?= $this->Form->input('password',['value'=>'']); ?>
								</div>
								<div class="col s12 m6">
								<?= $this->Form->input('password_confirm', ['type' => 'password']); ?>
								</div>
							</div>
						
						
					
						<div class="col s12">
							<input type="hidden" name="newsletter" value="0" />
							<input type="checkbox" name="newsletter" value="1" id="newsletter" <?= !empty($vd['user']['newsletter']) ? 'checked="checked"' : ''; ?>>
							<label for="newsletter"><?= __('Subscribe me to the newsletter'); ?></label>
						</div>
						<div class="col s12 mt20">
							<?= $this->Form->button(__('Save') . '<i class="material-icons right">save</i>', ['class' => 'btn-large waves-effect waves-light amber darken-3 right']); ?>
						</div>
					</div>
					<?= $this->Form->end(); ?>
				</div>
			</div>
		</div>
	</div>
</section>

<script type="text/javascript">
	(function($) {
		$(document).ready(function() {
			$('.datepicker').pickadate({
				selectMonths: true,
				selectYears: 200,
				format: 'yyyy-mm-dd'
			});
			
			

			$(document).on('submit', '#UsersAccountForm', function(e) {
				var birthday = $('#birthday').prop('value');
					var pass = $('#password').prop('value');
					var confirmpass = $('#password-confirm').prop('value');
					
				    var error = false;
				    if(pass!=confirmpass){
				        
					    
					    var $div = '<div class="error-message fw600 red-text mb20 fs12"><?= __('Password and confirm password should be same') ?></div>';
					    var $container1 = $('#password').parent();

				    	$('.error-message', $container1).remove();
				         	$container1.append($div);
				         		e.preventDefault();
					}

				if (birthday) {
					if (!birthday.match(/\d{4}-\d{2}-\d{2}/)) {
						e.preventDefault();
						error = true;
					}
				} else {
					e.preventDefault();
					error = true;
				}

                
				if (error) {
					var $div = '<div class="error-message fw600 red-text mb20 fs12"><?= __('Date of birth is required') ?></div>';
					var $container = $('#birthday').parent();

					$('.error-message', $container).remove();
					$container.append($div);
				}
			});
		});
	})(jQuery);
</script>
