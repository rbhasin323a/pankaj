<?php
$this->loadHelper('Form', [
	'templates' => 'app_form',
]);

$this->HtmlBetter->addCrumb(__('Home'), '/');
$this->HtmlBetter->addCrumb(__('Account'), '/account');
$this->HtmlBetter->addCrumb(__('Bookings'), '/account/bookings');
?>
<section id="UsersBookings">
	<div class="container">
		<div class="row">
			<div class="col s12 l3 sidenav-left">
				<?= $this->element('Sidebar/nav-account'); ?>
			</div>
			<div class="col s12 l9">
				<h4 class="page-heading"><?= __('Your Bookings'); ?></h4>
				<div class="card-panel p30">
					<?php if ($bookings->count()) { ?>
						<table>
							<thead>
								<tr>
									<th data-field="id"><?= __('Sale'); ?></th>
									<th data-field="id"><?= __('Check-in'); ?></th>
									<th data-field="id"><?= __('Check-out'); ?></th>
								</tr>
							</thead>
							<tbody>
								<?php foreach ($bookings as $booking) : ?>
									<tr>
										<td><a href="/sales/view/<?= $booking->sale->slug ?>"><?= $booking->sale->title ?></a></td>
										<td><?= date("Y-m-d", strtotime($booking->check_in)); ?></td>
										<td><?= date("Y-m-d", strtotime($booking->check_out)); ?></td>
									</tr>
								<?php endforeach; ?>
							</tbody>
						</table>
					<?php } else { ?>
						<p class="mbn"><?= __('You have not made any bookings yet'); ?></p>
					<?php } ?>
				</div>
			</div>
		</div>
	</div>
</section>
