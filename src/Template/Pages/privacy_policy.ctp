<?php
	$this->HtmlBetter->addCrumb(__('Home'), '/');
	$this->HtmlBetter->addCrumb(__('Privacy Policy'), '/pages/privacy-policy');
?>
<section id="PagePrivacyPolicy">
	<div class="mb50 pb50">
		<div class="container">
			<div class="row">
				<div class="col s12 l3 sidenav-left">
					<?= $this->element('Sidebar/nav-pages'); ?>
				</div>
				<div class="col s12 l9">
					<h4 class="page-heading"><?= __('Privacy Policy'); ?></h4>
					<div class="card-panel p30" style="text-align: justify;">
						<p>Luxury Discounts LTD &ndash; limited liability company ("<strong>Luxury Discounts</strong>") is the owner and operator of the <strong>luxury-discounts.com</strong> website. Luxury Discounts is a controller of personal data in accordance with the Bulgarian legislation on personal data protection. From May 25, 2018, the <strong>General Data Protection Regulation &ndash; Regulation (EU) 2016/679 (GDPR)</strong> is being implemented in Bulgaria, the purpose of which is to unify the policies of the Member States of the European Union for collection and use of personal data. We make the necessary efforts to protect your personal data. This Privacy Policy describes how we collect and process personal data, how we use it, and for what purposes, to whom it may be provided once it is saved from our platform or another shared way of collecting personal data. All data, on the basis of which a user can be identified, shall be considered personal data and shall be protected. These data can be names, mobile phone, home address, email address, IP address. We use your data only for purposes described in the <strong>General Terms and Conditions</strong>, by processing and storing the provided personal information in a secure software environment.</p>
						<p>According to the requirements of the GDPR that we enforce and observe, we should:</p>
						<ul>
						<li>Let you know what data we use and for what purposes;</li>
						<li>Ask for your consent to use data when we provide additional services (such as advertising) based thereon;</li>
						<li>Allow you to withdraw your consent at any time, when data processing is based on your consent;</li>
						<li>Ensure you can exercise your rights as a data subject under the GDPR (such as a right to information and access to, rectification or erasure of personal data, restriction and objection to processing, a right to transmit data, to lodge a complaint);</li>
						<li>Specify all third parties with whom we exchange your data.</li>
						</ul>
						<p>What data we collect from registered users of our website</p>
						<p>We inform you that for the successful storage of your reservation and the purchase of the selected tourist product or service we may request your full name, gender, home address, email address, mobile phone and other information if necessary for your reservation (passport or ID card, date of issue, including for the people who will be traveling with you). This information is required to handle your reservation and can be provided to service providers (such as hotels, tour operators, airlines, public authorities such as customs and immigration services, if necessary). In accordance with the selected destinations and the above-mentioned, if necessary the personal data may be transferred (with an adequate level of protection) outside the European Economic Area, for which you agree (as well as companions) by accepting the General Terms and Conditions.</p>
						<p>By accepting the General Terms and Conditions, the user agrees to provide the requested personal data for the purposes of the platform that will be processed in the order. The actions of the user on the website (visited offers, liked offers, frequently asked questions, comments) or information regarding the purchases made by the user (destinations, prices, dates, offer terms, discounts) can be used by us for the purposes of direct marketing and delivering relevant ads, new promotions and discounts. In accordance with the GDPR, we will ask for the user's consent to receive a newsletter or promotion via email, Google+, Facebook, or other social media. By accepting the General Terms and Conditions, the user agrees Luxury Discounts to provide their partners with email addresses and other personal information, if necessary to provide the service when buying a product from our platform. The user's consent to process data provided by him / her may be withdrawn at any time. If you choose to withdraw your consent, this may result in the termination of your registration when the consent is a condition for receiving the selected service through the platform.</p>
						<p>In order to access our website via a mobile device or our mobile application, the user agrees to allow Luxury Discounts to collect information regarding a current location that can be used for the tourist's most favorite destinations according to his / her interests using the platform.</p>
						<p>How long we store data</p>
						<p>We store the data until it ceases to be necessary to provide our services or while the user&rsquo;s registration is active - whichever occurs first. Personal data used to measure users' behavior on our website is stored for no more than 6 (six) months. Upon termination of the registration, all data of the respective user shall be deleted.</p>
						<p>The information we receive from you, may be stored for an extended period of time (at least one year) when it is subject to a legitimate request or for compliance with legal obligation, a state investigation, a breach of the General Terms and Conditions.</p>
						<p>When you make a reservation for a selected tourist service through our website, you will be referred to a payment portal where you can fill in your details for the chosen form of payment. We do not keep any information regarding your payments, such as credit / debit card information. The payment gateways we use handle information needed to pay for and keep a tourist service. Once we receive your payment, we transfer the same to the supplier of the selected tourist product or service purchased through our website.</p>
						<p>When we can share and disclose data</p>
						<p>Luxury Discounts is obliged not to share in any way the personal data provided by the user and not to disclose the information gathered to third parties (natural persons, legal entities, state authorities), unless: a) the user has explicitly consented to this upon registration or later; or (b) there is a legal basis (the information is required by a regulatory, judicial or other authority, in accordance with legal procedures and with a due cause); or (c) in other cases provided for by law and/or the General Terms and Conditions.</p>
						<p>We may access, store and share your information with regulatory, law enforcement or other official authority, provided there is a legitimate reason and upon a legitimate request; or for the necessity to prevent any illegal activity, unauthorized use of our product, breach of the General Terms and Conditions (including on the basis of inspection by supervising authority or state investigation), to protect our rights and product and/or your vital interests (or these of another person), or for the performance of a task carried out in the public interest.</p>
						<p>We restrict the access of our employees to user&rsquo;s data and information, unless it is necessary to provide the chosen service or is in connection with the performance of the assigned work of these employees. Luxury Discounts has physical, electronic and procedural means to protect the information gathered, in accordance with legal requirements.</p>
						<p>Rights of Data Subjects under the GDPR</p>
						<ul>
						<li><strong>Right of information and access to personal data</strong>: you have the right to request and receive confirmation that personal data are being processed for you, respectively you have the right to access the personal data and information;</li>
						<li><strong>Right to rectification of personal data</strong>: if you find that personal data processed for you are inaccurate, you may request to rectify this data;</li>
						<li><strong>Right to erasure of personal data (&ldquo;right to be forgotten&rdquo;):</strong> upon withdrawn consent (if the personal data processing is based on such consent) or if your personal data has been processed unlawfully, you have the right to ask for your personal data to be erased;</li>
						<li><strong>Right to restrict processing</strong>: if you have objected to the legitimate purpose of processing your personal data, you have the right to request the processing to be restricted until a solution is found;</li>
						<li><strong>Right to object to processing:</strong> if you have any doubts to the legitimacy of the interest in processing your personal data, you may, for specific reasons, object to such processing;</li>
						<li><strong>Right to transmit data</strong>: you have the right to request that we transmit your personal data to another data controller;</li>
						<li><strong>Right to lodge a complaint with a supervisory authority</strong>: you have the right to lodge a complaint with the relevant supervisory authority in connection with the processing of your personal data by us.</li>
						</ul>
						<p class="address">Luxury Discounts LTD<br/>
							Residential complex “Gotse Delchev”, 49A Bulgaria Blvd., 2nd floor, office 8<br/>
							Sofia, Bulgaria<br/>
							<a href="mailto:support@luxury-discounts.com">support@luxury-discounts.com</a> <br/>
							+359 2 4917 959
						</p>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
