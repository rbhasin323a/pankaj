<?php
$this->assign('title', __('Luxury Discounts'));
?>
<style>
	#PageHome .home-featured .card .card-title {
		font-family: 'Roboto', sans-serif !important;
	}

	#PageHome .home-account__sales-item .card-title {
		font-family: 'Roboto', sans-serif !important;
	}
</style>
<section id="PageHome">
	<?= $this->cell('HomeAccount', [], ['special_sales_limit' => 50]); ?>
	<?= $this->cell('HomeFeatured', [], ['categories_limit' => 4]); ?>
	<div class="inline-full va-t blue-grey lighten-5">
		<div class="container pv50 mv50">
			<div class="row mbn">
				<div class="col s12 m8">
					<h2 class="fs35 fw300 m-n mv5 purple-text text-darken-4"><?= __d('PageHome', 'OUR LUXURY NETWORK'); ?></h2>
					<h3 class="fs24 fw300 m-n"><?= __d('PageHome', 'JOIN OUR FAMILY OF EXCLUSIVE PARTNERS'); ?></h3>
				</div>
				<div class="col s12 m4">
					<a href="/contact" class="waves-effect btn-large btn-flat right mv15 white purple-text text-darken-4 fs20 fw500"><?= __d('PageHome', 'Get In Touch'); ?></a>
				</div>
			</div>
		</div>
	</div>
</section>
