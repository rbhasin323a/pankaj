<?php
	$this->assign('title', __('About Us'));

	$this->HtmlBetter->addCrumb(__('Home'), '/');
	$this->HtmlBetter->addCrumb(__('About Us'), '/pages/about-us');
?>
!!!!!
<section id="PageAboutUs">
	<div class="mb50 pb50">
		<div class="container">
			<div class="row">
				<div class="col s12 l3 sidenav-left">
					<?= $this->element('Sidebar/nav-pages'); ?>
				</div>
				<div class="col s12 l9">
					<h4 class="page-heading"><?= __('About Us'); ?></h4>
					<div class="card-panel p30" style="text-align: justify;">
						<p dir="ltr"><span>We are a small team of young travel experts, dedicated to running a profitable business and generating long-term value for our customers. We decided to create a website where we hand-pick the best hotels and holidays and create exclusive offers available only to our members. We are passionate about what we do and of course we all love to travel. </span></p>

						<p dir="ltr"><span class="amber-text text-darken-3 fw500">Luxury Discounts</span><span> is a members-only luxury travel club, offering unforgettable hand-picked getaways, at unbelievable prices - offered only for a limited time. By hand-picking hotels and holidays for our members, </span><span class="amber-text text-darken-3 fw500">Luxury Discounts</span><span> acts as an agent for the tour operators and hotels featured on the site.</span></p>

						<p dir="ltr"><span>At </span><span class="amber-text text-darken-3 fw500">Luxury Discounts</span><span>, we believe that booking a holiday should be an exciting, spontaneous and enjoyable experience. We will do all the research legwork, so you can simply sit back and relax. Our offers are designed to provide great experience at outstanding value - exclusive deals on the world&rsquo;s greatest hotels and travel experiences.</span></p>

						<p dir="ltr"><span>Each week you will receive newsletters from us, containing our best and most inspirational hand-picked offers of the week. We are independent in our reviews and we will never recommend a deal that we wouldn&rsquo;t book ourselves. We know the trick to a successful luxury retreat and we will negotiate exceptional hotel deals and exclusives for our customers. Our pricing controllers make sure you get the best possible deal with discounts of up to 70%. </span></p>

						<p dir="ltr"><span class="amber-text text-darken-3 fw500">Luxury Discounts</span><span> is not for those looking only for the cheapest.</span><br class="kix-line-break" />
						<span>We only ever feature hotels and holidays that we believe are a cut above the rest and good enough to recommend to our members &ndash; four and five-star hotels and resorts only. In the same time we work only with hotels that give us exclusive rates &ndash; we find prices you just won&#39;t get anywhere else. </span></p>

						<p dir="ltr"><span>Our mission is to make sure your </span><span class="amber-text text-darken-3 fw500">ADVENTURE IS WORTHWHILE</span><span style="font-size: 16px; font-family: Arial; color: rgb(255, 192, 0); background-color: transparent; vertical-align: baseline; white-space: pre-wrap;">.</span></p>

						<p><span>See the world in style and check our amazing deals by signing up to </span><span class="amber-text text-darken-3 fw500">Luxury Discounts</span><span> with your email address and nothing else. It doesn't cost a penny.</span></p>

						<div class="text-center">
							<a class="waves-effect btn-large mv10 amber darken-3" href="<?= $this->Url->build('/register', true); ?>">Become a member</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
