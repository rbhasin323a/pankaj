<?php
$this->HtmlBetter->addCrumb(__('Home'), '/');
$this->HtmlBetter->addCrumb(__('GDPR Tools'), '/pages/gdpr-tools');
?>
<style>
	#form-gdpr-edit-account-request,
	#form-gdpr-data-account-request,
	#form-gdpr-personal-data-report-request,
	#form-gdpr-delete-account-request {
		display: none;
	}

	#gdpr_page form .loading {
		display: inline-block;
		margin-left: 20px;
		display: none;
	}

	#gdpr_page form .loading:not(:required):after {
		content: '';
		display: block;
		font-size: 10px;
		width: 1em;
		height: 1em;
		margin-top: -0.5em;
		-webkit-animation: spinner 1500ms infinite linear;
		-moz-animation: spinner 1500ms infinite linear;
		-ms-animation: spinner 1500ms infinite linear;
		-o-animation: spinner 1500ms infinite linear;
		animation: spinner 1500ms infinite linear;
		border-radius: 0.5em;
		-webkit-box-shadow: rgba(120, 129, 136, 1) 1.5em 0 0 0, rgba(120, 129, 136, 1) 1.1em 1.1em 0 0, rgba(120, 129, 136, 1) 0 1.5em 0 0, rgba(120, 129, 136, 1) -1.1em 1.1em 0 0, rgba(120, 129, 136, 1) -1.5em 0 0 0, rgba(120, 129, 136, 1) -1.1em -1.1em 0 0, rgba(120, 129, 136, 1) 0 -1.5em 0 0, rgba(120, 129, 136, 1) 1.1em -1.1em 0 0;
		box-shadow: rgba(120, 129, 136, 1) 1.5em 0 0 0, rgba(120, 129, 136, 1) 1.1em 1.1em 0 0, rgba(120, 129, 136, 1) 0 1.5em 0 0, rgba(120, 129, 136, 1) -1.1em 1.1em 0 0, rgba(120, 129, 136, 1) -1.5em 0 0 0, rgba(120, 129, 136, 1) -1.1em -1.1em 0 0, rgba(120, 129, 136, 1) 0 -1.5em 0 0, rgba(120, 129, 136, 1) 1.1em -1.1em 0 0;
	}

	/* Animation */

	@-webkit-keyframes spinner {
		0% {
			-webkit-transform: rotate(0deg);
			-moz-transform: rotate(0deg);
			-ms-transform: rotate(0deg);
			-o-transform: rotate(0deg);
			transform: rotate(0deg);
		}

		100% {
			-webkit-transform: rotate(360deg);
			-moz-transform: rotate(360deg);
			-ms-transform: rotate(360deg);
			-o-transform: rotate(360deg);
			transform: rotate(360deg);
		}
	}

	@-moz-keyframes spinner {
		0% {
			-webkit-transform: rotate(0deg);
			-moz-transform: rotate(0deg);
			-ms-transform: rotate(0deg);
			-o-transform: rotate(0deg);
			transform: rotate(0deg);
		}

		100% {
			-webkit-transform: rotate(360deg);
			-moz-transform: rotate(360deg);
			-ms-transform: rotate(360deg);
			-o-transform: rotate(360deg);
			transform: rotate(360deg);
		}
	}

	@-o-keyframes spinner {
		0% {
			-webkit-transform: rotate(0deg);
			-moz-transform: rotate(0deg);
			-ms-transform: rotate(0deg);
			-o-transform: rotate(0deg);
			transform: rotate(0deg);
		}

		100% {
			-webkit-transform: rotate(360deg);
			-moz-transform: rotate(360deg);
			-ms-transform: rotate(360deg);
			-o-transform: rotate(360deg);
			transform: rotate(360deg);
		}
	}

	@keyframes spinner {
		0% {
			-webkit-transform: rotate(0deg);
			-moz-transform: rotate(0deg);
			-ms-transform: rotate(0deg);
			-o-transform: rotate(0deg);
			transform: rotate(0deg);
		}

		100% {
			-webkit-transform: rotate(360deg);
			-moz-transform: rotate(360deg);
			-ms-transform: rotate(360deg);
			-o-transform: rotate(360deg);
			transform: rotate(360deg);
		}
	}
</style>

<section id="PageGdprTools">
	<div class="container">
		<div class="row">
			<div class="col s12 l3 sidenav-left">
				<?= $this->element('Sidebar/nav-pages'); ?>
			</div>
			<div class="col s12 l9">
				<h4 class="page-heading"><?= __('GDPR Tools'); ?></h4>
				<div class="card-panel p30">
					<div id="gdpr_page">
						<h5>Data Portability</h5>
						<p>You can use the links below to download all the data we store and use for a better experience in our store.</p>
						<ul class="list-unstyled">
							<li><a id="btn-gdpr-personal-information-request" href="#">Personal information</a></li>
							<li><a id="btn-gdpr-bookings-request" href="#">Bookings</a></li>
						</ul>
						<div id="form-gdpr-data-account-request">
							<form>
								<label>Enter your email to confirm your identity</label> <input type="email" name="email" placeholder="example@email.com" autocorrect="off" autocapitalize="off" autofocus="" /> <input type="submit" class="btn" value="Submit" />
								<div class="loading"></div>
								<input type="hidden" name="request_type" value="" />
							</form>
						</div>
						<h5>Right to be Forgotten</h5>
						<p>Use this option if you want to remove your personal and other data from our store. Keep in mind that <strong>this process will delete your account, so you will no longer be able to access or use it anymore</strong>.</p>
						<ul class="list-unstyled">
							<li><a id="btn-gdpr-delete-account-request" href="#">Request personal data deletion</a></li>
						</ul>
						<div id="form-gdpr-delete-account-request">
							<form>
								<label>Enter your email to confirm your identity</label> <input type="email" name="email" placeholder="example@email.com" autocorrect="off" autocapitalize="off" autofocus="" /> <input type="submit" class="btn" value="Submit" />
								<div class="loading"></div>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

<script>
	var gdprSlideUpAll = function() {
		$('#form-gdpr-data-account-request').slideUp(100);
		$('#form-gdpr-delete-account-request').slideUp(100);
	};

	var gdprSendRequest = function(email, type, callback) {
		$.ajax({
			url: '/gdprRequests/submitRequest',
			method: 'POST',
			data: {
				email: email,
				type: type
			},
			success: function(resp) {
				if (!resp.error) {
					gdprSlideUpAll();
					alert("Your request has been submitted successfully. Please check your email for more information.");
				} else {
					alert(resp.error);
				}
				if (typeof callback == 'function') {
					callback(resp);
				}
			}
		});
	};

	$('#btn-gdpr-personal-information-request, #btn-gdpr-bookings-request').on('click', function(e) {
		e.preventDefault();
		gdprSlideUpAll();

		var type = '';

		switch ($(this).attr('id')) {
			case 'btn-gdpr-personal-information-request':
				type = 'customer/personal_info';
				break;
			case 'btn-gdpr-bookings-request':
				type = 'customer/bookings';
				break;
		}

		$('#form-gdpr-data-account-request form input[name="request_type"]').val(type);

		$('#form-gdpr-data-account-request').slideDown(200);
	});

	$('#form-gdpr-data-account-request form input[type=submit]').on('click', function(e) {
		e.preventDefault();
		var email = $('#form-gdpr-data-account-request form input[name=email]').val();
		var type = $('#form-gdpr-data-account-request form input[name="request_type"]').val();

		var submit_button = $(this);
		submit_button.prop('disabled', true);
		submit_button.next('.loading').css('display', 'inline-block');
		gdprSendRequest(email, type, function(resp) {
			submit_button.prop('disabled', false);
			submit_button.next('.loading').css('display', 'none');
		});
	});

	$('#btn-gdpr-delete-account-request').on('click', function(e) {
		e.preventDefault();
		gdprSlideUpAll();
		$('#form-gdpr-delete-account-request').slideDown(200);
	});

	$('#form-gdpr-delete-account-request form input[type=submit]').on('click', function(e) {
		e.preventDefault();
		var email = $('#form-gdpr-delete-account-request form input[name=email]').val();
		var type = 'customer/delete';

		var submit_button = $(this);
		submit_button.prop('disabled', true);
		submit_button.next('.loading').css('display', 'inline-block');
		gdprSendRequest(email, type, function(resp) {
			submit_button.prop('disabled', false);
			submit_button.next('.loading').css('display', 'none');
		});
	});
</script>
