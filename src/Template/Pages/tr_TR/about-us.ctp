<?php
	$this->assign('title', __('About Us'));

	$this->HtmlBetter->addCrumb(__('Home'), '/');
	$this->HtmlBetter->addCrumb(__('About Us'), '/pages/about-us');
?>
<section id="PageAboutUs">
	<div class="container">
		<div class="row">
			<div class="col s12 l3 sidenav-left">
				<?= $this->element('Sidebar/nav-pages'); ?>
			</div>
			<div class="col s12 l9">
				<h4 class="page-heading"><?= __('About Us'); ?></h4>
				<div class="card-panel p30" style="text-align: justify;">
					<p dir="ltr"><span>Biz turizm sektöründe genç uzmanlardan oluşan bir ekibiz. Hedefimiz müşterilerimiz ile uzun valideli ilişkiler kurarak kazançlı bir iş oluşturup gerçekleştirmek. Biz web sitemizin kullanıcılarına özel en iyi otelleri ve tatilleri seçtip  sadece kendi internet sayfamızda özel promosyonları sunmaktayız. Yaptığımız işleri gerçekten çok severiz  ve seyahat yapmak bizim tutkumuzdur.</span></p>

					<p dir="ltr"><span class="amber-text text-darken-3 fw500">Luxury Discounts</span><span> sitemiz lüks geziler ile ilgili bir platformdur ve sadece  kayıtlı kullanıcıların girebilmektedir. Biz unutulmaz kendi seçtiğimiz teklifler, inanılmaz fiyatlarla, sınırlı bir süre içinde geçerli seyahatlar sunuyoruz. Luxury Discounts sitesinde size özel promosyonlar vererek, tur operatörleri ve otelleri bir acente olarak faaliyet göstermektedir. </span><span class="amber-text text-darken-3 fw500">Luxury Discounts</span><span>`ta bir seyahatin veya bir otelde konaklama, heyecan verici, spontan ve eğlenceli olması gerektiğine inanıyoruz. Bundan dolayı sizler rahatça oturup dinlenirken biz bütün çalışmayı ve piyasanın ön analizini yapmış olacağız. Bizim promosyonlarımız dünyanın en iyi yerler ve otellerinde  olağanüstü fiyatlarla büyük bir macera yaşamanızı sağlamak için tasarlanmıştır.</span></p>

					<p dir="ltr"><span>Her hafta kendi seçtiğimiz o haftaya ait özel tekliflerimizi macera dolu seyahatleri içeren bültenimizi alacaksınız. Biz promosyonlarımızı seçtiğimizde dikkat ederiz ve kendimiz kalmayacağımız  bir otelde veya kendimiz gitmeyeceğimiz bir seyahati size tavsiye etmeyiz. Biz bir lüks tatilin bileşenlerini ve ihtiyaçlarını biliyoruz ve müşterilerimiz için özel teklifler ve promosyonlar sağlamakta çaba göstereceğiz. Sizin 70%’e kadar indirimli en iyi teklifi almanız için mümkün olan elimizden geleni her şeyi yapacağız. </span><span class="amber-text text-darken-3 fw500">Luxury Discounts</span><span> sadece düşük fiyatlardan faydalanmak isteyen müşteriler için uygun değil. Biz sadece dört ve beş yıldızlı oteller ve tatil köyleri yeterince iyi olduğunu düşünüyoruz ve bu nedenle müşterilerimize onları tavsiye ediyoruz.  Aynı zamanda başka hiçbir yerde bulamayacağınız sadece özel fiyatlar sunan otellerle çalışırız.</span></p>

					<p dir="ltr"><span>Misyonumuz size değeri olan bir macerayı yaşattırabilmemiz için elimizden geleni her şeyi yapmamız.</span></p>

					<p><span>Dünyaya şık ve lüks prizmasından bakınız. Özel tekliflerimize bakınız ve </span><span class="amber-text text-darken-3 fw500">Luxury Discounts</span><span>`un sitesine üye olunuz.</span></p>

					<hr />
					<div class="text-center">
						<a class="waves-effect waves-light btn-large" href="<?= $this->Url->build('/register', true); ?>" style="background-color: rgb(255, 192, 0);">Şimdi üye Ol</a>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
