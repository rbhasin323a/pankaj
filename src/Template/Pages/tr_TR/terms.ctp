<?php
	$this->assign('title', __('Terms & Conditions'));
	
	$this->HtmlBetter->addCrumb(__('Home'), '/');
	$this->HtmlBetter->addCrumb(__('Terms & Conditions'), '/pages/terms');
?>
<section id="PageTerms">
	<div class="container">
		<div class="row">
			<div class="col s12 l3 sidenav-left">
				<?= $this->element('Sidebar/nav-pages'); ?>
			</div>
			<div class="col s12 l9">
				<h4 class="page-heading"><?= __('Terms & Conditions'); ?></h4>
				<div class="card-panel p30" style="text-align: justify;">
					<p>
					    <em>
					        İşbu genel koşullar “Luxury Discounts” Ltd. tarafından sunulan
					        hizmetleri ve
					    </em>
					    <em>luxury</em>
					    <em>-</em>
					    <em>discounts</em>
					    <em>.</em>
					    <em>com</em>
					    <em> </em>
					    <em>
					        internet sitesinin çalışma şekli ve kullanım kurallarını içermektedir
					        (internet sitesi ve hizmetleri toplu “hizmetler” olarak anılmaktadır).
					    </em>
					    <em></em>
					</p>
					<p>
					    <em>
					        “Luxury Discounts” Ltd.’nin sahip olduğu ve onun tarafından bakımı
					        yapılan
					    </em>
					    <em> </em>
					    <a href="http://www.luxury-discounts.com/">
					        <em>www</em>
					        <em>.</em>
					        <em>luxury</em>
					        <em>-</em>
					        <em>discounts</em>
					        <em>.</em>
					        <em>com</em>
					    </a>
					    <em>
					        internet sitesininde sunulan hizmetleri kullanmadan önce bu Genel
					        koşullar hakkında bilgilenmeniz önerilir
					    </em>
					    <em>.</em>
					</p>
					<p>
					    <em>
					        Sunulan hizmetlerden yararlanmak için bu Genel koşulları kabul etmeniz
					        ve uygulamanız gerekmektedir. Bu koşullar
					    </em>
					    <a href="http://www.luxury-discounts.com/">
					        <em>www</em>
					        <em>.</em>
					        <em>luxury</em>
					        <em>-</em>
					        <em>discounts</em>
					        <em>.</em>
					        <em>com</em>
					    </a>
					    <em>
					        internet sitesini (aşağıda adı kısaca “web sitesi” olarak anılmaktadır)
					        ve sunulan himzetleri kullanan tüm ziyaretçiler ve kullanıcılar
					        tarafından uygulanmalıdır.
					    </em>
					</p>
					<p>
					    <em>
					        Otel konaklama, organize turistik seyahat (“tur” olarak anılmaktadır)
					        veya web sitesinde profil oluşturmak ile siz Genel koşulları kabul
					        etmiş sayılırsınız ve bunları uygulamak zorundasınız. Eğer bu şekilde
					        açıklanan genel koşulları kabul etmiyorsanız web sitesini ve ilgili
					        hizmetlerin kullanımını sona erdirmelisiniz.
					    </em>
					</p>
					<p>
					    İşbu Genel koşulların amaçları için ve Bulgaristan mevzuatına göre:
					</p>
					<p>
					    А) „Turizm acentesi işleri“, son tüketicilere toplam fiyatı ile organize
					    turistik seyahatlerinin satışı, yolcu hava, su ve otobüslü taşımacılık,
					    rezervasyon, vize ve sair ek turistik hizmetleri ve turistik seyahat ile
					    ilgili seyahatlaerin yapılmasında aracılık anlamına gelir;
					</p>
					<p>
					    Б) „Turizm acentesi“ Turizm Kanuna göre ve Kanunun usulünce kayıtlı veya
					    turoperatörler ve turizm acenteleri sicil memurluğunda turizm acentesi
					    işleri yapmak için kayıtlı kişidir;
					</p>
					<p>
					    В) „Turistik voucher“, toplu fiyat ile organize edilen turistik seyahatın
					    kullanıcısı ve turoperatör arasında var olan sözleşmeyi gösteren veya ana
					    ve/veya ek turistik hizmetinin satışını gösteren rapor altında belgedir;
					</p>
					<p>
					    Г) „Rezervasyon formu“, toplu fiyat ile organize edilmiş turistik seyahate
					    katılmadıkları durumunda turizm ajanı ve ana veya ek turistik hizmetinin
					    tüketicisi olarak turist arasında var olan sözleşmeyi gösteren rapor
					    altında belgedir;
					</p>
					<p>
					    Д) „Turistik ürün“, seyahati sırasında turiste teklif edilen ve turist
					    tarafından kullanılan belirli bir alan çerçevesinde özel ekonomi
					    faaliyetleri ve doğa ve antropojenik koşullar ve kaynakları içeren toplu
					    kavramdır.
					</p>
					<p>
					    Е) „Kullanıcı“
					    <a href="http://www.luxury-discounts.com/">
					        <em>www</em>
					        <em>.</em>
					        <em>luxury</em>
					        <em>-</em>
					        <em>discounts</em>
					        <em>.</em>
					        <em>com</em>
					    </a>
					    sitesine erişebilen her kişidir ve sitenin işlevlerii içeriğini
					    kullanabilir ve „
					    <a href="http://www.luxury-discounts.com/">
					        <em>www</em>
					        <em>.</em>
					        <em>luxury</em>
					        <em>-</em>
					        <em>discounts</em>
					        <em>.</em>
					        <em>com</em>
					    </a>
					    <em>“ </em>
					    web sitesi aracılığı ile rezerve edilmiş ve satın alınmış ana ve ek
					    turistik hizmetlerini kullanabilir.<em> </em><em></em>
					</p>
					<p>
					    Ж) „Web sitesi“ „
					    <a href="http://www.luxury-discounts.com/">
					        <em>www</em>
					        <em>.</em>
					        <em>luxury</em>
					        <em>-</em>
					        <em>discounts</em>
					        <em>.</em>
					        <em>com</em>
					    </a>
					    <em>“</em>
					    <em> </em>
					    elektronik adresinin yazılmasında browserinizde çıkan toplu başlıklı
					    internet sitesi internet siteleri toplu kavramıdır.
					</p>
					<p>
					    <strong>I. </strong>
					    <strong>Luxury discounts</strong>
					    <em></em>
					</p>
					<p>
					    “Luxury discounts“<em>Ltd.</em>, bireysel tanımlama numarası EİK:
					    203538748, faaliyet yeri ve yönetim adresi: Sofya şehri, Triaditsa bölgesi,
					    “Gotse Delçev” sitesi, “Slavovitsa” sok., bl.53E, giriş B, kat 2,
					    25.03.2016 tarihinden № PK-01-7550 No’lu turizm acentesi işleri yapabilmesi
					    için tasdik belgesi ile Turizm Kanununa göre tescil edilmiş turizm
					    acentesidir.
					</p>
					<p>
					    1.1. “Luxury discounts“<em>Ltd.</em> şirketinin kendi eketronik platformu
					    ile teklif ettiği(sunduğu) hizmet, sadece bizim
					    üyelerimiz(kullanıcılarımız) için öngörülen otel konaklama ve turlar için
					    cazip tekliflere erişim sağlamaktadır.
					</p>
					<p>
					    1.2. Sitede profil sahibi kişiler için (“kullanıcı” olarak anılmaktadırlar)
					    “Luxury discounts” OOD , turistik hizmetler ve ürünler teslimatı yapan
					    üçüncü kişiler tarafından teklif edilen otel konaklama ve organize edilmiş
					    turistik seyahatler (“Turistik teklifler” olarak anılmaktadırlar) için
					    rezervasyon yapmalarına fırsat verir. Web sitesinde sunulan tüm turistik
					    teklifler, ilgili teklifin açıklanmasında organize turistik seyahati
					    (“Tur”) için oldukları açıkca belirtilmemiş ise, otel konaklaması içindir.
					    Web sitemizde gösterilen tüm turistik teklifler anında ki mevcut olmalarına
					    bağlıdır ve bundan dolayı teklif edildikleri tüm dönem içinde erişilir
					    kalacaklarını garanti etmiyoruz.
					</p>
					<p>
					    1.3. “Luxury discounts“<em>Ltd.</em>, web sitemizde bulunan turistik
					    teklifleri veren tedarikçilerin turistik acentesi olarak çalışmaktadır.
					    “Luxury discounts“ OOD, turistik ürün ve hizmetlerin tedarikçileri
					    (Tedarikçiler) adından ve onların hesabına hareket etmektedir ve Siz ve
					    turistik ürün ve hizmetlerin tedarikçileri arasında sözleşme
					    akdedebilecekleri elektronik platformumuzdan yararlanmalarına fırsat verir.
					    “Luxury discounts“ OOD turoperatör faaliyeti gerçekleştirmez ve ana ve ek
					    turistik hizmetleri vermez.
					</p>
					<p>
					    1.3.1. Web sitesinden rezervasyon yapılmasında, “Luxury discounts“Ltd.,
					    Tedarikçinin ajanı ve aracısı sıfatı ile, turistik ürün ve hizmet verilmesi
					    için sözleşmede taraf değildir. İşbu genel koşullar, web sitesi bazında ve
					    “Luxury discounts” Ltd. tarafından sunulan elektronik hizmetin kullanımını
					    içerir, ancak satın aldığınız tüm turistik teklifler satın aldığınız
					    teklifte belirtilen süre ve koşullara göre ve tedarikçinin kuralları ve
					    koşullarına göre yapılır. Son belirtilenlere rezervasyon adımlarında
					    erişebilirsiniz.
					</p>
					<p>
					    1.3.2. Tedarikçiden turistik teklif satın almak ile Siz işbu Genel
					    koşulları kabul ediyorsunuz ve satın alınan teklif için geçerli olan
					    tedarikçinin Genel koşullarını da kabul ediyorsunuz. Web sitesinden
					    turistik teklif satın alarak, Siz, doğrudan turistik ürün veya hizmetin
					    Tedarikçisi ile sözleşme yaptığınızı ve aynı zamanda “Luxury discounts”
					    Ltd.’nin bu sözleşmede taraf olmadığını kabul ediyorsunuz.
					</p>
					<p>
					    1.4. Ajan olarak hareket ettiği için , “Luxury discounts” Ltd., tedarikçi
					    tarafından verilen hizmet için herhangi bir sorumluluk taşımaz ve satın
					    alınan teklif ile ilgili tarafınızdan uğradığınız zararlar içinde sorumlu
					    değildir. Eğer turistik teklifin satın alınmasından sonra bununla ilgili
					    sorularınız veya endişeleriniz varsa, doğrudan tarafınızdan satın alınan
					    hizmetin Tedarikçisi ile iletişime geçmelisiniz.
					</p>
					<p>
					    <strong>TEKLİF EDİLEN TURİSTİK HİZMETLERİN FİYATLARI</strong>
					</p>
					<p>
					    2.1 Her turistik teklifin fiyatı web sitede gösterilendir, gözle görünen
					    yanlışlık olduğu durumlar hariç. Fiyata KDV dahildir. Ödemeler banka ve
					    kredi kartı ile veya banka havale yolu ile yapılır.
					</p>
					<p>
					    2.2 Piyasada ki dinamik gelişimden dolayı bu tekliflerden bazıları doğru
					    olmayan fiyatlar ile gösterilmiş olabilir. Eğer turistik teklifin gerçek
					    fiyatı ilan edilmiş olandan daha düşük ise, tarafınızdan fazla ödenen
					    miktarı biz size geri iade edeceğiz. Eğer turistik teklifin gerçek fiyatı
					    ilan edilmiş olandan daha yüksek ise biz sizinle ek talimatlar ve
					    rezervasyonunuzun işleminden önce onaylamanız için, veya hizmet
					    Tedarikçisinin adından rezervasyonun iptal edilmesi için sizlerle iletişime
					    geçebiliriz.
					</p>
					<p>
					    2.3 Web sitede gösterilen fiyatın gözle görünür şekilde yanlış olduğu
					    durumlarda, biz size rezervasyon onayını göndersek bile, Tedarikçi
					    tarafınızdan yanlış(daha düşük) fiyata satın alınan hizmeti vermeyi kabul
					    etmek zorunda değildir.
					</p>
					<p>
					    2.4 Turistik tekliflerin fiyatları, tedarikçinin yoğunluğu ve turistik
					    sezona göre zamanla değişebilir, ancak değişmeler artık ödenmiş ve
					    Tedarikçi tarafından onaylanmış rezervasyonlara etkilemezler.
					</p>
					<p>
					    <strong>ÖDEME ŞEKLİ </strong>
					</p>
					<p>
					    4.1. İşbu genel koşullara göre ve “Luxury discounts” Ltd. çalışma
					    kurallarına gçre web sitesinden turistik teklifin rezervasyonunu yapmanız
					    için, siz teklifin fiyatını doğrudan “Luxury discounts” Ltd.’ye ödeme
					    yapıyorsunuz. Ödemeniz “Luxury discounts” Ltd. tarafından kabul ediliyor ve
					    bu tarafınızdan satın alınan hizmetin Tedarikçisinin ajanı/aracısı/ olarak
					    hareket etmektedir. Ödeme web sitesinde gösterilen yöntemler ile yapılır.
					    Tarafınızdan seçilen tarihlerde rezervasyon yapmanız için turistik teklifin
					    fiyatını ödemeniz gerekmektedir.
					</p>
					<p>
					    4.2. Web siteden rezervasyonun yapılması ile, ödeme için girdiğiniz
					    verilerin doğru olduğunu onaylıyorsunuz. Eğer ödeme yöntemlerden biri
					    reddedilmiş ise, aletrnatif yöntem için biz sizinle iletişime geçebiliriz
					    veya rezervasyonunuzu iptal edebiliriz.
					</p>
					<p>
					    4.3. Eğer kredi veya banka kartınız ile sizleri yanlış ücretlendirdiğimizi
					    düşünüyorsanız bizimle derhal irtibata geçmeniz gerekir. Eğer ödenen
					    paranın miktarının yanlış olduğunu tespit edersek, biz ilgili miktarı kredi
					    veya banka kartınıza geri iade edeceğiz.
					</p>
					<p>
					    4.4. Kredi kartı ile ödeme yapmışsanız işlem ücreti uygulanır. Bu işlem
					    ücreti ödeme işlemi ücreti masraflarımızdan daha fazla olmayacaktır. İşlem
					    ücretinin tutarı web siteden turistik teklifi ödemenizden önce
					    gösterilecektir.
					</p>
					<p>
					    4.5. “Luxury discounts” Ltd., sadece kullanıcı ve turistik hizmet
					    Tedarikçisi arasında ajan olarak hareket etmektedir ve bundan dolayı
					    tarafınızdan ödenen turistik teklif için fatura kesemez. “Luxury discounts”
					    Ltd. rezervasyon onayını verir ve bu, rezervasyon zamanında ödenen para
					    miktarını kullandığı için makbuz olarak kullanılabilir.
					</p>
					<p>
					    <strong>KULLANICI PROFİLİ </strong>
					</p>
					<p>
					    5.1.Bu web sitesinde teklif edilen hizmetlere sadece kullanıcı profili ile
					    kaydınızı girdikten sonra erişebilirsiniz.
					</p>
					<p>
					    5.1.1. Kullanıcı profilinin kayıt ettirilmesi 18(onsekiz) yaşından küçük
					    kişilere izin verilmez.
					</p>
					<p>
					    5.2. Profil oluşturmanız için elektronik posta adresinizi vermeniz ve birey
					    şifre oluşturmanız gerekir. Kullanıcı profili web sitede aynı zamanda diğer
					    sosyal ağalarda ki web sitelerinde Kişisel profillerinizi kullanarak da
					    kayıt edilebilir (facebook.com., google+, twitter etc.).
					</p>
					<p>
					    5.2.1. Eğer sosyal ağalarda profilinizi göstererek web sitemizde profil
					    oluşturduysanız, sosyal ağda kullanım için verdiğiniz bilgiye de erişime
					    izin verdiğinizi ve bizim kişisel veriler hakkında politikamıza göre sizin
					    logg token’ininizi de saklamamıza izin verdiğinizi kabul ediyorsunuz .
					</p>
					<p>
					    5.3. Profil oluşturmak için elektronik posta adresinizi kullandığınız
					    durumlarda, en az büyük ve küçük harfler, rakamlar ve işaretler
					    kombinasyonu içeren güvenirli şifreler kullanmanızı öneririz. Şifrenizi
					    gizli tutmak için sorumlu sizsiniz. Yabancı erişim veya profilinizin
					    kullanılmasından herhangi şüpheniz varsa derhal yardım için bizimle
					    iletişime geçiniz.
					</p>
					<p>
					    5.4. Eğer işbu genel koşulları ihlal etmişseniz, web sitemizin idarecisi
					    aşağıda ki mümkünatlardan birini kullanabilir: а) web siteye erişiminizi
					    bloke edebilir; б) web sitenin tarafınızdan kullanılmasına son verebilir;
					    в) kullanıcı profilinizi silebilir; г) kendisi veya turistik hizmetin
					    tedarikçisi ile birlikte turistik hizmetin verilmesini reddedebilir;.
					</p>
					<p>
					    <strong>TURİSTİK TEKLİFLER REZERFVASYONU</strong>
					    <strong></strong>
					</p>
					<p>
					    <strong> </strong>
					</p>
					<p>
					    6.1. Turistik telif rezervasyonu ve ödemesi için sıra ve adımlar website’ta
					    belirtilmiştir.
					</p>
					<p>
					    Website yolu ile turistik teklif rezerve ederseniz onu ödemeyi kabul
					    ediyorsunuz.
					</p>
					<p>
					    6.2. Rezervasyon esnasında, yanlış belirtilen bilgiler ve hatalar kontrol
					    etme ve aynı zamanda rezervasyonunuzu onaylamadan önce değişiklik yapma
					    imkanınız var.
					</p>
					<p>
					    6.3. Rezervasyonunuzu yapmaz dan önce, Sizin (ve yol arkadaşlarınız)
					    turistik tekliflerin şartlarına ve hükümlerine uyup uymadığınızı ve seçmiş
					    olduğunuz ilgili turistik teklifin tarihlerinde yolculuk yapma imkanınızın
					    olduğunu kontrol etmenizi tavsiye ederiz.
					</p>
					<p>
					    6.4. Eğer Sizin veya yol arkadaşlarınız özel isteği varsa, kayıt işlemi
					    esnasında belirtmeniz gereklidir.
					</p>
					<p>
					    "Luxury Discounts” Ltd.Şti., tarafınızdan yöneltilen her makul isteğinizi
					    Sağlayıcılara bildirmeyi taahhüt ediyor, fakat isteklerinizin turistik
					    hizmet sağlayıcıları tarafından yerine getirileceğini garanti edemiyoruz.
					</p>
					<p>
					    6.5. Rezervasyonunuz yapılmadan önce, sizin veya yol arkadaşınızın
					    rezervasyonunuzu etkileyecek tıbbi veya sağlık koşullarınızın olduğun
					    konusunda bizi bilgilendirmeniz gerekli zira turistik teklifin size uygun
					    olup olmadığını dikkate alabilelim.
					</p>
					<p>
					    6.6. Yapmış olduğunuz her bir rezervasyon, turistik hizmet sağlayıcısı
					    tarafından onaylanması gereklidir.
					</p>
					<p>
					    Sağlayıcının turistik acentesi sıfatı ile, Size rezervasyonunuzun
					    onaylandığına dair e-mail göndereceğiz.
					</p>
					<p>
					    6.7.1. Sizin ve turistik hizmet Sağlayıcısı arasında ki sözleşme, Turistik
					    hizmet sağlayıcısının turistik acentesi sıfatı ile Size rezervasyon
					    onayınızı gönderdikten sonra imzalanmış sayılacaktır.
					</p>
					<p>
					    Rezervasyonunuzun onaylanmasından sonra Sizin tarafınızdan seçilen yönteme
					    uygun olarak ödeme yapacaksınız.
					</p>
					<p>
					    6.7.2. Rezevasyonunuz işlendikten sonra turistik teklif sağlayıcısından,
					    satın alınan turistik teklifin detaylarını içeren, tatil (organize tatil
					    yolculuğu) rezervasyonunun yapılması esnasında istenebilecek veya Sağlayıcı
					    tarafından sağlanan diğer turistik hizmetler hakkında yeni bir e-mail
					    alacaksınız.
					</p>
					<p>
					    6.8. Sizinle iletişim bağlantıları için sunacağınız bilgilerin güncel ve
					    doğru olması gerekiyor.
					</p>
					<p>
					    Tarafınızdan satın alınan turistik teklifin başarılı olarak
					    gerçekleştirilmesi için, eğer bu gerekli ise, Biz vaya tarafınızdan satın
					    alınan teklif sağlayıcısı sizinle iletişime geçebiliriz.
					</p>
					<p>
					    Eğer bu madde de ki yükümlülüklerinizi yerine getirmezseniz, Tarafınızdan
					    satın alınan turistik tekliften faydalanamaz iseniz "Luxury Discounts”
					    Ltd.Şti. Sorumluluk taşımamaktadır.
					</p>
					<p>
					    6.9. Turistik teklif sağlayıcı tarafından rezervasyonunuz onaylandıktan ve
					    bizim aracılığımız ile onay aldıktan sonra, turistik hizmet Sağlaycısının
					    turizim acentesi sıfatı ile Sizin rezervasyonunuzun türüne bağlı olarak
					    e-mail göndereceğiz.
					</p>
					<p>
					    6.9.1. Sadece hotel konaklamasını içeren turistik teklif satın alınması
					    esnasında, Sizin rezervasyon formunu oluşturan ve Sizin rezervasyon
					    detaylarını, aynı zamanda turistik teklif Sağlayıcısı hakkında bilgi ve
					    iletişim bilgilerini içeren onayı e-mail’li alacaksınız. Bu e-mail
					    tarafınızdan seçilen teklifin alımını onaylar ve turistik teklif konusunda
					    Size gerekli bilgiyi içermektedir.
					</p>
					<p>
					    6.9.2. Organize turistik seyahat esnasında esnasında, turistik teklif ve
					    turistik hizmet Sağlayıcısı ayrıntılı bilgi içeren onay e-mail’i
					    alacaksınız.
					</p>
					<p>
					    Bu e-mail rezervasyonunuzun kesin onayı değildir ve yolculuk için gerekli
					    tüm bilgiyi içermemektedir. Turistik teklif (benzer faaliyet gerçekleştirme
					    şartlarına uygun olarak kayıtlı ilgili yabancı şirket veya tur operatörü)
					    sağlayıcısı, Size, turistik çek ve Sizin yolculuğunuzu ilgilendiren her
					    türlü ilave bilgi ve aynı zamanda diğer adımları veya rezervasyonunuzu
					    tamamlamak için gerekli bilgileri içeren ilave e-mail göndereceğiz.
					</p>
					<p>
					    6.9.3. "Lluxury Discounts” Ltd.Şti. tarafından gönderilen e-mail
					    6.9.2.madde uyarınca, sadece turistik teklifin ödemesini vezervasyonunuzun
					    tarihini ve aynı zamanda turistik teklif Sağlayıcısının Sizinle iletişime
					    geçeceğini teyit eder. Bu, tatil Sağlayıcısının onu onayladığı anlamına
					    gelmez.
					</p>
					<p>
					    6.9.4. Turistik teklif Sağlayıcısının Sizin rezervasyonunuzu, mesela Sizin
					    tarafınızdan seçilen tarihlerde veya website’de yanlış belirtilen fiyattan
					    dolayı onaylamadığı durumda, biz sizi rezervasyonunuzun iptal olduğu
					    konusunda bilgilendireceğiz.
					</p>
					<p>
					    "Luxury Discounts” Ltd.Şti. Turistik teklif Sağlayıcısı ile iletişime
					    geçerek Sizin tarafından ödenen meblağnın iadesini sağlayacaktır.
					</p>
					<p>
					    6.10. Sizin tarafınızdan satın alınan turistik teklif ile ilgili olarak
					    bilgi almanız anında, belirtmiş olduğunuz kimlik bilgilerinin doğru
					    olduğunu bir kez daha kontrol etmeniz gereklidir. Örneğin, isimler, yaşınız
					    ve b.g. kimliğinizde ki bilgiler ile aynı olup olmadığına. Eğer bir
					    uyumsuzluk tespit ederseniz, lütfen mümkün olan ilk anda turistik teklif
					    Sağlayıcısı ile irtibata geçiniz.
					</p>
					<p>
					    6.11. Bilet, bilgi ve/veya yol evrakları bilgiler e-mail yolu ile teslim
					    edilmezlerse satın alınan turistik tekliften faydalanamama durumunda,
					    "Luxury Discounts” Ltd.Şti. sorumluluk taşımamaktadır.
					</p>
					<p>
					    Eğer ilgili evrakları e-mail bildirimde almaz iseniz, lütfen Sağlayıcı ile
					    doğrudan irtibat geçiniz.
					</p>
					<p>
					    6.12.1. Ödenen turistik tekliflerin şartları tüketici tarafından
					    değiştirilemez ve aynı zamanda kendisi tarafından ödenen meblağların
					    "Luxury Discounts” Ltd.Şti. veya ödenen turistik teklifin Sağlayıcısı
					    tarafından iade edilmez eğer bu website’ye konulan turistik teklif
					    şartlarında belirtilmemişse.
					</p>
					<p>
					    İş bu madde genel hükümlerin 6.9.4.maddesinde ki durumlarda uygulanmaz.
					</p>
					<p>
					    6.12.2. 6.12.1.Maddeki durumlarda "Luxury Discounts” Ltd.Şti., turistik
					    teklif sağlayıcısı nezdinde kendi takdirine bağlı olarak, Tarafınızdan
					    istenilen değişiklikler konusunda yardımcı olabilir, fakat Sağlayıcının
					    bunu kabul edeceği konusunda angajman üstlenmiyor. İstenilen değişiklikleri
					    Sağlayıcının kabule etmesi durumunda ilave ücret öderseniz "Luxury
					    iscounts” Ltd.Şti., sorumluluk taşımamaktadır.
					</p>
					<p>
					    6.13. Sağlayıcı tarafından, satın aldığınız turistik teklif şartlarında
					    küçük değişiklikler gerçekleştirilmesi esnasında bunun için bizim
					    tarafımızdan veya satın almış olduğunuz turistik teklif Sağlayıcısı
					    tarafından haberdar edileceksiniz.
					</p>
					<p>
					    Bu turistik teklifin yerine getirilmesini durdurmaz.
					</p>
					<p>
					    6.14.1. Sizin rezervasyonunuzun başlamasından önce turistik teklif
					    Sağlayıcı tarafından durdurulması durumunda veya ilk başta ki içerikte
					    öngörülen şartlarda önemli değişiklikler yapılmış ise, eğer benzer bir
					    bilgiye sahip isek, bunun için mümkün olan ilk anda Sizi bilgilendireceğiz.
					</p>
					<p>
					    6.14.2. 6.14.1.Madde ki durumlarda, Biz, Sizin tarafınızdan Sağlayıcıdan
					    satın alınan turistik teklifin aşağıdaki alternatif seçeneklerden
					    hangisinin olacağı konusunda Sağlayıcı ile sözleşmek için makul çaba sarf
					    edeceğiz, fakat, Sağlayıcının işbirliğinin gerekli olmasından aynılarının
					    her zaman mümkün olabileceğini garanti etmiyoruz. :
					</p>
					<p>
					    Paralarını bizden mi yoksa Sağlayıcıdan mı isteyecekler.
					</p>
					<p>
					    Bizim Sağlayıcılarla olan sözleşmelerimizde, eğer onların hatasından dolayı
					    rezervasyon veya tatil gerçekleşmediyse, turistlere iade edebilmek için
					    Sağlayıcı meblağının tam tutarını BİZE iade etmelerini öngördük.
					</p>
					<p>
					    FAKAT, turistin parasını Sağlayıcıdan araması bizim için daha kolay
					    olacaktır.
					</p>
					<p>
					    Fakat turiste de daha zor olacak.
					</p>
					<p>
					    Sağlayıcının bize ödemesi daha iyi, aksi takdirde turiste ödemeyebilir.
					</p>
					<p>
					    Paralarını bizden aramaları daha iyi olacak.
					</p>
					<p>
					    6.14.2.1. Gerçekleştirilen değişiklikleri kabul edin;
					</p>
					<p>
					    6.14.2.2. Sadece hotel konaklaması rezervasyonları esnasında, ödenen tüm
					    meblağların iade edilmesi veya benzer bir öneriyi kabul etmeniz, aradaki
					    farkın tarafınıza ödenmesi veya Sizin tarafınızdan ilgili ek ödemelerin
					    gerçekleştirilmesi durumunda rezervasyonunuzdan vaz geçmeniz;
					</p>
					<p>
					    6.14.2.3. Organize turistik seyahat satın alındığında, ödenen meblağların
					    iade edilmesi veya turistik teklif Sağlayıcısından alternatif öneri kabul
					    etmeniz durumunda sözleşmeden vazgeçmeniz.
					</p>
					<p>
					    Eğer alternatif öneri daha düşük fiyatlı ise aradaki fark iade edilecek,
					    dolayısı ile fiyat daha yüksek ise Sizin tarafınızdan ek ödeme
					    yapılacaktır.
					</p>
					<p>
					    6.15. 6.14.1. ve 6.14.2.maddelerinde ki durumlarda "Luxury Discounts”
					    Ltd.Şti., Sizin tarafınızdan görülen zarar veya kayıplar konusunda her
					    hangi bir sorumluluk taşımamaktadır.
					</p>
					<p>
					    6.16. "Luxury Discounts” Ltd.Şti., website’de önerilen bazı turistik
					    teklifler için mevcut “bekleme” hizmetini sunmaktadır.
					</p>
					<p>
					    Bu hizmet tüketiciye ilgili turistik teklif için belirli bir tarihi tutma
					    imkanını sunmaktadır.
					</p>
					<p>
					    Belirli bir tarihi tutmak rezervasyon anlamına gelmez, website
					    kullanıcılarına aynı tarihi rezerve etmelerine ve teklif tüketicisi
					    tarafından seçilen boş yerlerin bitmesine imkan vermez.
					</p>
					<p>
					    “Bekleme” hizmeti turistik teklif fiyatı için depozito değildir ve turistik
					    teklifin onaylanması anlamına gelmez.
					</p>
					<p>
					    6.17.1. Tüketicinin 6.16. Maddeden faydalanması durumunda, fakat teklif
					    için belirlenen süre içinde rezervasyonunu ödemez ise, ödenen taks iade
					    edilmez.
					</p>
					<p>
					    6.17.2. Tüketicinin 6.16. maddede ki hizmetten faydalanması durumunda,
					    fakat seçilen turistik hizmet Sağlayıcı website’de ki sunumunu durdurursa,
					    "Luxury Discounts” Ltd.Şti., yukarıdaki maddeye göre ödenen meblağı tam
					    tutarında tüketici tarafından seçilen ödeme şeklini kullanarak iade
					    edecektir.
					</p>
					<p>
					    6.18.Madde kimdir?
					</p>
					<p>
					    6.18. Website’de belirtilen belirli turistik teklif şartları işbu Genel
					    şartlarla çelişiyorsa website’de belirtilen teklif önceliği vardır.
					</p>
					<p>
					    Website’de belirli teklif için belirtilen şartların sadece geçerlilik
					    süresi içinde faaliyetleri vardır.
					</p>
					<p>
					    <strong>REZERVASYON İPTALİ VE MEBLAĞ İADESİ</strong>
					    <strong></strong>
					</p>
					<p>
					    <strong> </strong>
					</p>
					<p>
					    7.1. Website üzerinden tüketicinin ödemesi gerçekleştirilmiş hotel
					    konaklaması ve/veya ilave turistik hizmet olması durumunda, turistik hizmet
					    Sağlayıcısı sözleşmeyi yerine getirmez ise o zaman tüetici tarafından
					    ödenen meblağı tam tutarı üzerinden ödemesi gereklidir.
					</p>
					<p>
					    7.2. Organze edilmiş ilgili turistik seyahati sunan tur operatörünün
					    sözleşmenin temel meddelerinden bazılarını önemli oranda değiştirmesi veya
					    sözleşmelerin maddelerine uymaya itiraz etmesi durumunda, tüketici ceza
					    veya tazminat borçlu olmadan sözleşmeden vazgeçebilir.
					</p>
					<p>
					    Bu durumlarda tüketici ve tur operatörü arasında ki ilişkiler kendi
					    aralarında çözülür ve "Luxury Discounts” Ltd.Şti. ise çözülmesi konusunda
					    yardımcı olabilir.
					</p>
					<p>
					    7.3.1. Tüketiciler tarafından website üzerinden gerçekleştirilen diğer
					    turistik hizmetlerden ve hotel konaklamasından vazgeçilmesi durumunda,
					    ödenen meblağlar satın alınan turistik hizmet Sağlayıcıdan veya "Luxury
					    Discounts” Ltd.Şti. Tarafından iade edilmesi söz konusu değildir eğer
					    ilgili teklifin şartlarında iade hakkı özellikle öngörülmemişse.
					</p>
					<p>
					    7.3.2. Eğer tüketiciye ödenen rezervasyondan vazgeçme imkanı sunulmuş ise
					    7.3.1.madde uyarınca, satın alınan turistik teklif Sağlayıcısının sözleşme
					    şartları uyarınca ceza kesebilir.
					</p>
					<p>
					    7.4.1. Tüketici tarafından, website üzerinden imzalanan organize edilmiş
					    turistik seyahat sözleşmesinden vazgeçmesi durumunda, ödenen meblağların
					    iadesi söz konusu değildir, iade edilmesi gerektiği durumda, iade organize
					    edilmiş ilgili turistik seyahat sunucusu tur operatörünün kuralları ve
					    şartları uyarınca gerçekleştirilir.
					</p>
					<p>
					    7.4.2. Yukarıda ki maddelerde ki durumlarda, tüketicinin doğrudan organize
					    turistik seyahati sağlayan tur operatörü ile iletişime geçmeli ve
					    sözleşmelidir.
					</p>
					<p>
					    <strong>AŞ</strong>
					    <strong>I</strong>
					    <strong>LAMAYAN GÜÇ/KUVVET</strong>
					</p>
					<p>
					    8. 20 (yirmi) günden daha fazla süren aşılamayan güç/kuvvet durumunda,
					    website üzerinde satın alınan turistik hizmet kullanıcılarının ve
					    tüketicilerinin gördükleri zararlardan "Luxury Discounts” Ltd.Şti. sorumlu
					    değildir.
					</p>
					<p>
					    Aşılamayan güç/kuvvet’ten aşağıda belirtilen olağan dışı karakterden olay
					    ve hareketler anlaşılmaktadır:
					</p>
					<p>
					    Savaş, abluka, doğal felaketler, ambargo, grevler, hükümet ve idari
					    kısıtlamalar, ve "luxury discounts” Ltd.Şti. ve turistik teklif Sağlayıcısı
					    veya hizmet kullanıcısı tarafından öngörülemeyen ve engellenemeyen ve
					    rezervasyonun gerçekleşmesinden sonra ortaya çıkan diğer benzer olaylar ve
					    hareketler.
					</p>
					<p>
					    <strong>SUNULAN BİLGİLER</strong>
					</p>
					<p>
					    9.1. "luxury discounts” Ltd.Şti., yolculuğunuz ile ilgili olarak ilgili
					    ülkeye giriş hakkı almak için pasaport, hudut veya diğer şartlar konusunda
					    genel bilgi sağlayabilir.
					</p>
					<p>
					    İlgili devlete giriş hakkı almak için özellikle pasaport, hudut veya diğer
					    şartları kontrol etmek, aynı zamanda elçilik ve/veya konsolosluklarda
					    ilgili kontrolleri yapmak sizin sorumluluğunuzdur.
					</p>
					<p>
					    Yukarıda belirtilen veya onlarla ilgili olan konularda "luxury discounts”
					    Ltd.Şti., tarafından sunulan her türlü bilgi, örnek ve tavsiye niteliğinde
					    olarak sayılmalıdır, fakat sunulan bilgiler temelinde alınan kararlar veya
					    Sizin tarafınızdan maruz kalınan olumsuz etkiler konusunda "luxury
					    discounts” Ltd.Şti. sorumluluk taşımamaktadır.
					</p>
					<p>
					    9.2. İlgili devlete giriş için Sizin ve tüm sizin yol arkadaşlarınızın tüm
					    şartlara uygun olduğunu kontrol ve aynı zamanda yolculuğunuzdan önce
					    gerekli tüm evraklara ve izinlere sahip olduğunuzu kontrol etmek sizin
					    yükümlülüğünüzdür.
					</p>
					<p>
					    Yukarıda belirtilen sebeplerden her hangi birinden dolayı, Sizin
					    tarafınızdan satın alınan turistik tekliften faydalanamazsanız, ne biz nede
					    turistik teklif Sağlayıcıları sorumluluk taşımamaktadır.
					</p>
					<p>
					    Lütfen, gerekli evraklar ve izinler, pasaportlar, kimlik kartları, vizeler
					    veya sigorta evrakları gibi gerekli olan her türlü belgelere sahip
					    olduğunuzdan ve aynı zamanda uçuşunuz öncesinde hava limanında kontrol için
					    yeterli zaman öngördüğünüzden emin olun.
					</p>
					<p>
					    <strong>DİĞER ŞARTLAR</strong>
					</p>
					<p>
					    10. İş bu genel hükümlerin her hangi bir maddesinin veya ilave olarak
					    uzlaşılan maddelerin geçersizliği, Genel hükümlerin veya şartın veya şartın
					    veya hükmün genel olarak geçersizliğini sağlamaz.
					</p>
					<p>
					    11. İşbu sözleşme ile çözülemeyen sorunlar için Bulgaristan Cumhuriyetinde
					    mevcut mevzuat uygulanır.
					</p>
					<p>
					    12. İşbu genel hükümlerin yorumlanması ve uygulanması esnasında veya
					    tüketici tarafından hak talepleri bulunması esnasında çıkan sorunlar,
					    aynıları taraflar arsında karşılıklı uzlaşı ile çözülür.
					</p>
					<p>
					    Uzlaşıya varılamaması durumunda, genel hükümlerden doğan veya onların
					    yorumlanması ile ilgili geçersizlik, yerine getirme veya sonlandırma, ve
					    aynı zamanda genel hükümlerdeki boşlukları doldurma veya yeni oluşan
					    koşullara uyum konusunda çıkan sorunlar, Bulgar mevzuatında öngörülen sıra
					    uyarınca çözülecektir.
					</p>
					<p>
					    13. www.luxury-discounts.com website’ının içeriği , web dizaynı, onun her
					    menüsü, kategori veya alt kategori, bölüm veya alt bölüm, sıralama şekli ve
					    bilginin yapılandırılması, veri tabanı ve yazılım sistemleri "Luxury
					    Discounts” Ltd.Şti.’nin münhasır mülkiyeti olup ve telif hakları kanunu
					    uyarınca ve benzeri haklar tarafından koruma altındadır.
					</p>
					<p>
					    ww.luxury-discounts.com website’inden her ne formda olursa olsun her
					    paylaşım, yayınlama, çoğaltma, bilgi satışı ve resim materyalinin "Luxury
					    Discounts” Ltd.Şti.’nin izni olmaksızın yasaktır ve yasa ihlali
					    sayılmaktadır.
					</p>
					<p>
					    http://www.luxury-discounts.com
					</p>
					<p>
					    http://www.luxury-discounts.com
					</p>
					<p>
					    14. Kullanıcının www.luxury-discounts.com website’dan sors kodunu, kesim,
					    elementler ve materyalleri "Luxury Discounts” Ltd.Şti.’nin önceden özel
					    izni olmadanmuhafaza etmeye hakkı yoktur.
					</p>
					<p>
					    http://www.luxury-discounts.com
					</p>
					<p>
					    15. Kullanıcı, www.luxury-discounts.com website’ından her ne şekilde olursa
					    olsun verileri veya içeriğini başka bir bilgisayara, server’a web sayfasına
					    veya toplu kullanım için başka bir ortama aktarmayacağına veya
					    göndermeyeceğine ve aynı zamanda onları ticari amaç için kullanmayacağını
					    kabul eder.
					</p>
					<p>
					    http://www.luxury-discounts.com
					</p>
					<p>
					    16. Kullanıcı, bu website’ın doğru çalışmasına karışmamak için cihazı veya
					    yazılım kullanmayacağını kabul eder.
					</p>
					<p>
					    Website’ın işbu genel hükümlerde belirtilmeyen şekilde kullanılması fikri
					    mülkiyet ile ilgili telif hakları kanununun ihlaline, ticari marka, ahlaklı
					    davranış ve ticari uygulamalar veya diğer kanunların ihlaline sebep
					    olabilir.
					</p>
					<p>
					    Telif hakkı işaretlerinin değiştirilmesi, saklanması veya silinmesine izin
					    verilmez.
					</p>
					<p>
					    17. Kullanıcının www.luxury-discounts.com website’ı kullanması sonucunda
					    telif hakkı, ticari marka veya diğer mülkiyet hakları ihlali sonucunda
					    meydana gelen tüm zararlar konusunda sorumluluk taşımaktadır.
					</p>
					<p>
					    http://www.luxury-discounts.com
					</p>
					<p>
					    <strong>KİŞİSEL BİLGİLER SİYASETİ</strong>
					</p>
					<p>
					    18. "Luxury Discounts” Ltd.Şti., www.luxury-discounts.com website’nin
					    sahibi ve operatörüdür.
					</p>
					<p>
					    "Luxury Discounts” Ltd.Şti., Kişisel Bilgiler Kanunu uyarınca kişisel
					    bilgiler admini olarak kayıtlıdır.
					</p>
					<p>
					    http://www.luxury-discounts.com
					</p>
					<p>
					    19. "Luxury Discounts” Ltd.Şti., sunulan hizmetin kullanımı ile ilgili
					    olarak kişisel bilgilerin güvenliği için gereken çabayı gösteriyor. Bu
					    başlık, "Luxury Discounts” Ltd.Şti., sizin kişisel bilgilerinizi, internet
					    sayfası yolu ile veya belirtilen başka bir şekilde topladıktan sonra ne
					    için kullanacağını ve kime sunulabileceğini belirtir.
					</p>
					<p>
					    "Luxury Discounts” Ltd.Şti., sizin kişisel bilgilerinizi genel hükümlerde
					    belirtilen amaçlar için kullanacaktır. Belirtilen amaçlar için Sizin
					    tarafınızdan sunulan bilgiler ayrıntılı olarak belirtilmiş değildir.
					    "Luxury Discounts” Ltd.Şti., Sizin tarafınızdan sunulan bilgi güvenli bri
					    yazılım ortamında işlem görür ve korunur.
					</p>
					<p>
					    20. "Luxury Discounts” Ltd.Şti., kulanıcı tarafından sunulan kişisel
					    bilgileri hiç bir şekilde açıklamama ve toplanan bilgiyi üçüncü kişilere -
					    gerçek veya tüzel kişilere, devlet kurumlarına ve diğerlerine aşağıda ki
					    durumların dışında sunmama yükümlülüğünü üstlenir:
					</p>
					<p>
					    a) eğer kullanıcı kayıt esnasında veya daha sonra ki bir aşamada açık onay
					    aldıysa; veya b) eğer bilgi devlet kurumları veya yasa ile belirlenen
					    prosedürler uyulması durumunda yürürlükte olan mevzuat gereği bu tür
					    bilgileri toplama ve isteme yetkisi olan yetkili kişiler; veya c) kanunda
					    veya işbu genel hükümlerde belirtilen durumlarda.
					</p>
					<p>
					    21. İşbu genel şartların kabul edilmesiyle, kullanıcı, belirtilen sıra
					    usulünce işlenecek olan, website yolu ile gerçekleştirilen hizmetin
					    amaçları için istenilen kişisel bilgileri sunmayı kabul eder
					</p>
					<p>
					    22. Kullanıcının website’ta (ziyaret edilen teklifler, beğenilen teklifler,
					    sorulan sorular, yorumlar) gerçekleştirdiği hareketler ve kendisi
					    tarafından satın alınan teklifler (destinasyonlar, fiyatlar, tairhler,
					    teklifte ki şartlar, toplam arttırılan meblağ) hakkında ki bilgi her zaman
					    "Luxury Discounts” Ltd.Şti. Tarafından doğrudan pazarlamanın amaçları için
					    her zaman kullanılabilir.
					</p>
					<p>
					    23. Sizi, rezervasyonunuzun başarı ile tamamlandığı konusunda
					    bilgilendirmek isterim ve Sizin tarafınızdan seçilen turistik ürün veya
					    hizmetin satın alınması, sizin rezervasyonunuz (örneğin pasaportunuzun
					    düzenlendiği tarih ve numarası veya kimlik kartı, kimlik No) için ve
					    gerekli olması durumunda isimlerinizin, cinsiyetinizin, elektronik
					    adresinizin ve telefonunuzun ve aynı zamanda diğer bilgileri sunmanız
					    gerekli olacaktır. Bu bilgi sizin rezervasyonunuzu işleyebilmemiz için
					    gereklidir ve sizin tarafınızdan satın alınan ürün veya hizmeti (örneğin
					    tur operatörlerine, hotellere, havacılık şirketlerine, gümrük ve göçmenlik
					    gibi kamu kurumlarına eğer gerekli ise ve b.g.) sunan kişilere de
					    sunulabilir.
					</p>
					<p>
					    Sizin tarafınızdan seçilen destinasyona bağlı olarak, bu genel şartları
					    kabul etmeniz ile onayını (ve grubunuza kattığınız kişilerin de onayını)
					    verdiğiniz yukarıda belirtilen kişisel bilgiler gerekli olması durumunda
					    avrupa ekonomik sahası dışına da sunulabilir.
					</p>
					<p>
					    24. Bizim site’ımız yolu ile rezervasyon yaptığınızda siz ödeme için
					    bilgilerinizi girebileceğiniz ödeme sayfasına yönlendirileceksiniz. Biz
					    sizin ödemeleriniz için bilgi saklamıyoruz, örneğin Sizin kredi kartı
					    numarası gibi. Ödeme için kullandığımız platform, rezervasyon esnasında
					    ilgili ödemeyi gerçekleştirmek için gerekli bilgiyi işliyor. Ödemenizi
					    bizim hesabımızda gördükten sonra sizin turistik ürün veya hizmet
					    sağlayıcınıza website yolu ile ilgili ödemeleri gerçekleştiriyoruz.
					</p>
					<p>
					    25. Kullanıcının “Luxury Discounts” Ltd.Şti.’nin genel şartlarını kabul
					    etmesi ile, sunulan hizmet ile ilgili olarak "Luxury Discounts”
					    Ltd.Şti.’nin işbirliği yaptığı tüccarlardan ve aynı zamanda "Luxury
					    Discounts” Ltd.Şti. Tüccarlara aşağıdakiler sunması için kullanıcı Google+,
					    Facebook gibi, website profillerinde veya sanal alanda her nerede olursa
					    olsun, veya kendisi tarafından belirtilen e-mail adresine tüccarlardan
					    kişisel, reklam veya diğer ticari bildirimler göndermelerine izin veriri.
					</p>
					<p>
					    Turistik hizmet veya ürünler veya başka hizmetler satın alan kullanıcının
					    isimleri ve e-mail adresleri.
					</p>
					<p>
					    26. Website’ımıza mobil cihaz veya "Luxury Discounts” Ltd.Şti.’nin mobil
					    cihazlar uygulaması ile erişmek için, kullanıcı, "Luxury Discounts”
					    Ltd.Şti.’nin kendisinin şimdi bulunduğu konum ile ilgili ve elektronik
					    platform yolu ile kendisinin tercih ettiği belirgin ve yakın tekliflerin
					    kendisine önerilmesi amacı ile kullanılabilecek verilerin toplanmasını
					    onaylar ve izin verir.
					</p>
					<p>
					    27. Alabileceğimiz diğer bilgi, bizim website’mize bağlanmak için cihazınız
					    hakkında alabileceğiniz bilgidir. Bu bilgi cihazınızın türü, IP numaranız
					    veya cihazınızın telefon numarası, cihazınızın benzersiz tanımlayıcı
					    numaraları, işletim sistemleri, tarayıcılar veya cihazınız tarafından
					    kullanılan, bizim site’mıza bağlı uygulamalar, Sizin internet hizmeti
					    tedarikçiniz veya mobil ağa bilgilerinizi içerebilir.
					</p>
				</div>
			</div>
		</div>
	</div>
</section>
