<?php
$this->HtmlBetter->addCrumb(__('Home'), '/');
$this->HtmlBetter->addCrumb(__('Privacy Policy'), '/pages/privacy-policy');
?>
<section id="PagePrivacyPolicy">
	<div class="container">
		<div class="row">
			<div class="col s12 l3 sidenav-left">
				<?= $this->element('Sidebar/nav-pages'); ?>
			</div>
			<div class="col s12 l9">
				<h4 class="page-heading"><?= __('Privacy Policy'); ?></h4>
				<div class="card-panel p30" style="text-align: justify;">
					<p>Luxury Discounts Ltd., luxury-discounts.com web sitesinin sahibi ve işletmecisidir. Luxury Discounts Ltd., kişisel bilgilerin korunmasıyla ilgili Bulgar kanunlarına uygun olarak kişisel verilerin kayıtlı yöneticisidir. Kişisel verilerin korunması için gerekli gayreti gösteriyoruz. Bu bölüm kişisel verilerinizin tarafımızdan nasıl işleneceğini ve kişisel verilerinizin toplanması için platformumuzdan veya bahsedilen diğer moddan toplandıktan sonra kime teslim edileceğini açıklamaktadır. Luxury Discounts Ltd., verilerinizi yalnızca kayıt ve koşullar bölümünde açıklanan amaçlar için kullanacaktır. Verilen kişisel verilerin amacı ayrıntılı olarak belirtilmemiştir. Luxury Discounts Ltd., tedarik edilen kişisel bilgileri güvenli yazılım ortamında işler ve korur.</p>
					<p>Luxury Discounts Ltd, sağlanan kişisel verilerin kullanıcı tarafından herhangi bir şekilde ifşa edilmemesi ve toplanan bilgilerin üçüncü taraflara - kişilerin, tüzel kişilerin, eyalet yetkililerinin, vb .- sunmamasının yükümlülüğünü kabul eder. (a) Kayıt sırasında veya daha sonra bir kullanıcının onayını almak; veya (b) yasal mercilere, yasal prosedürlere uygun olarak bilgi talep etme ve toplama yetkisi olan devlet makamları veya yetkilileri tarafından talep edilmesi; veya (c) diğer kanunlar veya bu şartlar ve koşullar ile belirtilir.</p>
					<p>Geçerli şart ve koşulları kabul ederek, kullanıcı, talep edilen kişisel veriyi, sonraki siparişte işlenecek olan platform hizmeti amacıyla vermeyi kabul eder. Kullanıcının web sitesindeki (ziyaret edilen teklifler, beğenilen teklifler, sık sorulan sorular, yorumlar) eylemleri veya kullanıcı tekliflerini (satın alınan yerler, fiyatlar, tarihler, teklif koşulları, indirim) dikkate alan bilgiler doğrudan pazarlama amacıyla her zaman Luxury Discounts Ltd. tarafından kullanılabilir.</p>
					<p>Rezervasyonunuzu başarılı bir şekilde yapmak ve seçtiğiniz turizm ürününü veya hizmeti satın aldığınızda sizden gerçek ad, cinsiyet, ev adresini, e-posta adresini, cep telefonu numarasını ve gerekirse diğer bilgileri isteyebiliriz. Rezervasyon (pasaport veya kimlik numarası, grubunuzdaki kişiler dâhil olmak üzere yayınlanma tarihi). Bu bilgi, rezervasyon işlemleriniz için gereklidir ve hizmeti veren yetkililere (tur operatörleri, oteller, havayolları, gümrük gibi kamu otoriteleri ve gerekirse göçmenlik hizmetleri) teslim edilebilir. Seçilen varış yerine uygun olarak, yukarıda belirtilen kişisel veriler, gerekliyse, şartları ve koşulları kabul etmeyi kabul ettiğiniz Avrupa Ekonomik Bölgesi dışında (birlikte gönderilen bir kişiye) teslim edilebilir.</p>
					<p>Web sitemiz aracılığıyla rezervasyon yaparken, ödeme bilgilerinizi doldurabileceğiniz ödeme ağ geçidine yönlendirilirsiniz. Kredi / bankamatik kartı ayrıntılarınız gibi ödemelerinizi göz önünde bulundurarak herhangi bir bilgiyi saklı tutmayız. Kullandığımız ödeme sağlayıcı, ödeme veya rezervasyon için gerekli olan bilgileri işler. Ödemenizi aldıktan sonra, web sitemiz aracılığıyla satın aldığınız turizm ürün veya hizmet tedarikçisine ödemeyi aktarırız. Lüks İndirimler Ltd'nin kayıt ve koşullarının kabul edilmesi ile birlikte, kullanıcı promosyonlarımızın e-posta, Google+, Facebook veya başka herhangi bir sosyal medya aracılığıyla haber bültenleri almayı kabul eder. Kullanıcı, Luxury Discounts Ltd., platformumuzdan ürün satın alırken, iş ortaklarının adlarına, e-postaya ve diğer hizmetlere ilişkin kişisel bilgileri tedarik etmesini kabul eder.</p>
					<p>Mobil cihaz veya mobil uygulamanızı kullanarak web sitesine erişmek için kullanıcı Luxury Discounts Ltd'nin mevcut konumu ile ilgili verileri toplamasına razıdır ve kullanıcının favori hedeflerini platform aracılığıyla belirgin çıkarlarına göre belirlemek üzere kullanılmasına izin verir.</p>
					<p class="address">Luxury Discounts Ltd<br />
						1. kat<br />
						Bulgaristan blvd. 1404, Vitosha İş Merkezi<br />
						Sofya, Bulgaristan<br />
						<a href="mailto:destek@luxury-discounts.com">destek@luxury-discounts.com</a> <br />
						+ 90 (850) 4551438, + 90 (850) 4551396
					</p>
				</div>
			</div>
		</div>
	</div>
</section>