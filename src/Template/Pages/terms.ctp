<?php
	$this->assign('title', __('Terms & Conditions'));
	
	$this->HtmlBetter->addCrumb(__('Home'), '/');
	$this->HtmlBetter->addCrumb(__('Terms & Conditions'), '/pages/terms');
?>
<section id="PageTerms">
	<div class="container">
		<div class="row">
			<div class="col s12 l3 sidenav-left">
				<?= $this->element('Sidebar/nav-pages'); ?>
			</div>
			<div class="col s12 l9">
				<h4 class="page-heading"><?= __('Terms & Conditions'); ?></h4>
				<div class="card-panel p30" style="text-align: justify;">
					<p>
					    <em>
					        These General Provisions settle the mode of work, the rules for using
					        the website luxury-discounts.com and the services provided by Luxury
					        Discounts LTD. through its use (the website and the services
					        collectively referred to as the “services”).
					    </em>
					</p>
					<p>
					    <em>
					        You are advised to get acquainted with these General Provisions
					        carefully before using the provided services on the website
					    </em>
					    <a href="http://www.luxury-discounts.com/">
					        <em>www.luxury-discounts.com</em>
					    </a>
					    <em>, property of and supported by Luxury Discounts LTD..</em>
					</p>
					<p>
					    <em>
					        To make use of the offered services, it is necessary to agree to and
					        comply with these General Provisions. The latter apply to all visitors
					        and users who use the website
					    </em>
					    <a href="http://www.luxury-discounts.com/">
					        <em>www.luxury-discounts.com</em>
					    </a>
					    <em>
					        (hereinafter referred to as the „website“) and the services offered via
					        it.
					    </em>
					</p>
					<p>
					    <em>
					        By booking or purchasing of hotel accommodation, an organized travel
					        (also called “tour”) or the creation of an account in the website, you
					        agree to these General Provisions and oblige to follow them. If you
					        don’t agree to the so described provisions, you should stop using the
					        website and the related services.
					    </em>
					</p>
					<p>
					    <em></em>
					</p>
					<p>
					    For the purposes of these General Provisions and pursuant to the Bulgarian
					    legislation:
					</p>
					<p>
					    А) „Travel agency operations“ is acting as an intermediary in: sales of
					    organized package travels for end customers, passenger aviation, waterborne
					    and us transport, booking, visa and other additional tourist services,
					    including insurances related to the travel;
					</p>
					<p>
					    B) „Travel agent“ is a person registered according to the procedure of the
					    Tourism Act or entered in the register of tour operators and travel agents
					    for performance of travel agency operations;
					</p>
					<p>
					    C) „Travel voucher“ is an accounting document, certifying the availability
					    of a contract between the tour operator and the tourist as a client of
					    organized package travel or the sale of a main and/or additional tourist
					    service(s);
					</p>
					<p>
					    D) „Reservation form“ is an accounting document, certifying the
					    availability of a contract between the travel agent and the tourist as a
					    client of a main or additional tourist service, when not a part of an
					    organized package travel;
					</p>
					<p>
					    E) „Tourist product“ is the combination of specific economic activities and
					    natural and anthropogenic conditions and resources within a certain
					    territory offered and used by the tourist during his/her travel.
					</p>
					<p>
					    F) „User“ is every person having access to the website „
					    <a href="http://www.luxury-discounts.com/">
					        <em>www.luxury-discounts.com</em>
					    </a>
					    <em>
					        “ and able to use its content and functions, as well as to use main and
					        additional tourist services booked and purchased via the website
					    </em>
					    „
					    <a href="http://www.luxury-discounts.com/">
					        <em>www.luxury-discounts.com</em>
					    </a>
					    <em>“.</em>
					</p>
					<p>
					    G) „Website“ is the aggregate of web pages, with a common homepage that is
					    loaded in your browser after writing the electronic address „
					    <a href="http://www.luxury-discounts.com/">
					        <em>www.luxury-discounts.com</em>
					    </a>
					    <em>“.</em>
					</p>
					<p>
					    <strong>I. </strong>
					    <strong>Luxury discounts</strong>
					    <em></em>
					</p>
					<p>
					    <em>Luxury Discounts LTD.</em>
					    , with Unified Identifier No. 203538748, based and registered at: Sofia,
					    Triaditsa District, Gotse Delchev Residential Area, Slavovitsa Str.,
					    building 53E, entrance C, floor 2, is a registered travel agent under the
					    Tourism Act, holder of a certificate for carrying out travel agency
					    operations No. PK-01-7550; date of issue - 25.03.2016.
					</p>
					<p>
					    1.1. The service rendered by <em>Luxury Discounts LTD. </em>via its
					    e-platform provides you with access to exclusive offers for hotel
					    accommodation and tours available only for our members/users/.
					</p>
					<p>
					    1.2. <em>Luxury Discounts LTD.</em> allows account holders (also referred
					    to as “users”) to make reservations for hotel accommodation and organized
					    package travel (also referred to as “Travel offers”) offered on our website
					    which are provided by third parties – providers of tourist services and
					    products. All travel offers offered via the website are for hotel
					    accommodation, unless it is clearly specified in the description of the
					    respective offer that it concerns an organized package travel (“tour”). All
					    travel offers displayed on our website depend on current availability,
					    therefore we do not guarantee that all of them will be available for the
					    whole period of time they are offered.
					</p>
					<p>
					    1.3. <em>Luxury Discounts LTD.</em> acts as a travel agents of the
					providers who supply the travel offers published on our website.    <em>Luxury Discounts LTD.</em> acts on behalf of and at expense of the
					    providers of tourist products and services (Providers) by giving them the
					    opportunity to make use of our e-platform via which a contract may be
					entered between you and the providers of tourist services and products.    <em>Luxury Discounts LTD.</em> neither performs any tour operator activity,
					    nor additional tourist services.
					</p>
					<p>
					    1.3.1. In case of a booking made via the website Luxury Discounts LTD. in
					    its capacity of an agent and intermediary of the Provider is not a party to
					    the concluded contract for the provision of the tourist product or service.
					    These General Provisions shall settle the use of the e-service based on the
					    website and provided by Luxury Discounts LTD. but all travel offers you buy
					    are settled according to the rules and conditions of the provider offering
					    it. The latter are available to you in the steps of the booking process.
					</p>
					<p>
					    1.3.2. By the purchase of a travel offer from a provider, you agree to
					    these General Provisions, as well as to the General Provisions of the
					    provider applicable to the respective offer. By purchasing a travel offer
					    via the website you agree that you are entering into a contract directly
					    with the provider of the tourist product or service, as well as that Luxury
					    Discounts LTD. is not a party to this legal relationship.
					</p>
					<p>
					    1.4. Since it acts as an agent, Luxury Discounts LTD. shall not be held
					    liable whatsoever for the provision of the service itself by the provider,
					    and it shall bear no responsibility for any losses you suffer as a result
					    of availing of the travel offer. If after purchasing a travel offer, you
					    have any questions or worries in this respect, you need to contact directly
					    the Provider of the service you have purchased.
					</p>
					<p>
					    <strong>PRICES OF THE OFFERED TRAVEL OFFERS </strong>
					</p>
					<p>
					    2.1 The price of every travel offer shall be the same as the ones specified
					    in the website, unless in case of an evident error. The price is inclusive
					    of VAT. Payments shall be accepted by a debit and credit card or by a wire
					    transfer.
					</p>
					<p>
					    2.2 Due to the the nature of the market and its dynamics it is possible
					    that some of this offers are indicated with an incorrect price. If the
					    correct price of the travel offer is lower than the announced price, we
					    will refund the amount you have overpaid. If the correct price is higher
					    than the announced price indicated in the website, we will normally and at
					    our own discretion either contact you for additional instructions and
					    confirmation before processing your reservation or cancel your reservation
					    on behalf of the provider of the service.
					</p>
					<p>
					    2.3 In the cases where the price of the travel offer indicated in the
					    website is obviously wrong and could be clearly and predictably recognized
					    as incorrect, the Provider shall not be obliged to agree to provide the
					    service you have purchased at the incorrect (lower) price, even after we
					    have sent a confirmation for the reservation.
					</p>
					<p>
					    2.4 The price of travel offers may change in time depending on the workload
					    of the Provider of the travel offer and the tourist season, however such
					    changes shall not affect bookings already paid and confirmed by the
					    Provider.
					</p>
					<p>
					    <strong>METHOD OF PAYMENT</strong>
					</p>
					<p>
					    4.1. According to these General Provisions and the rules of procedure of
					    Luxury Discounts LTD., in order to book a travel offer via the website you
					    shall make the payment directly to Luxury Discounts LTD.. Your payment
					    shall be accepted by Luxury Discounts LTD. acting as an agent of the
					    Provider of the service you have purchased. The payment is made by the
					    methods specified in the website. It is necessary to pay the price of the
					    travel offer in order to book it for the dates you have selected.
					</p>
					<p>
					    4.2. By making a reservation via the website, you are confirming that the
					    data you have entered for making the payment are true and correct. If any
					    of the methods of payment is rejected, we may contact you with a request
					    for an alternative method of payment or we may cancel your reservation.
					</p>
					<p>
					    4.3. If you believe that we have incorrectly charged you through your
					    credit and debit card, you should immediately contact us. In the event that
					    we find a payment has been taken in error, we will refund the amount
					    immediately to your credit or debit card.4.4. If you pay with a credit
					    card, a processing fee applies. This processing fee will not exceed our
					    expenses for processing the payment. The amount of the processing fee will
					    be specified before your payment for a travel offer via the website.
					</p>
					<p>
					    4.5. Luxury Discounts LTD. solely acts as an agent between the user and the
					    Provider of the tourist service, and therefore it cannot issue an invoice
					    for the travel offer you have paid. Luxury Discounts LTD. shall issue a
					    confirmation of the reservation which may be used as a receipt since it
					    indicates an amount paid at the time of booking.
					    <s>
					    </s>
					</p>
					<p>
					    <strong>USER ACCOUNT</strong>
					</p>
					<p>
					    <strong></strong>
					</p>
					<p>
					    5.1. The services offered on this website are accessible to you only after
					    you register by creating a user account.
					</p>
					<p>
					    5.1.1. The registration of a user account is not allowed for persons under
					    18 (eighteen) years of age.
					</p>
					<p>
					    5.2. To create an account you need to provide your email address and create
					    a personal password. A user account may be registered on the website also
					    by using your personal account in other social network websites
					    (facebook.com., google+, twitter etc.).
					</p>
					<p>
					    5.2.1. If you register an account on the website by referring to your
					    account in a social network, you agree to provide us access to the
					    information you have given for use to the social network, as well as that
					    we shall keep your log token according to our personal data policy .
					</p>
					<p>
					    5.3. In the cases when you use your email address for the creation of an
					    account, we advise you to use safe password containing at least a
					    combination of upper and lower case, numbers and symbols. You are
					    responsible for maintaining the confidentiality of your password. If you
					    have any doubts of external interference or use of your account,
					    immediately contact us for assistance.
					</p>
					<p>
					    5.4. In the event that you breach these General Provisions, the website
					    administrator is in the position to use any of the following penalties: а)
					    to block your access to the website; b) to stop your use of the website; c)
					    to delete your user account; d) solely or jointly with a provider of
					    tourist service, to refuse the provision of the tourist service.
					</p>
					<p>
					    <strong>RESERVATION OF TRAVEL OFFER</strong>
					</p>
					<p>
					    6.1. The procedure and steps for reservation and payment of a travel offer
					    are specified in the website. By booking a travel offer via the website,
					    you agree to pay for it.
					</p>
					<p>
					    6.2. While making your reservation, you will be able to check for errors or
					    incorrectly specified information, as well as to make changes before
					    confirming your reservation.
					</p>
					<p>
					    6.3. We advise you before making a reservation to ensure that you (and your
					    companions) meet the conditions and requirements of each travel offer and
					    you are eligible to travel on the dates you have selected for the
					    respective travel offer.
					</p>
					<p>
					    6.4. If you and your companions have any special requirements, you must
					    point them out during the registration process. Luxury Discounts LTD.
					    undertakes to inform the Provider of any of your reasonable requests,
					    however it cannot guarantee that such requests will be fulfilled by the
					    Providers of tourist services.
					</p>
					<p>
					    6.5. Before making your reservation, you need to inform us if you or your
					    companion have any special medical or health requirements that could affect
					    your reservation so that we can decide whether the travel offer is suitable
					    for you.
					</p>
					<p>
					    6.6. Every reservation you make needs to be approved by the Provider of
					    tourist services. In our capacity of travel agent, we will send you an
					    email to confirm your reservation.
					</p>
					<p>
					    6.7.1. The contract between you and the Provider of tourist services shall
					    be considered concluded only after we send you a confirmation of your
					    reservation in our capacity of a travel agent of the tourist provider. You
					    will be charged according to the method of payment you have chosen, after
					    approval of your reservation.
					</p>
					<p>
					    6.7.2.After your reservation is processed, you will receive a new email
					    from the provider of the travel offer containing the details of the
					    purchased travel offer, including a description of the remaining necessary
					    documentation which may be required upon booking a tour (organized travel)
					    or other tourist services rendered by the provider.
					</p>
					<p>
					    6.8. The contact details that you provide need to be up-to-date and valid.
					    We or the provider of the travel offer may contact you, if necessary, in
					    order to successfully provision the purchased travel offer. Luxury
					    Discounts LTD. shall not be held responsible in case you are not able to
					    use the travel service you have purchased due to non-compliance with your
					    obligations under this paragraph.
					</p>
					<p>
					    6.9. After your reservation is confirmed by the provider of the travel
					    offer and you have received the confirmation through us, in our capacity of
					    an agent of the provider of the travel offer, and you have successfully
					    paid the travel offer, we will send you an email depending on the type of
					    your reservation.
					</p>
					<p>
					    6.9.1. When purchasing a travel offer which includes only hotel
					    accommodation, you will receive a confirmation by email representing your
					    reservation form and specifying the details of your reservation, as well as
					    contact details and information about the provider of the travel offer.
					    This email confirms the purchase of the offer you have selected and contain
					    the information you need with respect to the travel offer.
					</p>
					<p>
					    6.9.2. Upon reservation of an organized travel you will receive a
					    confirmation by email containing detailed information about the travel
					    offer and the provider that offers it. This email is not a final
					    confirmation of your reservation and does not contain the full information
					    necessary for the travel. The provider (tour operator or the respective
					    foreign company registered and meeting the requirements for performance of
					    such activity) of the travel offer will send you an additional email
					    containing your travel voucher, all additional details, as well as the
					    other steps or information needed to complete your reservation.
					</p>
					<p>
					    6.9.3. Pursuant to paragraph 6.9.2., the email sent by Luxury Discounts
					    LTD. shall only confirm the payment of the travel offer for the dates of
					    your reservation, as well as confirmation that the provider of the travel
					    offer will contact you. This does not mean that the provider of the tour
					    has confirmed it.
					</p>
					<p>
					    6.9.4. If a provider of a travel offer does not confirm your reservation,
					    e.g. due to lack of availability for the dates you have chosen or due to
					    incorrectly indicated price on the website, we will inform you that your
					    reservation has been cancelled. Luxury Discounts LTD. will contact the
					    provider of the travel offer and arrange the refund of the amounts you have
					    paid.
					</p>
					<p>
					    6.10. Upon receipt of information about the travel offer you have
					    purchased, you need to check once again whether the personal data you have
					    specified are correct. For example, whether your names, age, etc.
					    correspond to the ones in your identity document. If you find any
					    discrepancy, please contact the provider of the travel offer at your
					    earliest convenience.
					</p>
					<p>
					    6.11. Luxury Discounts LTD. shall not bear responsibility if the tickets
					    and/or travel documents, information without which you cannot use the
					    purchased travel offer, etc. are not delivered to you by email or postal
					    services. Please, directly contact the provider indicated in the email
					    message if you don’t receive the respective documents.
					</p>
					<p>
					    6.12.1. The terms and conditions under already paid travel offers cannot be
					    changed by the user, and the amounts he/she has paid are not subject to
					    refund by Luxury Discounts LTD. or the provider of the purchased travel
					    offer, unless it is specified in the terms and conditions of the travel
					    offer published on the website. This paragraph shall not apply in the cases
					    of paragraph 6.9.4 of these General Provisions.
					</p>
					<p>
					    6.12.2. In the cases of paragraph 6.12.1., Luxury Discounts LTD. at its own
					    discretion may assist you before the provider of the travel offer in order
					    to make the changes you have requested, however it cannot guarantee that
					    the Provider will give its consent. Luxury Discounts LTD. shall not be
					    responsible if you are additionally charged in case the provider agrees to
					    the requested changes.
					</p>
					<p>
					    6.13. In case of minor changes made by the provider to the terms and
					    conditions of the travel offer you have purchased, you will be informed
					    thereof by us or the provider of the travel offer. This does not stop the
					    execution of the travel offer.
					</p>
					<p>
					    6.14.1. In cases where the provider of the travel offer stops its execution
					    before the beginning of your reservation or makes major changes to the
					    content of the initially envisaged terms and conditions, and such
					    information is available to us we will inform you as soon as possible.
					</p>
					<p>
					    6.14.2. In such cases as those under paragraph 6.14.1. we shall make
					    reasonable efforts to negotiate with the provider of the travel offer you
					    have purchased upon any of the following alternative variants but we cannot
					    guarantee that these will be always possible due to the need of consent by
					    the Provider :
					</p>
					<p>
					    6.14.2.1. To accept all changes made;
					</p>
					<p>
					    6.14.2.2. For reservations of hotel only accommodation, to cancel your
					    reservation and be refunded for all the amounts already paid, or to accept
					    another similar offer and get a refund of the difference or making the
					    respective extra payments on your side;
					</p>
					<p>
					    6.14.2.3. For reservations of organized travel, to cancel the purchased
					    travel offer with a full refund of all the monies you have paid or to
					    accept an alternative offer by the provider of the travel offer. If the
					    price of the alternative offer is lower, you will be refunded the
					    difference, respectively if the price is higher, you need to make an extra
					    payment;
					</p>
					<p>
					    6.15. In the cases under paragraph 6.14.1. and 6.14.2. Luxury Discounts
					    LTD. shall not bear any responsibility for any damages and losses incurred
					    by you.
					</p>
					<p>
					    6.16. Luxury Discounts LTD. offers the service “on hold” accessible for
					    some of the travel offers available on the website. This service allows the
					    user to pay a fee of in order to hold a certain date for the respective
					    travel offer. Holding a certain date is not a reservation, it just prevents
					    other users of the website to book the same date and exhaust the
					    availability of the offer selected by the user. The service „on hold“ is
					    not a deposit for the price of the travel offer and does not mean a
					    confirmation of the travel offer.
					</p>
					<p>
					    6.17.1 In the event that the client has used the hold function under
					    Article 6.16, but doesn’t pay his/her reservation within the term set for
					    the offer, the paid fee is not subject to return.
					</p>
					<p>
					    6.17.2. In the event that the client has used the hold function under
					    Article6.16, but the provider of the selected travel offer stops offering
					    it on the website, Luxury Discounts LTD. will refund the amount paid under
					    the above article in full, using the method of payment chosen by the
					    client.
					</p>
					<p>
					    6.18. If the terms and conditions of the particular travel offer indicated
					    on the website are contradicting these General Provisions, the information
					    specified in the offer on the website shall prevail. The terms and
					    conditions indicated on the website for a particular offer are effective
					    only for its term of validity.
					</p>
					<p>
					    <strong>REFUND OF AMOUNTS AND CANCELLATION OF RESERVATIONS </strong>
					</p>
					<p align="center">
					    <strong> </strong>
					</p>
					<p>
					    <strong>7</strong>
					    .1. If a user has paid a reservation for hotel accommodation and/or extra
					    tourist services made via the website, and the provider of the travel offer
					    refuses to execute the contract, then the latter should refund to the user
					    the paid amounts in full.
					</p>
					<p>
					    7.2. If the tour operator providing the respective organized travel
					    significantly changes some of the main clauses of the contract or refuses
					    to execute the contract, the user may cancel the contract without owing any
					    penalty or compensation. In these cases the relations between the user and
					    the tour operator are settled between them and Luxury Discounts LTD. may
					    only provide assistance for such settlement.
					</p>
					<p>
					    7.3.1<strong>. </strong>In case of cancellation by the user of booking for
					    hotel accommodation and other tourist services made via the website, the
					    paid amounts are not subject to return by Luxury Discounts LTD. or the
					    Provider of the purchased travel offer, unless the right for cancellation
					    is not explicitly stated in the terms and conditions of the particular
					    offer.
					</p>
					<p>
					    7.3.2. If the user is granted a possibility to cancel a reservation,
					    pursuant to Article 7.3.1 the provider of the purchased travel offer may
					    charge penalties according to the terms and conditions of the offer.
					</p>
					<p>
					    7.4.1<strong>. </strong>In case of a cancellation by the user of booking
					    for organized travel made via the website, the paid amounts are not subject
					    to return and in case they are, the refund is made according to the rules
					    and conditions of the tour operator providing the respective organized
					    travel.
					</p>
					<p>
					    7.4.2. In such cases as those under the foregoing paragraph, the user
					    should contact and negotiate directly with the tour operator providing the
					    respective organized travel.
					</p>
					<p>
					    <strong>FORCE MAJEURE</strong>
					</p>
					<p>
					    8. Luxury Discounts LTD. shall not be held liable for any losses incurred
					    by the clients and users of tourist services purchased via the website in
					    case of force majeure with duration of more than 20 (twenty) days. Force
					    Majeure is understood as such events and actions of an extraordinary
					    nature, such as: war, blockade, natural disasters, embargo, strikes,
					    governmental and administrative restrictions and other similar events and
					    actions which have occurred after making the reservation and are not
					    foreseeable and are unpreventable by Luxury Discounts LTD., the provider of
					the travel offer or the user of the services.    <strong>PROVIDED INFORMATION</strong>
					</p>
					<p>
					    9.1. Luxury Discounts LTD. may provide general information about passport,
					    visa or other requirements for obtaining the right of access to the
					    respective country in relation to your travel. It is your responsibility to
					    check upon the particular passport, visa or other requirements for
					    obtaining the right of access to the respective country, as well as the
					    respective checks with embassies and/or consular offices. Any information
					    provided by Luxury Discounts LTD. about the aforementioned or related
					    issues (such as clothing, luggage, personal belongings, etc.) should be
					    considered as exemplary and recommended, however Luxury Discounts LTD.
					    shall not bear any responsibility for any adverse consequences you have
					    suffered or decisions taken based on the provided information.
					</p>
					<p>
					    9.2. It is your responsibility to make sure that you and your companions
					    meet all requirements for admission in the respective country, as well as
					    that you possess all necessary documents and permits before your travel.
					    Neither we, nor the providers of travel offers shall be responsible if you
					    are not able to use the travel service you have purchased due to any of the
					    abovementioned reasons. Please make sure that you possess all necessary
					    documents, permits, passports, identity cards, visas or insurance
					    documents, as well as that you have planned enough time for the check at
					    the airport before your flight.
					</p>
					<p>
					    <strong></strong>
					</p>
					<p>
					    <strong>MISCELLANEOUS</strong>
					</p>
					<p>
					    10. The invalidity of any clause of these General Provisions or of
					    additionally contracted conditions shall not result in invalidity of
					    another clause or condition, or the General Provisions as a whole.
					</p>
					<p>
					    11. To any issues not explicitly settled herein, the effective legislation
					    of the Republic of Bulgaria shall apply.
					</p>
					<p>
					    12. Upon any disputes arising in relation to the interpretation and
					    application of these General Provisions or any claims on behalf of the
					    user, these shall be settled with the mutual consent of the parties. Should
					    it be impossible to arrive at an agreement, all disputes arising of the
					    General Provisions or related thereto, including any disputes arising of or
					    related to their interpretation, invalidity, performance of termination, as
					    well as the disputes for filling in gaps in the General Provisions or
					    adapting them to new circumstances, shall be resolved according to the
					    procedure provided in the Bulgarian legislation.
					</p>
					<p>
					    13. The content of the website
					    <a href="http://www.luxury-discounts.com/">
					        <em>www.luxury-discounts.com</em>
					    </a>
					    , the web design, each menu, category or subcategory, section or
					    subsection, the method of arrangement and structuring of the information,
					    databases and software systems shall be exclusive property of Luxury
					    Discounts LTD. and are protected under the Law on Copyright and Related
					    Rights. Each share, publication, copying and sale of information and
					    photographic material from the website
					    <a href="http://www.luxury-discounts.com/">
					        <em>www.luxury-discounts.com</em>
					    </a>
					    <em> </em>
					    under whatsoever form, without the express consent of Luxury Discounts LTD.
					    shall be prohibited and constitute a breach of law.
					</p>
					<p>
					    14. No user may save, copy or reproduce the source code, parts, elements or
					    materials from the website
					    <a href="http://www.luxury-discounts.com/">
					        <em>www.luxury-discounts.com</em>
					    </a>
					    <em> </em>
					    without prior express written consent of Luxury Discounts LTD..
					</p>
					<p>
					    15. Every user shall agree to avoid sending or transferring in whatsoever
					    way the website
					    <a href="http://www.luxury-discounts.com/">
					        <em>www.luxury-discounts.com</em>
					    </a>
					    , its data or content to another computer, server, webpage or another
					    medium for mass use, as well as using them for commercial purposes.
					</p>
					<p>
					    16. The user shall be obliged not to use a device or software by which to
					    interfere into the proper functioning of this website. The use of the
					    website in a manner not specified herein may result in a violation of the
					    Law on copyright, the trademark, good manners and commercial practices or
					    other laws related to intellectual property. The change, hiding or deletion
					    of copyright designations shall not be allowed.
					</p>
					<p>
					    17. The user shall be responsible for all damages occurring as a result of
					    an infringement of a copyright, trademark or other property rights, as well
					    as any other damages occurring as a result of the use of the website
					    <a href="http://www.luxury-discounts.com/">
					        <em>www.luxury-discounts.com</em>
					    </a>
					    .
					</p>
					<p>
					    <strong></strong>
					</p>
					<p>
					    <strong>PERSONAL DATA POLICY</strong>
					</p>
					<p>
					    18. Luxury Discounts LTD. is the owner and operator of the website
					    <a href="http://www.luxury-discounts.com/">
					        <em>www.luxury-discounts.com</em>
					    </a>
					    . Luxury Discounts LTD. is a registered personal data administrator under
					    the Personal Data Protection Act.
					</p>
					<p>
					    19. Luxury Discounts LTD. shall make the necessary efforts for the
					    protection of the personal data you provide in relation to the use of the
					    offered service. This chapter indicates the purposes for which Luxury
					    Discounts LTD. will use your personal data and to whom they may be provided
					    after being collected via our webpage or by another specified method.
					    Luxury Discounts LTD. will use your data only for the purposes described in
					    the General Provisions. The indicated purposes for which the information
					    provided by you is necessary are not exhaustively listed. Luxury Discounts
					    LTD. processes and keeps the information you provide in a secure software
					    environment.
					</p>
					<p>
					    20. Luxury Discounts LTD. undertakes not to disclose in any way personal
					    information provided by the user and not to provide the collected
					    information to third parties – natural persons and legal entities, state
					    authorities and other, except in the cases where: а) it has obtained the
					    express consent of the user upon registration or later in time or b) the
					    information is required by state authorities or officials who according to
					    the effective legislation are authorized to demand and collect such
					    information in accordance with the legally established procedures or c)
					    other cases specified in the law or these General Provisions.
					</p>
					<p>
					    21. By accepting these General Provisions the user agrees to provide the
					    required personal data for the purposes of the service rendered via the
					    website and they will be processed according to the specified procedure.
					</p>
					<p>
					    22. The actions performed by the user in the website (visited offers, liked
					    offers, asked questions, comments) and the information about the offers
					    purchased by him/her (destinations, prices, dates, terms and conditions of
					    the offer, total savings) may always be used by Luxury Discounts LTD. for
					    the purposes of direct marketing.
					</p>
					<p>
					    23. We inform you that for the successful completion of your reservation
					    and the purchasing of the tourist product or service you have selected, you
					    need to provide your names, gender, email address and phone, as well as
					    other information if necessary for your reservation (e.g. number and date
					    of issue of your passport or identity card, Personal ID No., as well as
					    those for the people in your group). This information is necessary to
					    enable us to process your reservation and may be given to the persons who
					    provide the product or service you have purchased (as for example, tour
					    operators, hotels, air carriers, public authorities such as the customs and
					    immigration services, if necessary, etc.). Depending on the destination you
					    have selected, the abovementioned personal information may be provided, if
					    necessary, outside the European Economic Area (EEA) for which you have
					    given your consent (and the consent of those included in your group) by
					    accepting these General Provisions.
					</p>
					<p>
					    24. By making a reservation via our site, you will be redirected to the
					    payment page where you can enter your payment details. We do not keep
					    information about your payment, such as credit card numbers. The payment
					    platform which we use processes the information necessary for making the
					    respective payment for a reservation. After we receive the payment to our
					    account, we make the respective payments to the provider of your tourist
					    product or service purchased via our website.
					</p>
					<p>
					    25. By accepting the General Provisions of Luxury Discounts LTD., the user
					    shall allow personal, advertising or other commercial messages from the
					    merchants with whom Luxury Discounts LTD. cooperates in relation to the
					    provided service to be sent to his/her user accounts in the website,
					    Facebook, Google+, or anywhere else in the virtual space, or to the email
					    address provided by him/her . Luxury Discounts LTD. may also provide to the
					    merchants names and email address of the user who has purchased tourist
					    services and products or other services.
					</p>
					<p>
					    26. For acquisition of access to the website via a mobile device or in case
					    of use of the application of Luxury Discounts LTD. for mobile devices, the
					    user shall agree and allow Luxury Discounts LTD. to collect data of his/her
					    current location which may be used for the purpose of offering similar
					    deals, depending on his/her preferences expressed via the e-platform.
					</p>
					<p>
					    27. Other information that we may receive is information about the device
					    you use to connect to our website. This information may contain data about
					    the type of device, your IP address or phone number of your device, unique
					    identification numbers (UIN) of the device, operating systems, browsers and
					    applications connected to our website and used by your device, your
					    Internet service provider or mobile network.
					</p>
				</div>
			</div>
		</div>
	</div>
</section>
