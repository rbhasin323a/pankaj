<?php
$this->assign('title', __('Maldopay Error Failed'));
$this->HtmlBetter->addCrumb(__('Home'), '/');
$this->HtmlBetter->addCrumb(__('Maldopay transaction failed'), '/landing/failed');
?>
<style>
.time-left:hover {
	cursor: default;
}
.alert {
  padding: 20px;
  background-color: #ff704d; /* red */
  color: white;
  margin-bottom: 30px;
	margin-top:30px;
	text-align:center;

}
.note {
	text-align:center;
}
#back-sales-view {
  margin-bottom: 25px;
	margin-bottom: 30px;
	margin-top:30px;

}

</style>
<section id="MladopayError">
  <div class="container">
		<div class="row">
			<div class="" >
				<div class="transaction-declined card-panel p30">

					<i class="fa fa-ban" style="color:#ff704d;font-size:100px;margin-left:42%" aria-hidden="true"></i>
			    <h4 class="alert">We were unable to process your payment.<br>The transaction has failed. </h4>
			    <a href="/sales/book/" id="back-sales-view" class="btn waves-effect waves-light btn-large amber darken-3" style="left:40%"> Continue</a>
					<div class="note"> If you have questions or think there was a mistake, <a href="https://www.luxury-discounts.com/contact">contact support</a>. </div>
				</div>
			</div>
  </div>
</div>
</section>
