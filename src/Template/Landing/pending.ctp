<?php
$this->assign('title', __('Maldopay Error Pending'));
$this->HtmlBetter->addCrumb(__('Home'), '/');
$this->HtmlBetter->addCrumb(__('Maldopay transaction pending'), '/landing/pending');
?>
<style>
.time-left:hover {
	cursor: default;
}
.alert {
  padding: 20px;
  background-color: #ffbf80; /* yellow */
  color: white;
  margin-bottom: 30px;
	margin-top:30px;
	text-align:center;

}
.note {
	text-align:center;
}
#back-sales-view {
  margin-bottom: 25px;
	margin-bottom: 30px;
	margin-top:30px;

}

</style>
<section id="MladopayError">
  <div class="container">
		<div class="row">
			<div class="transaction-declined card-panel p30">
				<i class="fa fa-clock-o"  style="color:#ffbf80;font-size:100px;margin-left:42%" aria-hidden="true"></i>
		    <h4 class="alert">The transaction is pending. </h4>
		    <a href="/sales/book/" id="back-sales-view" class="btn waves-effect waves-light btn-large amber darken-3" style="left:40%"> Continue</a>
				<div class="note"> If you have questions or think there was a mistake, <a href="https://www.luxury-discounts.com/contact">contact support</a>. </div>
			</div>
		</div>
</div>
</section>
