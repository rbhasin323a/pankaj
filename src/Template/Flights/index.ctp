<?php
$this->assign('title', __('Flights'));

$this->HtmlBetter->addCrumb(__('Home'), '/');
$this->HtmlBetter->addCrumb(__('Flights'), '/flights');
?>
<section id="Flights">
	<?php
	$current_lang = 'en';
	if (isset($_COOKIE['language'])) {
		$current_lang = $_COOKIE['language'] == 'gb' ? 'en' : $_COOKIE['language'];
	} else {
		if (isset($_COOKIE['autolanguage'])) {
			$current_lang = $_COOKIE['autolanguage'] == 'gb' ? 'en' : $_COOKIE['autolanguage'];
		}
	}
	?>
	<h4 class="page-heading"><?= __('Flights'); ?></h4>
	<div data-lang="<?= $current_lang; ?>" id="embed_charters">

</section>
<script>
	function setTitleBack() {
		$('title').html('<?= $seo_title ?>');
	}
</script>
<!-- Flights -->
<script src="https://luxury-discounts.fiesta-fly.com/wl/scr/embed.js" onload="setTimeout(setTitleBack, 3600)"></script>
