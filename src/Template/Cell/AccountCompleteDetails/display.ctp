<?php
/**
 * Account Complete Details Component Cell
 */
if (
	!empty($vd['user']['first_name']) &&
	!empty($vd['user']['last_name']) &&
	!empty($vd['user']['birthday']) &&
	!empty($vd['user']['phone_mobile'])
) {
	return false;
}

$this->loadHelper('Form', [
	'templates' => 'app_form',
]);

$c_id = substr(md5(uniqid(mt_rand(), true)), 0, 10);
$c_class = 'account-complete-details';
?>
<div id="AccountCompleteDetailsCell-<?= $c_id; ?>" class="<?= $c_class; ?>">
	<div id="AccountCompleteDetailsCell-<?= $c_id; ?>-modal" class="<?= $c_class; ?>__modal modal">
		<div class="modal-content">
			<?php if (!empty($account_logged_in)) { ?>
				<h5 class="fw300 mv10 pv10 text-center"><?= __d('AccountCompleteDetailsCell', 'SUCCESS!'); ?></h5>
				<p class="text-center"><?= __d('AccountCompleteDetailsCell', 'You have successfully logged in.'); ?></p>
			<?php } else if (!empty($account_registered)) { ?>
				<h5 class="fw300 mv10 pv10 text-center"><?= __d('AccountCompleteDetailsCell', 'CONGRATULATIONS!'); ?></h5>
				<p class="text-center mt25"><?= __d('AccountCompleteDetailsCell', 'You have successfully created your account.'); ?></p>
			<?php } else { ?>
				<h5 class="fw300 mv10 pv10 text-center"><?= __d('AccountCompleteDetailsCell', 'COMPLETE YOUR ACCOUNT!'); ?></h5>
			<?php } ?>

			<?php if (empty($vd['user']['active'])) { ?>
				<p class="text-center">
					<?= __d('AccountCompleteDetailsCell', 'Activation email was sent to your inbox.'); ?><br />
					<?= __d('AccountCompleteDetailsCell', 'If you haven\'t received your activation email <a href="/resend-validation" class="fw800">click here.</a>'); ?>
				</p>
			<?php } ?>

			<p class="text-center"><?= __d('AccountCompleteDetailsCell', 'Before proceeding, please complete your details:'); ?></p>

			<div class="<?= $c_class; ?>__form mt25 ib">
				<?= $this->Form->create(null, ['id' => 'AccountCompleteDetailsCell-' . $c_id . '-form', 'url' => '/api/account/complete-details']); ?>
				<div class="row">
					<div class="col s12 m6">
						<?= $this->Form->input('first_name', [
							'id' => 'AccountCompleteDetailsCell-' . $c_id . '-input-first-name',
							'required' => 'required',
							'label' => __d('AccountCompleteDetailsCell', 'First Name'),
							'value' => h(!empty($vd['user']['first_name']) ? $vd['user']['first_name'] : '')
						]); ?>
					</div>
					<div class="col s12 m6">
						<?= $this->Form->input('last_name', [
							'id' => 'AccountCompleteDetailsCell-' . $c_id . '-input-last-name',
							'required' => 'required',
							'label' => __d('AccountCompleteDetailsCell', 'Last Name'),
							'value' => h(!empty($vd['user']['last_name']) ? $vd['user']['last_name'] : '')
						]); ?>
					</div>
					<div class="col s12 m4">
						<?= $this->Form->input('birthday', [
							'id' => 'AccountCompleteDetailsCell-' . $c_id . '-input-birthday',
							'type' => 'text',
							'required' => 'required',
							'label' => __d('AccountCompleteDetailsCell', 'Date of Birth'),
							'value' => h(!empty($vd['user']['birthday']) ? $vd['user']['birthday'] : '')
						]); ?>
					</div>
					<div class="col s12 m8">
						<?= $this->Form->input('phone_mobile', [
							'id' => 'AccountCompleteDetailsCell-' . $c_id . '-input-phone-mobile',
							'required' => 'required',
							'label' => __d('AccountCompleteDetailsCell', 'Mobile Phone'),
							'value' => h(!empty($vd['user']['phone_mobile']) ? $vd['user']['phone_mobile'] : '')
						]); ?>
					</div>
					<?php if (empty($vd['user']['tos_date'])) { ?>
						<div class="col s12 pv5">
							<div class="ib">
								<input type="hidden" name="tos" value="0" />
								<input type="checkbox" name="tos" value="1" required="required" id="AccountCompleteDetailsCell-<?= $c_id; ?>-input-tos" <?= !empty($vd['user']['tos_date']) ? 'checked="checked"' : ''; ?>>
								<label for="AccountCompleteDetailsCell-<?= $c_id; ?>-input-tos"><?= sprintf(__d('AccountCompleteDetailsCell', 'I agree to the <a href="%s" rel="noopener noreferer" target="_blank">Terms & Conditions</a>'), '/pages/terms'); ?></label>
							</div>
						</div>
					<?php } ?>
					<?php if (empty($vd['user']['newsletter'])) { ?>
						<div class="col s12 pv5">
							<div class="ib">
								<input type="hidden" name="newsletter" value="0" />
								<input type="checkbox" name="newsletter" value="1" id="AccountCompleteDetailsCell-<?= $c_id; ?>-input-newsletter" <?= !empty($vd['user']['newsletter']) ? 'checked="checked"' : ''; ?>>
								<label for="AccountCompleteDetailsCell-<?= $c_id; ?>-input-newsletter"><?= __d('AccountCompleteDetailsCell', 'Subscribe me to the newsletter'); ?></label>
							</div>
						</div>
					<?php } ?>
					<div class="col s12 mt25 text-right">
						<div class="<?= $c_class; ?>__form__action">
							<?= $this->Form->button(__d('AccountCompleteDetailsCell', 'SUBMIT') . '<i class="material-icons right">lock</i>', ['class' => 'btn-large waves-effect waves-light blue darken-3']); ?>
						</div>
					</div>
				</div>
				<?php $this->Form->end(); ?>
			</div>
		</div>
	</div>
	<script type="text/javascript">
		(function($) {
			$(document).ready(function() {
				$('#AccountCompleteDetailsCell-<?= $c_id; ?>-modal').modal({
					dismissible: false,
					opacity: 0.9,
					inDuration: 0,
					outDuration: 0,
					ready: function(modal, trigger) {
						$('html').css({
							'overflow-y': 'hidden'
						});
					},
					complete: function() {
						$('html').css({
							'overflow-y': 'auto'
						});
					}
				});

				$('#AccountCompleteDetailsCell-<?= $c_id; ?>-modal').modal('open');

				$('#AccountCompleteDetailsCell-<?= $c_id; ?>-input-birthday').pickadate({
					selectMonths: true,
					selectYears: 200,
					format: 'yyyy-mm-dd',
					closeOnSelect: true,
					closeOnClear: true,
					container: '#AccountCompleteDetailsCell-<?= $c_id; ?>-modal',
					onOpen: function() {
						$('#AccountCompleteDetailsCell-<?= $c_id; ?>-modal').css({
							'overflow-y': 'hidden'
						}).scrollTop(0);
					},
					onClose: function() {
						setTimeout(function() {
							$('#AccountCompleteDetailsCell-<?= $c_id; ?>-modal').css({
								'overflow-y': 'auto'
							}).scrollTop($('#AccountCompleteDetailsCell-<?= $c_id; ?>-modal').height());
						}, 1000);
					}
				});

				$(document).on('submit', '#AccountCompleteDetailsCell-<?= $c_id; ?>-form', function(event) {
					event.preventDefault();

					var birthday = $('#AccountCompleteDetailsCell-<?= $c_id; ?>-input-birthday').prop('value');
					var error = false;

					if (birthday) {
						if (!birthday.match(/\d{4}-\d{2}-\d{2}/)) {
							error = true;
						}
					} else {
						error = true;
					}

					if (error) {
						var $div = '<div class="error-message fw600 red-text mb20 fs12"><?= __d('AccountCompleteDetailsCell', 'Date of birth is required'); ?></div>';
						var $container = $('#AccountCompleteDetailsCell-<?= $c_id; ?>-input-birthday').parent();

						$('.error-message', $container).remove();
						$container.append($div);
					}
				});

				AJAXForm('#AccountCompleteDetailsCell-<?= $c_id; ?>-form');
			});
		}(jQuery));
	</script>
</div>
