<?php

/**
 * Account Create Popup Component Cell
 */
$request_params = $this->request->getAttribute('params');

$excluded_params = [
	'pass' => ['terms', 'about-us', 'privacy-policy', 'gdpr-tools'],
	'controller' => ['Contact', 'Account', 'Users', 'GdprRequests', 'Language', 'Currency']
];

foreach ($excluded_params as $excluded_param_key => $excluded_param) {
	if (!empty($request_params[$excluded_param_key])) {
		$matched_param = $request_params[$excluded_param_key];

		if (is_array($excluded_param)) {
			foreach ($excluded_param as $excluded_param_value) {
				if (is_array($matched_param)) {
					foreach ($matched_param as $request_param_value) {
						if ($excluded_param_value === $request_param_value) {
							return false;
						}
					}
				}

				if (is_string($matched_param)) {
					if ($excluded_param_value === $matched_param) {
						return false;
					}
				}
			}
		}
	}
}

$this->loadHelper('Form', [
	'templates' => 'app_form',
]);

$c_id = substr(md5(uniqid(mt_rand(), true)), 0, 10);
$c_class = 'account-create-popup';
$c_uuid = 'AccountCreatePopupCell-' . $c_id;

?>
<div id="<?= $c_uuid; ?>" class="<?= $c_class; ?>">
	<div id="<?= $c_uuid; ?>-modal" class="<?= $c_class; ?>__modal modal">
		<div class="modal-content">
			<?= $this->Form->create(null, ['id' => $c_uuid . '-form', 'url' => '/api/account/register', 'class' => 'locked']); ?>
			<?= $this->Form->unlockField('tos'); ?>

			<div id="<?= $c_uuid; ?>-tabs" class="<?= $c_class; ?>__tabs">
				<div id="<?= $c_uuid; ?>-tabs-email">
					<h5 class="fw300 mv10 pv10 text-center"><?= __d('AccountCreatePopupCell', 'JOIN LUXURY DISCOUNTS'); ?></h5>

					<p class="text-center">
						<?= __d('AccountCreatePopupCell', 'Before proceeding, please enter your email address.'); ?><br />
						<?= __d('AccountCreatePopupCell', 'If you already have an account.'); ?> <a href="/login"><?= __d('AccountCreatePopupCell', 'Sign In'); ?></a>
						
					
					</p>
	
					<div class="<?= $c_class; ?>__form mt25">
						<div class="row">
							<div class="col s12">
								<?= $this->Form->input('email', [
									'id' => $c_uuid . '-input-email',
									'required' => 'required',
									'label' => __d('AccountCreatePopupCell', 'Email'),
									'value' => h(!empty($vd['user']['email']) ? $vd['user']['email'] : '')
								]); ?>
							</div>
							<?php if (empty($vd['user']['tos_date'])) { ?>
								<div class="col s12 pv5">
									<div class="ib">
										<input type="hidden" name="tos" value="0" />
										<input type="checkbox" name="tos" value="1" required="required" id="<?= $c_uuid; ?>-input-tos" <?= !empty($vd['user']['tos_date']) ? 'checked="checked"' : ''; ?>>
										<label for="<?= $c_uuid; ?>-input-tos"><?= sprintf(__d('AccountCreatePopupCell', 'I agree to the <a href="%s" rel="noopener noreferer" target="_blank">Terms & Conditions</a>'), '/pages/terms'); ?></label>
									</div>
								</div>
							<?php } ?>
							<div class="col s12 mt25 text-right">
								<div class="<?= $c_class; ?>__form__action">
									<?= $this->Form->button(__d('AccountCreatePopupCell', 'PROCEED') . '<i class="material-icons right">navigate_next</i>', ['class' => 'btn-large waves-effect waves-light blue darken-3']); ?>
								</div>
								<hr>
								<p style="text-align: center;">OR</p>
									<span class="text-center"><?php echo $this->Facebook->loginLink($options = []); ?></span>
							</div>
						</div>
					</div>
				</div>

				<div id="<?= $c_uuid; ?>-tabs-newsletter">
					<h5 class="fw300 mv10 pv10 text-center"><?= __d('AccountCreatePopupCell', 'EMAIL NOTIFICATIONS'); ?></h5>

					<p class="text-center">
						<?= __d('AccountCreatePopupCell', 'Please choose your preferred newsletter schedule.'); ?>
					</p>

					<div class="<?= $c_class; ?>__form mt25">
						<div class="row">
							<div class="col s6 pv5 <?= $c_class; ?>__form-choice">
								<div class="ib text-center">
									<input type="radio" name="newsletter" value="frequent" id="<?= $c_uuid; ?>-input-newsletter-frequent">
									<label for="<?= $c_uuid; ?>-input-newsletter-frequent">
										<img src="/assets/dist/img/component/account-create-popup-cell/sun.svg" />
										<?= sprintf(__d('AccountCreatePopupCell', 'Frequently - Every Month')); ?>
									</label>
								</div>
							</div>
							<div class="col s6 pv5 <?= $c_class; ?>__form-choice">
								<div class="ib text-center">
									<input type="radio" name="newsletter" value="occasional" id="<?= $c_uuid; ?>-input-newsletter-occasional">
									<label for="<?= $c_uuid; ?>-input-newsletter-occasional">
										<img src="/assets/dist/img/component/account-create-popup-cell/moon.svg" />
										<?= sprintf(__d('AccountCreatePopupCell', 'Occasionally - Every Quarter')); ?>
									</label>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col s12 mt15 text-center">
								<a id="<?= $c_uuid . '-button-newsletter'; ?>" href="#" class="btn-large waves-effect waves-light blue darken-3"><?= __d('AccountCreatePopupCell', 'SELECT') . '<i class="material-icons right">check</i>'; ?></a>
							</div>
						</div>
						<div class="row">
							<div class="col s12 mt15 text-center">
								<a href="javascript:void();" id="<?= $c_uuid; ?>-skip-newsletter"><?= __d('AccountCreatePopupCell', 'I don\'t want to receive any newsletter emails.'); ?></a>
							</div>
						</div>
					</div>
				</div>

				<div id="<?= $c_uuid; ?>-tabs-password">
					<h5 class="fw300 mv10 pv10 text-center"><?= __d('AccountCreatePopupCell', 'SET YOUR PASSWORD'); ?></h5>

					<p class="text-center">
						<?= __d('AccountCreatePopupCell', 'Enter a strong password for your account.'); ?>
					</p>

					<div class="<?= $c_class; ?>__form mt25">
						<div class="row">
							<div class="col s12">
								<?= $this->Form->input('password', [
									'id' => $c_uuid . '-input-password',
									'label' => __d('AccountCreatePopupCell', 'Password'),
								]); ?>
							</div>
						</div>
						<div class="row">
							<div class="col s12 mt15 text-center">
								<?= $this->Form->button(__d('AccountCreatePopupCell', 'SUBMIT') . '<i class="material-icons right">lock</i>', ['class' => 'btn-large waves-effect waves-light blue darken-3']); ?>
							</div>
						</div>
					</div>
				</div>

				<ul class="tabs tabs-fixed-width">
					<li class="tab"><a class="active" href="#<?= $c_uuid; ?>-tabs-email">email</a></li>
					<li class="tab"><a href="#<?= $c_uuid; ?>-tabs-newsletter">newsletter</a></li>
					<li class="tab"><a href="#<?= $c_uuid; ?>-tabs-password">password</a></li>
				</ul>
			</div>
			<?php $this->Form->end(); ?>
		</div>
	</div>
	<script type="text/javascript">
		(function($) {
			$(document).ready(function() {
				$('#<?= $c_uuid; ?>-modal').modal({
					dismissible: false,
					opacity: 0.9,
					inDuration: 0,
					outDuration: 0,
					ready: function(modal, trigger) {
						$('html').css({
							'overflow-y': 'hidden'
						});
					},
					complete: function() {
						$('html').css({
							'overflow-y': 'auto'
						});
					}
				});

				$('#<?= $c_uuid; ?>-modal').modal('open');

				$(document).on('submit', '#<?= $c_uuid; ?>-form', function(event) {
					event.preventDefault();

					var email = $('#<?= $c_uuid; ?>-input-email').prop('value');
					var password = $('#<?= $c_uuid; ?>-input-password').prop('value');

					if (typeof email !== 'undefined' && $('#<?= $c_uuid; ?>-tabs-email').hasClass('active')) {
						$('[href="#<?= $c_uuid; ?>-tabs-newsletter"]').click();
					}

					if (email && password) {
						$(this).removeClass('locked');
					}
				});

				$(document).on('click', '#<?= $c_uuid; ?>-button-newsletter', function(event) {
					$('[href="#<?= $c_uuid; ?>-tabs-password"]').click();
				});

				$(document).on('click', '#<?= $c_uuid; ?>-skip-newsletter', function(event) {
					$('#<?= $c_uuid; ?>-input-newsletter-frequent').prop('checked', false);
					$('#<?= $c_uuid; ?>-input-newsletter-occassional').prop('checked', false);
					$('[href="#<?= $c_uuid; ?>-tabs-password"]').click();
				});

				AJAXForm('#<?= $c_uuid; ?>-form');
			});
		}(jQuery));
	</script>
</div>
