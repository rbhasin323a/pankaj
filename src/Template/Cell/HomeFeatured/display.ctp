<?php
/**
 * Home Featured Component Cell
 */
if (empty($categories)) {
	return false;
}

$c_id = substr(md5(uniqid(mt_rand(), true)), 0, 10);
$c_class = 'home-featured';
?>
<div id="HomeFeaturedCell-<?= $c_id; ?>" class="<?= $c_class; ?> grey lighten-5 inline-full va-t">
	<div class="container">
		<div class="pt25 mt25 hide-on-med-and-down"></div>
		<div class="row">
			<div class="col s12 mv25 pv25">
				<h2 class="fs35 fw300 m-n mv5 blue-text text-darken-4"><?= __d('HomeFeaturedCell', 'EXCLUSIVE COLLECTIONS'); ?></h2>
				<h3 class="fs24 fw300 m-n"><?= __d('HomeFeaturedCell', 'CURRENTLY {0,plural,=0{NO FEATURED OFFERS} =1{{1} FEATURED OFFER} other{{1} FEATURED OFFERS}}', $total_category_sale_offers_count, '<span class="blue-text text-darken-4 fw400">' . $total_category_sale_offers_count . '</span>'); ?></h3>
			</div>
			<div class="col s12">
				<div class="row">
					<div class="col s12 l2 pull-l1 xl4 pull-xl1 hide-on-med-and-down">
						<div class="<?= $c_class; ?>__placeholder">
							<div class="<?= $c_class; ?>__placeholder-image"></div>
							<div class="<?= $c_class; ?>__placeholder-description">
								<p>
									<?= __d('HomeFeaturedCell', '"Tyulenovo" means "Village of seals" in Bulgarian. It is rumored that Queen Marie of Romania who had a summer palace in the nearby town of Balchik released a pair of seals she had raised for several years. {0}', '<a href="https://en.wikipedia.org/wiki/Tyulenovo" rel="noopener noreferer" target="_blank">' . __d('HomeFeaturedCell', 'Learn More') . '</a>'); ?>
								</p>
							</div>
						</div>
					</div>
					<?php if (!empty($categories)) { ?>
						<div class="col s12 l10 xl8 mv50">
							<div class="<?= $c_class; ?>__categories">
								<div class="row">
									<?php foreach ($categories as $category) { ?>
										<div class="col s12 m6 l6">
											<div class="<?= $c_class; ?>__categories-item" data-img="/files/images/<?= $category->image_path; ?>">
												<link rel="preload" href="/files/images/<?= $category->image_path; ?>" as="image" />
												<div class="card hoverable">
													<div class="card-content">
														<a href="/sales?filter_category=<?= $category->id; ?>" class="card-title blue-grey-text text-darken-4 truncate"><?= h($category->title); ?></a>
													</div>
													<div class="card-image hide show-on-medium-and-down" style="background-image: url('/files/images/<?= $category->image_path; ?>');"></div>
													<div class="card-action br-n">
														<table class="table">
															<tr>
																<td class="pn va-m pr10">
																	<?php if (!empty($category->sale_count)) { ?>
																		<span><?= __d('HomeFeaturedCell', '{0,plural,=0{No featured offers} =1{1 featured offer} other{# featured offers}} in {1,plural,=0{# sales} =1{1 sale} other{# sales}}', $category->sale_offers_count, $category->sale_count); ?></span>
																	<?php } ?>
																</td>
																<td class="pn va-t" width="1%">
																	<a href="/sales?filter_category=<?= $category->id; ?>" class="waves-effect btn-large btn-flat blue-grey lighten-5 blue-text text-darken-4 fw500"><?= __d('HomeFeaturedCell', 'VIEW'); ?></a>
																</td>
															</tr>
														</table>
													</div>
												</div>
											</div>
										</div>
									<?php } ?>
								</div>
							</div>
							<script type="text/javascript">
								(function($) {
									var $categories_item = $('#HomeFeaturedCell-<?= $c_id; ?> .<?= $c_class; ?>__categories .<?= $c_class; ?>__categories-item'),
										$placeholder = $('#HomeFeaturedCell-<?= $c_id; ?> .<?= $c_class; ?>__placeholder'),
										$placeholder_image = $('.<?= $c_class; ?>__placeholder-image', $placeholder);

									$categories_item.on('mouseover', function(event) {
										var $categories_item = $(this);

										$placeholder_image.css({
											'background-image': 'url("' + $categories_item.attr('data-img') + '")'
										});
									});

									$categories_item.on('mouseout', function(event) {
										var $categories_item = $(this);

										$placeholder_image.css({
											'background-image': ''
										});
									});
								})(jQuery);
							</script>
						</div>
					<?php } ?>
				</div>
			</div>
		</div>
		<div class="pb25 mb25 hide-on-med-and-down"></div>
	</div>
</div>
