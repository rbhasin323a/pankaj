<?php
use Cake\Core\Configure;

/**
 * Home Account Component Cell
 */
if (empty($special_sales) && !empty($vd['user'])) {
	return false;
}

$this->loadHelper('Form', [
	'templates' => 'app_form',
]);

$c_id = substr(md5(uniqid(mt_rand(), true)), 0, 10);
$c_class = 'home-account';
?>
<div id="HomeAccountCell-<?= $c_id; ?>" class="<?= $c_class; ?> inline-full va-t">
	<div class="container">
		<div class="pv25 mv25 hide-on-med-and-down"></div>
		<div class="row">
			<?php if (!empty($special_sales)) { ?>
				<div class="col s12 <?= (empty($vd['user'])) ? 'm6 push-m3 l6 push-l3 xl4' : ''; ?>">
					<h4 class="fw300 mv25 pv25 text-center white-text"><?= __d('HomeAccountCell', '{0,plural,=0{NO SPECIAL OFFERS} =1{{1} SPECIAL OFFER} other{{1} SPECIAL OFFERS}}', $special_sale_offers_count, '<span class="fw400">' . $special_sale_offers_count . '</span>'); ?></h4>
					<div class="<?= $c_class; ?>__sales carousel">
						<?php foreach ($special_sales as $sale) { ?>
							<?php $url = $this->Url->build(['controller' => 'Sales', 'action' => 'view', !empty($sale->slug) ? $sale->slug : $sale->id]); ?>
							<div class="<?= $c_class; ?>__sales-item carousel-item">
								<div class="card hoverable grey lighten-5">
									<div class="card-content">
										<a href="<?= $url; ?>" class="card-title blue-grey-text text-darken-4 fs26 mv20"><?= h($sale->title); ?></a>
										<p class="grey-text text-darken-2 fs16"><?= h($sale->sub_title); ?></p>
									</div>
									<div class="card-image" style="background-image: url('/files/images/sales/<?= $sale->main_image ?>');"></div>
									<div class="card-action br-n text-right">
										<table class="table">
											<tr>
												<td class="pn va-m pr10">
													<?php if (!empty($sale->sale_offers)) { ?>
														<span><?= __d('HomeAccountCell', '{0,plural,=0{No special offers} =1{1 special offer} other{# special offers}}', count($sale->sale_offers)); ?></span>
													<?php } ?>
												</td>
												<td class="pn va-t" width="1%"><a href="<?= $url; ?>" class="waves-effect btn-large btn-flat blue-grey lighten-5 blue-text text-darken-4 fw500"><?= __d('HomeAccountCell', 'VIEW'); ?></a></td>
											</tr>
										</table>
									</div>
								</div>
							</div>
						<?php } ?>
					</div>
					<script type="text/javascript">
						(function($) {
							var $sales = $('#HomeAccountCell-<?= $c_id; ?> .<?= $c_class; ?>__sales'),
								$sale_items = $('.<?= $c_class; ?>__sales-item', $sales);

							$(window).on('load', function(e) {
								$sales.hac_carousel();
							});
						})(jQuery);
					</script>
				</div>
			<?php } ?>
			<?php if (empty($vd['user'])) { ?>
				<div class="col s12 <?= (!empty($special_sales)) ? 'xl5 push-xl3' : 'xl8 push-xl2'; ?> mv50 pv50">
					<div class="row">
						<div class="col s12 m8 push-m2 xl10 push-xl1 mv25 pv25">
							<div id="HomeAccountCell-<?= $c_id; ?>-tabs" class="<?= $c_class; ?>__tabs">
								<ul class="tabs tabs-fixed-width">
									<li class="tab"><a class="active" href="#HomeAccountCell-<?= $c_id; ?>-tabs-login"><?= __d('HomeAccountCell', 'SIGN IN'); ?></a></li>
									<li class="tab"><a href="#HomeAccountCell-<?= $c_id; ?>-tabs-register"><?= __d('HomeAccountCell', 'SIGN UP'); ?></a></li>
								</ul>
								<div id="HomeAccountCell-<?= $c_id; ?>-tabs-login" class="col s12 <?= $c_class; ?>__form">
									<?= $this->Form->create(null, ['id' => 'HomeAccountCell-' . $c_id . '-form-login', 'url' => '/api/account/login']); ?>
									<?= $this->Form->unlockField('remember_me'); ?>
									<table class="table">
										<tr>
											<td>
												<div class="input-field-bg">
													<?= $this->Form->input('email', ['id' => 'HomeAccountCell-' . $c_id . '-input-login-email', 'required' => 'required', 'label' => __d('HomeAccountCell', 'Email')]); ?>
												</div>
											</td>
										</tr>
										<tr>
											<td>
												<div class="input-field-bg">
													<?= $this->Form->input('password', ['id' => 'HomeAccountCell-' . $c_id . '-input-login-password', 'required' => 'required', 'label' => __d('HomeAccountCell', 'Password')]); ?>
												</div>
											</td>
										</tr>
										<tr>
											<?php if (Configure::read('Users.RememberMe.active')) { ?>
												<td class="pv15">
													<div class="ib">
														<input type="hidden" name="remember_me" value="0" />
														<input type="checkbox" name="remember_me" value="1" id="remember-me">
														<label for="remember-me"><?= __d('HomeAccountCell', 'Remember Me'); ?></label>
													</div>
												</td>
											<?php } ?>
										</tr>
										<tr>
											<td class="pv10">
												<div class="<?= $c_class; ?>__form__action">
													<?= $this->Form->button(__d('HomeAccountCell', 'SUBMIT'), ['class' => 'waves-effect btn white blue-text text-darken-4 fw500']); ?>
												</div>
											</td>
										</tr>
									</table>
									<?= $this->Form->end(); ?>
								</div>
								<div id="HomeAccountCell-<?= $c_id; ?>-tabs-register" class="col s12 <?= $c_class; ?>__form">
									<?= $this->Form->create(null, ['id' => 'HomeAccountCell-' . $c_id . '-form-register', 'url' => '/api/account/register']); ?>
									<?= $this->Form->unlockField('tos'); ?>
									<table class="table">
										<tr>
											<td>
												<div class="input-field-bg">
													<?= $this->Form->input('email', ['id' => 'HomeAccountCell-' . $c_id . '-input-register-email', 'required' => 'required', 'label' => __d('HomeAccountCell', 'Email')]); ?>
												</div>
											</td>
										</tr>
										<tr>
											<td>
												<div class="input-field-bg">
													<?= $this->Form->input('password', ['id' => 'HomeAccountCell-' . $c_id . '-input-register-password', 'required' => 'required', 'label' => __d('HomeAccountCell', 'Password')]); ?>
												</div>
											</td>
										</tr>
										<tr>
											<td class="pv15">
												<div class="ib">
													<input type="hidden" name="tos" value="0" />
													<input type="checkbox" name="tos" value="1" required="required" id="tos">
													<label for="tos"><?= sprintf(__d('HomeAccountCell', 'I agree to the <a href="%s" rel="noopener noreferer" target="_blank">Terms & Conditions</a>'), '/pages/terms'); ?></label>
												</div>
											</td>
										</tr>
										<tr>
											<td class="pv10">
												<div class="<?= $c_class; ?>__form__action">
													<?= $this->Form->button(__d('HomeAccountCell', 'SUBMIT'), ['class' => 'waves-effect btn white blue-text text-darken-4 fw500']); ?>
												</div>
											</td>
										</tr>
									</table>
									<?= $this->Form->end(); ?>
								</div>
								<script type="text/javascript">
									(function($) {
										$(document).ready(function() {
											AJAXForm('#HomeAccountCell-<?= $c_id; ?>-form-login');
											AJAXForm('#HomeAccountCell-<?= $c_id; ?>-form-register');
										});
									}(jQuery));
								</script>
							</div>
						</div>
					</div>
				</div>
			<?php } ?>
		</div>
		<div class="pv25 mv25 hide-on-med-and-down"></div>
	</div>
</div>

<!-- Home Account Carousel -->
<script src="/assets/rev/js/<?= $this->Asset->getFileRev('website-components/home-account-cell/carousel.js', ROOT . '/webroot/assets/rev/js/rev-manifest.json'); ?>"></script>
