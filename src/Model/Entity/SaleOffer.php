<?php
namespace App\Model\Entity;

use Cake\ORM\Behavior\Translate\TranslateTrait;
use Cake\ORM\Entity;


/**
 * SaleOffer Entity.
 *
 * @property string $id
 * @property string $sale_id
 * @property string $name
 * @property bool $active
 * @property string $travel_date
 * @property int $minimum_stay
 * @property int $max_adults
 * @property int $max_children
 * @property float $child_rate
 * @property int $child_free
 * @property string $child_age_description
 * @property float $infant_rate
 * @property int $infant_free
 * @property string $infant_age_description
 * @property bool $deposit
 * @property string $single_policy
 * @property string $child_policy
 * @property string $summary_description
 * @property string $main_description
 * @property \Cake\I18n\Time $created
 * @property \Cake\I18n\Time $modified
 */
class SaleOffer extends Entity {
    use TranslateTrait;



    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false,
    ];
}
