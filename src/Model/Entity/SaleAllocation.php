<?php
namespace App\Model\Entity;

use Cake\ORM\Behavior\Translate\TranslateTrait;
use Cake\ORM\Entity;


/**
 * SaleAllocation Entity.
 *
 * @property string $id
 * @property string $sale_offer_id
 * @property \App\Model\Entity\SaleOffer $sale_offer
 * @property float $rate
 * @property float $rack_rate
 * @property float $single_rate
 * @property float $child_rate
 * @property float $infant_rate
 * @property float $single_deposit
 * @property float $child_deposit
 * @property float $infant_deposit
 * @property string $date_balance_due
 * @property int $min_nights
 * @property string $date_start
 * @property string $date_end
 * @property string $departure_airport_code
 * @property string $destination_airport_code
 * @property float $discount
 * @property int $booked_rooms
 * @property int $hold_rooms
 * @property int $locked_rooms
 * @property int $available_rooms
 * @property bool $outbound_overnight
 * @property bool $inbound_overnight
 * @property \Cake\I18n\Time $created
 * @property \Cake\I18n\Time $modified
 */
class SaleAllocation extends Entity {
    use TranslateTrait;
    
    

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false,
    ];
}
