<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * AclPhinxlog Entity.
 */
class AclPhinxlog extends Entity
{

	/**
	 * Fields that can be mass assigned using newEntity() or patchEntity().
	 *
	 * @var array
	 */
	protected $_accessible = [
		'version' => true,
		'start_time' => true,
		'end_time' => true,
		];

	

}
