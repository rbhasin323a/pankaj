<?php
namespace App\Model\Entity;

use Cake\ORM\Behavior\Translate\TranslateTrait;
use Cake\ORM\Entity;


/**
 * Booking Entity.
 *
 * @property string $id
 * @property string $type
 * @property string $provider
 * @property string $status
 * @property string $transaction_id
 * @property \App\Model\Entity\Transaction $transaction
 * @property string $payment_id
 * @property \App\Model\Entity\Payment $payment
 * @property string $user_id
 * @property \App\Model\Entity\User $user
 * @property string $sale_id
 * @property \App\Model\Entity\Sale $sale
 * @property string $details
 * @property string $notes
 * @property \Cake\I18n\Time $created
 * @property \Cake\I18n\Time $modified
 * @property \App\Model\Entity\BookingSummary[] $booking_summaries
 */
class Booking extends Entity {
    use TranslateTrait;
    
    

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false,
    ];
}
