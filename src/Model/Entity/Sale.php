<?php
namespace App\Model\Entity;

use Cake\ORM\Behavior\Translate\TranslateTrait;
use Cake\ORM\Entity;


/**
 * Sale Entity.
 *
 * @property string $id
 * @property string $title
 * @property string $sub_title
 * @property string $destination
 * @property string $slug
 * @property string $short_description
 * @property string $main_description
 * @property string $staff_description
 * @property string $hotel_description
 * @property string $travel_description
 * @property string $review_description
 * @property string $location_title
 * @property string $location_map
 * @property string $contractor_id
 * @property \App\Model\Entity\Contractor $contractor
 * @property string $supplier_id
 * @property \App\Model\Entity\Supplier $supplier
 * @property string $company_id
 * @property \App\Model\Entity\Company $company
 * @property string $currency_id
 * @property \App\Model\Entity\Currency $currency
 * @property string $country_id
 * @property \App\Model\Entity\Country $country
 * @property string $tax_id
 * @property \App\Model\Entity\Tax $tax
 * @property bool $status
 * @property bool $smart_stay
 * @property string $date_open
 * @property string $date_close
 * @property string $sale_type
 * @property string $board_type
 * @property bool $promoted
 * @property string $available_domains
 * @property bool $exclude_summary
 * @property bool $repeated
 * @property bool $enable_hold
 * @property float $commission
 * @property string $top_discounts
 * @property string $commission_type
 * @property bool $show_price
 * @property bool $show_discount
 * @property string $visible
 * @property string $scyscanner_airport_id
 * @property string $images
 * @property \Cake\I18n\Time $created
 * @property \Cake\I18n\Time $modified
 * @property \App\Model\Entity\TranslationSale $title_translation
 * @property \App\Model\Entity\TranslationSale $sub_title_translation
 * @property \App\Model\Entity\TranslationSale $destination_translation
 * @property \App\Model\Entity\TranslationSale $slug_translation
 * @property \App\Model\Entity\TranslationSale $short_description_translation
 * @property \App\Model\Entity\TranslationSale $main_description_translation
 * @property \App\Model\Entity\TranslationSale $staff_description_translation
 * @property \App\Model\Entity\TranslationSale $hotel_description_translation
 * @property \App\Model\Entity\TranslationSale $travel_description_translation
 * @property \App\Model\Entity\TranslationSale $review_description_translation
 * @property \App\Model\Entity\TranslationSale $location_title_translation
 * @property \App\Model\Entity\TranslationSale[] $_i18n
 */
class Sale extends Entity {
	use TranslateTrait;

	/**
	 * Fields that can be mass assigned using newEntity() or patchEntity().
	 *
	 * Note that when '*' is set to true, this allows all unspecified fields to
	 * be mass assigned. For security purposes, it is advised to set '*' to false
	 * (or remove it), and explicitly make individual fields accessible as needed.
	 *
	 * @var array
	 */
	protected $_accessible = [
		'*' => true,
		'id' => false,
        '_translations' => true
	];
}
