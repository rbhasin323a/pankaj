<?php
namespace App\Model\Entity;

use Cake\ORM\Behavior\Translate\TranslateTrait;
use Cake\ORM\Entity;


/**
 * Company Entity.
 *
 * @property string $id
 * @property string $name
 * @property string $email
 * @property string $contact_id
 * @property \App\Model\Entity\Contact $contact
 * @property string $supplier_id
 * @property \App\Model\Entity\Supplier $supplier
 * @property string $country
 * @property string $support_phone
 * @property string $support_email
 * @property string $reports_email
 * @property string $fax
 * @property string $notes
 * @property string $tos_url
 * @property \Cake\I18n\Time $created
 * @property \Cake\I18n\Time $modified
 * @property \App\Model\Entity\Sale[] $sales
 */
class Company extends Entity
{
	use TranslateTrait;

	/**
	 * Fields that can be mass assigned using newEntity() or patchEntity().
	 *
	 * Note that when '*' is set to true, this allows all unspecified fields to
	 * be mass assigned. For security purposes, it is advised to set '*' to false
	 * (or remove it), and explicitly make individual fields accessible as needed.
	 *
	 * @var array
	 */
	protected $_accessible = [
		'*' => true,
		'id' => false,
	];
}
