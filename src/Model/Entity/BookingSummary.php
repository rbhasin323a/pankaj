<?php
namespace App\Model\Entity;

use Cake\ORM\Behavior\Translate\TranslateTrait;
use Cake\ORM\Entity;


/**
 * BookingSummary Entity.
 *
 * @property string $id
 * @property string $booking_id
 * @property \App\Model\Entity\Booking $booking
 * @property string $sale_id
 * @property \App\Model\Entity\Sale $sale
 * @property string $contractor_id
 * @property \App\Model\Entity\Contractor $contractor
 * @property string $sale_offer_id
 * @property \App\Model\Entity\SaleOffer $sale_offer
 * @property string $departure_id
 * @property \App\Model\Entity\Departure $departure
 * @property string $user_id
 * @property \App\Model\Entity\User $user
 * @property string $transaction_id
 * @property \App\Model\Entity\Transaction $transaction
 * @property string $currency_id
 * @property \App\Model\Entity\Currency $currency
 * @property string $company_id
 * @property \App\Model\Entity\Company $company
 * @property string $supplier_id
 * @property \App\Model\Entity\Supplier $supplier
 * @property string $adults
 * @property string $children
 * @property string $infants
 * @property string $county
 * @property string $pax
 * @property string $date_booked
 * @property string $check_in
 * @property string $check_out
 * @property int $no_nights
 * @property int $rooms
 * @property string $territory
 * @property float $total_sell_rate_in_currency
 * @property float $rate_to_gbp
 * @property float $total_sell_rate
 * @property float $commission_ex_vat
 * @property float $vat_on_commission
 * @property float $gross_commission
 * @property float $total_net_rate
 * @property float $customer_total_price
 * @property float $customer_payment
 * @property float $credits_used
 * @property float $credit_amount_deductible_from_commission
 * @property float $booking_fee_net_rate
 * @property float $vat_on_booking_fee
 * @property float $booking_fee
 * @property string $payment_type
 * @property float $payment_surcharge_net_rate
 * @property float $vat_on_payment_surcharge
 * @property float $payment_surcharge
 * @property string $type
 * @property int $top_discount
 * @property int $total_room_nights
 * @property string $impulse
 * @property string $notes
 * @property float $gross_profit
 * @property string $destination_name
 * @property string $destination_type
 * @property string $post_code
 * @property string $country
 * @property string $division
 * @property string $city
 * @property string $city_district
 * @property float $total_custom_tax
 * @property \Cake\I18n\Time $created
 * @property \Cake\I18n\Time $modified
 */
class BookingSummary extends Entity {
    use TranslateTrait;
    
    

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false,
    ];
}
