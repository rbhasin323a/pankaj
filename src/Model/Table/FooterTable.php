<?php
namespace App\Model\Table;

use App\Utility\DBUtils;
use Search\Manager;

use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Footer Model
 */
class FooterTable extends Table
{

	public function beforeSave($event, $entity, $options)
	{
		if (empty($entity->id)) {
			$entity->id = $this->generate_id();
		}
	}

	public function searchConfiguration()
	{
		$search = new Manager($this);

		$search->like('q', [
			'before' => true,
			'after' => true,
			'field' => []
		]);

		return $search;
	}

	public function generate_id($exists = false)
	{
		if (empty($exists)) {
			$new_id = strtoupper(DBUtils::secure_random_string(12));

			// Generate Hard ID
			$results = $this->findById($new_id);

			foreach ($results as $result) {
				return $this->generate_id(true);
			}

			return $new_id;
		} else {
			return $this->generate_id();
		}
	}

	/**
	 * Initialize method
	 *
	 * @param array $config The configuration for the Table.
	 * @return void
	 */
	public function initialize(array $config)
	{
		parent::initialize($config);

		$this->addBehavior('Search.Search');

		$this->table('footer');
		$this->displayField('copyright_text');
		$this->primaryKey('id');

		$this->addBehavior('Timestamp');
		$this->addBehavior('Translate', [
			'fields' => [
				'copyright_text',
				'menu_content'
			],
			'translationTable' => 'TranslationFooter'
		]);
	}

	/**
	 * Default validation rules.
	 *
	 * @param \Cake\Validation\Validator $validator Validator instance.
	 * @return \Cake\Validation\Validator
	 */
	public function validationDefault(Validator $validator)
	{
		$validator
			->allowEmpty('id', 'create');

		$validator
			->requirePresence('copyright_text', 'create')
			->notEmpty('copyright_text');

		$validator
			->requirePresence('menu_content', 'create')
			->notEmpty('menu_content');

		return $validator;
	}
}
