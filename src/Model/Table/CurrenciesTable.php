<?php
namespace App\Model\Table;

use App\Utility\DBUtils;
use Search\Manager;

use App\Model\Entity\Currency;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Currencies Model
 *
 * @property \Cake\ORM\Association\HasMany $Sales
 */
class CurrenciesTable extends Table {

    public function beforeSave($event, $entity, $options) {
        if (empty($entity->id) ) {
            $entity->id = $this->generate_id();
        }
    }

    public function searchConfiguration(){
        $search = new Manager($this);

        $search->like('q', [
            'before' => true,
            'after' => true,
            'field' => []
        ]);

        return $search;
    }

    public function generate_id($exists = false) {
        if (empty($exists)) {
            $new_id = strtoupper(DBUtils::secure_random_string(12));
            
            // Generate Hard ID
            $results = $this->findById($new_id);
            
            foreach ($results as $result) {
                return $this->generate_id(true);
            }

            return $new_id;
        } else {
            return $this->generate_id();
        }
    }

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->addBehavior('Search.Search');

        $this->table('currencies');
        $this->displayField('name');
        $this->primaryKey('id');

        $this->addBehavior('Timestamp');

        /*$this->hasMany('Sales', [
            'foreignKey' => 'currency_id'
        ]);*/
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('name', 'create')
            ->notEmpty('name');

        $validator
            ->requirePresence('code', 'create')
            ->notEmpty('code');

        $validator
            ->requirePresence('symbol_left', 'create')
            ->allowEmpty('symbol_left');

        $validator
            ->requirePresence('symbol_right', 'create')
            ->allowEmpty('symbol_right');

        $validator
            ->requirePresence('decimal_place', 'create')
            ->notEmpty('decimal_place');

        $validator
            ->add('value', 'valid', ['rule' => 'numeric'])
            ->requirePresence('value', 'create')
            ->notEmpty('value');

        $validator
            ->add('status', 'valid', ['rule' => 'boolean'])
            ->requirePresence('status', 'create')
            ->notEmpty('status');

        return $validator;
    }
}
