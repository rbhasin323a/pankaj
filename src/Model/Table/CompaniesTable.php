<?php
namespace App\Model\Table;

use App\Utility\DBUtils;
use Search\Manager;

use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;


/**
 * Companies Model
 *
 * @property \Cake\ORM\Association\BelongsTo $Contacts
 * @property \Cake\ORM\Association\BelongsTo $Suppliers
 * @property \Cake\ORM\Association\HasMany $Sales
 */
class CompaniesTable extends Table
{

	public function beforeSave($event, $entity, $options)
	{
		if (empty($entity->id)) {
			$entity->id = $this->generate_id();
		}
	}

	public function searchConfiguration()
	{
		$search = new Manager($this);

		$search->like('q', [
			'before' => true,
			'after' => true,
			'field' => []
		]);

		return $search;
	}

	public function generate_id($exists = false)
	{
		if (empty($exists)) {
			$new_id = strtoupper(DBUtils::secure_random_string(12));

			// Generate Hard ID
			$results = $this->findById($new_id);

			foreach ($results as $result) {
				return $this->generate_id(true);
			}

			return $new_id;
		} else {
			return $this->generate_id();
		}
	}

	/**
	 * Initialize method
	 *
	 * @param array $config The configuration for the Table.
	 * @return void
	 */
	public function initialize(array $config)
	{
		parent::initialize($config);

		$this->addBehavior('Search.Search');

		$this->table('companies');
		$this->displayField('name');
		$this->primaryKey('id');

		$this->addBehavior('Timestamp');

		$this->belongsTo('Contacts', [
			'foreignKey' => 'contact_id',
			'joinType' => 'INNER'
		]);

		$this->belongsTo('Suppliers', [
			'foreignKey' => 'supplier_id',
			'joinType' => 'INNER'
		]);

		$this->hasMany('Sales', [
			'foreignKey' => 'company_id'
		]);
	}

	/**
	 * Default validation rules.
	 *
	 * @param \Cake\Validation\Validator $validator Validator instance.
	 * @return \Cake\Validation\Validator
	 */
	public function validationDefault(Validator $validator)
	{
		$validator
			->allowEmpty('id', 'create');

		$validator
			->requirePresence('name', 'create')
			->notEmpty('name');

		$validator
			->add('email', 'valid', ['rule' => 'email'])
			->requirePresence('email', 'create')
			->notEmpty('email');

		$validator
			->requirePresence('country', 'create')
			->notEmpty('country');

		$validator
			->requirePresence('support_phone', 'create')
			->notEmpty('support_phone');

		$validator
			->add('email', 'valid', ['rule' => 'email'])
			->requirePresence('support_email', 'create')
			->notEmpty('support_email');

		$validator
			->add('email', 'valid', ['rule' => 'email'])
			->requirePresence('reports_email', 'create')
			->notEmpty('reports_email');

		return $validator;
	}

	/**
	 * Returns a rules checker object that will be used for validating
	 * application integrity.
	 *
	 * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
	 * @return \Cake\ORM\RulesChecker
	 */
	public function buildRules(RulesChecker $rules)
	{
		$rules->add($rules->isUnique(['email']));
		$rules->add($rules->existsIn(['contact_id'], 'Contacts'));
		$rules->add($rules->existsIn(['supplier_id'], 'Suppliers'));
		return $rules;
	}
}
