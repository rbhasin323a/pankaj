<?php
namespace App\Model\Table;

use App\Utility\DBUtils;
use Search\Manager;

use App\Model\Entity\Currency;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * CurrencyRates Model
 *
 * @property \Cake\ORM\Association\HasMany $Sales
 */
class CurrencyRatesTable extends Table {
    
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('currency_rates');
        $this->primaryKey('id');
        $this->addBehavior('Timestamp');
    }

    public function beforeSave($event, $entity, $options) {
        $entity->modified = date("Y-m-d H:i:s");
        return true;        
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->allowEmpty('id', 'create');

        return $validator;
    }
}
