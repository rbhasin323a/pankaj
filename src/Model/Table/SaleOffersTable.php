<?php
namespace App\Model\Table;

use App\Utility\DBUtils;
use Search\Manager;

use App\Model\Entity\SaleOffer;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\Routing\Router;

/**
 * SaleOffers Model
 *
 * @property \Cake\ORM\Association\BelongsTo $Taxes
 * @property \Cake\ORM\Association\BelongsTo $Sales
 * @property \Cake\ORM\Association\HasMany $SaleAllocations
 */
class SaleOffersTable extends Table
{

	public function beforeSave($event, $entity, $options)
	{
		if (empty($entity->id)) {
			$entity->id = $this->generate_id();
		}

		$request = Router::getRequest();

		foreach ($entity->visibleProperties() as $property) {
			if (isset($request->data[$property])) {
				$value = $request->data[$property];
				if (!empty($value)) {
					if ($property != 'id' && $property != 'created' && $property != 'modified') {
						if (is_array($value)) {
							$entity->$property = json_encode($value);
						} else {
							$entity->$property = $value;
						}
					}
				}
			}
		}
	}

	public function searchConfiguration()
	{
		$search = new Manager($this);

		$search->like('q', [
			'before' => true,
			'after' => true,
			'field' => []
		]);

		return $search;
	}

	public function generate_id($exists = false)
	{
		if (empty($exists)) {
			$new_id = strtoupper(DBUtils::secure_random_string(12));

			// Generate Hard ID
			$results = $this->findById($new_id);

			foreach ($results as $result) {
				return $this->generate_id(true);
			}

			return $new_id;
		} else {
			return $this->generate_id();
		}
	}

	/**
	 * Initialize method
	 *
	 * @param array $config The configuration for the Table.
	 * @return void
	 */
	public function initialize(array $config)
	{
		parent::initialize($config);

		$this->addBehavior('Search.Search');

		$this->table('sale_offers');
		$this->displayField('name');
		$this->primaryKey('id');

		$this->addBehavior('Timestamp');

		$this->belongsTo('Sales', [
			'foreignKey' => 'sale_id',
			'joinType' => 'INNER'
		]);

		$this->addBehavior('Translate', [
			'fields' => [
				'name',
				'single_policy',
				'child_policy',
				'summary_description',
				'main_description'
			],
			'translationTable' => 'TranslationSaleOffers'
		]);

		$this->hasMany('Bookings', [
			'foreignKey' => 'offer_id'
		]);

		$this->hasMany('SaleAllocations', [
			'foreignKey' => 'sale_offer_id',
			'dependent' => true
		]);
	}

	/**
	 * Default validation rules.
	 *
	 * @param \Cake\Validation\Validator $validator Validator instance.
	 * @return \Cake\Validation\Validator
	 */
	public function validationDefault(Validator $validator)
	{
		$validator
			->allowEmpty('id', 'create');

		// $validator
		//     ->requirePresence('name', 'create')
		//     ->notEmpty('name');

		// $validator
		//     ->add('active', 'valid', ['rule' => 'boolean'])
		//     ->notEmpty('active', 'Please fill this field');

		// $validator
		//     ->requirePresence('offer_type', 'create')
		//     ->notEmpty('offer_type');

		// $validator
		//     ->add('max_adults', 'valid', ['rule' => 'numeric'])
		//     ->notEmpty('max_adults', 'Please fill this field');

		// $validator
		//     ->add('max_children', 'valid', ['rule' => 'numeric'])
		//     ->notEmpty('max_children', 'Please fill this field');

		// $validator
		//     ->add('child_free', 'valid', ['rule' => 'numeric'])
		//     ->notEmpty('child_free', 'Please fill this field');

		// $validator
		//     ->add('infant_free', 'valid', ['rule' => 'numeric'])
		//     ->notEmpty('infant_free', 'Please fill this field');

		// $validator
		//     ->add('deposit', 'valid', ['rule' => 'boolean'])
		//     ->notEmpty('deposit', 'Please fill this field');

		// $validator
		//     ->requirePresence('single_policy', 'create')
		//     ->notEmpty('single_policy');

		// $validator
		//     ->requirePresence('child_policy', 'create')
		//     ->notEmpty('child_policy');

		$validator
			->allowEmpty('child_age_from');

		$validator
			->allowEmpty('child_age_to');

		$validator
			->allowEmpty('infant_age_from');

		$validator
			->allowEmpty('infant_age_to');

		$validator
			->allowEmpty('summary_description');

		$validator
			->allowEmpty('main_description');

		return $validator;
	}

	/**
	 * Returns a rules checker object that will be used for validating
	 * application integrity.
	 *
	 * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
	 * @return \Cake\ORM\RulesChecker
	 */
	public function buildRules(RulesChecker $rules)
	{
		$rules->add($rules->existsIn(['sale_id'], 'Sales'));
		return $rules;
	}
}
