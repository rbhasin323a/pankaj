<?php
namespace App\Model\Table;

use App\Utility\DBUtils;
use Search\Manager;

use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Contacts Model
 *
 * @property \Cake\ORM\Association\HasMany $Companies
 */
class ContactsTable extends Table
{

	public function beforeSave($event, $entity, $options)
	{
		if (empty($entity->id)) {
			$entity->id = $this->generate_id();
		}
	}

	public function searchConfiguration()
	{
		$search = new Manager($this);

		$search->like('q', [
			'before' => true,
			'after' => true,
			'field' => []
		]);

		return $search;
	}

	public function generate_id($exists = false)
	{
		if (empty($exists)) {
			$new_id = strtoupper(DBUtils::secure_random_string(12));

			// Generate Hard ID
			$results = $this->findById($new_id);

			foreach ($results as $result) {
				return $this->generate_id(true);
			}

			return $new_id;
		} else {
			return $this->generate_id();
		}
	}

	/**
	 * Initialize method
	 *
	 * @param array $config The configuration for the Table.
	 * @return void
	 */
	public function initialize(array $config)
	{
		parent::initialize($config);

		$this->addBehavior('Search.Search');

		$this->table('contacts');
		$this->displayField('first_name');
		$this->primaryKey('id');

		$this->addBehavior('Timestamp');

		$this->hasMany('Companies', [
			'foreignKey' => 'contact_id'
		]);
	}

	/**
	 * Default validation rules.
	 *
	 * @param \Cake\Validation\Validator $validator Validator instance.
	 * @return \Cake\Validation\Validator
	 */
	public function validationDefault(Validator $validator)
	{
		$validator
			->allowEmpty('id', 'create');

		$validator
			->requirePresence('first_name', 'create')
			->notEmpty('first_name');

		$validator
			->requirePresence('last_name', 'create')
			->notEmpty('last_name');

		$validator
			->add('email', 'valid', ['rule' => 'email'])
			->requirePresence('email', 'create')
			->notEmpty('email');

		$validator
			->requirePresence('address_1', 'create')
			->notEmpty('address_1');

		$validator
			->requirePresence('postcode', 'create')
			->notEmpty('postcode');

		$validator
			->requirePresence('city', 'create')
			->notEmpty('city');

		$validator
			->requirePresence('country', 'create')
			->notEmpty('country');

		$validator
			->requirePresence('phone_mobile', 'create')
			->notEmpty('phone_mobile');

		return $validator;
	}

	/**
	 * Returns a rules checker object that will be used for validating
	 * application integrity.
	 *
	 * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
	 * @return \Cake\ORM\RulesChecker
	 */
	public function buildRules(RulesChecker $rules)
	{
		$rules->add($rules->isUnique(['email']));
		return $rules;
	}
}
