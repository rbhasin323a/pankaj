<?php
namespace App\Model\Table;


use App\Utility\DBUtils;
use Search\Manager;

use App\Model\Entity\Log;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\Routing\Router;

/**
 * Logs Model
 *
 * @property \Cake\ORM\Association\BelongsTo $MyUsers
 */
class LogsTable extends Table
{


	public function beforeSave($event, $entity, $options)
	{
		if (empty($entity->id)) {
			$entity->id = $this->generate_id();
		}

		$request = Router::getRequest();

		foreach ($entity->visibleProperties() as $property) {
			if (isset($request->data[$property])) {
				$value = $request->data[$property];
				if (!empty($value)) {
					if ($property != 'id' && $property != 'created' && $property != 'modified') {
						if (is_array($value)) {
							$entity->$property = json_encode($value);
						} else {
							$entity->$property = $value;
						}
					}
				}
			}
		}
	}

	public function generate_id($exists = false)
	{
		if (empty($exists)) {
			$new_id = strtoupper(DBUtils::secure_random_string(12));

			// Generate Hard ID
			$results = $this->findById($new_id);

			foreach ($results as $result) {
				return $this->generate_id(true);
			}

			return $new_id;
		} else {
			return $this->generate_id();
		}
	}

	// Configure how you want the search plugin to work with this table class
	public function searchConfiguration()
	{
		$search = new Manager($this);
		$search->value('id', [
			'field' => $this->aliasField('id')
		])
			->like('q', [
				'before' => true,
				'after' => true,
				'field' => [$this->aliasField('user_id'), $this->aliasField('data')]
			]);

		return $search;
	}

	/**
	 * Initialize method
	 *
	 * @param array $config The configuration for the Table.
	 * @return void
	 */
	public function initialize(array $config)
	{
		parent::initialize($config);

		$this->addBehavior('Search.Search');

		$this->table('logs');
		$this->displayField('id');
		$this->primaryKey('id');

		$this->addBehavior('Timestamp');

		$this->belongsTo('MyUsers', [
			'foreignKey' => 'user_id'
		]);
	}

	/**
	 * Default validation rules.
	 *
	 * @param \Cake\Validation\Validator $validator Validator instance.
	 * @return \Cake\Validation\Validator
	 */
	public function validationDefault(Validator $validator)
	{
		$validator
			->allowEmpty('id', 'create');

		$validator
			->allowEmpty('user_id', 'create');

		$validator
			->allowEmpty('data');

		return $validator;
	}

	/**
	 * Returns a rules checker object that will be used for validating
	 * application integrity.
	 *
	 * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
	 * @return \Cake\ORM\RulesChecker
	 */
	public function buildRules(RulesChecker $rules)
	{
		$rules->add($rules->existsIn(['user_id'], 'MyUsers'));
		return $rules;
	}
}
