<?php
namespace App\Model\Table;

use Cake\ORM\Table;
use Cake\Validation\Validator;

class BookingTravellersTable extends Table
{

	/**
	 * Initialize method
	 *
	 * @param array $config The configuration for the Table.
	 * @return void
	 */
	public function initialize(array $config)
	{
		parent::initialize($config);

		$this->table('booking_travellers');
		$this->displayField('id');
		$this->primaryKey('id');

		$this->belongsTo('Booking', [
			'foreignKey' => 'booking_id'
		]);
	}

	/**
	 * Default validation rules.
	 *
	 * @param \Cake\Validation\Validator $validator Validator instance.
	 * @return \Cake\Validation\Validator
	 */
	public function validationDefault(Validator $validator)
	{
		$validator
			->allowEmpty('id', 'create');

		$validator
			->notEmpty('title', 'Please fill this field');

		$validator
			->notEmpty('first_name', 'Please fill this field');

		$validator
			->notEmpty('last_name', 'Please fill this field');

		$validator
			->notEmpty('birthday', 'Please fill this field');

		$validator
			->notEmpty('type', 'Please fill this field');

		$validator
			->notEmpty('passport_number', 'Please fill this field');

		return $validator;
	}
}
