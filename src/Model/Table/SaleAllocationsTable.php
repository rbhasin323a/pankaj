<?php
namespace App\Model\Table;

use App\Utility\DBUtils;
use Search\Manager;

use App\Model\Entity\SaleAllocation;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * SaleAllocations Model
 *
 * @property \Cake\ORM\Association\BelongsTo $SaleOffers
 */
class SaleAllocationsTable extends Table {

    public function beforeSave($event, $entity, $options) {
        if (empty($entity->id) ) {
            $entity->id = $this->generate_id();
        }

        if (!empty($entity->rate) && !empty($entity->rack_rate)) { 
            $discount = 100 - ((100 * $entity->rate) / $entity->rack_rate);
            $entity->discount = ($discount > 0) ? ceil($discount) : 0;
        }
    }

    public function searchConfiguration(){
        $search = new Manager($this);

        $search->like('q', [
            'before' => true,
            'after' => true,
            'field' => []
        ]);

        return $search;
    }

    public function generate_id($exists = false) {
        if (empty($exists)) {
            $new_id = strtoupper(DBUtils::secure_random_string(12));
            
            // Generate Hard ID
            $results = $this->findById($new_id);
            
            foreach ($results as $result) {
                return $this->generate_id(true);
            }

            return $new_id;
        } else {
            return $this->generate_id();
        }
    }

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->addBehavior('Search.Search');

        $this->table('sale_allocations');
        $this->displayField('id');
        $this->primaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('SaleOffers', [
            'foreignKey' => 'sale_offer_id',
            'joinType' => 'INNER'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->allowEmpty('id', 'create');

        $validator
            ->add('rate', 'valid', ['rule' => 'numeric'])
            ->allowEmpty('rate');

        $validator
            ->add('rack_rate', 'valid', ['rule' => 'numeric'])
            ->allowEmpty('rack_rate');

        $validator
            ->add('single_rate', 'valid', ['rule' => 'numeric'])
            ->allowEmpty('single_rate');

        $validator
            ->add('child_rate', 'valid', ['rule' => 'numeric'])
            ->allowEmpty('child_rate');

        $validator
            ->add('infant_rate', 'valid', ['rule' => 'numeric'])
            ->allowEmpty('infant_rate');

        $validator
            ->add('single_deposit', 'valid', ['rule' => 'numeric'])
            ->allowEmpty('single_deposit');

        $validator
            ->add('child_deposit', 'valid', ['rule' => 'numeric'])
            ->allowEmpty('child_deposit');

        $validator
            ->add('infant_deposit', 'valid', ['rule' => 'numeric'])
            ->allowEmpty('infant_deposit');

        $validator
            ->allowEmpty('date_balance_due');

        $validator
            ->add('min_nights', 'valid', ['rule' => 'numeric'])
            ->allowEmpty('min_nights');

        $validator
            ->allowEmpty('date_start');

        $validator
            ->allowEmpty('date_end');

        $validator
            ->allowEmpty('departure_airport_code');

        $validator
            ->allowEmpty('destination_airport_code');

        $validator
            ->add('discount', 'valid', ['rule' => 'numeric'])
            ->allowEmpty('discount');

        $validator
            ->add('booked_rooms', 'valid', ['rule' => 'numeric'])
            ->allowEmpty('booked_rooms');

        $validator
            ->add('hold_rooms', 'valid', ['rule' => 'numeric'])
            ->allowEmpty('hold_rooms');

        $validator
            ->add('locked_rooms', 'valid', ['rule' => 'numeric'])
            ->allowEmpty('locked_rooms');

        $validator
            ->add('available_rooms', 'valid', ['rule' => 'numeric'])
            ->allowEmpty('available_rooms');

        $validator
            ->add('outbound_overnight', 'valid', ['rule' => 'boolean'])
            ->allowEmpty('outbound_overnight');

        $validator
            ->add('inbound_overnight', 'valid', ['rule' => 'boolean'])
            ->allowEmpty('inbound_overnight');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['sale_offer_id'], 'SaleOffers'));
        return $rules;
    }
}
