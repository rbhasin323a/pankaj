<?php
namespace App\Model\Table;

use App\Utility\DBUtils;
use Search\Manager;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Countries Model
 *
 * @property \Cake\ORM\Association\HasMany $CountryZones
 * @property \Cake\ORM\Association\HasMany $Sales
 */
class CountriesTable extends Table {

    public function beforeSave($event, $entity, $options) {
        if (empty($entity->id) ) {
            $entity->id = $this->generate_id();
        }
    }

    public function searchConfiguration(){
        $search = new Manager($this);

        $search->like('q', [
            'before' => true,
            'after' => true,
            'field' => []
        ]);

        return $search;
    }

    public function generate_id($exists = false) {
        if (empty($exists)) {
            $new_id = strtoupper(DBUtils::secure_random_string(12));

            // Generate Hard ID
            $results = $this->findById($new_id);

            foreach ($results as $result) {
                return $this->generate_id(true);
            }

            return $new_id;
        } else {
            return $this->generate_id();
        }
    }

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->addBehavior('Search.Search');

        $this->table('countries');
        $this->displayField('name');
        $this->primaryKey('id');

        $this->addBehavior('Timestamp');

        $this->hasMany('CountryZones', [
            'foreignKey' => 'country_id'
        ]);
        $this->hasMany('Sales', [
            'foreignKey' => 'country_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('name', 'create')
            ->notEmpty('name');

        return $validator;
    }
}
