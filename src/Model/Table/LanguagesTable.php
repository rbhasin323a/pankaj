<?php
namespace App\Model\Table;


use App\Utility\DBUtils;
use Search\Manager;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\Routing\Router;

/**
 * Languages Model
 *
 */
class LanguagesTable extends Table
{
	public function beforeSave($event, $entity, $options)
	{
		if (empty($entity->id)) {
			$entity->id = $this->generate_id();
		}

		$request = Router::getRequest();

		foreach ($entity->visibleProperties() as $property) {
			if (isset($request->data[$property])) {
				$value = $request->data[$property];
				if (!empty($value)) {
					if ($property != 'id' && $property != 'created' && $property != 'modified') {
						if (is_array($value)) {
							$entity->$property = json_encode($value);
						} else {
							$entity->$property = $value;
						}
					}
				}
			}
		}
	}

	public function generate_id($exists = false)
	{
		if (empty($exists)) {
			$new_id = strtoupper(DBUtils::secure_random_string(12));

			// Generate Hard ID
			$results = $this->findById($new_id);

			foreach ($results as $result) {
				return $this->generate_id(true);
			}

			return $new_id;
		} else {
			return $this->generate_id();
		}
	}

	// Configure how you want the search plugin to work with this table class
	public function searchConfiguration()
	{
		$search = new Manager($this);
		$search->value('id', [
			'field' => $this->aliasField('id')
		])
			->like('q', [
				'before' => true,
				'after' => true,
				'field' => [$this->aliasField('user_id'), $this->aliasField('data')]
			]);

		return $search;
	}

	/**
	 * Initialize method
	 *
	 * @param array $config The configuration for the Table.
	 * @return void
	 */
	public function initialize(array $config)
	{
		parent::initialize($config);

		$this->setTable('languages');
		$this->setDisplayField('name');
		$this->setPrimaryKey('id');

		$this->addBehavior('Timestamp');
	}

	/**
	 * Default validation rules.
	 *
	 * @param \Cake\Validation\Validator $validator Validator instance.
	 * @return \Cake\Validation\Validator
	 */
	public function validationDefault(Validator $validator)
	{
		$validator
			->allowEmpty('id', 'create');

		$validator
			->requirePresence('name', 'create')
			->notEmpty('name');

		$validator
			->requirePresence('code', 'create')
			->notEmpty('code');

		$validator
			->requirePresence('locale', 'create')
			->notEmpty('locale');

		$validator
			->add('sort_order', 'valid', ['rule' => 'numeric'])
			->requirePresence('sort_order', 'create')
			->notEmpty('sort_order');

		$validator
			->add('status', 'valid', ['rule' => 'boolean'])
			->allowEmpty('status');

		return $validator;
	}
}
