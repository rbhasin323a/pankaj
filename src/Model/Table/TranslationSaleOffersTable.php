<?php
namespace App\Model\Table;

use App\Utility\DBUtils;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * TranslationSaleOffers Model
 *
 */
class TranslationSaleOffersTable extends Table
{

	public function beforeSave($event, $entity, $options)
	{
		if (empty($entity->id)) {
			$entity->id = $this->generate_id();
		}
	}

	public function generate_id($exists = false)
	{
		if (empty($exists)) {
			$new_id = strtoupper(DBUtils::secure_random_string(12));

			// Generate Hard ID
			$results = $this->findById($new_id);

			foreach ($results as $result) {
				return $this->generate_id(true);
			}

			return $new_id;
		} else {
			return $this->generate_id();
		}
	}

	/**
	 * Initialize method
	 *
	 * @param array $config The configuration for the Table.
	 * @return void
	 */
	public function initialize(array $config)
	{
		parent::initialize($config);

		$this->table('translation_sale_offers');
		$this->displayField('id');
		$this->primaryKey('id');
	}

	/**
	 * Default validation rules.
	 *
	 * @param \Cake\Validation\Validator $validator Validator instance.
	 * @return \Cake\Validation\Validator
	 */
	public function validationDefault(Validator $validator)
	{
		$validator
			->allowEmpty('id', 'create');

		$validator
			->requirePresence('locale', 'create')
			->notEmpty('locale');

		$validator
			->requirePresence('model', 'create')
			->notEmpty('model');

		$validator
			->requirePresence('foreign_key', 'create')
			->notEmpty('foreign_key');

		$validator
			->requirePresence('field', 'create')
			->notEmpty('field');

		$validator
			->allowEmpty('content');

		return $validator;
	}
}
