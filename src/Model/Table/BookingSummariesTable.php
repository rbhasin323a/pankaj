<?php
namespace App\Model\Table;

use App\Utility\DBUtils;
use Search\Manager;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * BookingSummaries Model
 *
 * @property \Cake\ORM\Association\BelongsTo $Bookings
 * @property \Cake\ORM\Association\BelongsTo $Sales
 * @property \Cake\ORM\Association\BelongsTo $Contractors
 * @property \Cake\ORM\Association\BelongsTo $SaleOffers
 * @property \Cake\ORM\Association\BelongsTo $Departures
 * @property \Cake\ORM\Association\BelongsTo $MyUsers
 * @property \Cake\ORM\Association\BelongsTo $Transactions
 * @property \Cake\ORM\Association\BelongsTo $Currencies
 * @property \Cake\ORM\Association\BelongsTo $Companies
 * @property \Cake\ORM\Association\BelongsTo $Suppliers
 */
class BookingSummariesTable extends Table
{

	public function beforeSave($event, $entity, $options)
	{
		if (empty($entity->id)) {
			$entity->id = $this->generate_id();
		}
	}

	public function searchConfiguration()
	{
		$search = new Manager($this);

		$search->like('q', [
			'before' => true,
			'after' => true,
			'field' => []
		]);

		return $search;
	}

	public function generate_id($exists = false)
	{
		if (empty($exists)) {
			$new_id = strtoupper(DBUtils::secure_random_string(12));

			// Generate Hard ID
			$results = $this->findById($new_id);

			foreach ($results as $result) {
				return $this->generate_id(true);
			}

			return $new_id;
		} else {
			return $this->generate_id();
		}
	}

	/**
	 * Initialize method
	 *
	 * @param array $config The configuration for the Table.
	 * @return void
	 */
	public function initialize(array $config)
	{
		parent::initialize($config);

		$this->addBehavior('Search.Search');

		$this->table('booking_summaries');
		$this->displayField('id');
		$this->primaryKey('id');

		$this->addBehavior('Timestamp');

		$this->belongsTo('Bookings', [
			'foreignKey' => 'booking_id',
			'joinType' => 'INNER'
		]);
		$this->belongsTo('Sales', [
			'foreignKey' => 'sale_id'
		]);
		$this->belongsTo('Contractors', [
			'foreignKey' => 'contractor_id'
		]);
		$this->belongsTo('SaleOffers', [
			'foreignKey' => 'sale_offer_id'
		]);
		$this->belongsTo('Departures', [
			'foreignKey' => 'departure_id'
		]);
		$this->belongsTo('MyUsers', [
			'foreignKey' => 'user_id'
		]);
		$this->belongsTo('Transactions', [
			'foreignKey' => 'transaction_id'
		]);
		$this->belongsTo('Currencies', [
			'foreignKey' => 'currency_id'
		]);
		$this->belongsTo('Companies', [
			'foreignKey' => 'company_id'
		]);
		$this->belongsTo('Suppliers', [
			'foreignKey' => 'supplier_id'
		]);
	}

	/**
	 * Default validation rules.
	 *
	 * @param \Cake\Validation\Validator $validator Validator instance.
	 * @return \Cake\Validation\Validator
	 */
	public function validationDefault(Validator $validator)
	{
		$validator
			->allowEmpty('id', 'create');

		$validator
			->requirePresence('adults', 'create')
			->notEmpty('adults');

		$validator
			->requirePresence('children', 'create')
			->notEmpty('children');

		$validator
			->requirePresence('infants', 'create')
			->notEmpty('infants');

		$validator
			->requirePresence('county', 'create')
			->notEmpty('county');

		$validator
			->requirePresence('pax', 'create')
			->notEmpty('pax');

		$validator
			->requirePresence('date_booked', 'create')
			->notEmpty('date_booked');

		$validator
			->requirePresence('check_in', 'create')
			->notEmpty('check_in');

		$validator
			->requirePresence('check_out', 'create')
			->notEmpty('check_out');

		$validator
			->add('no_nights', 'valid', ['rule' => 'numeric'])
			->allowEmpty('no_nights');

		$validator
			->add('rooms', 'valid', ['rule' => 'numeric'])
			->allowEmpty('rooms');

		$validator
			->requirePresence('territory', 'create')
			->notEmpty('territory');

		$validator
			->add('total_sell_rate_in_currency', 'valid', ['rule' => 'numeric'])
			->allowEmpty('total_sell_rate_in_currency');

		$validator
			->add('rate_to_gbp', 'valid', ['rule' => 'numeric'])
			->allowEmpty('rate_to_gbp');

		$validator
			->add('total_sell_rate', 'valid', ['rule' => 'numeric'])
			->allowEmpty('total_sell_rate');

		$validator
			->add('commission_ex_vat', 'valid', ['rule' => 'numeric'])
			->allowEmpty('commission_ex_vat');

		$validator
			->add('vat_on_commission', 'valid', ['rule' => 'numeric'])
			->allowEmpty('vat_on_commission');

		$validator
			->add('gross_commission', 'valid', ['rule' => 'numeric'])
			->allowEmpty('gross_commission');

		$validator
			->add('total_net_rate', 'valid', ['rule' => 'numeric'])
			->allowEmpty('total_net_rate');

		$validator
			->add('customer_total_price', 'valid', ['rule' => 'numeric'])
			->allowEmpty('customer_total_price');

		$validator
			->add('customer_payment', 'valid', ['rule' => 'numeric'])
			->allowEmpty('customer_payment');

		$validator
			->add('credits_used', 'valid', ['rule' => 'numeric'])
			->allowEmpty('credits_used');

		$validator
			->add('credit_amount_deductible_from_commission', 'valid', ['rule' => 'numeric'])
			->allowEmpty('credit_amount_deductible_from_commission');

		$validator
			->add('booking_fee_net_rate', 'valid', ['rule' => 'numeric'])
			->allowEmpty('booking_fee_net_rate');

		$validator
			->add('vat_on_booking_fee', 'valid', ['rule' => 'numeric'])
			->allowEmpty('vat_on_booking_fee');

		$validator
			->add('booking_fee', 'valid', ['rule' => 'numeric'])
			->allowEmpty('booking_fee');

		$validator
			->requirePresence('payment_type', 'create')
			->notEmpty('payment_type');

		$validator
			->add('payment_surcharge_net_rate', 'valid', ['rule' => 'numeric'])
			->allowEmpty('payment_surcharge_net_rate');

		$validator
			->add('vat_on_payment_surcharge', 'valid', ['rule' => 'numeric'])
			->allowEmpty('vat_on_payment_surcharge');

		$validator
			->add('payment_surcharge', 'valid', ['rule' => 'numeric'])
			->allowEmpty('payment_surcharge');

		$validator
			->requirePresence('type', 'create')
			->notEmpty('type');

		$validator
			->add('top_discount', 'valid', ['rule' => 'numeric'])
			->allowEmpty('top_discount');

		$validator
			->add('total_room_nights', 'valid', ['rule' => 'numeric'])
			->allowEmpty('total_room_nights');

		$validator
			->requirePresence('impulse', 'create')
			->notEmpty('impulse');

		$validator
			->requirePresence('notes', 'create')
			->notEmpty('notes');

		$validator
			->add('gross_profit', 'valid', ['rule' => 'numeric'])
			->allowEmpty('gross_profit');

		$validator
			->requirePresence('destination_name', 'create')
			->notEmpty('destination_name');

		$validator
			->requirePresence('destination_type', 'create')
			->notEmpty('destination_type');

		$validator
			->requirePresence('post_code', 'create')
			->notEmpty('post_code');

		$validator
			->requirePresence('country', 'create')
			->notEmpty('country');

		$validator
			->requirePresence('division', 'create')
			->notEmpty('division');

		$validator
			->requirePresence('city', 'create')
			->notEmpty('city');

		$validator
			->requirePresence('city_district', 'create')
			->notEmpty('city_district');

		$validator
			->add('total_custom_tax', 'valid', ['rule' => 'numeric'])
			->allowEmpty('total_custom_tax');

		return $validator;
	}

	/**
	 * Returns a rules checker object that will be used for validating
	 * application integrity.
	 *
	 * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
	 * @return \Cake\ORM\RulesChecker
	 */
	public function buildRules(RulesChecker $rules)
	{
		$rules->add($rules->existsIn(['booking_id'], 'Bookings'));
		$rules->add($rules->existsIn(['sale_id'], 'Sales'));
		$rules->add($rules->existsIn(['contractor_id'], 'Contractors'));
		$rules->add($rules->existsIn(['sale_offer_id'], 'SaleOffers'));
		$rules->add($rules->existsIn(['departure_id'], 'Departures'));
		$rules->add($rules->existsIn(['user_id'], 'MyUsers'));
		$rules->add($rules->existsIn(['transaction_id'], 'Transactions'));
		$rules->add($rules->existsIn(['currency_id'], 'Currencies'));
		$rules->add($rules->existsIn(['company_id'], 'Companies'));
		$rules->add($rules->existsIn(['supplier_id'], 'Suppliers'));
		return $rules;
	}
}
