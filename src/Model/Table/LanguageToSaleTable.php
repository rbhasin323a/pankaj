<?php
namespace App\Model\Table;

use Cake\ORM\Table;

class LanguageToSaleTable extends Table
{
	/**
	 * Initialize method
	 *
	 * @param array $config The configuration for the Table.
	 * @return void
	 */
	public function initialize(array $config)
	{
		parent::initialize($config);

		$this->table('language_to_sale');
		$this->displayField('id');
		$this->primaryKey('id');

		$this->addBehavior('Timestamp');

		$this->hasOne('Sale', [
			'foreignKey' => 'id',
			'bindingKey' => 'sale_id'
		]);
	}
}
