<?php
namespace App\Model\Table;

use Cake\ORM\Table;
use Cake\Validation\Validator;

class SaleToCategoryTable extends Table
{

	public function beforeSave($event, $entity, $options)
	{ }

	/**
	 * Initialize method
	 *
	 * @param array $config The configuration for the Table.
	 * @return void
	 */
	public function initialize(array $config)
	{
		parent::initialize($config);

		$this->table('sale_to_category');
		$this->displayField('id');
		$this->primaryKey('id');

		$this->addBehavior('Timestamp');

		$this->hasOne('Sales', [
			'foreignKey' => 'id',
			'bindingKey' => 'sale_id'
		]);

		$this->hasOne('Categories', [
			'foreignKey' => 'id',
			'bindingKey' => 'category_id'
		]);
	}

	/**
	 * Default validation rules.
	 *
	 * @param \Cake\Validation\Validator $validator Validator instance.
	 * @return \Cake\Validation\Validator
	 */
	public function validationDefault(Validator $validator)
	{

		return $validator;
	}
}
