<?php
namespace App\Model\Table;

use App\Utility\DBUtils;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Utility\Hash;
use Cake\Validation\Validator;

/**
 * Users Model
 */
class MyUsersTable extends Table
{
	/**
	 * Role Constants
	 */
	const ROLE_USER = 'user';
	const ROLE_ADMIN = 'admin';

	/**
	 * Flag to set email check in buildRules or not
	 *
	 * @var bool
	 */
	public $isValidateEmail = false;

	/**
	 * Initialize method
	 *
	 * @param array $config The configuration for the Table.
	 * @return void
	 */
	public function initialize(array $config)
	{
		parent::initialize($config);

		$this->table('users');
		$this->displayField('username');
		$this->primaryKey('id');
		$this->addBehavior('Timestamp');
		$this->addBehavior('CakeDC/Users.Register');
		$this->addBehavior('CakeDC/Users.Password');
		$this->addBehavior('CakeDC/Users.Social');
		$this->addBehavior('Search.Search');

		$this->hasMany('SocialAccounts', [
			'foreignKey' => 'user_id',
			'className' => 'CakeDC/Users.SocialAccounts'
		]);

		$this->hasMany('Bookings', [
			'foreignKey' => 'user_id'
		]);

		$this->hasMany('Suppliers', [
			'foreignKey' => 'user_id'
		]);

		// Setup search filter using search manager
		$this->searchManager()
			->value('author_id')
			->add('q', 'Search.Like', [
				'before' => true,
				'after' => true,
				'fieldMode' => 'OR',
				'comparison' => 'LIKE',
				'wildcardAny' => '*',
				'wildcardOne' => '?',
				'field' => ['first_name', 'last_name', 'email']
			])
			->add('foo', 'Search.Callback', [
				'callback' => function ($query, $args, $filter) {
					// Modify $query as required
				}
			]);
	}

	/**
	 * Adds some rules for password confirm
	 * @param Validator $validator Cake validator object.
	 * @return Validator
	 */
	public function validationPasswordConfirm(Validator $validator)
	{
		$validator
			->requirePresence('password_confirm', 'create')
			->notEmpty('password_confirm');

		$validator->add('password', 'custom', [
			'rule' => function ($value, $context) {
				$confirm = Hash::get($context, 'data.password_confirm');
				if (!is_null($confirm) && $value != $confirm) {
					return false;
				}

				return true;
			},
			'message' => __d('CakeDC/Users', 'Your password does not match your confirm password. Please try again'),
			'on' => ['create', 'update'],
			'allowEmpty' => false
		]);

		return $validator;
	}

	/**
	 * Adds rules for current password
	 *
	 * @param Validator $validator Cake validator object.
	 * @return Validator
	 */
	public function validationCurrentPassword(Validator $validator)
	{
		$validator
			->notEmpty('current_password');

		return $validator;
	}

	public function beforeSave($event, $entity, $options)
	{
		if (empty($entity->username)) {
			$entity->username = $this->generate_id(false, 'username');
		}
	}

	public function generate_id($exists = false, $key = 'id')
	{
		if (empty($exists)) {
			$new_id = strtoupper(DBUtils::secure_random_string(12));

			// Generate Hard ID
			$existing = $this->find('all', ['conditions' => [$key => $new_id]])->first();

			if (!empty($existing)) {
				return $this->generate_id(true, $key);
			}

			return $new_id;
		} else {
			return $this->generate_id(false, $key);
		}
	}

	/**
	 * Default validation rules.
	 *
	 * @param Validator $validator Validator instance.
	 * @return Validator
	 */
	public function validationDefault(Validator $validator)
	{
		$validator
			->allowEmpty('id', 'create');

		$validator
			->allowEmpty('username');

		$validator
			->requirePresence('password', 'create')
			->notEmpty('password');

		$validator
			->allowEmpty('first_name');

		$validator
			->allowEmpty('last_name');

		$validator
			->allowEmpty('birthday');

		$validator
			->allowEmpty('phone_mobile');

		$validator
			->allowEmpty('phone_home');

		$validator
			->allowEmpty('token');

		$validator
			->add('token_expires', 'valid', ['rule' => 'datetime'])
			->allowEmpty('token_expires');

		$validator
			->allowEmpty('api_token');

		$validator
			->add('activation_date', 'valid', ['rule' => 'datetime'])
			->allowEmpty('activation_date');

		$validator
			->add('tos_date', 'valid', ['rule' => 'datetime'])
			->allowEmpty('tos_date');

		$validator
			->allowEmpty('newsletter');

		$validator
			->allowEmpty('newsletter_type');

		return $validator;
	}

	/**
	 * Wrapper for all validation rules for register
	 * @param Validator $validator Cake validator object.
	 *
	 * @return Validator
	 */
	public function validationRegister(Validator $validator)
	{
		$validator = $this->validationDefault($validator);
		$validator = $this->validationPasswordConfirm($validator);

		return $validator;
	}

	/**
	 * Returns a rules checker object that will be used for validating
	 * application integrity.
	 *
	 * @param RulesChecker $rules The rules object to be modified.
	 * @return RulesChecker
	 */
	public function buildRules(RulesChecker $rules)
	{
		$rules->add($rules->isUnique(['email']), '_isUnique', [
			'errorField' => 'email',
			'message' => __d('CakeDC/Users', 'Email already exists')
		]);

		return $rules;
	}

	/**
	 * Custom finder to filter active users
	 *
	 * @param Query $query Query object to modify
	 * @param array $options Query options
	 * @return Query
	 */
	public function findActive(Query $query, array $options = [])
	{
		$query->where([$this->aliasField('active') => 1]);

		return $query;
	}

	/**
	 * Custom finder to log in users
	 *
	 * @param Query $query Query object to modify
	 * @param array $options Query options
	 * @return Query
	 * @throws \BadMethodCallException
	 */
	public function findAuth(Query $query, array $options = [])
	{
		$identifier = Hash::get($options, 'username');
		if (empty($identifier)) {
			throw new \BadMethodCallException(__d('CakeDC/Users', 'Missing \'username\' in options data'));
		}

		$query
			->orWhere([$this->aliasField('email') => $identifier])
			->find('active', $options);

		return $query;
	}
}
