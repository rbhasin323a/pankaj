<?php
namespace App\Model\Table;

use App\Utility\DBUtils;
use Search\Manager;

use App\Model\Entity\Country;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

class ImageToSaleTable extends Table {

    public function beforeSave($event, $entity, $options) {
        
    }

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('image_to_sale');
        $this->displayField('image_to_sale_id');
        $this->primaryKey('image_to_sale_id');

        $this->addBehavior('Timestamp');
        
        $this->addBehavior('Translate', [
			'fields' => [
				'image_alt',
			],
			'translationTable' => 'TranslationImageToSale'
		]);

    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->allowEmpty('image_alt');
        
        $validator
            ->requirePresence('image_path', 'sale_id');

        return $validator;
    }
}
