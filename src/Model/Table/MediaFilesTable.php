<?php
namespace App\Model\Table;

use App\Utility\DBUtils;
use Search\Manager;

use App\Model\Entity\MediaFile;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * MediaFiles Model
 *
 */
class MediaFilesTable extends Table {

    public function beforeSave($event, $entity, $options) {
        if (empty($entity->id) ) {
            $entity->id = $this->generate_id();
        }
    }

    public function searchConfiguration(){
        $search = new Manager($this);

        $search->like('q', [
            'before' => true,
            'after' => true,
            'field' => []
        ]);

        return $search;
    }

    public function generate_id($exists = false) {
        if (empty($exists)) {
            $new_id = strtoupper(DBUtils::secure_random_string(12));
            
            // Generate Hard ID
            $results = $this->findById($new_id);
            
            foreach ($results as $result) {
                return $this->generate_id(true);
            }

            return $new_id;
        } else {
            return $this->generate_id();
        }
    }

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->addBehavior('Search.Search');

        $this->table('media_files');
        $this->displayField('name');
        $this->primaryKey('id');

        $this->addBehavior('Timestamp');

    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->allowEmpty('id', 'create');

        $validator
            ->allowEmpty('name');

        $validator
            ->requirePresence('filename', 'create')
            ->notEmpty('filename');

        return $validator;
    }
}
