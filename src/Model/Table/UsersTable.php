<?php
namespace App\Model\Table;

use Cake\ORM\Table;

/**
 * Sales Model
 *
 * @property \Cake\ORM\Association\BelongsTo $Contractors
 * @property \Cake\ORM\Association\BelongsTo $Suppliers
 * @property \Cake\ORM\Association\BelongsTo $Companies
 * @property \Cake\ORM\Association\BelongsTo $Currencies
 * @property \Cake\ORM\Association\BelongsTo $Countries
 * @property \Cake\ORM\Association\BelongsTo $Taxes
 * @property \Cake\ORM\Association\HasMany $SaleOffers
 */
class UsersTable extends Table
{
	/**
	 * Initialize method
	 *
	 * @param array $config The configuration for the Table.
	 * @return void
	 */
	public function initialize(array $config)
	{
		parent::initialize($config);
		$this->addBehavior('Search.Search');

		$this->table('users');
		$this->primaryKey('id');

		$this->hasMany('Bookings', [
			'foreignKey' => 'user_id'
		]);

		$this->hasMany('Suppliers', [
			'foreignKey' => 'user_id'
		]);

		// Setup search filter using search manager
		$this->searchManager()
			->value('author_id')
			->add('q', 'Search.Like', [
				'before' => true,
				'after' => true,
				'fieldMode' => 'OR',
				'comparison' => 'LIKE',
				'wildcardAny' => '*',
				'wildcardOne' => '?',
				'field' => ['first_name', 'last_name', 'email']
			])
			->add('foo', 'Search.Callback', [
				'callback' => function ($query, $args, $filter) {
					// Modify $query as required
				}
			]);
	}

	public function beforeSave($event, $entity, $options)
	{ }
}
