<?php
namespace App\Model\Table;

use App\Utility\DBUtils;
use Search\Manager;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Bookings Model
 *
 * @property \Cake\ORM\Association\BelongsTo $Transactions
 * @property \Cake\ORM\Association\BelongsTo $Payments
 * @property \Cake\ORM\Association\BelongsTo $MyUsers
 * @property \Cake\ORM\Association\BelongsTo $Sales
 * @property \Cake\ORM\Association\HasMany $BookingSummaries
 */
class BookingsTable extends Table
{

	public function beforeSave($event, $entity, $options)
	{
		if (empty($entity->id)) {
			$entity->id = $this->generate_id();
		}
	}

	public function searchConfiguration()
	{
		$search = new Manager($this);

		$search->like('q', [
			'before' => true,
			'after' => true,
			'field' => []
		]);

		return $search;
	}

	public function generate_id($exists = false)
	{
		if (empty($exists)) {
			$new_id = strtoupper(DBUtils::secure_random_string(12));

			// Generate Hard ID
			$results = $this->findById($new_id);

			foreach ($results as $result) {
				return $this->generate_id(true);
			}

			return $new_id;
		} else {
			return $this->generate_id();
		}
	}

	/**
	 * Initialize method
	 *
	 * @param array $config The configuration for the Table.
	 * @return void
	 */
	public function initialize(array $config)
	{
		parent::initialize($config);

		$this->addBehavior('Search.Search');

		$this->table('bookings');
		$this->displayField('id');
		$this->primaryKey('id');

		$this->addBehavior('Timestamp');

		$this->belongsTo('Transactions', [
			'foreignKey' => 'transaction_id',
			'joinType' => 'INNER'
		]);

		$this->belongsTo('MyUsers', [
			'foreignKey' => 'user_id',
			'joinType' => 'INNER'
		]);

		$this->belongsTo('Sales', [
			'foreignKey' => 'sale_id',
			'joinType' => 'INNER'
		]);

		$this->belongsTo('SaleOffers', [
			'foreignKey' => 'offer_id',
			'joinType' => 'INNER'
		]);

		$this->hasOne('SaleAllocations', [
			'foreignKey' => 'id',
			'bindingKey' => 'sale_allocation_id'
		]);

		$this->hasMany('BookingTravellers', [
			'foreignKey' => 'booking_id'
		]);

		$this->hasMany('BookingAllocations', [
			'foreignKey' => 'booking_id'
		]);
		/*$this->hasMany('BookingSummaries', [
            'foreignKey' => 'booking_id'
        ]);*/
	}

	/**
	 * Default validation rules.
	 *
	 * @param \Cake\Validation\Validator $validator Validator instance.
	 * @return \Cake\Validation\Validator
	 */
	public function validationDefault(Validator $validator)
	{
		$validator
			->allowEmpty('id', 'create');

		return $validator;
	}

	/**
	 * Returns a rules checker object that will be used for validating
	 * application integrity.
	 *
	 * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
	 * @return \Cake\ORM\RulesChecker
	 */
	public function buildRules(RulesChecker $rules)
	{
		$rules->add($rules->existsIn(['transaction_id'], 'Transactions'));
		$rules->add($rules->existsIn(['user_id'], 'MyUsers'));
		$rules->add($rules->existsIn(['sale_id'], 'Sales'));
		return $rules;
	}
}
