<?php
namespace App\Model\Table;

use Cake\ORM\Table;

/**
 * Sales Model
 *
 * @property \Cake\ORM\Association\BelongsTo $Contractors
 * @property \Cake\ORM\Association\BelongsTo $Suppliers
 * @property \Cake\ORM\Association\BelongsTo $Companies
 * @property \Cake\ORM\Association\BelongsTo $Currencies
 * @property \Cake\ORM\Association\BelongsTo $Countries
 * @property \Cake\ORM\Association\BelongsTo $Taxes
 * @property \Cake\ORM\Association\HasMany $SaleOffers
 */
class UserRolesTable extends Table
{
	/**
	 * Initialize method
	 *
	 * @param array $config The configuration for the Table.
	 * @return void
	 */
	public function initialize(array $config)
	{
		parent::initialize($config);
		$this->addBehavior('Search.Search');

		$this->table('user_roles');
		$this->primaryKey('id');
	}
}
