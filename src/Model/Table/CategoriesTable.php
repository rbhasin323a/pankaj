<?php
namespace App\Model\Table;

use App\Utility\DBUtils;
use Search\Manager;

use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Categories Model
 *
 * @property \Cake\ORM\Association\BelongsTo $Contractors
 * @property \Cake\ORM\Association\BelongsTo $Suppliers
 * @property \Cake\ORM\Association\BelongsTo $Companies
 * @property \Cake\ORM\Association\BelongsTo $Currencies
 * @property \Cake\ORM\Association\BelongsTo $Countries
 * @property \Cake\ORM\Association\BelongsTo $Taxes
 */
class CategoriesTable extends Table
{
	/**
	 * Initialize method
	 *
	 * @param array $config The configuration for the Table.
	 * @return void
	 */
	public function initialize(array $config)
	{
		parent::initialize($config);

		$this->addBehavior('Search.Search');

		$this->table('categories');
		$this->displayField('id');
		$this->primaryKey('id');

		$this->addBehavior('Timestamp');

		$this->addBehavior('Translate', [
			'fields' => [
				'title'
			],
			'translationTable' => 'TranslationCategories'
		]);

		$this->hasMany('SaleToCategory', [
			'foreignKey' => 'category_id',
			'dependent' => true
		]);
	}

	public function beforeSave($event, $entity, $options)
	{
		if (empty($entity->id)) {
			$entity->id = $this->generate_id();
		}
	}

	public function generate_id($exists = false)
	{
		if (empty($exists)) {
			$new_id = strtoupper(DBUtils::secure_random_string(12));

			// Generate Hard ID
			$results = $this->findById($new_id);

			foreach ($results as $result) {
				return $this->generate_id(true);
			}

			return $new_id;
		} else {
			return $this->generate_id();
		}
	}

	public function searchConfiguration()
	{
		$search = new Manager($this);

		$search->value('id', [
			'field' => $this->aliasField('id')
		])->like('q', [
			'before' => true,
			'after' => true,
			'field' => [$this->aliasField('title'), $this->aliasField('sub_title'), $this->aliasField('destination'), $this->aliasField('location_title')]
		]);

		return $search;
	}

	/**
	 * Default validation rules.
	 *
	 * @param \Cake\Validation\Validator $validator Validator instance.
	 * @return \Cake\Validation\Validator
	 */
	public function validationDefault(Validator $validator)
	{
		$validator
			->allowEmpty('id', 'create');

		$validator
			->allowEmpty('image', 'update');

		$validator
			->notEmpty('title', 'Please fill this field');

		$validator
			->add('columns', 'valid', [
				'rule' => ['range', 1, 12],
				'message' => 'Value should be between 1 and 12'
			])
			->notEmpty('columns');

		return $validator;
	}

	/**
	 * Returns a rules checker object that will be used for validating
	 * application integrity.
	 *
	 * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
	 * @return \Cake\ORM\RulesChecker
	 */
	public function buildRules(RulesChecker $rules)
	{
		$rules->add($rules->existsIn(['category_id'], 'SaleToCategory'));
		return $rules;
	}
}
