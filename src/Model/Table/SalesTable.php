<?php
namespace App\Model\Table;

use App\Utility\DBUtils;
use Search\Manager;

use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Sales Model
 *
 * @property \Cake\ORM\Association\BelongsTo $Contractors
 * @property \Cake\ORM\Association\BelongsTo $Suppliers
 * @property \Cake\ORM\Association\BelongsTo $Companies
 * @property \Cake\ORM\Association\BelongsTo $Currencies
 * @property \Cake\ORM\Association\BelongsTo $Countries
 * @property \Cake\ORM\Association\BelongsTo $Taxes
 * @property \Cake\ORM\Association\HasMany $SaleOffers
 */
class SalesTable extends Table
{
	/**
	 * Initialize method
	 *
	 * @param array $config The configuration for the Table.
	 * @return void
	 */
	public function initialize(array $config)
	{
		parent::initialize($config);

		$this->addBehavior('Search.Search');

		$this->table('sales');
		$this->displayField('id');
		$this->primaryKey('id');

		$this->addBehavior('Timestamp');

		$this->belongsTo('Contractors', [
			'foreignKey' => 'contractor_id'
		]);

		$this->belongsTo('Suppliers', [
			'foreignKey' => 'supplier_id'
		]);

		$this->belongsTo('Companies', [
			'foreignKey' => 'company_id'
		]);

		$this->belongsTo('Countries', [
			'foreignKey' => 'country_id'
		]);

		$this->belongsTo('Taxes', [
			'foreignKey' => 'tax_id'
		]);

		$this->addBehavior('Translate', [
			'fields' => [
				'title',
				'sub_title',
				'destination',
				'slug',
				'main_description',
				'staff_description',
				'hotel_description',
				'travel_description',
				'good_for',
				'location_title',
				'meta_title',
				'meta_description'
			],
			'translationTable' => 'TranslationSales'
		]);

		$this->hasMany('Bookings', [
			'foreignKey' => 'sale_id'
		]);

		$this->hasMany('SaleOffers', [
			'foreignKey' => 'sale_id',
			'dependent' => true
		]);

		$this->hasMany('ImageToSale', [
			'foreignKey' => 'sale_id',
			'dependent' => true
		]);

		$this->hasMany('SaleToCategory', [
			'foreignKey' => 'sale_id',
			'dependent' => true
		]);

		$this->hasOne('Currencies', [
			'foreignKey' => 'id',
			'bindingKey' => 'currency_id'
		]);

		$this->hasMany('LanguageToSale', [
			'foreignKey' => 'sale_id',
			'dependent' => true
		]);
	}

	public function beforeSave($event, $entity, $options)
	{
		if (empty($entity->id)) {
			$entity->id = $this->generate_id();
		}
	}

	public function generate_id($exists = false)
	{
		if (empty($exists)) {
			$new_id = strtoupper(DBUtils::secure_random_string(12));

			// Generate Hard ID
			$results = $this->findById($new_id);

			foreach ($results as $result) {
				return $this->generate_id(true);
			}

			return $new_id;
		} else {

			return $this->generate_id();
		}
	}

	public function searchConfiguration()
	{
		$search = new Manager($this);

		$search->value('id', [
			'field' => $this->aliasField('id')
		])->like('q', [
			'before' => true,
			'after' => true,
			'field' => [$this->aliasField('title'), $this->aliasField('sub_title'), $this->aliasField('destination'), $this->aliasField('location_title'), $this->aliasField('SalesToCategory.category_id')]
		]);

		return $search;
	}

	/**
	 * Default validation rules.
	 *
	 * @param \Cake\Validation\Validator $validator Validator instance.
	 * @return \Cake\Validation\Validator
	 */
	public function validationDefault(Validator $validator)
	{
		$validator
			->allowEmpty('id', 'create');

		$validator
			->notEmpty('title', 'Please fill this field');

		$validator
			->allowEmpty('sub_title');

		$validator
			->notEmpty('destination', 'Please fill this field');

		$validator
			->allowEmpty('slug', 'create');

		$validator
			->notEmpty('main_description', 'Please fill this field');

		$validator
			->allowEmpty('staff_description');

		$validator
			->notEmpty('hotel_description', 'Please fill this field');

		$validator
			->notEmpty('travel_description', 'Please fill this field');

		$validator
			->allowEmpty('location_title');

		$validator
			->allowEmpty('location_map');

		$validator
			->allowEmpty('date_open');

		$validator
			->allowEmpty('date_close');

		$validator
			->allowEmpty('sale_type');

		$validator
			->allowEmpty('board_type');

		$validator
			->add('promoted', 'valid', [
				'rule' => ['range', 0, 100],
				'message' => 'Value should be between 0 and 100'
			])
			->allowEmpty('promoted');

		$validator
			->allowEmpty('available_domains');

		$validator
			->add('enable_hold', 'valid', ['rule' => 'boolean'])
			->allowEmpty('enable_hold');

		$validator
			->add('commission', 'valid', ['rule' => 'numeric'])
			->notEmpty('commission', 'Please fill this field');

		$validator
			->allowEmpty('top_discounts');

		$validator
			->allowEmpty('commission_type');

		return $validator;
	}

	/**
	 * Returns a rules checker object that will be used for validating
	 * application integrity.
	 *
	 * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
	 * @return \Cake\ORM\RulesChecker
	 */
	public function buildRules(RulesChecker $rules)
	{
		$rules->add($rules->existsIn(['contractor_id'], 'Contractors'));
		$rules->add($rules->existsIn(['supplier_id'], 'Suppliers'));
		$rules->add($rules->existsIn(['company_id'], 'Companies'));
		$rules->add($rules->existsIn(['country_id'], 'Countries'));
		$rules->add($rules->existsIn(['tax_id'], 'Taxes'));

		return $rules;
	}
}
