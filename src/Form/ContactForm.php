<?php

namespace App\Form;

use Cake\Form\Form;
use Cake\Form\Schema;
use Cake\Validation\Validator;
use Cake\Mailer\Email;
use Cake\Core\Configure;

class ContactForm extends Form
{

    protected function _buildSchema(Schema $schema)
    {
        return $schema->addField('name', 'string')
            ->addField('email', ['type' => 'string'])
            ->addField('message', ['type' => 'text']);
    }

    protected function _buildValidator(Validator $validator)
    {
        return $validator
		->add('name', 'length', [
            'rule' => ['minLength', 10],
            'message' => __('Name is required')
        ])->add('email', 'format', [
            'rule' => 'email',
            'message' => __('Valid email address is required')
        ])->add('message', 'length', [
            'rule' => ['maxLength', 250],
            'message' => __('Message is too long')
        ]);
    }

    protected function _execute(array $data) {
		$email = new Email();
        
		$email->from([$data['email'] => $data['name']])
			->to(Configure::read('EmailAccounts.support'))
			->subject('New Support Request')
			->send($data['message']);

        return true;
    }
}
