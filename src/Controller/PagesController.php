<?php
namespace App\Controller;

use Cake\Core\Configure;
use Cake\Event\Event;
use Cake\Network\Exception\NotFoundException;
use Cake\View\Exception\MissingTemplateException;
use Cake\I18n\I18n;

class PagesController extends AppController
{
	public function beforeFilter(Event $event)
	{
		parent::beforeFilter($event);

		$this->Auth->allow();
	}


	public function display()
	{

		$this->loadModel('TranslationSeo');
		$this->loadModel('Seo');

		$path = func_get_args();

		$count = count($path);

		if (!$count) {
			if (isset($_COOKIE['language']) && in_array($_COOKIE['language'], array('en', 'bg', 'tr'))) {
				$target = '/' . $_COOKIE['language'];
			} else if (isset($_COOKIE['autolanguage']) && in_array($_COOKIE['autolanguage'], array('en', 'bg', 'tr'))) {
				$target = '/' . $_COOKIE['autolanguage'];
			} else {
				$target = '/';
			}

			if ($target != $this->request->getAttribute('here') && ($target . '/') != $this->request->getAttribute('here')) {
				return $this->redirect($target);
			}
		}

		$page = $subpage = null;

		if (!empty($path[0])) {
			$page = $path[0];
		}

		if (!empty($path[1])) {
			$subpage = $path[1];
		}

		//load the SEO titles
		$query = $this->Seo->find()->where(['Seo.page' => $page]);
		$seo_data = $query->first();

		$seo_title = isset($seo_data['seo_title']) ? $seo_data['seo_title'] : '';
		$seo_description = isset($seo_data['seo_description']) ? $seo_data['seo_description'] : '';

		$this->set(compact('page', 'subpage', 'seo_title', 'seo_description'));

		try {
			if (file_exists(APP . 'Template' . DS . $this->_viewPath() . DS . I18n::locale() . DS . $path[0] . '.ctp')) {
				$path[0] =  I18n::locale() . '/' . $path[0];
			}

			$this->render(implode('/', $path));
		} catch (MissingTemplateException $e) {
			if (Configure::read('debug')) {
				throw $e;
			}
			throw new NotFoundException();
		}
	}
}
