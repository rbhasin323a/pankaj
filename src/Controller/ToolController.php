<?php
namespace App\Controller;

use Cake\Event\Event;
use App\Controller\AppController;

class ToolController extends AppController
{
	public function beforeFilter(Event $event)
	{
		parent::beforeFilter($event);

		$this->Auth->allow();
	}
}
