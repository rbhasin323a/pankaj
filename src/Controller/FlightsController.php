<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Event\Event;


class FlightsController extends AppController
{
	public function initialize()
	{
		parent::initialize();

		if ($this->request->action === 'index') {
			$this->loadComponent('Search.Prg');
		}
	}

	public function beforeFilter(Event $event)
	{
		parent::beforeFilter($event);
		$this->Auth->allow(['index', 'breturn']);
	}

	public function index()
	{
		$this->loadModel('TranslationSeo');
		$this->loadModel('TranslationFooter');
		$this->loadModel('Seo');
		$this->loadModel('Footer');

		//load the SEO titles
		$query = $this->Seo->find()->where(['Seo.page' => 'flights']);
		$seo_data = $query->first();

		$seo_title = isset($seo_data['seo_title']) ? $seo_data['seo_title'] : '';
		$seo_description = isset($seo_data['seo_description']) ? $seo_data['seo_description'] : '';

		//load footer
		$query_footer = $this->Footer->find()->where(['Footer.id' => 'XANQW4QFAODS']);
		$footer_data = $query_footer->first();
		$footer_menu = $footer_data['menu_content'];
		$footer_copyright_text = $footer_data['copyright_text'];

		$this->set('footer_menu', $footer_menu);
		$this->set('footer_copyright_text', $footer_copyright_text);
		$this->set('seo_title', $seo_title);
		$this->set('seo_description', $seo_description);
	}
}
