<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Event\Event;

require_once(MALDOPAY_PATH . 'maldopay.php');

/**
 * Sales Controller
 *
 * @property \App\Model\Table\SalesTable $Sales
 */
class MaldopayController extends AppController
{
	public function initialize()
	{
		parent::initialize();
	}

	public function beforeFilter(Event $event)
	{
		parent::beforeFilter($event);
		$this->Auth->allow();
	}

	/**
	 * Index method
	 *
	 * @return void
	 */

	public function index()
	{
		file_put_contents(LOGS . 'maldopay_all_logs.txt', var_export($_REQUEST, true), FILE_APPEND);

		$maldopay_response = isset($_REQUEST) ? $_REQUEST : '';
		$transaction_result = isset($maldopay_response['result']) ? $maldopay_response['result'] : '';

		if (!empty($maldopay_response)) {
			if ($transaction_result == 'CONFIRMED') {
				$this->loadModel('Bookings');
				$booking = $this->Bookings->get($maldopay_response['referenceOrderId']);
				$booking->transaction_status = 0;
				$this->Bookings->save($booking);
			}

			exit;
		}
	}
}
