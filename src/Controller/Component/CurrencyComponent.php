<?php

namespace App\Controller\Component;

use Cake\ORM\TableRegistry;
use Cake\Controller\Component;
use Cake\I18n\Number;

class CurrencyComponent extends Component
{

	public function convert($from_currency_id = 1, $to_currency_id = null, $amount, $enable_format = true, $round_up = true, array $options = [])
	{
		$formatter = Number::formatter(['type' => Number::FORMAT_CURRENCY] + $options);

		$session_currency = $this->request->getSession()->read('Config.currency');

		if (is_null($to_currency_id)) {
			if (!empty($session_currency)) {
				$to_currency = $session_currency;
			} else {
				$to_currency = Number::defaultCurrency();
			}
		} else {
			$to_currency = TableRegistry::get('Currencies')->findById($to_currency_id)->first()->code;
		}

		if ($to_currency == "TRY") {
			$to_currency = "EUR";
		}

		if ($to_currency == "USD") {
			$to_currency = "EUR";
		}

		if (!empty($session_currency)) {
			$to_currency = $session_currency;
		}

		$abs = abs($amount);

		$before = isset($options['before']) ? $options['before'] : null;
		$after = isset($options['after']) ? $options['after'] : null;
		$rate = 1;

		$from_currency_query = TableRegistry::get('Currencies')->get($from_currency_id);

		if (!empty($from_currency_query)) {
			$from_currency = $from_currency_query->code;
		}

		if ($from_currency != $to_currency) {

			$arrReturn = $this->checkToFind($from_currency, $to_currency);

			if (isset($arrReturn['rate'])) {
				$rate = $arrReturn['rate'];
			}

			if ($enable_format) {
				if (!empty($options['fractionSymbol']) && $abs > 0 && $abs < 1) {
					$amount = ceil($amount * 100);
					$pos = isset($options['fractionPosition']) ? $options['fractionPosition'] : 'after';

					return Number::format($amount, ['precision' => 0, $pos => $options['fractionSymbol']]);
				}

				return $before . $formatter->formatCurrency(ceil($amount * $rate), $to_currency) . $after;
			} else {
				return number_format(ceil($amount) * $rate, 8);
			}
		} else {
			if ($enable_format) {
				return $before . $formatter->formatCurrency(ceil($amount * $rate), $to_currency) . $after;
			} else {
				return ceil($amount * $rate);
			}
		}
	}

	public function checkToFind($from_currency, $to_currency, $hourDifference = 6)
	{
		$arrReturn = [];
		$find = 0;
		$rate = 0;

		$currency_rates = TableRegistry::get('CurrencyRates');

		$result = $currency_rates->find('all')
			->where(['fromCurrency' => $from_currency, 'toCurrency' => $to_currency]);

		foreach ($result as $row) {
			$find = 1;

			$lastUpdated = date('Y-m-d H:i:s', strtotime($row['modified']->i18nFormat('yyyy-MM-dd HH:mm:ss')));
			$now = date('Y-m-d H:i:s');
			$dStart = new \DateTime($now);
			$dEnd = new \DateTime($lastUpdated);
			$diff = $dStart->diff($dEnd);

			/* Remove false if you want to update rates from remote service */
			if (false && (((int)$diff->y >= 1) || ((int)$diff->m >= 1) || ((int)$diff->d >= 1) || ((int)$diff->h >= $hourDifference) || ((double)$row['rates'] == 0))) {
				$rate = $this->_getRates($from_currency, $to_currency);

				$row['rates'] = $rate;
				$row['modified'] = date('Y-m-d H:i:s');
				$entity = $currency_rates->newEntity($row->toArray());
				$currency_rates->save($entity);
			} else {
				$rate = $row['rates'];
			}
		}

		$arrReturn['find'] = $find;
		$arrReturn['rate'] = $rate;

		return ($arrReturn);
	}

	public function applySign($total)
	{
		$formatter = Number::formatter(['type' => Number::FORMAT_CURRENCY]);

		$to_currency = Number::defaultCurrency();

		if ($to_currency == "TRY") {
			$to_currency = "EUR";
		}

		if ($to_currency == "USD") {
			$to_currency = "EUR";
		}

		return $formatter->formatCurrency($total, $to_currency);
	}

	private function _getRates($from_currency, $to_currency)
	{
		/* Yahoo Finance does not work anymore. Use something else. */
		$url = 'http://finance.yahoo.com/d/quotes.csv?e=.csv&f=sl1d1t1&s=' . $from_currency . $to_currency . '=X';
		$handle = @fopen($url, 'r');

		if ($handle) {
			$result = fgets($handle, 4096);
			fclose($handle);
		}

		if (isset($result)) {
			$allData = explode(',', $result); /* Get all the contents to an array */
			$rate = $allData[1];
		} else {
			$rate = 0;
		}

		return ($rate);
	}
}
