<?php
namespace App\Controller\Admin;

use App\Controller\AppController;

/**
 * Bookings Controller
 *
 * @property \App\Model\Table\BookingsTable $Bookings
 */
class BookingsController extends AppController
{
	public function initialize()
	{
		parent::initialize(true);

		$this->loadModel('MyUsers');
	}

	/**
	 * Index method
	 *
	 * @return void
	 */
	public function index()
	{
		$this->paginate = [
			'limit' => 24,
			'order' => [
				'created' => 'desc'
			],
			'contain' => ['Transactions', 'MyUsers', 'Sales.Currencies']
		];

		if (!empty($this->request->query)) {
			$conditions = [];

			// Notes
			if (isset($this->request->query['filter_notes']) && $this->request->query['filter_notes'] != 'all') {
				$filter_notes = [];

				if (!$this->request->query['filter_notes']) {
					$filter_notes = array('notes' => '');
				} elseif ($this->request->query['filter_notes']) {
					$filter_notes = array('not' => array('notes' => ''));
				}

				$conditions = array_merge($conditions, $filter_notes);
			}

			// Supplier
			if (isset($this->request->query['filter_supplier']) && !empty($this->request->query['filter_supplier'])) {
				$filter_supplier = array('Sales.supplier_id' => $this->request->query['filter_supplier']);

				$conditions = array_merge($conditions, $filter_supplier);
			}

			// Sale
			if (isset($this->request->query['filter_sale']) && !empty($this->request->query['filter_sale'])) {
				$filter_sale = array('sale_id' => $this->request->query['filter_sale']);

				$conditions = array_merge($conditions, $filter_sale);
			}

			// Date From
			if (isset($this->request->query['filter_date_from']) && !empty($this->request->query['filter_date_from'])) {
				$filter_date_from = array('Bookings.created >=' => $this->request->query['filter_date_from']);

				$conditions = array_merge($conditions, $filter_date_from);
			}

			// Date To
			if (isset($this->request->query['filter_date_to']) && !empty($this->request->query['filter_date_to'])) {
				$filter_date_to = array('Bookings.created <=' => $this->request->query['filter_date_to']);

				$conditions = array_merge($conditions, $filter_date_to);
			}

			$query = $this->Bookings->find('all')->contain(['Sales'])->where($conditions);
		} else {
			$query = $this->Bookings->find('all')->contain(['Sales']);
		}

		$query = $this->paginate($query);

		foreach ($query as $booking) {
			$booking->sale_total = $this->Currency->convert($booking->currency_id, 1, $booking->sale_total);
		}

		// Supplier filter
		$this->loadModel('Suppliers');

		$suppliers_query = $this->Suppliers->find('all');

		// Sale filter
		$this->loadModel('Sales');

		$sales_query = $this->Sales->find('all');

		$this->set('sales', $sales_query);
		$this->set('suppliers', $suppliers_query);
		$this->set('bookings', $query);
	}

	/**
	 * View method
	 *
	 * @param string|null $id Booking id.
	 * @return void
	 * @throws \Cake\Network\Exception\NotFoundException When record not found.
	 */
	public function view($id = null)
	{
		$booking = $this->Bookings->get($id, [
			'contain' => ['Transactions', 'MyUsers', 'Sales', 'BookingSummaries']
		]);

		$this->set('booking', $booking);
	}

	/**
	 * Add method
	 *
	 * @return void Redirects on successful add, renders view otherwise.
	 */
	public function add()
	{
		$booking = $this->Bookings->newEntity();
		if ($this->request->is('post')) {
			$booking = $this->Bookings->patchEntity($booking, $this->request->data);
			if ($this->Bookings->save($booking)) {
				$this->Flash->success(__('The booking has been saved.'));
				return $this->redirect(['action' => 'index']);
			} else {
				$this->Flash->error(__('The booking could not be saved. Please, try again.'));
			}
		}

		$transactions = $this->Bookings->Transactions->find('list', ['limit' => 200]);
		$users = $this->Bookings->MyUsers->find('list', ['limit' => 200]);
		$sales = $this->Bookings->Sales->find('list', ['limit' => 200]);
		$this->set(compact('booking', 'transactions', 'users', 'sales'));
	}

	/**
	 * Edit method
	 *
	 * @param string|null $id Booking id.
	 * @return void Redirects on successful edit, renders view otherwise.
	 * @throws \Cake\Network\Exception\NotFoundException When record not found.
	 */
	public function edit($id = null)
	{
		$booking = $this->Bookings->get($id, [
			'contain' => ['BookingTravellers']
		]);

		if ($this->request->is(['patch', 'post', 'put'])) {
			$booking = $this->Bookings->patchEntity($booking, $this->request->data);
			if ($this->Bookings->save($booking)) {
				$this->Flash->success(__('The booking has been saved.'));
				return $this->redirect(['action' => 'index']);
			} else {
				$this->Flash->error(__('The booking could not be saved. Please, try again.'));
			}
		}

		$transactions = $this->Bookings->Transactions->find('list', ['limit' => 200]);
		$users = $this->Bookings->MyUsers->find('list', ['limit' => 200]);
		$sales = $this->Bookings->Sales->find('list', ['limit' => 200]);

		$this->set(compact('booking', 'transactions', 'users', 'sales'));
	}

	public function csvExport()
	{
		$this->loadModel('Currencies');
		$this->loadModel('Countries');
		$this->loadModel('Taxes');

		$csv_data = [];
		$bookings = [];
		$conditions = [];

		if (!empty($this->request->query)) {
			// Notes
			if (isset($this->request->query['filter_notes']) && $this->request->query['filter_notes'] != 'all') {
				$filter_notes = [];

				if (!$this->request->query['filter_notes']) {
					$filter_notes = array('notes' => '');
				} elseif ($this->request->query['filter_notes']) {
					$filter_notes = array('not' => array('notes' => ''));
				}

				$conditions = array_merge($conditions, $filter_notes);
			}

			// Supplier
			if (isset($this->request->query['filter_supplier']) && !empty($this->request->query['filter_supplier'])) {
				$filter_supplier = array('Sales.supplier_id' => $this->request->query['filter_supplier']);

				$conditions = array_merge($conditions, $filter_supplier);
			}

			// Sale
			if (isset($this->request->query['filter_sale']) && !empty($this->request->query['filter_sale'])) {
				$filter_sale = array('sale_id' => $this->request->query['filter_sale']);

				$conditions = array_merge($conditions, $filter_sale);
			}

			// Date From
			if (isset($this->request->query['filter_date_from']) && !empty($this->request->query['filter_date_from'])) {
				$filter_date_from = array('Bookings.created >=' => $this->request->query['filter_date_from']);

				$conditions = array_merge($conditions, $filter_date_from);
			}

			// Date To
			if (isset($this->request->query['filter_date_to']) && !empty($this->request->query['filter_date_to'])) {
				$filter_date_to = array('Bookings.created <=' => $this->request->query['filter_date_to']);

				$conditions = array_merge($conditions, $filter_date_to);
			}
		}

		if (empty($this->request->data['selected'])) {
			$booking_entities = $this->Bookings->find('all')->contain(['Sales'])->where($conditions);

			foreach ($booking_entities as $entity) {
				$bookings[] = $entity->id;
			}
		} else {
			$bookings = $this->request->data['selected'];
		}

		foreach ($bookings as $selected) {
			$booking = $this->Bookings->get($selected, [
				'contain' => [
					'Sales' => function ($q) {
						return $q->contain(['Companies', 'Suppliers']);
					},
					'SaleOffers',
					'SaleAllocations',
					'Transactions'
				]
			]);

			// Nights stay
			$diff = strtotime($booking->check_out) - strtotime($booking->check_in);
			$diff /= (60 * 60 * 24);

			// Currency code
			$currency_query = $this->Currencies->findById($booking->currency_id)->first();

			// Sale type
			switch ($booking->sale->sale_type) {
				case 0:
					$sale_type = 'Hotel';
					break;
				case 1:
					$sale_type = 'Package';
					break;
				default:
					$sale_type = '';
			}

			$tax = $this->Taxes->findById($booking->sale->tax_id)->first();
			$tax_value = 0;

			if ($tax->type == "percent") {
				$tax_value = ($tax->value / 100) * $booking->sale_total;
			} else {
				$tax_value = $tax->value;
			}

			// Commission type == 0 -> fixed
			// Commission type == 1 -> percentage
			$comission_value = 0;

			if ($booking->sale->commission_type == 1) {
				$commission_value = ($booking->sale->commission / 100) * $booking->sale_total;
				$vat_on_commission = (19.9 / 100) * $commission_value;
			} else {
				$commission_value = $booking->sale->commission;
				$vat_on_commission = (19.9 / 100) * $commission_value;
			}

			if (isset($this->request->data['csv-eu-format']) && $this->request->data['csv-eu-format'] === "1") {
				$csv_data[] = array(
					'Booking ID' => $booking->id,
					'Sale ID' => $booking->sale_id,
					'Sale Name' => $booking->sale->title,
					'Offer Name' => $booking->sale_offer->name,
					'Departure Airport Code' => $booking->sale_allocation->departure_airport_code,
					'Adults' => $booking->adults,
					'Children' => $booking->children,
					'Infants' => $booking->infants,
					'Customer Name' => $booking->first_name . ' ' . $booking->last_name,
					'User ID' => $booking->user_id,
					'Booked' => date('d-m-Y H:i', strtotime($booking->created)),
					'Check In' => date('d-m-Y', strtotime($booking->check_in)),
					'Check Out' => date('d-m-Y', strtotime($booking->check_out)),
					'Nights' => $diff,
					'Rooms' => $booking->rooms,
					'Currency' => $currency_query->code,
					'Country' => $booking->transaction->country,
					'Total Euro' => $this->Currency->convert($booking->currency_id, 1, $booking->sale_total, false),
					'Total Sale Rate in Currency' => $this->Currency->convert($booking->currency_id, $booking->currency_id, $booking->sale_total - $tax_value),
					'Comission No VAT' => $this->Currency->convert($booking->currency_id, 1, $commission_value - $vat_on_commission, false),
					'VAT on Comission' => $this->Currency->convert($booking->currency_id, 1, $vat_on_commission, false),
					'Gross Comission' => $this->Currency->convert($booking->currency_id, 1, $commission_value, false),
					'Total NET' => $this->Currency->convert($booking->currency_id, 1, $booking->sale_total - $tax_value, false),
					'Sale Type' => $sale_type,
					'Transaction ID' => $booking->transaction->id,
					'Company' => $booking->sale->company->name,
					'Supplier' => $booking->sale->supplier->name,
					'Sale Date Open' =>  date('d-m-Y', strtotime($booking->sale->date_open)),
					'Sale Date Close' => date('d-m-Y', strtotime($booking->sale->date_close)),
					'Destination Name' => $booking->sale->location_title,
					'Device' => $booking->user_agent
				);
			} else {
				$csv_data[] = array(
					'Booking ID' => $booking->id,
					'Sale ID' => $booking->sale_id,
					'Sale Name' => $booking->sale->title,
					'Offer Name' => $booking->sale_offer->name,
					'Departure Airport Code' => $booking->sale_allocation->departure_airport_code,
					'Adults' => $booking->adults,
					'Children' => $booking->children,
					'Infants' => $booking->infants,
					'Customer Name' => $booking->first_name . ' ' . $booking->last_name,
					'User ID' => $booking->user_id,
					'Booked' => date('Y-m-d H:i', strtotime($booking->created)),
					'Check In' => date('Y-m-d', strtotime($booking->check_in)),
					'Check Out' => date('Y-m-d', strtotime($booking->check_out)),
					'Nights' => $diff,
					'Rooms' => $booking->rooms,
					'Currency' => $currency_query->code,
					'Country' => $booking->transaction->country,
					'Total Euro' => $this->Currency->convert($booking->currency_id, 1, $booking->sale_total, false),
					'Total Sale Rate in Currency' => $this->Currency->convert($booking->currency_id, $booking->currency_id, $booking->sale_total - $tax_value),
					'Comission No VAT' => $this->Currency->convert($booking->currency_id, 1, $commission_value - $vat_on_commission, false),
					'VAT on Comission' => $this->Currency->convert($booking->currency_id, 1, $vat_on_commission, false),
					'Gross Comission' => $this->Currency->convert($booking->currency_id, 1, $commission_value, false),
					'Total NET' => $this->Currency->convert($booking->currency_id, 1, $booking->sale_total - $tax_value, false),
					'Sale Type' => $sale_type,
					'Transaction ID' => $booking->transaction->id,
					'Company' => $booking->sale->company->name,
					'Supplier' => $booking->sale->supplier->name,
					'Sale Date Open' => $booking->sale->date_open,
					'Sale Date Close' => $booking->sale->date_close,
					'Destination Name' => $booking->sale->location_title,
					'Device' => $booking->user_agent
				);
			}
		}

		$this->createCsv($csv_data, ',');
	}

	private function createCsv($data, $separator = ',')
	{
		if (count($data)) {
			$fp = fopen('php://memory', 'w');

			fputcsv($fp, array_keys($data[0]), $separator);

			foreach ($data as $entry) {
				fputcsv($fp, $entry, $separator);
			}

			fseek($fp, 0);
			$contents = '';
			while (!feof($fp)) {
				$contents .= fread($fp, 8192);
			}

			header('Content-Type: application/csv');

			if (isset($this->request->data['csv-eu-format']) && $this->request->data['csv-eu-format'] === "1") {
				header('Content-Disposition: attachement; filename="Bookings_Report_' . date('d-m-Y') . '.csv"');
			} else {
				header('Content-Disposition: attachement; filename="Bookings_Report_' . date('Y-m-d') . '.csv"');
			}
			echo chr(239) . chr(187) . chr(191) . $contents;

			fclose($fp);

			exit;
		}
	}

	/**
	 * Delete method
	 *
	 * @param string|null $id Booking id.
	 * @return \Cake\Network\Response|null Redirects to index.
	 * @throws \Cake\Network\Exception\NotFoundException When record not found.
	 */
	public function delete($id = null)
	{
		$this->request->allowMethod(['post', 'delete']);

		$booking = $this->Bookings->get($id);

		if ($this->Bookings->delete($booking)) {
			$this->Flash->success(__('The booking has been deleted.'));
		} else {
			$this->Flash->error(__('The booking could not be deleted. Please, try again.'));
		}
		return $this->redirect(['action' => 'index']);
	}
}
