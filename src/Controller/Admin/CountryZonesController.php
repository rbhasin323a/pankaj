<?php
namespace App\Controller\Admin;

use App\Controller\AppController;

/**
 * CountryZones Controller
 *
 * @property \App\Model\Table\CountryZonesTable $CountryZones
 */
class CountryZonesController extends AppController
{

	public function initialize()
	{
		parent::initialize(true);

		if ($this->request->action === 'index') {
			$this->loadComponent('Search.Prg');
		}
	}

	/**
	 * Index method
	 *
	 * @return void
	 */
	public function index()
	{
		$this->paginate = [
			'contain' => ['Countries']
		];
		$this->set('countryZones', $this->paginate($this->CountryZones));
	}

	/**
	 * View method
	 *
	 * @param string|null $id Country Zone id.
	 * @return void
	 * @throws \Cake\Network\Exception\NotFoundException When record not found.
	 */
	public function view($id = null)
	{
		$countryZone = $this->CountryZones->get($id, [
			'contain' => ['Countries']
		]);
		$this->set('countryZone', $countryZone);
	}

	/**
	 * Add method
	 *
	 * @return void Redirects on successful add, renders view otherwise.
	 */
	public function add()
	{
		$countryZone = $this->CountryZones->newEntity();
		if ($this->request->is('post')) {
			$countryZone = $this->CountryZones->patchEntity($countryZone, $this->request->data);
			if ($this->CountryZones->save($countryZone)) {
				$this->Flash->success(__('The country zone has been saved.'));
				return $this->redirect(['action' => 'index']);
			} else {
				$this->Flash->error(__('The country zone could not be saved. Please, try again.'));
			}
		}
		$countries = $this->CountryZones->Countries->find('list', ['limit' => 200]);
		$this->set(compact('countryZone', 'countries'));
	}

	/**
	 * Edit method
	 *
	 * @param string|null $id Country Zone id.
	 * @return void Redirects on successful edit, renders view otherwise.
	 * @throws \Cake\Network\Exception\NotFoundException When record not found.
	 */
	public function edit($id = null)
	{
		$countryZone = $this->CountryZones->get($id, [
			'contain' => []
		]);
		if ($this->request->is(['patch', 'post', 'put'])) {
			$countryZone = $this->CountryZones->patchEntity($countryZone, $this->request->data);
			if ($this->CountryZones->save($countryZone)) {
				$this->Flash->success(__('The country zone has been saved.'));
				return $this->redirect(['action' => 'index']);
			} else {
				$this->Flash->error(__('The country zone could not be saved. Please, try again.'));
			}
		}
		$countries = $this->CountryZones->Countries->find('list', ['limit' => 200]);
		$this->set(compact('countryZone', 'countries'));
	}

	/**
	 * Delete method
	 *
	 * @param string|null $id Country Zone id.
	 * @return void Redirects to index.
	 * @throws \Cake\Network\Exception\NotFoundException When record not found.
	 */
	public function delete($id = null)
	{
		$this->request->allowMethod(['post', 'delete']);
		$countryZone = $this->CountryZones->get($id);
		if ($this->CountryZones->delete($countryZone)) {
			$this->Flash->success(__('The country zone has been deleted.'));
		} else {
			$this->Flash->error(__('The country zone could not be deleted. Please, try again.'));
		}
		return $this->redirect(['action' => 'index']);
	}
}
