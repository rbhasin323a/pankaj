<?php
namespace App\Controller\Admin;

use App\Controller\AppController;

/**
 * BookingSummaries Controller
 *
 * @property \App\Model\Table\BookingSummariesTable $BookingSummaries
 */
class BookingSummariesController extends AppController
{
	public function initialize()
	{
		parent::initialize(true);

		$this->loadModel('MyUsers');
	}

	/**
	 * Index method
	 *
	 * @return void
	 */
	public function index()
	{
		$this->paginate = [
			'contain' => ['Bookings', 'Sales', 'Contractors', 'SaleOffers', 'Departures', 'MyUsers', 'Transactions', 'Currencies', 'Companies', 'Suppliers']
		];
		$this->set('bookingSummaries', $this->paginate($this->BookingSummaries));
	}

	/**
	 * View method
	 *
	 * @param string|null $id Booking Summary id.
	 * @return void
	 * @throws \Cake\Network\Exception\NotFoundException When record not found.
	 */
	public function view($id = null)
	{
		$bookingSummary = $this->BookingSummaries->get($id, [
			'contain' => ['Bookings', 'Sales', 'Contractors', 'SaleOffers', 'Departures', 'MyUsers', 'Transactions', 'Currencies', 'Companies', 'Suppliers']
		]);
		$this->set('bookingSummary', $bookingSummary);
	}

	/**
	 * Add method
	 *
	 * @return void Redirects on successful add, renders view otherwise.
	 */
	public function add()
	{
		$bookingSummary = $this->BookingSummaries->newEntity();
		if ($this->request->is('post')) {
			$bookingSummary = $this->BookingSummaries->patchEntity($bookingSummary, $this->request->data);
			if ($this->BookingSummaries->save($bookingSummary)) {
				$this->Flash->success(__('The booking summary has been saved.'));
				return $this->redirect(['action' => 'index']);
			} else {
				$this->Flash->error(__('The booking summary could not be saved. Please, try again.'));
			}
		}
		$bookings = $this->BookingSummaries->Bookings->find('list', ['limit' => 200]);
		$sales = $this->BookingSummaries->Sales->find('list', ['limit' => 200]);
		$contractors = $this->BookingSummaries->Contractors->find('list', ['limit' => 200]);
		$saleOffers = $this->BookingSummaries->SaleOffers->find('list', ['limit' => 200]);
		$departures = $this->BookingSummaries->Departures->find('list', ['limit' => 200]);
		$users = $this->BookingSummaries->MyUsers->find('list', ['limit' => 200]);
		$transactions = $this->BookingSummaries->Transactions->find('list', ['limit' => 200]);
		$currencies = $this->BookingSummaries->Currencies->find('list', ['limit' => 200]);
		$companies = $this->BookingSummaries->Companies->find('list', ['limit' => 200]);
		$suppliers = $this->BookingSummaries->Suppliers->find('list', ['limit' => 200]);
		$this->set(compact('bookingSummary', 'bookings', 'sales', 'contractors', 'saleOffers', 'departures', 'users', 'transactions', 'currencies', 'companies', 'suppliers'));
	}

	/**
	 * Edit method
	 *
	 * @param string|null $id Booking Summary id.
	 * @return void Redirects on successful edit, renders view otherwise.
	 * @throws \Cake\Network\Exception\NotFoundException When record not found.
	 */
	public function edit($id = null)
	{
		$bookingSummary = $this->BookingSummaries->get($id, [
			'contain' => []
		]);
		if ($this->request->is(['patch', 'post', 'put'])) {
			$bookingSummary = $this->BookingSummaries->patchEntity($bookingSummary, $this->request->data);
			if ($this->BookingSummaries->save($bookingSummary)) {
				$this->Flash->success(__('The booking summary has been saved.'));
				return $this->redirect(['action' => 'index']);
			} else {
				$this->Flash->error(__('The booking summary could not be saved. Please, try again.'));
			}
		}
		$bookings = $this->BookingSummaries->Bookings->find('list', ['limit' => 200]);
		$sales = $this->BookingSummaries->Sales->find('list', ['limit' => 200]);
		$contractors = $this->BookingSummaries->Contractors->find('list', ['limit' => 200]);
		$saleOffers = $this->BookingSummaries->SaleOffers->find('list', ['limit' => 200]);
		$departures = $this->BookingSummaries->Departures->find('list', ['limit' => 200]);
		$users = $this->BookingSummaries->MyUsers->find('list', ['limit' => 200]);
		$transactions = $this->BookingSummaries->Transactions->find('list', ['limit' => 200]);
		$currencies = $this->BookingSummaries->Currencies->find('list', ['limit' => 200]);
		$companies = $this->BookingSummaries->Companies->find('list', ['limit' => 200]);
		$suppliers = $this->BookingSummaries->Suppliers->find('list', ['limit' => 200]);
		$this->set(compact('bookingSummary', 'bookings', 'sales', 'contractors', 'saleOffers', 'departures', 'users', 'transactions', 'currencies', 'companies', 'suppliers'));
	}

	/**
	 * Delete method
	 *
	 * @param string|null $id Booking Summary id.
	 * @return \Cake\Network\Response|null Redirects to index.
	 * @throws \Cake\Network\Exception\NotFoundException When record not found.
	 */
	public function delete($id = null)
	{
		$this->request->allowMethod(['post', 'delete']);
		$bookingSummary = $this->BookingSummaries->get($id);
		if ($this->BookingSummaries->delete($bookingSummary)) {
			$this->Flash->success(__('The booking summary has been deleted.'));
		} else {
			$this->Flash->error(__('The booking summary could not be deleted. Please, try again.'));
		}
		return $this->redirect(['action' => 'index']);
	}
}
