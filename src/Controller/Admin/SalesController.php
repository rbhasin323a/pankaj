<?php
namespace App\Controller\Admin;

use App\Controller\AppController;
use Cake\Event\Event;
use Cake\ORM\TableRegistry;
use Cake\Core\Configure;
use App\Utility\DBUtils;

/**
 * Sales Controller
 *
 * @property \App\Model\Table\SalesTable $Sales
 */
class SalesController extends AppController
{
	public $sale_types = array('HOTEL', 'PACKAGE');
	public $board_types = array('ROOM_ONLY', 'BED_N_BREAKFAST', 'HALF_BOARD', 'FULL_BOARD', 'ALL_INCLUSIVE');
	public $commission_types = array('FIXED', 'PERCENTAGE');
	public $offer_types = array('FIXED', 'FLEXIBLE');

	public function beforeRender(Event $event)
	{
		parent::beforeRender($event);

		$this->set('sale_types', $this->sale_types);
		$this->set('board_types', $this->board_types);
		$this->set('commission_types', $this->commission_types);
		$this->set('offer_types', $this->offer_types);
	}

	public function initialize()
	{
		parent::initialize(true);

		if ($this->request->action === 'index') {
			$this->loadComponent('Search.Prg');
		}
	}

	/**
	 * Index method
	 *
	 * @return void
	 */
	public function index()
	{

		$this->paginate = [
			'limit' => 24,
			'order' => [
				'created' => 'desc'
			],
			'contain' => ['Contractors', 'Suppliers', 'Companies', 'Countries', 'Taxes', 'SaleOffers']
		];
		if (!empty($this->request->query['language']) || !empty($this->request->query['lang'])) { //check for lang query vars
			$this->set('sales', $this->paginate());
		} else {
			if (!empty($this->request->query['q'])) {
				$conditions = [];
				$conditions['AND'] = [];
				$conditions['AND'] = ['OR' => [
					'title LIKE' => '%' . $this->request->query['q'] . '%',
					'sub_title LIKE' => '%' . $this->request->query['q'] . '%',
					'Sales.id' => $this->request->query['q'],
					'location_title LIKE' => '%' . $this->request->query['q'] . '%'
				]];

				$query = $this->Sales->find('all')->where($conditions);
			} else {
				$query = $this->Sales->find('all');
			}
			$this->set('sales', $this->paginate($query));
		}
	}

	/**
	 * Add method
	 *
	 * @return void Redirects on successful add, renders view otherwise.
	 */
	public function add()
	{
		$this->loadModel('Languages');
		$this->loadModel('LanguageToSale');

		$sale = $this->Sales->newEntity();

		$languages_query = $this->Languages->find('all');

		$languages = [];

		foreach ($languages_query as $key => $value) {
			if ($value['name'] == 'English') {
				continue;
			}

			$languages[$value['locale']] = $value['name'];
		}

		$language_to_sale = [];

		if (!empty($sale->language_to_sale)) {
			foreach ($sale->language_to_sale as $lang) {
				$language_to_sale[] = $lang['locale'];
			}
		}

		if ($this->request->is('post')) {
			if (!empty($this->request->data['_translations'])) {
				$translations = $this->request->data['_translations'];

				$this->set('_translations', $translations);

				$default_locale = Configure::read('App.defaultLocale');

				if (!empty($translations[$default_locale])) {
					foreach ($translations[$default_locale] as $field => $field_translation) {
						$this->request->data[$field] = $field_translation;
					}
				}

				unset($this->request->data['_translations']);
			}

			$sale = $this->Sales->patchEntity($sale, $this->request->data);

			if ($this->Sales->save($sale)) {
				// Translate
				if (!empty($translations)) {
					$sale = $this->Sales->get($sale->id, [
						'contain' => []
					]);

					foreach ($translations as $lang => $data) {
						$sale->translation($lang)->set($data, ['guard' => false]);
					}

					$this->Sales->save($sale);
				}
				// End Translations

				$this->Flash->success(__('The sale has been saved.'));
				return $this->redirect(['action' => 'index']);
			} else {
				$this->Flash->error(__('The sale could not be saved. Please, try again.'));
			}
		}

		$contractors = $this->Sales->Contractors->find('list', ['limit' => 200]);
		$suppliers = $this->Sales->Suppliers->find('list', ['limit' => 200]);
		$companies = $this->Sales->Companies->find('list', ['limit' => 200]);
		$currencies = $this->Sales->Currencies->find('list', ['limit' => 200]);
		$countries = $this->Sales->Countries->find('list', ['limit' => 200]);
		$taxes = $this->Sales->Taxes->find('list', ['limit' => 200]);
		$this->set(compact('sale', 'contractors', 'suppliers', 'companies', 'currencies', 'countries', 'taxes', 'languages', 'language_to_sale'));
		$this->render('add_edit');
	}

	/**
	 * Edit method
	 *
	 * @param string|null $id Sale id.
	 * @return void Redirects on successful edit, renders view otherwise.
	 * @throws \Cake\Network\Exception\NotFoundException When record not found.
	 */
	public function edit($id = null)
	{

		$this->loadModel('SaleOffers');
		$this->loadModel('ImageToSale');
		$this->loadModel('Languages');
		$this->loadModel('LanguageToSale');

		$sale = $this->Sales->get($id, [
			'contain' => [
				'SaleOffers' => function ($q) {
					return $q->order(['SaleOffers.created' => 'ASC']);
				},
				'ImageToSale' => function ($q) {
					return $q->order(['ImageToSale.sort_order' => 'ASC']);
				},
				'SaleToCategory' => function ($q) {
					return $q->contain(['Categories']);
				},
				'LanguageToSale'
			]
		]);

		$sale_translation = $this->Sales->find('translations')->where(['Sales.id' => $id])->contain(['ImageToSale'])->first()->toArray();
		$this->set('_translations', $sale_translation['_translations']);

		$image_translations = $this->ImageToSale->find('translations')->where(['ImageToSale.sale_id' => $id])->toArray();
		$this->set('_image_translations', $image_translations);

		$languages_query = $this->Languages->find('all');

		$languages = [];

		foreach ($languages_query as $key => $value) {
			if ($value['name'] == 'English') {
				continue;
			}

			$languages[$value['locale']] = $value['name'];
		}

		$language_to_sale = [];

		if (!empty($sale->language_to_sale)) {
			foreach ($sale->language_to_sale as $lang) {
				$language_to_sale[] = $lang['locale'];
			}
		}

		$_sale_offer = $this->SaleOffers->newEntity();

		$this->set(compact('_sale_offer'));

		if ($this->request->is(['patch', 'post', 'put'])) {
			//////
			// New Sale Offer
			//
			if (!empty($this->request->data['_sale_offer'])) {
				$_sale_offer = $this->SaleOffers->newEntity();

				$this->request->data['_sale_offer']['sale_id'] = $id;

				// Sale Offer Translations
				if (!empty($this->request->data['_sale_offer']['_translations'])) {
					$sale_offer_translations = $this->request->data['_sale_offer']['_translations'];

					$this->set('_sale_offer_translations', $sale_offer_translations);

					$default_locale = Configure::read('App.defaultLocale');

					if (!empty($sale_offer_translations[$default_locale])) {
						foreach ($sale_offer_translations[$default_locale] as $field => $field_translation) {
							$this->request->data['_sale_offer'][$field] = $field_translation;
						}
					}

					unset($this->request->data['_sale_offer']['_translations']);
				}

				$_sale_offer = $this->SaleOffers->patchEntity($_sale_offer, $this->request->data['_sale_offer']);

				if ($this->SaleOffers->save($_sale_offer)) {
					// Sale Offer Translate
					if (!empty($sale_offer_translations)) {
						$_sale_offer = $this->SaleOffers->get($_sale_offer->id, [
							'contain' => []
						]);

						foreach ($sale_offer_translations as $lang => $data) {
							$_sale_offer->translation($lang)->set($data, ['guard' => false]);
						}

						$this->SaleOffers->save($_sale_offer);
					}
					// End Sale Offer Translations

					$this->Flash->success(__('The sale offer has been saved.'));
				} else {
					$this->Flash->error(__('The sale offer could not be saved. Please, try again.'));
				}

				$this->set(compact('_sale_offer'));
			}

			//
			// Save Sale
			//

			// Image Translations
			if (!empty($this->request->data['_translations']) && isset($this->request->data['_image_translations'])) {
				$image_translations = $this->request->data['_image_translations'];
				$this->set('_image_translations', $image_translations);
			}

			// Sale Translations
			if (!empty($this->request->data['_translations'])) {
				$translations = $this->request->data['_translations'];

				$this->set('_translations', $translations);

				$default_locale = Configure::read('App.defaultLocale');

				if (!empty($translations[$default_locale])) {
					foreach ($translations[$default_locale] as $field => $field_translation) {
						if ($field == 'image_to_sale') {
							continue;
						}

						$this->request->data[$field] = $field_translation;
					}
				}
				unset($this->request->data['_translations']);
			}

			// Tiny INT Toggles
			$toggles = array(
				'status',
				'smart_stay',
				'promoted',
				'exclude_summary',
				'repeated',
				'enable_hold',
				'show_price',
				'show_discount',
			);

			foreach ($toggles as $toggle) {
				if (empty($this->request->data[$toggle])) {
					$this->request->data[$toggle] = 0;
				}
			}

			// Images
			if (!empty($this->request->data['images'])) {
				$table_image_to_sale = TableRegistry::get('image_to_sale');

				foreach ($this->request->data['images'] as $file) {
					if (!empty($file['name'])) {
						$ext = substr(strtolower(strrchr($file['name'], '.')), 1);
						$allowed_ext = array('jpg', 'jpeg', 'gif', 'png');

						if (in_array($ext, $allowed_ext)) {
							$existing_image = $table_image_to_sale->find()->where(['image_path' => $file['name']])->first();

							if (!empty($existing_image)) {
								$file['name'] = strtoupper(DBUtils::secure_random_string(6)) . '_' . $file['name'];
							}

							$filePath = WWW_ROOT . 'files/images/sales/' . $file['name'];
							$result = move_uploaded_file($file['tmp_name'], $filePath);

							$this->request->data['images'] = $file['name'];

							$new_entity = $table_image_to_sale->newEntity();
							$new_entity->image_path = $file['name'];
							$new_entity->image_alt =  '';
							$new_entity->sale_id = $id;
							$new_entity->sort_order = '99';
							$table_image_to_sale->save($new_entity);
						} else {
							$this->Flash->error(__('One or more images have formats different from .jpg, .jpeg or .gif'));
							continue;
						}
					}
				}

				unset($this->request->data['images']);
			}

			// Territory
			$this->LanguageToSale->deleteAll(array(
				'sale_id' => $sale->id
			));
			if (!empty($this->request->data['territory'])) {
				foreach ($this->request->data['territory'] as $territory) {
					$language_to_sale = $this->LanguageToSale->newEntity();
					$entity = array(
						'locale' => $territory,
						'sale_id' => $sale->id
					);
					$language_to_sale = $this->SaleOffers->patchEntity($language_to_sale, $entity);
					$this->LanguageToSale->save($language_to_sale);
				}
			}

			// Categories
			$table_sale_to_category = TableRegistry::get('sale_to_category');
			$query_sale_to_category = $table_sale_to_category->query();
			$query_sale_to_category->delete()
				->where(['sale_id' => $sale->id])
				->execute();

			if (!empty($this->request->data['sale_categories'])) {
				foreach ($this->request->data['sale_categories'] as $category) {
					$new_entity = $table_sale_to_category->newEntity();
					$new_entity->category_id = $category;
					$new_entity->sale_id = $sale->id;
					$table_sale_to_category->save($new_entity);
				}

				unset($this->request->data['sale_categories']);
			}

			$sale = $this->Sales->patchEntity($sale, $this->request->data);

			if ($this->Sales->save($sale)) {
				// Translate
				if (!empty($translations)) {
					$sale = $this->Sales->get($sale->id, [
						'contain' => []
					]);

					foreach ($translations as $lang => $data) {
						$sale->translation($lang)->set($data, ['guard' => false]);
					}

					$this->Sales->save($sale);
				}


				if (!empty($image_translations)) {
					foreach ($image_translations as $lang => $data) {
						if ($data['image_to_sale']) {
							foreach ($data['image_to_sale'] as $image) {
								$image_to_sale = $this->ImageToSale->get($image['image_to_sale_id']);

								if ($image_to_sale) {
									$image_to_sale->translation($lang)->set($image, ['guard' => false]);
								}

								if ($image_to_sale && isset($_POST['img_to_sale'][$image['image_to_sale_id']]['sort_order'])) {
									$image_to_sale->sort_order = (int)$_POST['img_to_sale'][$image['image_to_sale_id']]['sort_order'];
								}


								$this->ImageToSale->save($image_to_sale);
							}
						}
					}
				}
				// End Translations

				$this->Flash->success(__('The sale has been saved.'));

				return $this->redirect(['action' => 'index']);
			} else {
				$this->Flash->error(__('The sale could not be saved. Please, try again.'));
			}
		}

		foreach ($sale->image_to_sale as &$image) {
			if ($image['image_path'] == $sale->main_image) {
				$image['is_main'] = true;
			} else {
				$image['is_main'] = false;
			}
		}

		$contractors = $this->Sales->Contractors->find('list', ['limit' => 200]);
		$suppliers = $this->Sales->Suppliers->find('list', ['limit' => 200]);
		$companies = $this->Sales->Companies->find('list', ['limit' => 200]);
		$currencies = $this->Sales->Currencies->find('list', ['limit' => 200]);
		$countries = $this->Sales->Countries->find('list', ['limit' => 200]);
		$taxes = $this->Sales->Taxes->find('list', ['limit' => 200]);

		$this->set(compact('sale', 'contractors', 'suppliers', 'companies', 'currencies', 'countries', 'taxes', 'languages', 'language_to_sale'));
		$this->set('images', $sale->image_to_sale);
		$this->render('add_edit');
	}

	public function duplicate($id = null)
	{
		$this->loadModel('SaleOffers');

		$sale_data = $this->Sales->find('all')->where(['Sales.id' => $id])->contain(['ImageToSale', 'SaleOffers'])->first()->toArray();
		$sale_translation = $this->Sales->find('translations')->where(['Sales.id' => $id])->first()->toArray();

		unset($sale_data['id']);
		unset($sale_data['created']);
		unset($sale_data['modified']);
		unset($sale_data['slug']);

		if (!empty($sale_data['sale_offers'])) {
			$sale_offers = [];

			foreach ($sale_data['sale_offers'] as &$offer) {
				$sale_offers[] = $this->SaleOffers->find('translations')
					->where(['SaleOffers.id' => $offer['id']])->first()->toArray();
			}

			$offers_translations = [];

			foreach ($sale_offers as &$offer) {
				$offers_translations[] = $offer['_translations'];
				unset($offer['_translations']);
			}

			unset($sale_data['sale_offers']);
		}
		$toggles = array(
			'status',
			'smart_stay',
			'promoted',
			'exclude_summary',
			'repeated',
			'enable_hold',
			'show_price',
			'show_discount',
		);

		foreach ($toggles as $toggle) {
			if (empty($sale_data[$toggle])) {
				$sale_data[$toggle] = 0;
			}
		}

		// Sale Translations
		$translations = [];

		if (!empty($sale_translation['_translations'])) {
			$translations = $sale_translation['_translations'];
			$default_locale = Configure::read('App.defaultLocale');

			if (!empty($translations[$default_locale])) {
				foreach ($translations[$default_locale] as $field => $field_translation) {
					$sale_translation[$field] = $field_translation;
				}
			}

			unset($sale_translation['_translations']);
		}

		// Offers Translations
		if (!empty($offers_translations['_translations'])) {
			$offers_translations = $offers_translations['_translations'];
			$default_locale = Configure::read('App.defaultLocale');

			if (!empty($offers_translations[$default_locale])) {
				foreach ($offers_translations[$default_locale] as $field => $field_translation) {
					$offers_translations[$field] = $field_translation;
				}
			}

			unset($offers_translations['_translations']);
		}

		$sale = $this->Sales->newEntity();
		$sale = $this->Sales->patchEntity($sale, $sale_data);

		if ($this->Sales->save($sale)) {
			// Translate
			if (!empty($translations)) {
				$sale = $this->Sales->get($sale->id, [
					'contain' => []
				]);

				foreach ($translations as $lang => $data) {
					unset($data['slug']);
					$sale->translation($lang)->set($data, ['guard' => false]);
				}

				$this->Sales->save($sale);
			}
			// End Translations

			// Sale Offers
			if (!empty($sale_offers)) {
				$faulty_offers = [];

				foreach ($sale_offers as $key => &$sale_offer) {
					$new_sale_offer = $this->SaleOffers->newEntity();
					$sale_offer['sale_id'] = $sale->id;

					$cached_offer_id = $sale_offer['id'];
					unset($sale_offer['id']);
					unset($sale_offer['created']);
					unset($sale_offer['modified']);

					$new_sale_offer = $this->SaleOffers->patchEntity($new_sale_offer, $sale_offer);

					if ($this->SaleOffers->save($new_sale_offer)) {
						if (!empty($offers_translations)) {
							$saved_sale_offer = $this->SaleOffers->get($new_sale_offer->id, [
								'contain' => []
							]);

							foreach ($offers_translations[$key] as $lang => $data) {
								$saved_sale_offer->translation($lang)->set($data, ['guard' => false]);
							}

							$this->SaleOffers->save($saved_sale_offer);
						}
					} else {
						$faulty_offers[] = $cached_offer_id;
					}
				}
			}
			// End Sale Offers

			$this->Flash->success(__('The sale has been saved.'));

			if (!empty($faulty_offers)) {
				$this->Flash->error(__('Offer/s with id/s ' . implode(', ', $faulty_offers) . ' were not duplicated due to missing or identical to some other offer content'));
			}
		} else {
			$this->Flash->error(__('The sale could not be saved. Please, try again.'));
		}

		return $this->redirect(['action' => 'index']);
	}

	/**
	 * Delete method
	 *
	 * @param string|null $id Sale id.
	 * @return void Redirects to index.
	 * @throws \Cake\Network\Exception\NotFoundException When record not found.
	 */
	public function delete($id = null)
	{
		$this->request->allowMethod(['post', 'delete']);
		$sale = $this->Sales->get($id);
		if ($this->Sales->delete($sale)) {
			$this->Flash->success(__('The sale has been deleted.'));
		} else {
			$this->Flash->error(__('The sale could not be deleted. Please, try again.'));
		}
		return $this->redirect(['action' => 'index']);
	}

	public function imageDelete($sale_id = null, $filename = null)
	{
		$this->autoRender = false;
		$this->viewBuilder()->setLayout(false);


		$images_dir = ROOT . DS . 'webroot' . DS . 'files' . DS . 'images' . DS . 'sales';

		if (!empty($filename)) {
			if (file_exists($images_dir . DS . $filename)) {
				@unlink($images_dir . DS . $filename);
				return true;
			}
		}

		return false;
	}

	public function sync()
	{
		$sales = $this->Sales->find('all');

		foreach ($sales as $sale) {
			$sale = $this->Sales->get($sale->id, [
				'contain' => []
			]);
			$translations = array(
				'en_US' => $sale->toArray(),
				'bg_BG' => $sale->toArray(),
				'tr_TR' => $sale->toArray()
			);

			foreach ($translations as $lang => $data) {
				$sale->translation($lang)->set($data, ['guard' => false]);
			}

			$this->Sales->save($sale);
		}
	}

	public function deleteImage()
	{

		$this->autoRender = false;

		if ($this->request->is('post')) {

			$image_to_sale_id = $this->request->data['image_to_sale_id'];
			$image_name = $this->request->data['image_name'];
			$imageToSaleTable = TableRegistry::get('ImageToSale');
			$entity = $imageToSaleTable->get($image_to_sale_id);
			$imageToSaleTable->delete($entity);
			$image_delete = $this->imageDelete(null, $image_name);

			echo true;
		}
	}
}
