<?php
namespace App\Controller\Admin;

use App\Controller\AppController;

/**
 * Companies Controller
 *
 * @property \App\Model\Table\CompaniesTable $Companies
 */
class CompaniesController extends AppController
{

	public function initialize()
	{
		parent::initialize(true);

		if ($this->request->action === 'index') {
			$this->loadComponent('Search.Prg');
		}
	}

	/**
	 * Index method
	 *
	 * @return void
	 */
	public function index()
	{
		$this->paginate = [];

		if (!empty($this->request->query['q'])) {
			$conditions = [];
			$conditions['AND'] = [];
			$conditions['AND'] = ['OR' => [
				'Companies.email LIKE' => '%' . $this->request->query['q'] . '%',
				'Companies.country LIKE' => '%' . $this->request->query['q'] . '%',
				'Companies.id' => $this->request->query['q'],
				'Companies.name LIKE' => '%' . $this->request->query['q'] . '%'
			]];

			$query = $this->Companies->find('all')->where($conditions);
		} else {
			$query = $this->Companies->find('all');
		}
		$this->set('companies', $this->paginate($query));
	}

	/**
	 * View method
	 *
	 * @param string|null $id Company id.
	 * @return void
	 * @throws \Cake\Network\Exception\NotFoundException When record not found.
	 */
	public function view($id = null)
	{
		$company = $this->Companies->get($id, [
			'contain' => ['Contacts', 'Suppliers', 'Sales']
		]);
		$this->set('company', $company);
	}

	/**
	 * Add method
	 *
	 * @return void Redirects on successful add, renders view otherwise.
	 */
	public function add()
	{
		$company = $this->Companies->newEntity();
		if ($this->request->is('post')) {
			$company = $this->Companies->patchEntity($company, $this->request->data);
			if ($this->Companies->save($company)) {
				$this->Flash->success(__('The company has been saved.'));
				return $this->redirect(['action' => 'index']);
			} else {
				$this->Flash->error(__('The company could not be saved. Please, try again.'));
			}
		}
		$contacts = $this->Companies->Contacts->find('list', ['limit' => 200]);
		$suppliers = $this->Companies->Suppliers->find('list', ['limit' => 200]);
		$this->set(compact('company', 'contacts', 'suppliers'));
	}

	/**
	 * Edit method
	 *
	 * @param string|null $id Company id.
	 * @return void Redirects on successful edit, renders view otherwise.
	 * @throws \Cake\Network\Exception\NotFoundException When record not found.
	 */
	public function edit($id = null)
	{
		$company = $this->Companies->get($id, [
			'contain' => []
		]);
		if ($this->request->is(['patch', 'post', 'put'])) {
			$company = $this->Companies->patchEntity($company, $this->request->data);
			if ($this->Companies->save($company)) {
				$this->Flash->success(__('The company has been saved.'));
				return $this->redirect(['action' => 'index']);
			} else {
				$this->Flash->error(__('The company could not be saved. Please, try again.'));
			}
		}
		$contacts = $this->Companies->Contacts->find('list', ['limit' => 200]);
		$suppliers = $this->Companies->Suppliers->find('list', ['limit' => 200]);
		$this->set(compact('company', 'contacts', 'suppliers'));
	}

	/**
	 * Delete method
	 *
	 * @param string|null $id Company id.
	 * @return void Redirects to index.
	 * @throws \Cake\Network\Exception\NotFoundException When record not found.
	 */
	public function delete($id = null)
	{
		$this->request->allowMethod(['post', 'delete']);
		$company = $this->Companies->get($id);
		if ($this->Companies->delete($company)) {
			$this->Flash->success(__('The company has been deleted.'));
		} else {
			$this->Flash->error(__('The company could not be deleted. Please, try again.'));
		}
		return $this->redirect(['action' => 'index']);
	}
}
