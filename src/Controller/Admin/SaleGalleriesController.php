<?php
namespace App\Controller\Admin;

use App\Controller\AppController;

/**
 * SaleGalleries Controller
 *
 * @property \App\Model\Table\SaleGalleriesTable $SaleGalleries
 */
class SaleGalleriesController extends AppController
{

	public function initialize()
	{
		parent::initialize(true);

		if ($this->request->action === 'index') {
			$this->loadComponent('Search.Prg');
		}
	}

	/**
	 * Index method
	 *
	 * @return void
	 */
	public function index()
	{
		$this->set('saleGalleries', $this->paginate($this->SaleGalleries));
	}

	/**
	 * View method
	 *
	 * @param string|null $id Sale Gallery id.
	 * @return void
	 * @throws \Cake\Network\Exception\NotFoundException When record not found.
	 */
	public function view($id = null)
	{
		$saleGallery = $this->SaleGalleries->get($id, [
			'contain' => ['SaleDetails']
		]);
		$this->set('saleGallery', $saleGallery);
	}

	/**
	 * Add method
	 *
	 * @return void Redirects on successful add, renders view otherwise.
	 */
	public function add()
	{
		$saleGallery = $this->SaleGalleries->newEntity();
		if ($this->request->is('post')) {
			$saleGallery = $this->SaleGalleries->patchEntity($saleGallery, $this->request->data);
			if ($this->SaleGalleries->save($saleGallery)) {
				$this->Flash->success(__('The sale gallery has been saved.'));
				return $this->redirect(['action' => 'index']);
			} else {
				$this->Flash->error(__('The sale gallery could not be saved. Please, try again.'));
			}
		}
		$this->set(compact('saleGallery'));
	}

	/**
	 * Edit method
	 *
	 * @param string|null $id Sale Gallery id.
	 * @return void Redirects on successful edit, renders view otherwise.
	 * @throws \Cake\Network\Exception\NotFoundException When record not found.
	 */
	public function edit($id = null)
	{
		$saleGallery = $this->SaleGalleries->get($id, [
			'contain' => []
		]);
		if ($this->request->is(['patch', 'post', 'put'])) {
			$saleGallery = $this->SaleGalleries->patchEntity($saleGallery, $this->request->data);
			if ($this->SaleGalleries->save($saleGallery)) {
				$this->Flash->success(__('The sale gallery has been saved.'));
				return $this->redirect(['action' => 'index']);
			} else {
				$this->Flash->error(__('The sale gallery could not be saved. Please, try again.'));
			}
		}
		$this->set(compact('saleGallery'));
	}

	/**
	 * Delete method
	 *
	 * @param string|null $id Sale Gallery id.
	 * @return void Redirects to index.
	 * @throws \Cake\Network\Exception\NotFoundException When record not found.
	 */
	public function delete($id = null)
	{
		$this->request->allowMethod(['post', 'delete']);
		$saleGallery = $this->SaleGalleries->get($id);
		if ($this->SaleGalleries->delete($saleGallery)) {
			$this->Flash->success(__('The sale gallery has been deleted.'));
		} else {
			$this->Flash->error(__('The sale gallery could not be deleted. Please, try again.'));
		}
		return $this->redirect(['action' => 'index']);
	}
}
