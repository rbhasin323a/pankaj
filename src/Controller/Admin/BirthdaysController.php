<?php
namespace App\Controller\Admin;

use Cake\Datasource\ConnectionManager;
use App\Controller\AppController;

class BirthdaysController extends AppController
{
	private $last_sunday;

	public function initialize()
	{
		parent::initialize(true);

		$this->last_sunday = date('Y-m-d', strtotime("sunday last week"));
	}

	public function index()
	{
		$conn = ConnectionManager::get('default');

		$users = $conn->execute("SELECT id, birthday, first_name, last_name,
                                        FLOOR(DATEDIFF('" . $this->last_sunday . "',  birthday) / 365.25) AS age_now,
                                        FLOOR(DATEDIFF(DATE_ADD('" . $this->last_sunday . "', INTERVAL 8 DAY), birthday) / 365.25) AS age_one_week_from_now
                                 FROM users
                                 WHERE 1 = (FLOOR(DATEDIFF(DATE_ADD('" . $this->last_sunday . "', INTERVAL 8 DAY), birthday) / 365.25))
                                           -
                                           (FLOOR(DATEDIFF('" . $this->last_sunday . "', birthday) / 365.25))")->fetchAll('assoc');

		$this->set(compact('users'));
	}
}
