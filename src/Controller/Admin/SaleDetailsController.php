<?php
namespace App\Controller\Admin;

use App\Controller\AppController;

/**
 * SaleDetails Controller
 *
 * @property \App\Model\Table\SaleDetailsTable $SaleDetails
 */
class SaleDetailsController extends AppController
{

	public function initialize()
	{
		parent::initialize(true);

		if ($this->request->action === 'index') {
			$this->loadComponent('Search.Prg');
		}
	}

	/**
	 * Index method
	 *
	 * @return void
	 */
	public function index()
	{
		$this->paginate = [
			'contain' => ['SaleGalleries']
		];
		$this->set('saleDetails', $this->paginate($this->SaleDetails));
	}

	/**
	 * View method
	 *
	 * @param string|null $id Sale Detail id.
	 * @return void
	 * @throws \Cake\Network\Exception\NotFoundException When record not found.
	 */
	public function view($id = null)
	{
		$saleDetail = $this->SaleDetails->get($id, [
			'contain' => ['SaleGalleries', 'I18n', 'Sales']
		]);
		$this->set('saleDetail', $saleDetail);
	}

	/**
	 * Add method
	 *
	 * @return void Redirects on successful add, renders view otherwise.
	 */
	public function add()
	{
		$saleDetail = $this->SaleDetails->newEntity();
		if ($this->request->is('post')) {
			$saleDetail = $this->SaleDetails->patchEntity($saleDetail, $this->request->data);
			if ($this->SaleDetails->save($saleDetail)) {
				$this->Flash->success(__('The sale detail has been saved.'));
				return $this->redirect(['action' => 'index']);
			} else {
				$this->Flash->error(__('The sale detail could not be saved. Please, try again.'));
			}
		}
		$saleGalleries = $this->SaleDetails->SaleGalleries->find('list', ['limit' => 200]);
		$i18n = $this->SaleDetails->I18n->find('list', ['limit' => 200]);
		$this->set(compact('saleDetail', 'saleGalleries', 'i18n'));
	}

	/**
	 * Edit method
	 *
	 * @param string|null $id Sale Detail id.
	 * @return void Redirects on successful edit, renders view otherwise.
	 * @throws \Cake\Network\Exception\NotFoundException When record not found.
	 */
	public function edit($id = null)
	{
		$saleDetail = $this->SaleDetails->get($id, [
			'contain' => ['I18n']
		]);
		if ($this->request->is(['patch', 'post', 'put'])) {
			$saleDetail = $this->SaleDetails->patchEntity($saleDetail, $this->request->data);
			if ($this->SaleDetails->save($saleDetail)) {
				$this->Flash->success(__('The sale detail has been saved.'));
				return $this->redirect(['action' => 'index']);
			} else {
				$this->Flash->error(__('The sale detail could not be saved. Please, try again.'));
			}
		}
		$saleGalleries = $this->SaleDetails->SaleGalleries->find('list', ['limit' => 200]);
		$i18n = $this->SaleDetails->I18n->find('list', ['limit' => 200]);
		$this->set(compact('saleDetail', 'saleGalleries', 'i18n'));
	}

	/**
	 * Delete method
	 *
	 * @param string|null $id Sale Detail id.
	 * @return void Redirects to index.
	 * @throws \Cake\Network\Exception\NotFoundException When record not found.
	 */
	public function delete($id = null)
	{
		$this->request->allowMethod(['post', 'delete']);
		$saleDetail = $this->SaleDetails->get($id);
		if ($this->SaleDetails->delete($saleDetail)) {
			$this->Flash->success(__('The sale detail has been deleted.'));
		} else {
			$this->Flash->error(__('The sale detail could not be deleted. Please, try again.'));
		}
		return $this->redirect(['action' => 'index']);
	}
}
