<?php
namespace App\Controller\Admin;

use App\Controller\AppController;

/**
 * Taxes Controller
 *
 * @property \App\Model\Table\TaxesTable $Taxes
 */
class TaxesController extends AppController
{

	public function initialize()
	{
		parent::initialize(true);

		if ($this->request->action === 'index') {
			$this->loadComponent('Search.Prg');
		}
	}

	/**
	 * Index method
	 *
	 * @return void
	 */
	public function index()
	{
		$this->set('taxes', $this->paginate($this->Taxes));
	}

	/**
	 * View method
	 *
	 * @param string|null $id Tax id.
	 * @return void
	 * @throws \Cake\Network\Exception\NotFoundException When record not found.
	 */
	public function view($id = null)
	{
		$tax = $this->Taxes->get($id, [
			'contain' => ['SaleOffers', 'Sales']
		]);
		$this->set('tax', $tax);
	}

	/**
	 * Add method
	 *
	 * @return void Redirects on successful add, renders view otherwise.
	 */
	public function add()
	{
		$tax = $this->Taxes->newEntity();
		if ($this->request->is('post')) {
			$tax = $this->Taxes->patchEntity($tax, $this->request->data);
			if ($this->Taxes->save($tax)) {
				$this->Flash->success(__('The tax has been saved.'));
				return $this->redirect(['action' => 'index']);
			} else {
				$this->Flash->error(__('The tax could not be saved. Please, try again.'));
			}
		}
		$this->set(compact('tax'));
	}

	/**
	 * Edit method
	 *
	 * @param string|null $id Tax id.
	 * @return void Redirects on successful edit, renders view otherwise.
	 * @throws \Cake\Network\Exception\NotFoundException When record not found.
	 */
	public function edit($id = null)
	{
		$tax = $this->Taxes->get($id, [
			'contain' => []
		]);
		if ($this->request->is(['patch', 'post', 'put'])) {
			$tax = $this->Taxes->patchEntity($tax, $this->request->data);
			if ($this->Taxes->save($tax)) {
				$this->Flash->success(__('The tax has been saved.'));
				return $this->redirect(['action' => 'index']);
			} else {
				$this->Flash->error(__('The tax could not be saved. Please, try again.'));
			}
		}
		$this->set(compact('tax'));
	}

	/**
	 * Delete method
	 *
	 * @param string|null $id Tax id.
	 * @return void Redirects to index.
	 * @throws \Cake\Network\Exception\NotFoundException When record not found.
	 */
	public function delete($id = null)
	{
		$this->request->allowMethod(['post', 'delete']);
		$tax = $this->Taxes->get($id);
		if ($this->Taxes->delete($tax)) {
			$this->Flash->success(__('The tax has been deleted.'));
		} else {
			$this->Flash->error(__('The tax could not be deleted. Please, try again.'));
		}
		return $this->redirect(['action' => 'index']);
	}
}
