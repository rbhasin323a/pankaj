<?php
namespace App\Controller\Admin;

use App\Controller\AppController;

/**
 * Contractors Controller
 *
 * @property \App\Model\Table\ContractorsTable $Contractors
 */
class ContractorsController extends AppController
{

	public function initialize()
	{
		parent::initialize(true);

		if ($this->request->action === 'index') {
			$this->loadComponent('Search.Prg');
		}
	}

	/**
	 * Index method
	 *
	 * @return void
	 */
	public function index()
	{
		$this->set('contractors', $this->paginate($this->Contractors));
	}

	/**
	 * View method
	 *
	 * @param string|null $id Contractor id.
	 * @return void
	 * @throws \Cake\Network\Exception\NotFoundException When record not found.
	 */
	public function view($id = null)
	{
		$contractor = $this->Contractors->get($id, [
			'contain' => ['Sales']
		]);
		$this->set('contractor', $contractor);
	}

	/**
	 * Add method
	 *
	 * @return void Redirects on successful add, renders view otherwise.
	 */
	public function add()
	{
		$contractor = $this->Contractors->newEntity();
		if ($this->request->is('post')) {
			$contractor = $this->Contractors->patchEntity($contractor, $this->request->data);
			if ($this->Contractors->save($contractor)) {
				$this->Flash->success(__('The contractor has been saved.'));
				return $this->redirect(['action' => 'index']);
			} else {
				$this->Flash->error(__('The contractor could not be saved. Please, try again.'));
			}
		}
		$this->set(compact('contractor'));
	}

	/**
	 * Edit method
	 *
	 * @param string|null $id Contractor id.
	 * @return void Redirects on successful edit, renders view otherwise.
	 * @throws \Cake\Network\Exception\NotFoundException When record not found.
	 */
	public function edit($id = null)
	{
		$contractor = $this->Contractors->get($id, [
			'contain' => []
		]);
		if ($this->request->is(['patch', 'post', 'put'])) {
			$contractor = $this->Contractors->patchEntity($contractor, $this->request->data);
			if ($this->Contractors->save($contractor)) {
				$this->Flash->success(__('The contractor has been saved.'));
				return $this->redirect(['action' => 'index']);
			} else {
				$this->Flash->error(__('The contractor could not be saved. Please, try again.'));
			}
		}
		$this->set(compact('contractor'));
	}

	/**
	 * Delete method
	 *
	 * @param string|null $id Contractor id.
	 * @return void Redirects to index.
	 * @throws \Cake\Network\Exception\NotFoundException When record not found.
	 */
	public function delete($id = null)
	{
		$this->request->allowMethod(['post', 'delete']);
		$contractor = $this->Contractors->get($id);
		if ($this->Contractors->delete($contractor)) {
			$this->Flash->success(__('The contractor has been deleted.'));
		} else {
			$this->Flash->error(__('The contractor could not be deleted. Please, try again.'));
		}
		return $this->redirect(['action' => 'index']);
	}
}
