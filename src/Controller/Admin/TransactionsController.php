<?php
namespace App\Controller\Admin;

use App\Controller\AppController;

/**
 * Bookings Controller
 *
 * @property \App\Model\Table\BookingsTable $Bookings
 */
class TransactionsController extends AppController
{

	/**
	 * Index method
	 *
	 * @return void
	 */
	public function index()
	{
		$this->paginate = [
			'limit' => 24,
			'order' => [
				'created' => 'desc'
			]
		];

		if (!empty($this->request->query['q'])) {
			$query = $this->Transactions->find('search', ['search' => $this->request->getQueryParams()]);
		} else {
			$query = $this->Transactions->find('all');
		}

		$this->set('transactions', $this->paginate($query));
	}

	/**
	 * View method
	 *
	 * @param string|null $id Booking id.
	 * @return void
	 * @throws \Cake\Network\Exception\NotFoundException When record not found.
	 */
	public function view($id = null)
	{
		$booking = $this->Transactions->get($id, [
			'contain' => ['Transactions', 'MyUsers', 'Sales', 'BookingSummaries']
		]);

		$this->set('booking', $booking);
	}

	/**
	 * Edit method
	 *
	 * @param string|null $id Booking id.
	 * @return void Redirects on successful edit, renders view otherwise.
	 * @throws \Cake\Network\Exception\NotFoundException When record not found.
	 */
	public function edit($id = null)
	{
		$transaction = $this->Transactions->get($id, [
			'contain' => []
		]);

		if ($this->request->is(['patch', 'post', 'put'])) {
			$transaction = $this->Transactions->patchEntity($transaction, $this->request->data);
			if ($this->Transactions->save($transaction)) {
				$this->Flash->success(__('The transaction has been saved.'));
				return $this->redirect(['action' => 'index']);
			} else {
				$this->Flash->error(__('The transaction could not be saved. Please, try again.'));
			}
		}

		$this->set(compact('transaction'));
	}

	/**
	 * Delete method
	 *
	 * @param string|null $id Booking id.
	 * @return \Cake\Network\Response|null Redirects to index.
	 * @throws \Cake\Network\Exception\NotFoundException When record not found.
	 */
	public function delete($id = null)
	{
		$this->request->allowMethod(['post', 'delete']);
		$transaction = $this->Transactions->get($id);
		if ($this->Transactions->delete($transaction)) {
			$this->Flash->success(__('The transaction has been deleted.'));
		} else {
			$this->Flash->error(__('The transaction could not be deleted. Please, try again.'));
		}
		return $this->redirect(['action' => 'index']);
	}
}
