<?php
namespace App\Controller\Admin;

use App\Controller\AppController;
use Cake\Core\Configure;

/**
 * Footer Controller
 *
 * @property \App\Model\Table\FooterTable $footer
 */
class FooterController extends AppController
{
	public function initialize()
	{
		parent::initialize(true);
	}
	/**
	 * Index method
	 *
	 * @return void
	 */
	public function index()
	{
		$this->loadModel('Footer');
		$this->loadModel('Languages');
		$id = 'XANQW4QFAODS';
		// load languages
		$languages_query = $this->Languages->find('all');
		$languages = [];

		foreach ($languages_query as $key => $value) {
			if ($value['name'] == 'English') continue;
			$languages[$value['locale']] = $value['name'];
		}
		$footer = $this->Footer->get($id, [
			'contain' => []
		]);

		// SEO Translations from DB
		$footer_translation = $this->Footer->find('translations')->where(['Footer.id' => $id])->first()->toArray();
		$this->set('_translations', $footer_translation['_translations']);

		if ($this->request->is(['patch', 'post', 'put'])) {
			// Footer Translations
			if (!empty($this->request->data['_translations'])) {
				$translations = $this->request->data['_translations'];

				$this->set('_translations', $translations);

				$default_locale = Configure::read('App.defaultLocale');

				if (!empty($translations[$default_locale])) {
					foreach ($translations[$default_locale] as $field => $field_translation) {
						$this->request->data[$field] = $field_translation;
					}
				}

				unset($this->request->data['_translations']);
			}

			$footer = $this->Footer->patchEntity($footer, $this->request->data);

			if ($this->Footer->save($footer)) {
				// Translate
				if (!empty($translations)) {
					$footer = $this->Footer->get($footer->id, [
						'contain' => []
					]);
					foreach ($translations as $lang => $data) {

						$footer->translation($lang)->set($data, ['guard' => false]);
					}
					$this->Footer->save($footer);
				}
				// End Translations
				$this->Flash->success(__('The footer data has been saved.'));
				return $this->redirect(['action' => 'index']);
			} else {
				$this->Flash->error(__('The footer data could not be saved. Please, try again.'));
			}
		}
		$this->set(compact('footer'));
	}
}
