<?php
namespace App\Controller\Admin;

use App\Controller\AppController;
use Cake\Event\Event;
use Cake\Core\Configure;

/**
 * SaleOffers Controller
 *
 * @property \App\Model\Table\SaleOffersTable $SaleOffers
 */
class SaleOffersController extends AppController
{
	public $offer_types = array('FIXED', 'FLEXIBLE');

	public function beforeRender(Event $event)
	{
		parent::beforeRender($event);

		$this->set('offer_types', $this->offer_types);
	}

	public function initialize()
	{
		parent::initialize(true);

		if ($this->request->action === 'index') {
			$this->loadComponent('Search.Prg');
		}
	}

	/**
	 * Index method
	 *
	 * @return void
	 */
	public function index()
	{
		$this->paginate = [
			'contain' => ['Sales']
		];

		$this->set('saleOffers', $this->paginate($this->SaleOffers));
	}

	/**
	 * Add method
	 *
	 * @return void Redirects on successful add, renders view otherwise.
	 */
	public function add()
	{
		$saleOffer = $this->SaleOffers->newEntity();

		if ($this->request->is('post')) {
			if (!empty($this->request->data['_translations'])) {
				$translations = $this->request->data['_translations'];

				$this->set('_translations', $translations);

				$default_locale = ini_get('intl.default_locale');

				if (!empty($translations[$default_locale])) {
					foreach ($translations[$default_locale] as $field => $field_translation) {
						$this->request->data[$field] = $field_translation;
					}
				}

				unset($this->request->data['_translations']);
			}

			$saleOffer = $this->SaleOffers->patchEntity($saleOffer, $this->request->data);

			if ($this->SaleOffers->save($saleOffer)) {
				// Translate
				if (!empty($translations)) {
					$saleOffer = $this->SaleOffers->get($saleOffer->id, [
						'contain' => []
					]);

					foreach ($translations as $lang => $data) {
						$saleOffer->translation($lang)->set($data, ['guard' => false]);
					}

					$this->SaleOffers->save($saleOffer);
				}
				// End Translations

				$this->Flash->success(__('The sale has been saved.'));
				return $this->redirect(['controller' => 'Sales', 'action' => 'edit', $saleOffer->sale_id, '#' => 'offers_tab']);
			} else {
				$this->Flash->error(__('The sale could not be saved. Please, try again.'));
			}
		}

		$taxes = $this->SaleOffers->Taxes->find('list', ['limit' => 200]);
		$sales = $this->SaleOffers->Sales->find('list', ['limit' => 200]);
		$this->set(compact('saleOffer', 'taxes', 'sales'));
		$this->render('add_edit');
	}

	/**
	 * Edit method
	 *
	 * @param string|null $id Sale Offer id.
	 * @return void Redirects on successful edit, renders view otherwise.
	 * @throws \Cake\Network\Exception\NotFoundException When record not found.
	 */
	public function edit($id = null)
	{
		$this->loadModel('SaleAllocations');

		$saleOffer = $this->SaleOffers->get($id, [
			'contain' => [
				'SaleAllocations' => function ($q) {
					return $q->order(['date_start' => 'ASC']);
				},
				'Sales' => function ($q) {
					return $q->select(['sale_type']);
				}
			]
		]);

		$saleOffer_translation = $this->SaleOffers->find('translations')->where(['SaleOffers.id' => $id])->first()->toArray();

		$this->set('_translations', $saleOffer_translation['_translations']);

		$_sale_allocation = $this->SaleAllocations->newEntity();

		$this->set(compact('_sale_allocation'));

		if ($this->request->is(['patch', 'post', 'put'])) {
			//
			// New Sale Offer Allocation
			//
			if (!empty($this->request->data['_sale_allocation'])) {

				if ($saleOffer->sale->sale_type == 0) {
					// Hotel

					$current_date = strtotime($this->request->data['_sale_allocation']['date_start']);

					while ($current_date <= strtotime($this->request->data['_sale_allocation']['date_end'])) {
						$new_allocation = $this->SaleAllocations->newEntity();
						$new_allocation_data = [];

						$new_allocation_data = $this->request->data['_sale_allocation'];
						$new_allocation_data['sale_id'] = $saleOffer->sale_id;
						$new_allocation_data['sale_offer_id'] = $id;
						$new_allocation_data['date_start'] = date('Y-m-d', $current_date);

						$date_week_day = date('N', $current_date);

						if (in_array($date_week_day, $this->request->data['weekdays'])) {
							$current_date = strtotime(date('Y-m-d', $current_date) . " +1day");
							$new_allocation_data['date_end'] = date('Y-m-d', $current_date);

							$new_allocation = $this->SaleAllocations->patchEntity($new_allocation, $new_allocation_data);

							$this->SaleAllocations->save($new_allocation);
						} else {
							$current_date = strtotime(date('Y-m-d', $current_date) . " +1day");
						}
					}

					$this->Flash->success(__('The sale offer has been saved.'));
				} else {
					// Package
					$_sale_allocation = $this->SaleAllocations->newEntity();

					$this->request->data['_sale_allocation']['sale_id'] = $saleOffer->sale_id;
					$this->request->data['_sale_allocation']['sale_offer_id'] = $id;

					$_sale_allocation = $this->SaleAllocations->patchEntity($_sale_allocation, $this->request->data['_sale_allocation']);

					if ($this->SaleAllocations->save($_sale_allocation)) {
						$this->Flash->success(__('The sale offer has been saved.'));
					} else {
						$this->Flash->error(__('The sale offer could not be saved. Please, try again.'));
					}

					$this->set(compact('_sale_allocation'));
				}
			}

			//
			// Save Sale Offer
			//

			// Sale Offer Translations
			if (!empty($this->request->data['_translations'])) {
				$translations = $this->request->data['_translations'];

				$this->set('_translations', $translations);

				$default_locale = ini_get('intl.default_locale');

				if (!empty($translations[$default_locale])) {
					foreach ($translations[$default_locale] as $field => $field_translation) {
						$this->request->data[$field] = $field_translation;
					}
				}

				unset($this->request->data['_translations']);
			}

			if (!isset($this->request->data['_sale_allocation'])) {
				$toggles = array(
					'active',
					'deposit',
				);

				foreach ($toggles as $toggle) {
					if (empty($this->request->data[$toggle])) {
						$this->request->data[$toggle] = 0;
					}
				}
			} else {
				if (empty($this->request->data['active'])) {
					$this->request->data['active'] = 0;
				}
			}

			$saleOffer = $this->SaleOffers->patchEntity($saleOffer, $this->request->data);

			if ($this->SaleOffers->save($saleOffer)) {
				// Translate
				if (!empty($translations)) {
					$saleOffer = $this->SaleOffers->get($saleOffer->id, [
						'contain' => []
					]);

					foreach ($translations as $lang => $data) {
						$saleOffer->translation($lang)->set($data, ['guard' => false]);
					}

					$this->SaleOffers->save($saleOffer);
				}
				// End Translations

				$this->Flash->success(__('The sale offer has been saved.'));

				return $this->redirect(['controller' => 'Sales', 'action' => 'edit', $saleOffer->sale_id, '#' => 'offers_tab']);
			} else {
				$this->Flash->error(__('The sale offer could not be saved. Please, try again.'));
			}
		}

		// $sales = $this->SaleOffers->Sales->find('list', ['limit' => 200]);
		// $this->set(compact('saleOffer', 'sales'));
		$this->set('saleOffer', $saleOffer);

		// Hotel
		if ($saleOffer->sale->sale_type == 0) {
			$this->render('add_edit_hotel');
		} else {
			// Package
			$this->render('add_edit');
		}
	}

	public function duplicate($id = null)
	{
		$this->loadModel('Sales');

		$saleOffer_data = $this->SaleOffers->find('all')->where(['SaleOffers.id' => $id])->first()->toArray();
		$saleOffer_translation = $this->SaleOffers->find('translations')->where(['SaleOffers.id' => $id])->first()->toArray();

		unset($saleOffer_data['id']);
		unset($saleOffer_data['created']);
		unset($saleOffer_data['modified']);

		$toggles = array(
			'active',
			'deposit',
		);

		foreach ($toggles as $toggle) {
			if (empty($saleOffer_data[$toggle])) {
				$saleOffer_data[$toggle] = 0;
			}
		}

		// Sale Offer Translations
		$translations = [];

		if (!empty($saleOffer_translation['_translations'])) {
			$translations = $saleOffer_translation['_translations'];

			$default_locale = Configure::read('App.defaultLocale');

			if (!empty($translations[$default_locale])) {
				foreach ($translations[$default_locale] as $field => $field_translation) {
					$saleOffer_translation[$field] = $field_translation;
				}
			}

			unset($saleOffer_translation['_translations']);
		}
		// Sale Offer Translations end

		$sale_offer = $this->SaleOffers->newEntity();
		$sale_offer = $this->SaleOffers->patchEntity($sale_offer, $saleOffer_data);
		// $sale = $this->Sales->patchEntity($sale, $saleOffer_data);

		// Translate
		if (!empty($translations)) {
			foreach ($translations as $lang => $data) {
				$sale_offer->translation($lang)->set($data, ['guard' => false]);
			}
		}
		// Translate

		if ($this->SaleOffers->save($sale_offer)) {
			$this->Flash->success(__('The sale has been saved.'));

			return $this->redirect(['controller' => 'Sales', 'action' => 'edit', $sale_offer->sale_id, '#' => 'offers_tab']);
		} else {
			$this->Flash->error(__('The sale could not be saved. Please, try again.'));
		}
	}

	public function uploadAllocations($sale_offer_id = null)
	{
		$this->loadModel('SaleAllocations');

		// Validate file
		$allowed_ext = [
			'csv'
		];

		if (!empty($this->request->data['allocations_csv'])) {
			$file = $this->request->data['allocations_csv'];
			$ext = substr(strtolower(strrchr($file['name'], '.')), 1);
		}

		if (empty($this->request->data) || !in_array($ext, $allowed_ext)) {
			$this->Flash->error(__('Please, upload a file with .csv extension and try again. Please, try again.'));
			return $this->redirect(['controller' => 'SaleOffers', 'action' => 'edit', $sale_offer_id, '#' => 'offers_tab']);
		}

		// Upload file
		$upload_dir = WWW_ROOT . 'csvs/';
		$unique_filename = date('Y_m_d_H_i') . $file['name'];
		move_uploaded_file($file['tmp_name'], $upload_dir . $unique_filename);

		$fp = fopen($upload_dir . $unique_filename, "r");

		// Read the headings row
		$first_row = fgetcsv($fp);

		// Array for the preview page
		$allocations_preview = [
			'new' => [],
			'existing' => []
		];

		// Iterate
		while (($data = fgetcsv($fp)) !== false) {
			$sale_allocation_data = [];
			$is_new = false;

			// 6th (5th key) column in .csv file is date_start, hence $data[5]
			$sale_allocation = $this->SaleAllocations->find('all', [
				'conditions' => [
					'date_start' => $data[5],
					'sale_offer_id' => $sale_offer_id
				]
			])->first();

			if (empty($sale_allocation)) {
				// New entity specific data
				$sale_allocation_data['created'] = date('Y-m-d H:i:s');
				$is_new = true;
			} else {
				// Save data before update
				$old_allocation_data = $sale_allocation->toArray();
			}


			// Array with updated data
			if (isset($data[5]) && !empty($data[5])) {
				$sale_allocation_data = [
					'sale_offer_id'				=> $sale_offer_id,
					'rate' 						=> !empty($data[0]) ? $data[0] : (!empty($sale_allocation->rate) ? $sale_allocation->rate : 0),
					'rack_rate' 				=> !empty($data[1]) ? $data[1] : (!empty($sale_allocation->rack_rate) ? $sale_allocation->rack_rate : 0),
					'single_rate' 				=> !empty($data[2]) ? $data[2] : (!empty($sale_allocation->single_rate) ? $sale_allocation->single_rate : 0),
					'child_rate' 				=> !empty($data[3]) ? $data[3] : (!empty($sale_allocation->child_rate) ? $sale_allocation->child_rate : 0),
					'infant_rate' 				=> !empty($data[4]) ? $data[4] : (!empty($sale_allocation->infant_rate) ? $sale_allocation->infant_rate : 0),
					'date_start' 				=> $data[5],
					'date_end' 					=> !empty($data[6]) ? $data[6] : (!empty($sale_allocation->end_date) ? $sale_allocation->end_date : 0),
					'available_rooms' 			=> !empty($data[7]) ? $data[7] : (!empty($sale_allocation->available_rooms) ? $sale_allocation->available_rooms : 0),
					'departure_airport_code' 	=> !empty($data[8]) ? $data[8] : (!empty($sale_allocation->departure_airport_code) ? $sale_allocation->departure_airport_code : ''),
					'destination_airport_code' 	=> !empty($data[9]) ? $data[9] : (!empty($sale_allocation->destination_airport_code) ? $sale_allocation->destination_airport_code : ''),
					'total_deposit' 			=> !empty($data[10]) ? $data[10] : (!empty($sale_allocation->total_deposit) ? $sale_allocation->total_deposit : 0),
					'child_deposit' 			=> !empty($data[11]) ? $data[11] : (!empty($sale_allocation->child_deposit) ? $sale_allocation->child_deposit : 0),
					'infant_deposit' 			=> !empty($data[12]) ? $data[12] : (!empty($sale_allocation->infant_deposit) ? $sale_allocation->infant_deposit : 0),
					'date_balance_due' 			=> !empty($data[13]) ? $data[13] : (!empty($sale_allocation->date_balance_due) ? $sale_allocation->date_balance_due : ''),
					'outbound_overnight'		=> !empty($data[14]) ? ($data[14] == 'Y' ? 1 : 0) : (!empty($sale_allocation->outbound_overnight) ? ($sale_allocation->outbound_overnight == 'Y' ? 1 : 0) : 0),
					'inbound_overnight' 		=> !empty($data[15]) ? ($data[15] == 'Y' ? 1 : 0) : (!empty($sale_allocation->inbound_overnight) ? ($sale_allocation->inbound_overnight == 'Y' ? 1 : 0) : 0),
					'min_nights'				=> !empty($data[16]) ? $data[16] : (!empty($sale_allocation->min_nights) ? $sale_allocation->min_nights : 1),
					'modified'					=> date('Y-m-d H:i:s')
				];

				if ($is_new) {
					$allocations_preview['new'][] = $sale_allocation_data;
				} else {
					// Save id for existing allocations
					// as a key in the update array, so it can be easily patched later
					$sale_allocation_data['id'] = $sale_allocation->id;

					// Save old data for existing allocations
					$allocations_preview['existing'][] = [
						'old' => $old_allocation_data,
						'new' => $sale_allocation_data
					];
				}
			}
		}

		fclose($fp);

		$this->set(compact('allocations_preview'));
		$this->set(compact('sale_offer_id'));
	}

	public function saveUploadedAllocations()
	{
		if ($this->request->is('post')) {
			$this->loadModel('SaleAllocations');

			$allocations = $this->request->data['data'];

			if (isset($allocations['new'])) {
				foreach ($allocations['new'] as $new_allocation_data) {
					if (!empty($new_allocation_data['date_start'])) {
						$new_allocation = $this->SaleAllocations->newEntity();

						$this->SaleAllocations->patchEntity($new_allocation, $new_allocation_data);

						if (!$this->SaleAllocations->save($new_allocation)) {
							$this->Flash->error('New allocation with date_start = ' . $new_allocation_data['date_start'] . ' could not be saved');
						}
					}
				}
			}

			if (isset($allocations['existing'])) {
				foreach ($allocations['existing'] as $existing_allocation_data) {
					$allocation = $this->SaleAllocations->get($existing_allocation_data['id']);

					$this->SaleAllocations->patchEntity($allocation, $existing_allocation_data);

					if (!$this->SaleAllocations->save($allocation)) {
						$this->Flash->error('Existing allocation with date_start = ' . $existing_allocation_data['date_start'] . ' could not be saved');
					}
				}
			}

			$this->redirect(['action' => 'edit', $this->request->data['sale_offer_id'], '#' => 'allocation_tab']);
		}
	}

	/**
	 * Delete method
	 *
	 * @param string|null $id Sale Offer id.
	 * @return void Redirects to index.
	 * @throws \Cake\Network\Exception\NotFoundException When record not found.
	 */
	public function delete($id = null)
	{
		$this->request->allowMethod(['post', 'delete']);
		$saleOffer = $this->SaleOffers->get($id);
		if ($this->SaleOffers->delete($saleOffer)) {
			$this->Flash->success(__('The sale offer has been deleted.'));
		} else {
			$this->Flash->error(__('The sale offer could not be deleted. Please, try again.'));
		}
		return $this->redirect(['controller' => 'Sales', 'action' => 'edit', $saleOffer->sale_id, '#' => 'offers_tab']);
	}
}
