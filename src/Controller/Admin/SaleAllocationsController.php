<?php
namespace App\Controller\Admin;

use App\Controller\AppController;

/**
 * SaleAllocations Controller
 *
 * @property \App\Model\Table\SaleAllocationsTable $SaleAllocations
 */
class SaleAllocationsController extends AppController
{

	/**
	 * Index method
	 *
	 * @return void
	 */
	public function index()
	{
		$this->paginate = [
			'contain' => ['SaleOffers']
		];
		$this->set('saleAllocations', $this->paginate($this->SaleAllocations));
	}

	/**
	 * Add method
	 *
	 * @return void Redirects on successful add, renders view otherwise.
	 */
	public function add()
	{
		$saleAllocation = $this->SaleAllocations->newEntity();
		if ($this->request->is('post')) {
			$saleAllocation = $this->SaleAllocations->patchEntity($saleAllocation, $this->request->data);
			if ($this->SaleAllocations->save($saleAllocation)) {
				$this->Flash->success(__('The sale allocation has been saved.'));
				return $this->redirect([
					'controller' => 'SaleAllocations',
					'action' => 'edit',
					$saleAllocation['sale_offer_id'],
					'#' => 'allocations_tab'
				]);
			} else {
				$this->Flash->error(__('The sale allocation could not be saved. Please, try again.'));
			}
		}
		$saleOffers = $this->SaleAllocations->SaleOffers->find('list', ['limit' => 200]);
		$this->set(compact('saleAllocation', 'saleOffers'));
	}

	/**
	 * Edit method
	 *
	 * @param string|null $id Sale Allocation id.
	 * @return void Redirects on successful edit, renders view otherwise.
	 * @throws \Cake\Network\Exception\NotFoundException When record not found.
	 */
	public function edit($id = null)
	{
		$saleAllocation = $this->SaleAllocations->get($id, [
			'contain' => [
				'SaleOffers' => function ($q) {
					return $q->select(['id', 'name', 'deposit'])->contain([
						'Sales' => function ($q) {
							return $q->select(['sale_type', 'id']);
						}
					]);
				}
			]
		]);

		if ($this->request->is(['patch', 'post', 'put'])) {
			// Tiny INT Toggles
			$toggles = array(
				'outbound_overnight',
				'inbound_overnight',
			);

			foreach ($toggles as $toggle) {
				if (empty($this->request->data[$toggle])) {
					$this->request->data[$toggle] = 0;
				}
			}

			$saleAllocation = $this->SaleAllocations->patchEntity($saleAllocation, $this->request->data);
			if ($this->SaleAllocations->save($saleAllocation)) {
				$this->Flash->success(__('The sale allocation has been saved.'));

				return $this->redirect([
					'controller' => 'SaleOffers',
					'action' => 'edit',
					$saleAllocation['sale_offer_id'],
					'#' => 'allocations_tab'
				]);
			} else {
				$this->Flash->error(__('The sale allocation could not be saved. Please, try again.'));
			}
		}

		$this->set(compact('saleAllocation'));
		// Hotels
		if ($saleAllocation->sale_offer->sale->sale_type == 0) {
			$this->render('edit_hotel');
		} else {
			// Packages
			$this->render('edit');
		}
	}

	/**
	 * Delete method
	 *
	 * @param string|null $id Sale Allocation id.
	 * @return \Cake\Network\Response|null Redirects to index.
	 * @throws \Cake\Network\Exception\NotFoundException When record not found.
	 */
	public function delete($id = null)
	{
		$this->request->allowMethod(['post', 'delete']);
		$saleAllocation = $this->SaleAllocations->get($id);

		if ($this->SaleAllocations->delete($saleAllocation)) {
			$this->Flash->success(__('The sale allocation has been deleted.'));
		} else {
			$this->Flash->error(__('The sale allocation could not be deleted. Please, try again.'));
		}

		return $this->redirect([
			'controller' => 'SaleOffers',
			'action' => 'edit',
			$saleAllocation['sale_offer_id'],
			'#' => 'allocations_tab'
		]);
	}
}
