<?php
namespace App\Controller\Admin;

use App\Controller\AppController;
use Cake\Core\Configure;

/**
 * Categories Controller
 *
 * @property \App\Model\Table\CategoriesTable $Categories
 */
class CategoriesController extends AppController
{
	public function index()
	{
		$this->paginate = [
			'limit' => 24,
			'order' => [
				'created' => 'desc'
			]
		];

		if (!empty($this->request->query['q'])) {
			$query = $this->Categories->find('search', ['search' => $this->request->getQueryParams()]);
		} else {
			$query = $this->Categories->find('translations');
		}

		$this->set('categories', $this->paginate($query));
	}

	public function add()
	{
		$category = $this->Categories->newEntity();

		if ($this->request->is('post')) {
			if (!empty($this->request->data['_translations'])) {
				$translations = $this->request->data['_translations'];

				$this->set('_translations', $translations);

				$default_locale = Configure::read('App.defaultLocale');

				if (!empty($translations[$default_locale])) {
					foreach ($translations[$default_locale] as $field => $field_translation) {
						$this->request->data[$field] = $field_translation;
					}
				}

				// unset($this->request->data['_translations']);
			}

			// Images
			if (!empty($this->request->data['image']) && !empty($this->request->data['image']['name'])) {
				$file = $this->request->data['image'];

				$ext = substr(strtolower(strrchr($file['name'], '.')), 1);
				$allowed_ext = array('jpg', 'jpeg', 'gif', 'png');

				if (in_array($ext, $allowed_ext)) {
					move_uploaded_file($file['tmp_name'], WWW_ROOT . 'files/images/categories/' . $file['name']);
				}

				$this->request->data['image_path'] = 'categories/' . $file['name'];
			}

			$category = $this->Categories->patchEntity($category, $this->request->data);

			if ($this->Categories->save($category)) {
				// Translate
				if (!empty($translations)) {
					$category = $this->Categories->get($category->id, [
						'contain' => []
					]);

					foreach ($translations as $lang => $data) {
						$category->translation($lang)->set($data, ['guard' => false]);
					}

					$this->Categories->save($category);
				}
				// End Translations

				$this->Flash->success(__('The category has been saved.'));
				return $this->redirect(['action' => 'index']);
			} else {
				$this->Flash->error(__('The category could not be saved. Please, try again.'));
			}
		}

		$this->set(compact('category'));
		$this->render('add_edit');
	}

	public function edit($id = null)
	{
		$category = $this->Categories->get($id);

		$category_translation = $this->Categories->find('translations')->where(['Categories.id' => $id])->first()->toArray();

		$this->set('_translations', $category_translation['_translations']);

		if ($this->request->is(['patch', 'post', 'put'])) {
			// Translations
			if (!empty($this->request->data['_translations'])) {
				$translations = $this->request->data['_translations'];

				$this->set('_translations', $translations);

				$default_locale = Configure::read('App.defaultLocale');

				if (!empty($translations[$default_locale])) {
					foreach ($translations[$default_locale] as $field => $field_translation) {
						$this->request->data[$field] = $field_translation;
					}
				}
			}

			// Images
			if (!empty($this->request->data['image']) && !empty($this->request->data['image']['name'])) {
				$file = $this->request->data['image'];

				$ext = substr(strtolower(strrchr($file['name'], '.')), 1);
				$allowed_ext = array('jpg', 'jpeg', 'gif', 'png');

				if (in_array($ext, $allowed_ext)) {
					move_uploaded_file($file['tmp_name'], WWW_ROOT . 'files/images/categories/' . $file['name']);
				}

				$this->request->data['image_path'] = 'categories/' . $file['name'];
			}

			$category = $this->Categories->patchEntity($category, $this->request->data);

			if ($this->Categories->save($category)) {
				$this->Flash->success(__('The category has been saved.'));
				return $this->redirect(['action' => 'index']);
			} else {
				$this->Flash->error(__('The category could not be saved. Please, try again.'));
			}
		}

		$this->set(compact('category'));
		$this->render('add_edit');
	}

	public function delete($id = null)
	{
		$this->request->allowMethod(['post', 'delete']);
		$category = $this->Categories->get($id);
		if ($this->Categories->delete($category)) {
			$this->Flash->success(__('The category has been deleted.'));
		} else {
			$this->Flash->error(__('The category could not be deleted. Please, try again.'));
		}
		return $this->redirect(['action' => 'index']);
	}

	public function links()
	{
		$categories = $this->Categories->find('all')
			->contain(['SaleToCategory' => function ($q) {
				return $q->contain(['Sales']);
			}]);

		$this->set('categories', $categories);
	}

	public function autocomplete()
	{
		$term = $this->request->query['term'];

		$categories = $this->Categories->find('all')
			->select(['id', 'title'])
			->where(['title LIKE ' => '%' . $term . '%'])
			->limit(5);

		$json = [];

		foreach ($categories as $category) {
			$json[] = [
				'label' => $category->title,
				'value' => $category->id
			];
		}

		echo json_encode($json);
		exit;
	}
}
