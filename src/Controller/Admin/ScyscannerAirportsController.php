<?php
namespace App\Controller\Admin;

use App\Controller\AppController;

/**
 * ScyscannerAirports Controller
 *
 * @property \App\Model\Table\ScyscannerAirportsTable $ScyscannerAirports
 */
class ScyscannerAirportsController extends AppController
{

	public function initialize()
	{
		parent::initialize(true);

		if ($this->request->action === 'index') {
			$this->loadComponent('Search.Prg');
		}
	}

	/**
	 * Index method
	 *
	 * @return void
	 */
	public function index()
	{
		$this->set('scyscannerAirports', $this->paginate($this->ScyscannerAirports));
	}

	/**
	 * View method
	 *
	 * @param string|null $id Scyscanner Airport id.
	 * @return void
	 * @throws \Cake\Network\Exception\NotFoundException When record not found.
	 */
	public function view($id = null)
	{
		$scyscannerAirport = $this->ScyscannerAirports->get($id, [
			'contain' => ['Sales']
		]);
		$this->set('scyscannerAirport', $scyscannerAirport);
	}

	/**
	 * Add method
	 *
	 * @return void Redirects on successful add, renders view otherwise.
	 */
	public function add()
	{
		$scyscannerAirport = $this->ScyscannerAirports->newEntity();
		if ($this->request->is('post')) {
			$scyscannerAirport = $this->ScyscannerAirports->patchEntity($scyscannerAirport, $this->request->data);
			if ($this->ScyscannerAirports->save($scyscannerAirport)) {
				$this->Flash->success(__('The scyscanner airport has been saved.'));
				return $this->redirect(['action' => 'index']);
			} else {
				$this->Flash->error(__('The scyscanner airport could not be saved. Please, try again.'));
			}
		}
		$this->set(compact('scyscannerAirport'));
	}

	/**
	 * Edit method
	 *
	 * @param string|null $id Scyscanner Airport id.
	 * @return void Redirects on successful edit, renders view otherwise.
	 * @throws \Cake\Network\Exception\NotFoundException When record not found.
	 */
	public function edit($id = null)
	{
		$scyscannerAirport = $this->ScyscannerAirports->get($id, [
			'contain' => []
		]);
		if ($this->request->is(['patch', 'post', 'put'])) {
			$scyscannerAirport = $this->ScyscannerAirports->patchEntity($scyscannerAirport, $this->request->data);
			if ($this->ScyscannerAirports->save($scyscannerAirport)) {
				$this->Flash->success(__('The scyscanner airport has been saved.'));
				return $this->redirect(['action' => 'index']);
			} else {
				$this->Flash->error(__('The scyscanner airport could not be saved. Please, try again.'));
			}
		}
		$this->set(compact('scyscannerAirport'));
	}

	/**
	 * Delete method
	 *
	 * @param string|null $id Scyscanner Airport id.
	 * @return void Redirects to index.
	 * @throws \Cake\Network\Exception\NotFoundException When record not found.
	 */
	public function delete($id = null)
	{
		$this->request->allowMethod(['post', 'delete']);
		$scyscannerAirport = $this->ScyscannerAirports->get($id);
		if ($this->ScyscannerAirports->delete($scyscannerAirport)) {
			$this->Flash->success(__('The scyscanner airport has been deleted.'));
		} else {
			$this->Flash->error(__('The scyscanner airport could not be deleted. Please, try again.'));
		}
		return $this->redirect(['action' => 'index']);
	}
}
