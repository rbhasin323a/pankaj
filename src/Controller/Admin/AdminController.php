<?php
namespace App\Controller\Admin;

use App\Controller\AppController;

class AdminController extends AppController
{
	public $components = array(
		'Utils'
	);

	public function initialize()
	{
		parent::initialize(true);

		if ($this->request->action === 'index') {
			$this->loadComponent('Search.Prg');
		}
	}

	/**
	 * Index method
	 *
	 * @return void
	 */
	public function index()
	{
		$this->render('/Admin/dashboard');
	}
}
