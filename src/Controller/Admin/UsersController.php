<?php
namespace App\Controller\Admin;

use App\Controller\AppController;
use Cake\Auth\DefaultPasswordHasher;

/**
 * Users Controller
 *
 * @property \App\Model\Table\UsersTable $Users
 */
class UsersController extends AppController
{
	public function initialize()
	{
		parent::initialize(true);

		if ($this->request->action === 'index') {
			$this->loadComponent('Search.Prg');
		}

		$this->loadModel('MyUsers');
	}

	/**
	 * Index method
	 *
	 * @return void
	 */
	public function index()
	{
		$this->paginate = [
			'order' => array('created DESC')
		];

		$this->set('users_count', $this->MyUsers->find('all')->count());

		if (!empty($this->request->query['q'])) {
			$query = $this->MyUsers->find('search', ['search' => $this->request->getQueryParams()]);

			$this->set('users', $this->paginate($query));
		} else {
			$this->set('users', $this->paginate($this->MyUsers));
		}
	}

	public function deleteRequests()
	{
		$this->paginate = [];

		$this->loadModel('DeleteRequests');

		if (!empty($this->request->query)) {
			$query = $this->DeleteRequests->find('search', ['search' => $this->request->getQueryParams()]);

			$this->set('users', $this->paginate($query));
		} else {
			$this->set('users', $this->paginate($this->DeleteRequests));
		}
	}

	/**
	 * View method
	 *
	 * @param string|null $id User id.
	 * @return void
	 * @throws \Cake\Network\Exception\NotFoundException When record not found.
	 */
	public function view($id = null)
	{
		$user = $this->MyUsers->get($id);
		$this->set('user', $user);
	}

	/**
	 * Edit method
	 *
	 * @param string|null $id User id.
	 * @return void Redirects on successful edit, renders view otherwise.
	 * @throws \Cake\Network\Exception\NotFoundException When record not found.
	 */
	public function edit($id = null)
	{
			$user = $this->MyUsers->get($id, [
			'contain' => []
		]);
		$this->loadModel('UserRoles');
		$roles = $this->UserRoles->find('all')->toArray();
		$user_roles = [];
		foreach ($roles as $role) {
			$user_roles[$role['name']] = $role['name'];
		}

		if ($this->request->is(['patch', 'post', 'put'])) {
		     $user->role=$this->request->data['role'];
			$user = $this->MyUsers->patchEntity($user, $this->request->data);

			if ($this->MyUsers->save($user)) {
				$this->Flash->success(__('The user has been saved.'));
				return $this->redirect(['action' => 'index']);
			} else {
				$this->Flash->error(__('The user could not be saved. Please, try again.'));
			}
		}

		$this->set(compact('user'));
		$this->set(compact('user_roles'));
	}

	/**
	 * Add method
	 *
	 * @return void Redirects on successful add, renders view otherwise.
	 */
	public function add()
	{
		$user = $this->MyUsers->newEntity();
		$this->loadModel('UserRoles');
		$this->loadModel('MyUsers');
		$roles = $this->UserRoles->find('all')->toArray();
		$user_roles = [];
		foreach ($roles as $role) {
			$user_roles[$role['name']] = $role['name'];
		}

		if ($this->request->is('post')) {
		    $user->role=$this->request->data['role'];
			$user = $this->MyUsers->patchEntity($user, $this->request->data);
			//echo "<pre>";print_r($user);die;
			$password_hasher = new DefaultPasswordHasher();

			$db_user = $this->MyUsers->find('all', [
				'conditions' => ['email LIKE' => $this->request->data['email']]
			])->first();


			if ($db_user['email'] === $this->request->data['email']) {
				$this->Flash->error(__('The user could not be saved. The email already exists.'));
				return $this->redirect($this->here);
			}
			//if (!empty($user->password)) {

			//	$user->password = $password_hasher->hash($user->password);
		//	}
			if ($this->MyUsers->save($user)) {
				$this->Flash->success(__('The user has been saved.'));
				return $this->redirect(['action' => 'index']);
			} else {
				$this->Flash->error(__('The user could not be saved. Please, try again.'));
			}
		}

		$this->set(compact('user'));
		$this->set(compact('user_roles'));
	}

	/**
	 * Delete method
	 *
	 * @param string|null $id User id.
	 * @return void Redirects to index.
	 * @throws \Cake\Network\Exception\NotFoundException When record not found.
	 */
	public function delete($id = null)
	{
		$this->loadModel('DeleteRequests');
		$this->request->allowMethod(['post', 'delete']);
		$user = $this->MyUsers->get($id);
		if ($this->MyUsers->delete($user)) {
			$this->Flash->success(__('The user has been deleted.'));
			$deletion_requests = $this->DeleteRequests->find('all', [
				'conditions' => [
					'user_id' => $id
				]
			]);

			foreach ($deletion_requests as $deletion_request) {
				$request = $this->DeleteRequests->get($deletion_request->id);
				$request->status = 'processed';
				$this->DeleteRequests->save($request);
			}
		} else {
			$this->Flash->error(__('The user could not be deleted. Please, try again.'));
		}
		return $this->redirect(['action' => 'index']);
	}
}
