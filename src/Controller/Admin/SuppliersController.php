<?php
namespace App\Controller\Admin;

use App\Controller\AppController;

/**
 * Suppliers Controller
 *
 * @property \App\Model\Table\SuppliersTable $Suppliers
 */
class SuppliersController extends AppController
{

	public function initialize()
	{
		parent::initialize(true);

		$this->loadModel('MyUsers');

		if ($this->request->action === 'index') {
			$this->loadComponent('Search.Prg');
		}
	}

	/**
	 * Index method
	 *
	 * @return void
	 */
	public function index()
	{
		$this->set('suppliers', $this->paginate($this->Suppliers));
	}

	/**
	 * View method
	 *
	 * @param string|null $id Supplier id.
	 * @return void
	 * @throws \Cake\Network\Exception\NotFoundException When record not found.
	 */
	public function view($id = null)
	{
		$supplier = $this->Suppliers->get($id, [
			'contain' => ['Companies', 'Sales']
		]);
		$this->set('supplier', $supplier);
	}

	/**
	 * Add method
	 *
	 * @return void Redirects on successful add, renders view otherwise.
	 */
	public function add()
	{
		$supplier = $this->Suppliers->newEntity();

		$all_users = $this->MyUsers->find('all');
		$all_roles = $this->UserRoles->find('all');
		$users = [];

		$users[0] = 'No user';

		foreach ($all_users as $user) {
			$users[$user['id']] = $user['first_name'] . ' ' . $user['last_name'];
		}

		if ($this->request->is('post')) {
			$supplier = $this->Suppliers->patchEntity($supplier, $this->request->data);
			$db_user = $this->MyUsers->find('all', [
				'conditions' => ['email LIKE' => $this->request->data['email']]
			])->first();

			if ($db_user['email'] === $this->request->data['email']) {
				$this->Flash->error(__('The user could not be saved. The email already exists.'));
				return $this->redirect($this->here);
			}
			if ($this->Suppliers->save($supplier)) {
				$this->Flash->success(__('The supplier has been saved.'));
				return $this->redirect(['action' => 'index']);
			} else {
				$this->Flash->error(__('The supplier could not be saved. Please, try again.'));
			}
		}

		$this->set(compact('supplier'));
		$this->set(compact('users'));
		$this->render('add_edit');
	}

	/**
	 * Edit method
	 *
	 * @param string|null $id Supplier id.
	 * @return void Redirects on successful edit, renders view otherwise.
	 * @throws \Cake\Network\Exception\NotFoundException When record not found.
	 */
	public function edit($id = null)
	{
		$supplier = $this->Suppliers->get($id, [
			'contain' => []
		]);

		$all_users = $this->MyUsers->find('all');
		$users = [];

		$users[0] = 'No user';

		foreach ($all_users as $user) {
			$users[$user['id']] = $user['first_name'] . ' ' . $user['last_name'];
		}

		if ($this->request->is(['patch', 'post', 'put'])) {
			$supplier = $this->Suppliers->patchEntity($supplier, $this->request->data);

			if ($this->Suppliers->save($supplier)) {
				$this->Flash->success(__('The supplier has been saved.'));
				return $this->redirect(['action' => 'index']);
			} else {
				$this->Flash->error(__('The supplier could not be saved. Please, try again.'));
			}
		}

		$this->set(compact('supplier'));
		$this->set(compact('users'));
		$this->render('add_edit');
	}

	/**
	 * Delete method
	 *
	 * @param string|null $id Supplier id.
	 * @return void Redirects to index.
	 * @throws \Cake\Network\Exception\NotFoundException When record not found.
	 */
	public function delete($id = null)
	{
		$this->request->allowMethod(['post', 'delete']);
		$supplier = $this->Suppliers->get($id);
		if ($this->Suppliers->delete($supplier)) {
			$this->Flash->success(__('The supplier has been deleted.'));
		} else {
			$this->Flash->error(__('The supplier could not be deleted. Please, try again.'));
		}
		return $this->redirect(['action' => 'index']);
	}
}
