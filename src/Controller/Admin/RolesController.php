<?php
namespace App\Controller\Admin;

use App\Controller\AppController;

/**
 * Users Controller
 *
 * @property \App\Model\Table\UsersTable $Users
 */
class RolesController extends AppController
{
	public function initialize()
	{
		parent::initialize(true);

		$this->loadModel('MyUsers');
		$this->loadModel('UserRoles');
		if ($this->request->action === 'index') {
			$this->loadComponent('Search.Prg');
		}
	}

	/**
	 * Index method
	 *
	 * @return void
	 */
	public function index()
	{

		$this->paginate = [];

		if (!empty($this->request->query['q'])) {
			$query = $this->UserRoles->find('search', ['search' => $this->request->getQueryParams()]);
			// ->contain(['AclGroups']);

			$this->set('roles', $this->paginate($query));
		} else {
			$this->set('roles', $this->paginate($this->UserRoles));
		}
	}

	/**
	 * View method
	 *
	 * @param string|null $id User id.
	 * @return void
	 * @throws \Cake\Network\Exception\NotFoundException When record not found.
	 */
	public function view($id = null)
	{
		$user = $this->MyUsers->get($id);
		$this->set('user', $user);
	}

	/**
	 * Edit method
	 *
	 * @param string|null $id User id.
	 * @return void Redirects on successful edit, renders view otherwise.
	 * @throws \Cake\Network\Exception\NotFoundException When record not found.
	 */
	public function edit($id = null)
	{
		$role = $this->UserRoles->get($id, [
			'contain' => []
		]);


		$role_permissions = (array)json_decode($role['permissions']);

		$permissions = $this->getPermissions();
		if ($this->request->is(['patch', 'post', 'put'])) {
			if (!empty($this->request->data['permissions'])) {

				$allowed_permissions = json_encode($this->request->data['permissions']);

				$update_role = array(
					'id' => $role['id'],
					'name' => $role['name'],
					'permissions' =>  $allowed_permissions
				);

				$role = $this->UserRoles->patchEntity($role, $update_role);

				if ($this->UserRoles->save($role)) {
					$this->Flash->success(__('The role has been saved.'));
					return $this->redirect(['action' => 'index']);
				}
			} else {
				$this->Flash->error(__('The role could not be saved. Please, try again.'));
			}
		}

		$this->set(compact('role'));
		$this->set(compact('role_permissions'));
		$this->set(compact('permissions'));
	}

	/**
	 * Add method
	 *
	 * @return void Redirects on successful add, renders view otherwise.
	 */
	public function add()
	{
		$role = $this->UserRoles->newEntity();

		if ($this->request->is('post')) {
			$default_permission = array(
				'access' => array(
					'admin/index'
				)
			);
			$this->request->data['permissions'] = json_encode($default_permission);
			$role = $this->UserRoles->patchEntity($role, $this->request->data);

			if ($this->UserRoles->save($role)) {
				$this->Flash->success(__('The user has been saved.'));
				return $this->redirect(['action' => 'index']);
			} else {
				$this->Flash->error(__('The user could not be saved. Please, try again.'));
			}
		}

		$this->set(compact('role'));
	}

	/**
	 * Delete method
	 *
	 * @param string|null $id User id.
	 * @return void Redirects to index.
	 * @throws \Cake\Network\Exception\NotFoundException When record not found.
	 */
	public function delete($id = null)
	{
		$this->request->allowMethod(['post', 'delete']);
		$role = $this->UserRoles->get($id);
		if ($this->UserRoles->delete($role)) {
			$this->Flash->success(__('The user has been deleted.'));
		} else {
			$this->Flash->error(__('The user could not be deleted. Please, try again.'));
		}
		return $this->redirect(['action' => 'index']);
	}

	public function getControllers()
	{
		$files = scandir('../src/Controller/Admin');
		$results = [];
		$ignoreList = [
			'.',
			'..',
			'Component',
			'AppController.php',
		];

		foreach ($files as $file) {
			if (!in_array($file, $ignoreList)) {
				$controller = explode('.', $file)[0];
				array_push($results, str_replace('Controller', '', $controller));
			}
		}
		return $results;
	}

	private function getPermissions()
	{
		$controllers = $this->getControllers();
		$resources = [];

		foreach ($controllers as $controller) {
			$className = 'App\\Controller\\Admin\\' . $controller . 'Controller';

			$class = new \ReflectionClass($className);

			$actions = $class->getMethods(\ReflectionMethod::IS_PUBLIC);
			$results = [];
			$ignoreList = ['beforeFilter', 'beforeRender', 'afterFilter', 'initialize'];
			foreach ($actions as $action) {
				if ($action->class == $className && !in_array($action->name, $ignoreList)) {
					$permission = strtolower($controller) . '/' . $action->name;
					$resources[] = $permission;
				}
			}
		}
		return $resources;
	}
}
