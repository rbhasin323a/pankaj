<?php
namespace App\Controller\Admin;

use App\Controller\AppController;

/**
 * Owncloud Controller
 *
 */
class OwncloudController extends AppController
{

	public function initialize()
	{
		parent::initialize(true);

		if ($this->request->action === 'index') {
			//$this->loadComponent('Search.Prg');
		}
	}

	public function index()
	{ }

	public function files()
	{ }
}
