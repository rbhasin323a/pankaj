<?php
namespace App\Controller\Admin;

use App\Controller\AppController;

class CurrencyRatesController extends AppController
{

    /**
     * Index method
     *
     * @return void
     */
    public function index()
    {
        $this->set('currency_rates', $this->paginate($this->CurrencyRates));
    }

    /**
     * View method
     *
     * @param string|null $id Language id.
     * @return void
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function view($id = null)
    {
        $language = $this->Languages->get($id, [
            'contain' => []
        ]);
        $this->set('language', $language);
    }

    /**
     * Add method
     *
     * @return void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $language = $this->Languages->newEntity();
        if ($this->request->is('post')) {
            $language = $this->Languages->patchEntity($language, $this->request->data);
            if ($this->Languages->save($language)) {
                $this->Flash->success(__('The language has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The language could not be saved. Please, try again.'));
            }
        }
        $this->set(compact('language'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Language id.
     * @return void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $currency_rate = $this->CurrencyRates->get($id);
        if ($this->request->is(['patch', 'post', 'put'])) {

            $currency_rate = $this->CurrencyRates->patchEntity($currency_rate, $this->request->data);

            if ($this->CurrencyRates->save($currency_rate)) {
                $this->Flash->success(__('The currency rate has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The currency rate could not be saved. Please, try again.'));
            }
        }
        $this->set(compact('currency_rate'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Language id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $currency_rate = $this->CurrencyRates->get($id);
        if ($this->CurrencyRates->delete($currency_rate)) {
            $this->Flash->success(__('The currency rate has been deleted.'));
        } else {
            $this->Flash->error(__('The currency rate could not be deleted. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }
}
