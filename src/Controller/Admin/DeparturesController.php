<?php
namespace App\Controller\Admin;

use App\Controller\AppController;

/**
 * Departures Controller
 *
 * @property \App\Model\Table\DeparturesTable $Departures
 */
class DeparturesController extends AppController
{

	public function initialize()
	{
		parent::initialize(true);

		if ($this->request->action === 'index') {
			$this->loadComponent('Search.Prg');
		}
	}

	/**
	 * Index method
	 *
	 * @return void
	 */
	public function index()
	{
		$this->set('departures', $this->paginate($this->Departures));
	}

	/**
	 * View method
	 *
	 * @param string|null $id Departure id.
	 * @return void
	 * @throws \Cake\Network\Exception\NotFoundException When record not found.
	 */
	public function view($id = null)
	{
		$departure = $this->Departures->get($id, [
			'contain' => []
		]);
		$this->set('departure', $departure);
	}

	/**
	 * Add method
	 *
	 * @return void Redirects on successful add, renders view otherwise.
	 */
	public function add()
	{
		$departure = $this->Departures->newEntity();
		if ($this->request->is('post')) {
			$departure = $this->Departures->patchEntity($departure, $this->request->data);
			if ($this->Departures->save($departure)) {
				$this->Flash->success(__('The departure has been saved.'));
				return $this->redirect(['action' => 'index']);
			} else {
				$this->Flash->error(__('The departure could not be saved. Please, try again.'));
			}
		}
		$this->set(compact('departure'));
	}

	/**
	 * Edit method
	 *
	 * @param string|null $id Departure id.
	 * @return void Redirects on successful edit, renders view otherwise.
	 * @throws \Cake\Network\Exception\NotFoundException When record not found.
	 */
	public function edit($id = null)
	{
		$departure = $this->Departures->get($id, [
			'contain' => []
		]);
		if ($this->request->is(['patch', 'post', 'put'])) {
			// Tiny INT Toggles
			$toggles = array(
				'active',
			);

			foreach ($toggles as $toggle) {
				if (empty($this->request->data[$toggle])) {
					$this->request->data[$toggle] = 0;
				}
			}

			$departure = $this->Departures->patchEntity($departure, $this->request->data);
			if ($this->Departures->save($departure)) {
				$this->Flash->success(__('The departure has been saved.'));
				return $this->redirect(['action' => 'index']);
			} else {
				$this->Flash->error(__('The departure could not be saved. Please, try again.'));
			}
		}
		$this->set(compact('departure'));
	}

	/**
	 * Delete method
	 *
	 * @param string|null $id Departure id.
	 * @return void Redirects to index.
	 * @throws \Cake\Network\Exception\NotFoundException When record not found.
	 */
	public function delete($id = null)
	{
		$this->request->allowMethod(['post', 'delete']);
		$departure = $this->Departures->get($id);
		if ($this->Departures->delete($departure)) {
			$this->Flash->success(__('The departure has been deleted.'));
		} else {
			$this->Flash->error(__('The departure could not be deleted. Please, try again.'));
		}
		return $this->redirect(['action' => 'index']);
	}
}
