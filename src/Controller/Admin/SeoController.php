<?php
namespace App\Controller\Admin;

use App\Controller\AppController;
use Cake\Core\Configure;

/**
 * Seo Controller
 *
 * @property \App\Model\Table\SeoTable $SEO
 */
class SeoController extends AppController
{

	public function initialize()
	{
		parent::initialize(true);

		if ($this->request->action === 'index') {
			$this->loadComponent('Search.Prg');
		}
	}

	/**
	 * Index method
	 *
	 * @return void
	 */
	public function index()
	{
		$this->set('seos', $this->paginate($this->Seo));
	}

	/**
	 * Add method
	 *
	 * @return void Redirects on successful add, renders view otherwise.
	 */
	public function add()
	{
		$this->loadModel('Languages');

		// load front-end pages
		$pages = [];
		foreach (glob(APP . 'Template' . DS . 'Pages/*.*') as $file) {
			$path_parts = explode('/', $file);
			foreach ($path_parts as $part) {
				if (strpos($part, 'ctp')) {
					$page_name_parts = explode('.', $part);
					$pages[$page_name_parts[0]] = $page_name_parts[0];
				}
			}
		}
		$pages['contact'] = 'contact';
		$pages['login'] = 'login';
		$pages['register'] = 'register';
		$pages['account'] = 'account';
		$pages['bookings'] = 'bookings';
		$pages['flights'] = 'flights';
		$pages['resend_validation'] = 'resend_validation';
		$pages['latest_sales'] = 'latest_sales';
		// load languages
		$languages_query = $this->Languages->find('all');

		$languages = [];

		foreach ($languages_query as $key => $value) {
			if ($value['name'] == 'English') continue;
			$languages[$value['locale']] = $value['name'];
		}

		$seo = $this->Seo->newEntity();
		if ($this->request->is('post')) {
			// SEO Translations
			if (!empty($this->request->data['_translations'])) {
				$translations = $this->request->data['_translations'];

				$this->set('_translations', $translations);

				$default_locale = Configure::read('App.defaultLocale');

				if (!empty($translations[$default_locale])) {
					foreach ($translations[$default_locale] as $field => $field_translation) {
						$this->request->data[$field] = $field_translation;
					}
				}

				unset($this->request->data['_translations']);
			}

			$seo = $this->Seo->patchEntity($seo, $this->request->data);

			if ($this->Seo->save($seo)) {
				// Translate
				if (!empty($translations)) {
					$seo = $this->Seo->get($seo->id, [
						'contain' => []
					]);
					foreach ($translations as $lang => $data) {

						$seo->translation($lang)->set($data, ['guard' => false]);
					}
					$this->Seo->save($seo);
				}
				// End Translations
				$this->Flash->success(__('The SEO data has been saved.'));
				return $this->redirect(['action' => 'index']);
			} else {
				$this->Flash->error(__('The SEO data could not be saved. Please, try again.'));
			}
		}
		$this->set(compact('seo', 'pages'));
	}

	/**
	 * Edit method
	 *
	 * @param string|null $id Contact id.
	 * @return void Redirects on successful edit, renders view otherwise.
	 * @throws \Cake\Network\Exception\NotFoundException When record not found.
	 */
	public function edit($id = null)
	{
		$this->loadModel('Languages');

		// load front-end pages
		$pages = [];
		$contact_page = (APP . 'Template' . DS . 'Contact' . DS . 'index.ctp');
		foreach (glob(APP . 'Template' . DS . 'Pages/*.*') as $file) {
			$path_parts = explode('/', $file);
			foreach ($path_parts as $part) {
				if (strpos($part, 'ctp')) {
					$page_name_parts = explode('.', $part);
					$pages[$page_name_parts[0]] = $page_name_parts[0];
				}
			}
		}
		$pages['contact'] = 'contact';
		$pages['login'] = 'login';
		$pages['register'] = 'register';
		$pages['account'] = 'account';
		$pages['bookings'] = 'bookings';
		$pages['resend_validation'] = 'resend_validation';
		$pages['flights'] = 'flights';
		$pages['latest_sales'] = 'latest_sales';

		$seo = $this->Seo->get($id, [
			'contain' => []
		]);

		$languages_query = $this->Languages->find('all');

		$languages = [];

		foreach ($languages_query as $key => $value) {
			if ($value['name'] == 'English') continue;
			$languages[$value['locale']] = $value['name'];
		}

		// SEO Translations from DB
		$seo_translation = $this->Seo->find('translations')->where(['Seo.id' => $id])->first()->toArray();
		$this->set('_translations', $seo_translation['_translations']);

		if ($this->request->is(['patch', 'post', 'put'])) {
			// SEO Translations new
			if (!empty($this->request->data['_translations'])) {
				$translations = $this->request->data['_translations'];

				$this->set('_translations', $translations);

				$default_locale = Configure::read('App.defaultLocale');

				if (!empty($translations[$default_locale])) {
					foreach ($translations[$default_locale] as $field => $field_translation) {
						if ($field == 'image_to_sale') continue;
						$this->request->data[$field] = $field_translation;
					}
				}
				unset($this->request->data['_translations']);
			}

			$seo = $this->Seo->patchEntity($seo, $this->request->data);
			if ($this->Seo->save($seo)) {
				// save new translations
				if (!empty($translations)) {
					$seo = $this->Seo->get($seo->id, [
						'contain' => []
					]);
					foreach ($translations as $lang => $data) {

						$seo->translation($lang)->set($data, ['guard' => false]);
					}
					$this->Seo->save($seo);
				}
				// end of save new translations
				$this->Flash->success(__('The contact has been saved.'));
				return $this->redirect(['action' => 'index']);
			} else {
				$this->Flash->error(__('The contact could not be saved. Please, try again.'));
			}
		}
		$this->set(compact('seo', 'pages'));
	}

	/**
	 * Delete method
	 *
	 * @param string|null $id Contact id.
	 * @return void Redirects to index.
	 * @throws \Cake\Network\Exception\NotFoundException When record not found.
	 */
	public function delete($id = null)
	{
		$this->request->allowMethod(['post', 'delete']);
		$seo = $this->Seo->get($id);
		if ($this->Seo->delete($seo)) {
			$this->Flash->success(__('The SEO data has been deleted.'));
		} else {
			$this->Flash->error(__('The SEO data could not be deleted. Please, try again.'));
		}
		return $this->redirect(['action' => 'index']);
	}
}
