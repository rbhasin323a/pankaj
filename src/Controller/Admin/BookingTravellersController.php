<?php
namespace App\Controller\Admin;

use App\Controller\AppController;

class BookingTravellersController extends AppController
{

	public function index()
	{ }

	public function edit()
	{
		if (!empty($this->request->params['booking_id'])) {
			$booking_id = $this->request->params['booking_id'];
		}

		$post_data = $this->request->data['traveller'];
		$travellers = $this->BookingTravellers->find('all')->where(['booking_id' => $booking_id]);

		if ($this->request->is(['patch', 'post', 'put'])) {
			foreach ($post_data as $traveller) {
				$entity = $this->BookingTravellers->newEntity($traveller);
				$patched_traveller = $this->BookingTravellers->patchEntity($entity, $traveller);
				$this->BookingTravellers->save($patched_traveller);
			}


			$this->Flash->success(__('Success. You have modified the travellers for booking ' . $booking_id . '.'));
			return $this->redirect('/admin/bookings/edit/' . $booking_id);
		}
	}
}
