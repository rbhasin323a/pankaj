<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Event\Event;
use Cake\I18n\I18n;
use Cake\I18n\Number;
use Cake\I18n\Time;
use Cake\ORM\TableRegistry;
use Cake\Mailer\Email;
use Cake\Core\Configure;

/**
 * Sales Controller
 *
 * @property \App\Model\Table\SalesTable $Sales
 */
class SalesController extends AppController
{
	public function initialize()
	{
		parent::initialize();

		if ($this->request->action === 'index') {
			$this->loadComponent('Search.Prg');

			/**
			 * Generate cache for logged out GET requests
			 */
			// $session_user = $this->request->getSession()->read('Auth.User');

			// if ($this->request->is('get') && empty($session_user['id'])) {
			// 	$this->loadComponent('Cache.Cache');
			// }
		}
	}

	public function beforeFilter(Event $event)
	{
		parent::beforeFilter($event);
		$this->Auth->allow(['index', 'breturn']);
		$this->Auth->allow(['view', 'breturn']);
		$this->Auth->allow(['getTimeRemaining', 'breturn']);
		$this->Auth->allow(['setExpiredStatus', 'breturn']);
		$this->Auth->allow(['sortOffersByMinRate', 'breturn']);
		$this->Auth->allow(['bookTravellers', 'breturn']);
		$this->Auth->allow(['getSaleAllocations', 'breturn']);
	}

	/**
	 * Index method
	 *
	 * @return void
	 */
	public function index()
	{
	    //echo "<pre>";print_r($_REQUEST);die;
	  
	    //phpinfo();die;
        $this->loadModel('TranslationSeo');
		$this->loadModel('TranslationFooter');
		$this->loadModel('Seo');
		$this->loadModel('Footer');

		$this->paginate = [
			'limit' => 10,
			'order' => [
				'created' => 'desc'
			],
			'contain' => ['SaleOffers.SaleAllocations', 'ImageToSale', 'SaleToCategory'],
		];

    
		if (!empty($this->request->query)) {
		   if(!empty($this->Auth->user('id'))){
		       return $this->redirect(['controller' => 'Sales', 'action' => 'index']);
		       
		   }
		    
		    echo 'A';
			$conditions = [
				'OR' => [
					['Sales.active' => 1],
					['Sales.active' => 2]
				],
				'date_open <= ' => date('Y-m-d H:i'),
				'date_close >= ' => date('Y-m-d H:i')
			];

			$conditions['AND'] = [];
			if (!empty($this->request->query['q'])) {
				$conditions['AND'] = ['OR' => [
					'title LIKE' => '%' . $this->request->query['q'] . '%',
					'sub_title LIKE' => '%' . $this->request->query['q'] . '%',
					'destination LIKE' => '%' . $this->request->query['q'] . '%',
					'location_title LIKE' => '%' . $this->request->query['q'] . '%'
				]];
			}

			if (!empty($this->request->query['filter_category'])) {
				$conditions['AND']['stc.category_id LIKE'] = '%' . $this->request->query['filter_category'] . '%';
			}
			$query = $this->Sales
				->find('all')
				->select(['id', 'title', 'sub_title', 'sale_type', 'main_image', 'date_open', 'date_close', 'destination', 'location_title', 'promoted', 'slug', 'currency_id', 'created', 'active'])
				->contain(['SaleOffers.SaleAllocations', 'ImageToSale'])
				->join([
					'table' => 'sale_to_category',
					'alias' => 'stc',
					'type' => 'LEFT',
					'conditions' => 'stc.sale_id = Sales.id'
				])
				->where($conditions)
				->order([
					'promoted' => 'DESC'
				]);
		} else {
			$query = $this->Sales
				->find('all')
				->select(['id', 'title', 'sub_title', 'sale_type', 'main_image', 'date_open', 'date_close', 'destination', 'location_title', 'promoted', 'slug', 'currency_id', 'created', 'active'])
				->contain(['SaleOffers.SaleAllocations', 'ImageToSale'])
				->where([
					'OR' => [
						['active' => 1],
						['active' => 2],
					],
					'date_open <= ' => date('Y-m-d H:i'),
					'date_close >= ' => date('Y-m-d H:i')
				])->order([
					'promoted' => 'DESC'
				]);
		}

		$paginated_query = $query->all();

		/**
		 * @todo Fix $paginated_query duplicate entries, temporary
		 */
		$sales = [];

		foreach ($paginated_query->toArray() as $sale_key => $sale) {
			if (!empty($sale) && !empty($sale->id)) {
				$sales[$sale->id] = $sale;
			}
		}

		$discounts = [];
		$time_lefts = [];
		$prices = [];
		$deposits = [];

		// Iterating through all Sales
		foreach ($sales as $sale) {
			$time_lefts[$sale->id] = $this->getTimeRemaining($sale);
			$discounts[$sale->id] = 0;
			$deposits[$sale->id] = false;

			$fixed = false;
			$flexible = false;

			if (!empty($sale->sale_offers)) {
				$min_rate = PHP_INT_MAX;

				foreach ($sale->sale_offers as $sale_offer) {
					if ($sale_offer->offer_type == '1') {
						$flexible = true;
					} else {
						$fixed = true;
					}
				}

				// Iterating through all Sale Offers
				foreach ($sale->sale_offers as $sale_offer) {

					// Check if the Sale has deposit
					if ($sale_offer->deposit) {
						$deposits[$sale->id] = true;
					}

					$min_rate_days_iterations = 0;
					$min_rate_price_iterations = 0;

					// Check if the Sale Offer is active and has Sale Allocations
					if ($sale_offer->active && !empty($sale_offer->sale_allocations)) {

						// Iterating through all Sale Allocations
						foreach ($sale_offer->sale_allocations as $sale_allocation) {

							// Check if the Sale Allocation has discounts
							if ($sale_allocation->discount > $discounts[$sale->id]) {
								$discounts[$sale->id] = $sale_allocation->discount;
							}

							// Check if the Sale Allocation start date is after or today
							if ($sale_allocation->date_start >= date('Y-m-d') && $sale_allocation->available_rooms != 0) {

								// Assign start and end dates
								$date_end = strtotime($sale_allocation->date_end);
								$date_start = strtotime($sale_allocation->date_start);

								// Calculate days difference between the start and end date
								$days_diff = floor(($date_end - $date_start) / (60 * 60 * 24));

								// When Sale Offer type is NOT flexible and there are min nights set for the sale allocation > days difference for the current sale allocation
								// This is used only for the Fixed Sale Offers because they have min nights set in the settings and we should go through the next days in order to calculate the min rate.
								if ($min_rate_days_iterations == 0 && $days_diff < $sale_allocation->min_nights && $sale_offer->offer_type != 1) {
									// Assign the min nights of the allocation to the min rate days iterations variable, so we can know how many days should be added to the min rate.
									$min_rate_days_iterations = $sale_allocation->min_nights;
									$min_rate_price_iterations = $sale_allocation->rate;
									// Go to the next allocation/day
									continue;
								}
								if ($sale_allocation->rate < $min_rate) {
									$min_rate = $sale_allocation->rate;
									$respective_rack_rate = $sale_allocation->rack_rate;
									$sale_type = $sale_offer->offer_type;
									$text = __('per night');
								}
							}
						}
					}
				}

				// Assign the calculated min rates and rack rates to the template
				if ($min_rate != PHP_INT_MAX && $min_rate < $respective_rack_rate) {
					$prices[$sale->id]['min_rate'] = $this->Currency->convert($sale->currency_id, null, $min_rate);
					$prices[$sale->id]['respective_rack_rate'] = $this->Currency->convert($sale->currency_id, null, $respective_rack_rate);
				} else {
					$prices[$sale->id]['min_rate'] = $this->Currency->convert($sale->currency_id, null, $respective_rack_rate);
					$prices[$sale->id]['respective_rack_rate'] = $this->Currency->convert($sale->currency_id, null, $respective_rack_rate);
				}

				$prices[$sale->id]['sale_type'] = !empty($sale_type) ? $sale_type : '';
				$prices[$sale->id]['text'] = !empty($text) ? $text : '';
				$prices[$sale->id]['fixed'] = !empty($fixed) ? $fixed : '';
				$prices[$sale->id]['flexible'] = !empty($flexible) ? $flexible : '';
			}
		}

		//load footer
		$query_footer = $this->Footer->find()->where(['Footer.id' => 'XANQW4QFAODS']);
		$footer_data = $query_footer->first();
		$footer_menu = $footer_data['menu_content'];
		$footer_copyright_text = $footer_data['copyright_text'];

		//load the SEO titles
		$query = $this->Seo->find()->where(['Seo.page' => 'latest_sales']);
		$seo_data = $query->first();

		$seo_title = isset($seo_data['seo_title']) ? $seo_data['seo_title'] : '';
		$seo_description = isset($seo_data['seo_description']) ? $seo_data['seo_description'] : '';

		$this->set('time_lefts', $time_lefts);
		$this->set('prices', $prices);
		$this->set('sales', $sales);
		$this->set('deposits', $deposits);
		$this->set('discounts', $discounts);
		$this->set('footer_menu', $footer_menu);
		$this->set('footer_copyright_text', $footer_copyright_text);
		$this->set('seo_title', $seo_title);
		$this->set('seo_description', $seo_description);
	}

	public function getTimeRemaining($sale)
	{
		$end = strtotime($sale->date_close);
		$diff = $end - time();

		if ($diff > 0 && $sale->active == 1) {
			$days = floor($diff / 3600 / 24);
			$hours = floor($diff / 3600 % 24);
			$mins = ($diff / 60 % 60);

			return array('days' => $days, 'hours' => $hours, 'mins' => $mins);
		} else {
			$this->setExpiredStatus($sale);

			return array();
		}
	}

	private function setExpiredStatus($sale)
	{
		$sale_entity = $this->Sales->get($sale->id);

		$sale_entity = $this->Sales->patchEntity($sale_entity, ['active' => 2]);

		if ($this->Sales->save($sale_entity)) {
			$sale->active = 2;
		}
	}

	public function getBestOffer($sale_offer)
	{
		$allocation_id = '';
		$max_discount = 0;

		if (isset($sale_offer->sale_allocations)) {
			foreach ($sale_offer->sale_allocations as $sale_allocation) {
				if ($sale_allocation->date_start >= date("Y-m-d")) {
					if ($sale_allocation->discount > $max_discount) {
						$max_discount = $sale_allocation->discount;
						$allocation_id = $sale_allocation->id;
					}
				}
			}
		}
		return $allocation_id;
	}

	public function view($param = null)
	{
		$this->loadModel('TranslationFooter');
		$this->loadModel('Footer');

		if (!empty($this->request->params['slug'])) {
			$param = $this->request->params['slug'];
		} else if (!empty($this->request->params['id'])) {
			$param = $this->request->params['id'];
		}

		$is_admin = $this->Auth->user('role') == 'admin' ? true : false;

		$conditions = [];

		if (!$is_admin) {
			$conditions = [
				'AND' => [
					'OR' => [
						['Sales.active' => 1],
						['Sales.active' => 2]
					]
				]
			];
		}

		$query = $this->Sales->find()
			->where([
				'AND' => [
					'OR' => ['Sales.id' => $param, 'Sales.slug' => $param],
					$conditions
				]
			])->contain([
				'SaleOffers' =>
				function ($q) {
					return $q->where(['SaleOffers.active' => 1])->contain(['SaleAllocations']);
				},
				'ImageToSale' => function ($q) {
					return $q->order(['ImageToSale.sort_order' => 'ASC']);
				},
				'Companies.Contacts'
			]);

		$sale = $query->first();

		if (empty($sale)) {
			$this->loadModel('TranslationSales');

			$translation_query = $this->TranslationSales->find()
				->where(['TranslationSales.field' => 'slug'])
				->andWhere(['TranslationSales.content' => $param]);

			$translated_slug = $translation_query->first();

			if (!empty($translated_slug)) {
				$query = $this->Sales->find()
					->where(['Sales.id' => $translated_slug->foreign_key])
					->andWhere(['Sales.active' => 1])
					->contain([
						'SaleOffers' =>
						function ($q) {
							return $q->where(['SaleOffers.active' => 1])->contain(['SaleAllocations']);
						},
						'ImageToSale' => function ($q) {
							return $q->order(['ImageToSale.sort_order' => 'ASC']);
						},
					]);
			}

			$sale = $query->first();
		}

		if (empty($sale)) {
			return $this->redirect(['controller' => 'Sales', 'action' => 'index']);
		}

		$airports = [];
		$deposits = [];

		$this->loadModel('ImageToSale');
		$images_with_translations = $this->ImageToSale->find('translations', ['locales' => [I18n::locale()]])->order(['ImageToSale.sort_order' => 'ASC'])->where(['ImageToSale.sale_id' => $sale->id])->toArray();
		foreach ($images_with_translations as &$image_with_translations) {
			if (!empty($image_with_translations['_translations'][I18n::locale()]['image_alt'])) {
				$image_with_translations['image_alt'] = $image_with_translations['_translations'][I18n::locale()]['image_alt'];
			}
		}
		$this->set('images_with_translations', $images_with_translations);

		// Iterating through all Sale Offers
		foreach ($sale->sale_offers as &$sale_offer) {
			$this->loadModel('SaleAllocations');

			$sale_allocation_id = $this->getBestOffer($sale_offer);

			$best_sale_offer_allocation_query = $this->SaleAllocations->find()
				->where(['SaleAllocations.id' => $sale_allocation_id]);

			$sale_offer->best_sale_offer_allocation = $best_sale_offer_allocation_query->first();

			$fixedMaxMinNights = -1;
			$prices_for_offers_with_2_nights_stay = [];

			// Check if the Sale has deposit
			if ($sale_offer->deposit) {
				$deposits[$sale_offer->id] = PHP_INT_MAX;
			}

			$sale_offer->min_rate = PHP_INT_MAX;

			$min_rate_days_iterations = 0;
			$min_rate_price_iterations = 0;

			// Iterating through all Sale Allocations
			foreach ($sale_offer->sale_allocations as $sale_allocation) {
				$dateStart = strtotime($sale_allocation->date_start);
				if ($dateStart < time()) {
					continue;
				}

				$prices_for_offers_with_2_nights_stay[] = $sale_allocation->rate;
				$fixedMaxMinNights = ($sale_allocation->min_nights > $fixedMaxMinNights) ? $sale_allocation->min_nights : $fixedMaxMinNights;

				// Check if the Sale Allocation start date is after or today
				if ($sale_allocation->date_start >= date('Y-m-d')) {
					$airports[$sale_offer->id][] = $sale_allocation->departure_airport_code;

					// Assign start and end dates
					$date_end = strtotime($sale_allocation->date_end);
					$date_start = strtotime($sale_allocation->date_start);

					// Calculate days difference between the start and end date
					$days_diff = floor(($date_end - $date_start) / (60 * 60 * 24));

					// If the Sale Offer is fixed, then the days difference should be the min nights set for the allocation
					if ($sale_offer->offer_type == 0) {
						$days_diff = $sale_allocation->min_nights;
					}

					// When Sale Offer type is NOT flexible and there are min nights set for the sale allocation > days difference for the current sale allocation
					// This is used only for the Fixed Sale Offers because they have min nights set in the settings and we should go through the next days in order to calculate the min rate.
					if ($min_rate_days_iterations == 0 && $days_diff < $sale_allocation->min_nights && $sale_offer->offer_type != 1) {
						// Assign the min nights of the allocation to the min rate days iterations variable, so we can know how many days should be added to the min rate.
						$min_rate_days_iterations = $sale_allocation->min_nights;
						$min_rate_price_iterations = $sale_allocation->rate;
						// Go to the next allocation/day
						continue;
					}

					// When we finish with the iterations OR the Sale Offer is flexible
					if ($min_rate_days_iterations == 0) {
						if ($sale_allocation->rate < $sale_offer->min_rate) {
							// If the sale allocation rate is smaller than the calculated min rate
							$sale_offer->min_rate = $sale_allocation->rate * ($sale_offer->offer_type != 1 ? $sale_allocation->min_nights : 1);
						}
					} else {

						if ($sale_allocation->available_rooms > 0) {
							// Add the price of each next allocation to the min_rate, so we calculate how much costs the whole stay.
							$min_rate_price_iterations += $sale_allocation->rate;
						}

						// Decrement the iterations on each price addition
						$min_rate_days_iterations--;

						if ($min_rate_days_iterations == 0) {

							// When we are close to the end of the iterations, we should make the calculations
							if ($min_rate_price_iterations < $sale_offer->min_rate) {
								$sale_offer->min_rate = $min_rate_price_iterations;
							}
						}
					}
				}

				if ($sale_offer->deposit && !is_null($sale_allocation->total_deposit)) {
					if ($sale_allocation->total_deposit < $deposits[$sale_offer->id]) {
						$deposits[$sale_offer->id] = $sale_allocation->total_deposit;
					}
				}
			}

			if (!empty($sale_allocation) && $min_rate_days_iterations == 0 && !empty($prices_for_offers_with_2_nights_stay)) {
				$sale_offer->min_rate = min($prices_for_offers_with_2_nights_stay) * ($sale_offer->offer_type != 1 ? $fixedMaxMinNights : 1);
			}

			if (!empty($airports[$sale_offer->id])) {
				$airports[$sale_offer->id] = array_values(array_unique($airports[$sale_offer->id]));
			}

			if ($sale_offer->offer_type != 1) {
				// Fixed number of nights offer, just use the new calculation method, which finds the cheapest stretch of min_nights which is actually possible to be booked
				$sale_offer->min_rate = $this->calculateMinPriceForFixedOffer($sale_offer);
			}
		}

		// Sort sale offers
		usort($sale->sale_offers, function ($a, $b) {
			return $a->min_rate - $b->min_rate;
		});

		// Check for YouTube Video ID
		preg_match('/\?v=(.+)/', $sale->video_url, $matches);

		if (isset($matches[1])) {
			$video_id = $matches[1];
		} else {
			$video_id = null;
		}

		// Check if expired
		if ($sale->active == 2) {
			$this->Flash->error(__('This offer has expired. Bookings for it cannot be made.'));
		}

		//load footer
		$query_footer = $this->Footer->find()->where(['Footer.id' => 'XANQW4QFAODS']);
		$footer_data = $query_footer->first();
		$footer_menu = $footer_data['menu_content'];
		$footer_copyright_text = $footer_data['copyright_text'];

		$this->set('video_id', $video_id);
		$this->set('airports', $airports);
		$this->set('sale', $sale);
		$this->set('deposits', $deposits);
		$this->set('footer_menu', $footer_menu);
		$this->set('footer_copyright_text', $footer_copyright_text);
		$this->set('images', $sale->image_to_sale);
	}

	public function getLocalCountry()
	{
		require_once("../dbip-client.class.php");
		$addrInfo = Address::lookup($_SERVER["REMOTE_ADDR"]);
		$addrInfo = json_decode(json_encode($addrInfo), true);
		return $addrInfo['countryCode'];
	}

	public function book($param = null)
	{
		$this->loadModel('MyUsers');
		$this->loadModel('TranslationFooter');
		$this->loadModel('Footer');
		$session = $this->request->getSession();
		$post_data = $this->request->data;

		if (!empty($post_data['book'])) {
			$session->write('booking', $post_data['book']);
			$this->set('book', $post_data['book']);
		} else if (empty($post_data)) {
			if (!empty($param)) {
				return $this->redirect(['controller' => 'Sales', 'action' => 'view', $param]);
			} else {
				return $this->redirect(['action' => 'index']);
			}
		}

		$query = $this->Sales->find('all')
			->where(['Sales.id' => $param])
			->orWhere(['Sales.slug' => $param])
			->andWhere(['Sales.active' => 1])
			->contain([
				'Contractors', 'Suppliers', 'Companies', 'Currencies', 'Countries', 'Taxes',
				'SaleOffers' => function ($q) {
					return $q->where(['SaleOffers.active' => 1]);
				}
			]);

		$sale = $query->first();

		$sale_allocations = [];

		$date_start = "";
		$date_end = "";

		if (!empty($post_data['book']['offer_id']) && !empty($post_data['book']['sale_allocation_id'])) {
			$this->loadModel('SaleAllocations');
			$this->loadModel('SaleOffers');

			$sale_allocations_ids = explode(',', $post_data['book']['sale_allocation_id']);

			$new_sale_allocations = [];

			foreach ($sale_allocations_ids as $id) {
				$new_sale_allocations[] = array(
					'id' => $id
				);
			}

			$sale_allocations = $this->SaleAllocations->find('all', array(
				'conditions' => array(
					'OR' => $new_sale_allocations
				)
			));
			if (count($sale_allocations->toArray()) > 1) {
				$date_start = $post_data['book']['start_flexible_date'];
				$date_end = $post_data['book']['end_flexible_date'];
			} else {
				$sale_allocation = $sale_allocations->toArray();
				$date_start = $sale_allocation[0]['date_start'];
				$date_end = $sale_allocation[0]['date_end'];
			}

			$sale_offer = $this->SaleOffers->find()->where(['SaleOffers.id' => $post_data['book']['offer_id']])->first();
		} else {
			$sale_allocation = [];
			$sale_offer = [];
		}

		if (empty($sale) /*or empty($sale->available_rooms)*/) {
			return $this->redirect(['controller' => 'Sales', 'action' => 'index']);
		}

		//load footer
		$query_footer = $this->Footer->find()->where(['Footer.id' => 'XANQW4QFAODS']);
		$footer_data = $query_footer->first();
		$footer_menu = $footer_data['menu_content'];
		$footer_copyright_text = $footer_data['copyright_text'];

		$this->set('footer_menu', $footer_menu);
		$this->set('footer_copyright_text', $footer_copyright_text);
		$this->set('sale', $sale);
		$this->set('sale_allocations', $sale_allocations);
		$this->set('date_start', $date_start);
		$this->set('date_end', $date_end);
		$this->set('sale_offer', $sale_offer);
		$this->set('countries', $this->getAllCountries());

		if ($this->request->is('post') && !empty($post_data['transaction'])) {
			$this->loadModel('Bookings');
			$this->loadModel('Transactions');
			$this->loadModel('BookingAllocations');
			$this->loadModel('Currencies');

			$currency = $this->request->getSession()->read('Config.currency');

			if (empty($currency)) {
				$currency = Number::defaultCurrency();
			}

			if ($currency == "USD") {
				$currency = "EUR";
			}

			if ($currency == "TRY") {
				$currency = "TRY";
			}

			$currency_query = $this->Currencies->find('all')->where(['code' => $currency])->first();

			$post_data['user_id'] = $this->Auth->user('id');
			$post_data['currency_id'] = $currency_query->id;
			$post_data['user_agent'] = $this->request->env('HTTP_USER_AGENT');
			$post_data['sale_total'] = str_replace(',', '', $post_data['sale_total']);

			$booking = $this->Bookings->newEntity();

			$booking = $this->Bookings->patchEntity($booking, $post_data, [
				'associated' => ['Transactions', 'BookingAllocations']
			]);

			if ($this->Bookings->save($booking)) {
				$lang = $this->request->getSession()->read('Config.language');
				$language = 'EN';
				if (!empty($lang) && $lang != 'tr') {
					$language = strtoupper($lang);
				}

				if ($currency == 'TRY') {
					require_once(MALDOPAY_PATH . 'maldopay.php');

					if ($language == "GB") {
						$language_m = "EN";
					} else {
						$language_m = $language;
					}
					$country_id = $this->getLocalCountry();
					$id = $this->Auth->user('id');
					$user = $this->MyUsers->get($id);
					$player_id = substr(md5($post_data['user_id']), 0, 12);

					$passportNo = "test";
					$address = isset($user['address_1']) ? $user['address_1'] : "";
					$postCode = isset($user['postcode']) ? $user['postcode'] : "";

					$authenticate_user = maldopay::authentication(
						$post_data['first_name'],
						$post_data['last_name'],
						$address,
						$user['birthday'],
						$passportNo,
						$country_id,
						$user['city'],
						$player_id,
						$postCode,
						$currency,
						$language_m,
						$user['email'],
						$post_data['phone'],
						$_SERVER['REMOTE_ADDR']
					);

					if (isset($authenticate_user['auth']['response']['userId']) && isset($authenticate_user['auth']['response']['codeMessage']) && $authenticate_user['auth']['response']['codeMessage'] != 'Successful') {
						$this->set('codeMessage', $authenticate_user['auth']['response']['codeMessage']);
						$this->set('booking_id', $booking->id);
						$this->render('maldopay_authentication_error');
					} else {
						$token = $authenticate_user['response']['token'];
						//set the correct currency rates
						$this->CurrencyRates = TableRegistry::get('CurrencyRates');
						$eur_to_try = (float)$this->CurrencyRates->find('all', ['conditions' => ['fromCurrency' => 'EUR', 'toCurrency' => 'TRY']])->first()->rates;
						$bgn_to_eur = (float)$this->CurrencyRates->find('all', ['conditions' => ['fromCurrency' => 'BGN', 'toCurrency' => 'EUR']])->first()->rates;

						if ($currency == 'BGN') {
							$amount = (ceil(str_replace(',', '', $booking['sale_total'])) * $bgn_to_eur) * $eur_to_try;
						} else if ($currency == 'EUR') {
							$amount = ceil(str_replace(',', '', $booking['sale_total'])) * $eur_to_try;
						} else {
							$amount = ceil(str_replace(',', '', $booking['sale_total']));
						}

						$referenceOrderId = $booking->id;
						$chainMode = false;
						$national_id_number = 'test';
						$maldopay = maldopay::transaction_processing($currency, $token, $amount, $referenceOrderId, $chainMode, $national_id_number);

						if ($maldopay['transaction']['codeId'] == 4009 || $maldopay['transaction']['codeId'] == 400) {
							$this->Flash->error($maldopay['transaction']['codeMessage']);
							return $this->redirect(['controller' => 'Sales', 'action' => 'book', $sale->id]);
						}

						$maldopay_url = isset($maldopay['redirect']) ? $maldopay['redirect'] : '';
						$this->autoRender = false;
						header('Location: ' . $maldopay_url);
					}
				} else {
					if ($sale->sale_type == 1) {
						return $this->redirect(['action' => 'bookTravellers', $booking->id]);
					} else {
						$this->autoRender = false;

						require_once(BORICA_PATH . 'eborica.php');
						$transactionCode = '10';

						$amount = ceil(str_replace(',', '', $booking['sale_total']));
						$terminalID = '62161099';

						if ($currency == 'EUR') {
							$terminalID = '62161140';
						}

						$orderID = $booking->id;
						$orderDescription = 'Booking #' . $booking->id;
						$lang = $this->request->getSession()->read('Config.language');
						$language = 'EN';
						if (!empty($lang) && $lang != 'tr') {
							$language = strtoupper($lang);
						}

						$protocolVersion = '1.1';
						$privateKeyFileName = BORICA_PATH . 'luxury-discounts.com.real.key';

						if ($currency == 'EUR') {
							$privateKeyFileName = BORICA_PATH . 'eur/luxury-discounts.com.real.key';
						}

						$privateKeyPassword = '';

						$message = eBorica::generateBOReq(
							$privateKeyFileName,
							$privateKeyPassword,
							$transactionCode,
							$amount,
							$terminalID,
							$orderID,
							$orderDescription,
							$language,
							$protocolVersion,
							$currency
						);

						$url = eBorica::gatewayURL . "registerTransaction?eBorica=" .
							urlencode(base64_encode($message));
						header('Location: ' . $url);
					}
				}
			} else {
				$this->set('book', $post_data);
				$this->Flash->error(__('The booking could not be saved. Please, try again.'));
			}
		}
	}

	public function bookTravellers($param = null)
	{
		$session = $this->request->getSession();

		if (!empty($param)) {
			$booking_id = $param;
			$this->loadModel('Bookings');
			$this->loadModel('Sales');
			$this->loadModel('BookingTravellers');

			$booking = $this->Bookings->find()->where(['Bookings.id' => $booking_id])->first();
			$sale = $this->Sales->find()->where(['Sales.id' => $booking->sale_id])->first();
			$post_data = $this->request->data;

			if ($this->request->is('post')) {
				$book_travellers = $this->BookingTravellers->newEntities($post_data['travellers']);
				$success = true;

				foreach ($book_travellers as $traveller) {
					if (!$this->BookingTravellers->save($traveller)) {
						$success = false;
					}
				}

				if ($success) {
					$this->Flash->success(__('Success. Congratulations on your booking.'));
					return $this->redirect(['action' => 'success', $booking->id]);
				} else {
					$corrupted_travellers = $this->BookingTravellers->find()->where(['BookingTravellers.booking_id' => $booking_id]);
					foreach ($corrupted_travellers as $traveller) {
						$this->BookingTravellers->delete($traveller);
					}
					$this->set('post_data', $post_data);
					$this->Flash->error(__('Please fill all fields and try again.'));
				}
			}

			$this->set('booking', $booking);
			$this->set('sale', $sale);
		}
	}

	public function getSaleAllocations()
	{
		$this->autoRender = false;

		$json = [];

		$sale_offer_id = !empty($this->request->query['sale_offer_id']) ? $this->request->query['sale_offer_id'] : 0;
		$airport_code = !empty($this->request->query['airport_code']) ? $this->request->query['airport_code'] : '';
		$sale_type = !empty($this->request->query['sale_type']) ? $this->request->query['sale_type'] : 0;
		$currency_id = !empty($this->request->query['currency_id']) ? $this->request->query['currency_id'] : 1;

		$this->loadModel('Currencies');

		$currency = Number::defaultCurrency();

		if ($currency == "TRY") {
			$currency = "EUR";
		}

		if ($currency == "USD") {
			$currency = "EUR";
		}

		$session_currency = $this->request->getSession()->read('Config.currency');

		if (!empty($session_currency)) {
			$currency = $session_currency;
		}

		$currency_rate = $this->Currency->convert($currency_id, null, 1, false);

		$conditions = [];

		$conditions['sale_offer_id'] = $sale_offer_id;

		$this->loadModel('SaleOffers');
		$sale_offer = $this->SaleOffers->find()->where(array('SaleOffers.id' => $sale_offer_id))->first();

		if ($sale_type == 1) {
			$conditions['departure_airport_code LIKE'] = $airport_code;
		}
		$conditions['date_start >='] = date('Y-m-d');
		if ($this->request->is('get') && !empty($sale_offer_id)) {
			$this->loadModel('SaleAllocations');
			$sale_allocations = $this->SaleAllocations->find()->where($conditions);

			$json = array(
				'status' => 'success',
				'data' => array(
					'sale_allocations' => $sale_allocations,
					'sale_offer' => $sale_offer,
					'currency'	=> $currency,
					'currency_rate' => $currency_rate
				)
			);
		} else {
			$json = array(
				'status' => 'error',
				'data' => array(
					'message' => 'Missing parameters.'
				)
			);
		}

		$this->response->body(json_encode($json));
	}

	public function success($booking_id)
	{
		if (!empty($booking_id)) {
			$this->loadModel('Bookings');
			$this->loadModel('SaleOffers');
			$this->loadModel('BookingTravellers');

			$booking = $this->Bookings->find()->where(['Bookings.id' => $booking_id])->contain(['BookingAllocations', 'MyUsers'])->first();
			$travellers = $this->BookingTravellers->find()->where(['BookingTravellers.booking_id' => $booking_id]);
			$sale = $this->Sales->find()->where(['Sales.id' => $booking->sale_id])->contain(['Companies.Contacts', 'Suppliers'])->first();
			$sale_offer = $this->SaleOffers->findById($booking->offer_id)->first();

			$max_discount = 0; // Reliable since all discounts are >= 0

			foreach ($booking->booking_allocations as $allocation) {
				$allocations_table = TableRegistry::get('SaleAllocations');
				$allocation = $allocations_table->get($allocation->allocation_id);
				$allocation->available_rooms -= 1;
				$allocations_table->save($allocation);

				if ($allocation->discount >= $max_discount) {
					$max_discount = $allocation->discount;
				}
			}

			// Find difference between dates to determine stay in nights
			$check_in_time = new Time($booking->check_in->i18nFormat('yyyy-MM-dd HH:mm:ss'));
			$check_in_time->modify('+14 hours');
			$check_in_time = $check_in_time->format('d/m/y h A');
			$check_out_time = new Time($booking->check_out->i18nFormat('yyyy-MM-dd HH:mm:ss'));
			$check_out_time->modify('+12 hours');
			$check_out_time = $check_out_time->format('d/m/y h A');

			$diff = strtotime($booking->check_out) - strtotime($booking->check_in);
			$diff /= (60 * 60 * 24);

			// Convert coordinates
			// from "[coord1, coord2]"
			// to $coordinates[0] and $coordinates[1]
			// format
			if (!empty($sale->location_map)) {
				$coordinates = explode(',', substr($sale->location_map, 1, strlen($sale->location_map) - 2));
			} else {
				$coordinates = '';
			}

			$this->set('mobile', $this->request->is('mobile'));
			$this->set('travellers', $travellers);
			$this->set('sale_offer', $sale_offer);
			$this->set('booking', $booking);
			$this->set('max_discount', $max_discount);
			$this->set('nights', $diff);
			$this->set('coordinates', $coordinates);
			$this->set('sale', $sale);
			$this->set('check_in_time', $check_in_time);
			$this->set('check_out_time', $check_out_time);

			$user_email = new Email();

			$user_email
				->template((I18n::locale() != "en_US" ? I18n::locale() . "/" : "") . 'book_success')
				->emailFormat('html')
				->from(array(Configure::read('EmailAccounts.support') => 'Luxury Discounts'))
				->to($booking['my_user']['email'])
				->addBcc('svilen@luxury-discounts.com')
				->subject('Your booking with id #' . $booking_id . ' was successfully created!')
				->viewVars([
					'travellers' 		=> $travellers,
					'sale_offer' 		=> $sale_offer,
					'booking' 			=> $booking,
					'check_in_time'		=> $check_in_time,
					'check_out_time'	=> $check_out_time,
					'max_discount' 		=> $max_discount,
					'nights' 			=> $diff,
					'coordinates' 		=> $coordinates,
					'sale' 				=> $sale,
					'total_price'		=> $this->Currency->convert($booking->currency_id, null, $booking->sale_total)
				])
				->send();

			/**
			 * Send to Company main email
			 */
			if (!empty($sale->company->email)) {
				$alt_emails[] = $sale->company->email;
			}

			/**
			 * Send to Company support email
			 */
			if (!empty($sale->company->support_email)) {
				$alt_emails[] = $sale->company->support_email;
			}

			/**
			 * Send to Company reports email
			 */
			if (!empty($sale->company->reports_email)) {
				$alt_emails[] = $sale->company->reports_email;
			}

			/**
			 * Send to Supplier main email
			 */
			if (!empty($sale->supplier->email)) {
				$alt_emails[] = $sale->supplier->email;
			}

			/**
			 * Send to Supplier support email
			 */
			if (!empty($sale->supplier->support_email)) {
				$alt_emails[] = $sale->supplier->support_email;
			}

			/**
			 * Send to Supplier reports email
			 */
			if (!empty($sale->supplier->reports_email)) {
				$alt_emails[] = $sale->supplier->reports_email;
			}

			switch ($sale->board_type) {
				case 0:
					$board_type = 'ROOM_ONLY';
					break;
				case 1:
					$board_type = 'BED_N_BREAKFAST';
					break;
				case 2:
					$board_type = 'HALF_BOARD';
					break;
				case 3:
					$board_type = 'FULL_BOARD';
					break;
				case 3:
					$board_type = 'ALL_INCLUSIVE';
					break;
				default:
					$board_type = 'ROOM_ONLY';
					break;
			}

			foreach ($alt_emails as $alt_email) {
				$alt_email_instance = new Email();

				$alt_email_instance
					->template((I18n::locale() != "en_US" ? I18n::locale() . "/" : "") . 'book_success_company')
					->emailFormat('html')
					->from([Configure::read('EmailAccounts.support') => 'Luxury Discounts'])
					->to($alt_email)
					->subject("We have just received a new reservation!")
					->viewVars([
						'travellers' 		=> $travellers,
						'sale_offer' 		=> $sale_offer,
						'booking' 			=> $booking,
						'check_in_time'		=> $check_in_time,
						'check_out_time'	=> $check_out_time,
						'max_discount' 		=> $max_discount,
						'nights' 			=> $diff,
						'coordinates' 		=> $coordinates,
						'sale' 				=> $sale,
						'board_type'		=> $board_type,
						'total_price'		=> $this->Currency->convert($booking->currency_id, null, $booking->sale_total),
						'commission'		=> empty($sale->commission) ? $this->Currency->convert($sale->currency_id, null, $sale->commission) : $sale->commission . '%',
						'price_to_company'  => empty($sale->commission) ? $this->Currency->convert($booking->currency_id, null, $booking->sale_total - $sale->commission) : $this->Currency->convert($booking->currency_id, null, $booking->sale_total - ($booking->sale_total * $sale->commission / 100))
					])
					->send();
			}

			if (isset($this->request->query['print'])) {
				$this->autoRender = false;
				$this->render('print');
			}
		} else {
			return $this->redirect(['action' => 'index']);
		}
	}

	private function getAllCountries()
	{
		$countries = array("Afghanistan", "Aland Islands", "Albania", "Algeria", "American Samoa", "Andorra", "Angola", "Anguilla", "Antarctica", "Antigua", "Argentina", "Armenia", "Aruba", "Australia", "Austria", "Azerbaijan", "Bahamas", "Bahrain", "Bangladesh", "Barbados", "Barbuda", "Belarus", "Belgium", "Belize", "Benin", "Bermuda", "Bhutan", "Bolivia", "Bosnia", "Botswana", "Bouvet Island", "Brazil", "British Indian Ocean Trty.", "Brunei Darussalam", "Bulgaria", "Burkina Faso", "Burundi", "Caicos Islands", "Cambodia", "Cameroon", "Canada", "Cape Verde", "Cayman Islands", "Central African Republic", "Chad", "Chile", "China", "Christmas Island", "Cocos (Keeling) Islands", "Colombia", "Comoros", "Congo", "Congo, Democratic Republic of the", "Cook Islands", "Costa Rica", "Cote d'Ivoire", "Croatia", "Cuba", "Cyprus", "Czech Republic", "Denmark", "Djibouti", "Dominica", "Dominican Republic", "Ecuador", "Egypt", "El Salvador", "Equatorial Guinea", "Eritrea", "Estonia", "Ethiopia", "Falkland Islands (Malvinas)", "Faroe Islands", "Fiji", "Finland", "France", "French Guiana", "French Polynesia", "French Southern Territories", "Futuna Islands", "Gabon", "Gambia", "Georgia", "Germany", "Ghana", "Gibraltar", "Greece", "Greenland", "Grenada", "Guadeloupe", "Guam", "Guatemala", "Guernsey", "Guinea", "Guinea-Bissau", "Guyana", "Haiti", "Heard", "Herzegovina", "Holy See", "Honduras", "Hong Kong", "Hungary", "Iceland", "India", "Indonesia", "Iran (Islamic Republic of)", "Iraq", "Ireland", "Isle of Man", "Israel", "Italy", "Jamaica", "Jan Mayen Islands", "Japan", "Jersey", "Jordan", "Kazakhstan", "Kenya", "Kiribati", "Korea", "Korea (Democratic)", "Kuwait", "Kyrgyzstan", "Lao", "Latvia", "Lebanon", "Lesotho", "Liberia", "Libyan Arab Jamahiriya", "Liechtenstein", "Lithuania", "Luxembourg", "Macao", "Macedonia", "Madagascar", "Malawi", "Malaysia", "Maldives", "Mali", "Malta", "Marshall Islands", "Martinique", "Mauritania", "Mauritius", "Mayotte", "McDonald Islands", "Mexico", "Micronesia", "Miquelon", "Moldova", "Monaco", "Mongolia", "Montenegro", "Montserrat", "Morocco", "Mozambique", "Myanmar", "Namibia", "Nauru", "Nepal", "Netherlands", "Netherlands Antilles", "Nevis", "New Caledonia", "New Zealand", "Nicaragua", "Niger", "Nigeria", "Niue", "Norfolk Island", "Northern Mariana Islands", "Norway", "Oman", "Pakistan", "Palau", "Palestinian Territory, Occupied", "Panama", "Papua New Guinea", "Paraguay", "Peru", "Philippines", "Pitcairn", "Poland", "Portugal", "Principe", "Puerto Rico", "Qatar", "Reunion", "Romania", "Russian Federation", "Rwanda", "Saint Barthelemy", "Saint Helena", "Saint Kitts", "Saint Lucia", "Saint Martin (French part)", "Saint Pierre", "Saint Vincent", "Samoa", "San Marino", "Sao Tome", "Saudi Arabia", "Senegal", "Serbia", "Seychelles", "Sierra Leone", "Singapore", "Slovakia", "Slovenia", "Solomon Islands", "Somalia", "South Africa", "South Georgia", "South Sandwich Islands", "Spain", "Sri Lanka", "Sudan", "Suriname", "Svalbard", "Swaziland", "Sweden", "Switzerland", "Syrian Arab Republic", "Taiwan", "Tajikistan", "Tanzania", "Thailand", "The Grenadines", "Timor-Leste", "Tobago", "Togo", "Tokelau", "Tonga", "Trinidad", "Tunisia", "Turkey", "Turkmenistan", "Turks Islands", "Tuvalu", "Uganda", "Ukraine", "United Arab Emirates", "United Kingdom", "United States", "Uruguay", "US Minor Outlying Islands", "Uzbekistan", "Vanuatu", "Vatican City State", "Venezuela", "Vietnam", "Virgin Islands (British)", "Virgin Islands (US)", "Wallis", "Western Sahara", "Yemen", "Zambia", "Zimbabwe");

		return array_combine($countries, $countries);
	}

	// returns what is the lowest possible booking for a given fixed saled offer
	protected function calculateMinPriceForFixedOffer($sale_offer)
	{
		$tempCalendar = [];

		$minAllocationPrice = PHP_INT_MAX;
		$minSingleAllocationPrice = PHP_INT_MAX;

		// create a nested array [ year => [ month => [ day => [min_nights, rate]]]] for easier look ups of available dates
		foreach ($sale_offer->sale_allocations as $sale_allocation) {
			if (empty($sale_allocation->available_rooms) || $sale_allocation->available_rooms <= 0) {
				continue;
			}
			$expl = explode('-', $sale_allocation->date_start);
			if (count($expl) != 3) {
				// old style date, just skip it, we don't expect new valid dates in this format
				continue;
			}
			$year = $expl[0];
			$month = $expl[1];
			$day = $expl[2];

			if (!isset($tempCalendar[$year])) $tempCalendar[$year] = [];
			if (!isset($tempCalendar[$year][$month])) $tempCalendar[$year][$month] = [];

			$tempCalendar[$year][$month][$day] = [$sale_allocation->min_nights, $sale_allocation->rate];
		}

		// check each allocation day if there are min_nights available dates ahead of it, and if yes, check if that is the lowest total allocation price encountered so far
		foreach ($sale_offer->sale_allocations as $sale_allocation) {
			// get the list of dates for a booking starting from the current allocation date, going min_nights forward
			$exploded = explode('-', $sale_allocation->date_start);
			if (count($exploded) != 3) {
				continue;
			}
			if (strtotime($sale_allocation->date_start) < time()) continue;

			$possibleAllocation = $this->getConsecutiveAllocationDates($sale_allocation->date_start, $sale_allocation->min_nights);
			// track if all dates in this theoretical booking are possible and what the total price of the booking would be
			$possible = true;
			$price = 0;
			// check each theoretical booking date
			foreach ($possibleAllocation as $date) {
				$expl = explode('-', $date);
				if (count($expl) != 3) {
					// old style date, just skip it, we don't expect new valid dates in this format
					continue;
				}

				$year = $expl[0];
				$month = $expl[1];
				$day = $expl[2];

				if (!isset($tempCalendar[$year]) || !isset($tempCalendar[$year][$month]) || !isset($tempCalendar[$year][$month][$day])) {
					$possible = false;
					break;
				}

				if ($price <= $tempCalendar[$year][$month][$day][1]) {
					$price = $tempCalendar[$year][$month][$day][1];
				}
			}

			// update the min price if the booking was possible and cheaper
			if ($possible && $minAllocationPrice > $price) {
				$minAllocationPrice = $price;
			}
		}
		return ($minAllocationPrice == PHP_INT_MAX) ? 0 : $minAllocationPrice;
	}

	// returns an array of consecutive $minNights dates, starting from $startingFromDate
	protected function getConsecutiveAllocationDates($startingFromDate, $minNights)
	{ // $startingFromDate = "2019-01-13", $minNights >= 2
		$starting = strtotime($startingFromDate);

		$return = [$startingFromDate];
		$last = $startingFromDate;

		for ($i = 1; $i < $minNights; ++$i) {
			$last = date('Y-m-d', strtotime($last . ' +1 day'));
			$return[] = $last;
		}

		return $return;
	}
}
