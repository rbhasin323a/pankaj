<?php
namespace App\Controller;

use Cake\Event\Event;

require_once(BORICA_PATH . 'eborica.php');

class BookController extends AppController
{
	public function beforeFilter(Event $event)
	{
		parent::beforeFilter($event);

		$this->Auth->allow();
	}

	public function index()
	{ }

	public function order()
	{
		$this->autoRender = false;

		$transactionCode = '10';
		$amount = 10;
		$terminalID = '62161099';
		$orderID = mt_rand(10, 999999);
		$orderDescription = 'order';
		$language = 'EN';
		$protocolVersion = '1.0';
		$privateKeyFileName = BORICA_PATH . 'luxury-discounts.com.real.key';
		$privateKeyPassword = '';

		$message = eBorica::generateBOReq(
			$privateKeyFileName,
			$privateKeyPassword,
			$transactionCode,
			$amount,
			$terminalID,
			$orderID,
			$orderDescription,
			$language,
			$protocolVersion
		);
		$url = eBorica::gatewayURL . "registerTransaction?eBorica=" . urlencode(base64_encode($message));
		header('Location: ' . $url);
		exit;
	}

	public function error()
	{
		$this->autoRender = false;
		echo 'Transaction failed! Please try again or contact our support.';
		exit;
	}

	public function back()
	{
		$this->autoRender = false;
		require_once(BORICA_PATH . 'eborica.php');
		$eborica = (!empty($_GET['eBorica'])) ? $_GET['eBorica'] : '';
		if (empty($eborica)) {
			header('Location: /');
		}

		$resp = eBorica::parseBOResp($eborica, BORICA_PATH . 'luxury-discounts.com.real.key');

		if (!isset($resp['TERMINAL_ID'])) {
			header('Location: /');
		}


		if ($resp['TERMINAL_ID'] == '62161140') { // EUR terminal
			$resp = eBorica::parseBOResp($eborica, BORICA_PATH . 'eur/luxury-discounts.com.real.key');
		}

		file_put_contents(LOGS . 'borica_responses.log', '$resp = ' . var_export($resp, true) . PHP_EOL, FILE_APPEND);

		if (!isset($resp['RESPONSE_CODE'])) {
			header('Location: /');
		}

		if (!isset($resp['ORDER_ID'])) {
			header('Location: /');
		}

		if ($resp['RESPONSE_CODE'] == 00) {
			$this->loadModel('Bookings');
			$booking = $this->Bookings->get($resp['ORDER_ID']);
			$booking->transaction_status = 0;
			$this->Bookings->save($booking);
			header('Location: /sales/success/' . $resp['ORDER_ID']);
		} else {
			header('Location: /book/error/');
		}

		exit;
	}
}
