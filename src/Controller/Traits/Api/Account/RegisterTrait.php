<?php
namespace App\Controller\Traits\Api\Account;

use CakeDC\Users\Controller\Component\UsersAuthComponent;
use Cake\Core\Configure;
use Cake\Datasource\EntityInterface;
use Cake\Network\Exception\NotFoundException;
use Cake\Network\Response;

/**
 * Covers the registration
 *
 */
trait RegisterTrait
{
	/**
	 * Register a new user
	 *
	 * @throws NotFoundException
	 * @return type
	 */
	public function register()
	{
		if (!empty($this->request->data['password']) && !isset($this->request->data['password_confirm'])) {
			$this->request->data('password_confirm', $this->request->data['password']);
		}

		if (!Configure::read('Users.Registration.active')) {
			throw new NotFoundException();
		}

		$userId = $this->Auth->user('id');

		if (!empty($userId) && !Configure::read('Users.Registration.allowLoggedIn')) {
			return $this->setApiResponseMessage(['error' => __d('ApiAccountRegisterTrait', 'You must log out to register a new user account')]);
		}

		$usersTable = $this->getUsersTable();
		$user = $usersTable->newEntity();
		$validateEmail = (bool)Configure::read('Users.Email.validate');
		$useTos = (bool)Configure::read('Users.Tos.required');
		$tokenExpiration = Configure::read('Users.Token.expiration');

		$options = [
			'token_expiration' => $tokenExpiration,
			'validate_email' => $validateEmail,
			'use_tos' => $useTos
		];

		$event = $this->dispatchEvent(UsersAuthComponent::EVENT_BEFORE_REGISTER, [
			'usersTable' => $usersTable,
			'options' => $options,
			'userEntity' => $user,
		]);

		if ($event->result instanceof EntityInterface) {
			if ($userSaved = $usersTable->register($user, $event->result->toArray(), $options)) {
				return $this->_afterRegister($userSaved);
			}
		}

		$userSaved = $usersTable->register($user, $this->request->data, $options);

		$errors = $user->errors();

		if (!empty($errors) && is_array($errors)) {
			foreach ($errors as $field => $error) {
				if (!empty($error) && is_array($error)) {
					foreach ($error as $validation => $message) {
						if ($validation === '_isUnique') {
							//return $this->setAction('login');
						}

						return $this->setApiResponseMessage(['error' => $message]);
					}
				}
			}
		}

		if (!$userSaved) {
			return $this->setApiResponseMessage(['error' => __d('ApiAccountRegisterTrait', 'The account could not be created')]);
		}

		return $this->_afterRegister($userSaved);
	}

	/**
	 * Prepare messages after registration, and dispatch afterRegister event
	 *
	 * @param EntityInterface $userSaved User entity saved
	 * @return Response
	 */
	protected function _afterRegister(EntityInterface $userSaved)
	{
		$event = $this->dispatchEvent(UsersAuthComponent::EVENT_AFTER_REGISTER, [
			'user' => $userSaved
		]);

		if (!empty($userSaved['id'])) {
			$usersTable = $this->getUsersTable();

			$db_user = $usersTable->find('all', [
				'conditions' => ['id LIKE' => $userSaved['id']]
			])->first();
		}

		if (!empty($db_user)) {
			$this->request->getSession()->write('API.Account.Registered', true);

			$user = $db_user->toArray();

			$this->Auth->setUser($user);
			$this->setApiResponseData(['redirect' => '/']);

			return $this->setApiResponseMessage(['success' => __d('ApiAccountRegisterTrait', 'You have registered successfully')]);
		}
	}
}
