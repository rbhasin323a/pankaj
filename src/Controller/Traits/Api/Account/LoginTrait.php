<?php
namespace App\Controller\Traits\Api\Account;

use CakeDC\Users\Controller\Component\UsersAuthComponent;

/**
 * Covers the login
 *
 */
trait LoginTrait
{
	/**
	 * Login user
	 *
	 * @return mixed
	 */
	public function login()
	{
		$event = $this->dispatchEvent(UsersAuthComponent::EVENT_BEFORE_LOGIN);

		if (is_array($event->result)) {
			return $this->_afterIdentifyUser($event->result);
		}

		if ($event->isStopped()) {
			return $this->redirect($event->result);
		}

		if ($this->request->is('post')) {
			$user = $this->Auth->identify();
			return $this->_afterIdentifyUser($user);
		}
	}

	/**
	 * Update remember me and determine redirect url after user identified
	 * @param array $user user data after identified
	 * @return array
	 */
	protected function _afterIdentifyUser($user)
	{
		if (!empty($user)) {
			$this->Auth->setUser($user);

			$event = $this->dispatchEvent(UsersAuthComponent::EVENT_AFTER_LOGIN, ['user' => $user]);

			if (is_array($event->result)) {
				return $this->setApiResponseData(['redirect' => $event->result]);
			}

			$url = $this->Auth->redirectUrl();


			$this->request->getSession()->write('API.Account.LoggedIn', true);
			$this->setApiResponseData(['redirect' => $url]);

			return $this->setApiResponseMessage(['success' => __d('ApiAccountRegisterTrait', 'You have logged in successfully')]);
		} else {
			$message = __d('ApiAccountLoginTrait', 'Incorrect credentials or account is not validated.');

			return $this->setApiResponseMessage(['error' => $message]);
		}
	}
}
