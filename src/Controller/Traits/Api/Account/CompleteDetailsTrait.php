<?php
namespace App\Controller\Traits\Api\Account;

/**
 * Covers the complete details
 *
 */
trait CompleteDetailsTrait
{
	/**
	 * Complete Details
	 *
	 * @return mixed
	 */
	public function completeDetails()
	{
		$session_user = $this->Auth->user();

		if (empty($session_user) || empty($session_user['id'])) {
			return false;
		}

		$usersTable = $this->getUsersTable();

		$user = $usersTable->get($session_user['id']);
		$user = $usersTable->patchEntity($user, $this->request->data);

		if ($usersTable->save($user)) {
			if (!empty($session_user['id'])) {
				$db_user = $usersTable->find('all', [
					'conditions' => ['id LIKE' => $session_user['id']]
				])->first();
			}

			if (!empty($db_user)) {
				$user = $db_user->toArray();
				$this->Auth->setUser($user);
			}

			$this->setApiResponseData(['redirect' => '/']);
			return $this->setApiResponseMessage(['success' => __d('ApiAccountRegisterTrait', 'Your details have been updated.')]);
		} else {
			return $this->setApiResponseMessage(['error' => __d('ApiAccountRegisterTrait', 'Your details could not be updated. Please check your form for errors and try again.')]);
		}
	}
}
