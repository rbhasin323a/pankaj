<?php
namespace App\Controller\Traits\Api;

use Cake\Event\Event;

/**
 * Covers the login, logout
 *
 */
trait BaseTrait
{
	private $api_response = [
		'data' => null,
		'message' => null
	];

	public function initialize()
	{
		parent::initialize();
		$this->loadComponent('RequestHandler');
	}

	public function beforeFilter(Event $event)
	{
		parent::beforeFilter($event);

		$this->autoRender = false;

		if ($this->components()->has('Auth')) {
			$this->Auth->allow();
		}

		if ($this->components()->has('RememberMe')) {
			$this->components()->unload('RememberMe');
		}

		if (isset($this->request->params['prefix'])) {
			unset($this->request->params['prefix']);
		}
	}

	public function afterFilter(Event $event)
	{
		$this->response->type('json');
		$this->response->body(json_encode($this->api_response));

		return $this->response;
	}

	/**
	 * API Set Response Data
	 */
	protected function setApiResponseData($data)
	{
		if (empty($data) || !is_array($data)) {
			return false;
		}

		if (empty($this->api_response['data']) || !is_array($this->api_response['data'])) {
			$this->api_response['data'] = [];
		}

		$this->api_response['data'] = array_merge($this->api_response['data'], $data);
	}

	/**
	 * API Set Response Message
	 */
	protected function setApiResponseMessage($message)
	{
		if (empty($message) || !is_array($message)) {
			return false;
		}

		if (empty($this->api_response['message']) || !is_array($this->api_response['message'])) {
			$this->api_response['message'] = [];
		}

		$this->api_response['message'] = array_merge($this->api_response['message'], $message);
	}
}
