<?php
namespace App\Controller;

use Cake\Core\Configure;
use Cake\Log\Log;
use Cake\Event\Event;
use Cake\Mailer\Email;
use Cake\Routing\Router;

class GdprRequestsController extends AppController
{
	public function initialize()
	{
		parent::initialize();
		$this->loadModel('MyUsers');
	}

	public function beforeFilter(Event $event)
	{
		parent::beforeFilter($event);
		$this->Auth->allow(['submitRequest', 'personalInfo', 'getPersonalInfo', 'bookings', 'getBookings', 'delete']);
	}

	public function submitRequest()
	{
		$this->autoRender = false;
		$data = $this->request->data;
		$json = array('error' => true);

		if ($this->request->is('post') && !empty($data['email']) && !empty($data['type'])) {
			$customer = $this->MyUsers->find('all', array(
				'conditions' => array(
					'email' => $data['email']
				)
			))->first();

			if (!empty($customer)) {
				$hash = $this->generateRequestHash($data['email'], $customer->id);

				$request = array(
					'user_id' => $customer->id,
					'hash' => $hash,
					'email' => $data['email'],
					'type' => $data['type'],
					'ip_address' => !empty($_SERVER['REMOTE_ADDR']) ? $_SERVER['REMOTE_ADDR'] : '',
					'user_agent' => !empty($_SERVER['HTTP_USER_AGENT']) ? $_SERVER['HTTP_USER_AGENT'] : '',
					'created' => date('Y-m-d H:i:s'),
					'modified' => date('Y-m-d H:i:s'),
				);

				$new_request_entity = $this->GdprRequests->newEntity($request);
				$saved_request = $this->GdprRequests->save($new_request_entity);

				if ($saved_request) {
					$json['error'] = false;
					$this->prepareEmail($saved_request, $customer);
				}
			}
		} else {
			$json['error'] = 'Email address not provided!';
		}

		header('Content-Type: application/json');
		echo json_encode($json);
		exit;
	}

	private function prepareEmail($saved_request = array(), $customer = array())
	{
		if (!empty($saved_request) && !empty($customer)) {

			switch ($saved_request['type']) {
				case 'customer/edit':
					$request_handler_url = $this->generateLink('gdpr_requests', 'edit', ['h' => $saved_request['hash']]);
					$email_text = 'Hi, ' . $customer->first_name . ' ' . $customer->last_name . ' recently you requested to change your account info. Please click on the button below to continue!';

					$email_subject = '[Luxury Discounts] Customer edit';
					break;
				case 'customer/requests':
					$request_handler_url = $this->generateLink('gdpr_requests', 'requests', ['h' => $saved_request['hash']]);
					$email_text = 'Hi, ' . $customer->first_name . ' ' . $customer->last_name . ' recently you requested all GDPR requests using your account info. Please click on the button below to continue!';
					$email_subject = '[Luxury Discounts] All requests';
					break;
				case 'customer/personal_info':
					$request_handler_url = $this->generateLink('gdpr_requests', 'personalInfo', ['h' => $saved_request['hash']]);
					$email_text = 'Hi, ' . $customer->first_name . ' ' . $customer->last_name . ' recently you requested your personal using your account info. Please click on the button below to continue!';
					$email_subject = '[Luxury Discounts] Personal info';
					break;
				case 'customer/bookings':
					$request_handler_url = $this->generateLink('gdpr_requests', 'bookings', ['h' => $saved_request['hash']]);
					$email_text = 'Hi, ' . $customer->first_name . ' ' . $customer->last_name . ' recently you requested your bookings using your account info. Please click on the button below to continue!';
					$email_subject = '[Luxury Discounts] Bookings';
					break;
				case 'customer/report':
					$request_handler_url = $this->generateLink('gdpr_requests', 'report', ['h' => $saved_request['hash']]);
					$email_text = 'Hi, ' . $customer->first_name . ' ' . $customer->last_name . ' recently you requested report using your account info. Please click on the button below to continue!';
					$email_subject = '[Luxury Discounts] Report';
					break;
				case 'customer/delete':
					$request_handler_url = $this->generateLink('gdpr_requests', 'delete', ['h' => $saved_request['hash']]);
					$email_text = 'Hi, ' . $customer->first_name . ' ' . $customer->last_name . ' recently you requested to delete your account. Please click on the button below to continue!';
					$email_subject = '[Luxury Discounts] Delete account';
					break;
				default:

					break;
			}
		}

		$email_vars = [
			'text' => $email_text,
			'button_text' => 'Click here',
			'button_link' => $request_handler_url
		];

		$this->sendEmail($saved_request['email'], $email_subject, $email_text, $email_vars);
	}

	public function sendEmail($to = '', $subject = '', $message = '', $view_vars = [])
	{
		try {
			$email = new Email();
			$email->from(array(Configure::read('EmailAccounts.support') => 'Luxury Discounts'))
				->emailFormat('html')
				->to($to)
				->subject($subject)
				->template('gdpr_request')
				->viewVars($view_vars)
				->send();
		} catch (Exception $e) {
			Log::write('debug', $e->getMessage());
		}
	}

	private function generateRequestHash($email = '', $user_id = 0)
	{
		if (!empty($email) && !empty($user_id)) {
			$hash_string = $email . '|' . $user_id . '|' . time();
			return hash('sha256', $hash_string);
		}
	}

	private function generateLink($controller = '', $action = '', $query = array())
	{
		return Router::url([
			'controller' => $controller,
			'action' => $action,
			'?' => $query
		], true);
	}

	private function checkIfValidRequest($hash)
	{
		$this->loadModel('GdprRequests');
		$gdpr_request_time_out =  date('Y-m-d H:i:s', strtotime(date('Y-m-d H:i:s') . ' -3 day'));
		return $this->GdprRequests->findByHash($hash)->where(['created >=' => $gdpr_request_time_out])->first();
	}

	public function personalInfo()
	{
		if ($this->request->is('get') && !empty($this->request->query['h'])) {
			$this->loadModel('GdprRequests');

			// Check if the request is valid and is not expired
			$request = $this->checkIfValidRequest($this->request->query['h']);

			$data['error'] = false;
			if (!empty($request)) {
				$user = $this->MyUsers->get($request['user_id']);

				// Find the customer
				$data['hash'] = $this->request->query['h'];
				$data['request'] = $request;
				$data['download_csv'] = $this->generateLink('gdpr_requests', 'getPersonalInfo', ['h' => $data['hash'], 'type' => 'csv']);
				$data['download_json'] = $this->generateLink('gdpr_requests', 'getPersonalInfo', ['h' => $data['hash'], 'type' => 'json']);
			} else {
				$data['error'] = 'Invalid or expired GDPR request!';
			}

			$this->set(compact('data'));
		}
	}

	public function getPersonalInfo()
	{
		if ($this->request->is('get') && !empty($this->request->query['h'])) {
			$this->loadModel('GdprRequests');
			$type = $this->request->query['type'];

			$request = $this->checkIfValidRequest($this->request->query['h']);

			if (!empty($request)) {
				$customer = $this->MyUsers->get($request['user_id']);
				// Find the customer

				if (!empty($customer)) {

					if ($type == 'csv') {
						$out = fopen('php://output', 'w');
						header("Content-Type:application/csv");
						header("Content-Disposition:attachment;filename=personal_info.csv");

						$headings = array(
							'email',
							'first_name',
							'last_name',
							'birthday',
							'phone_mobile',
							'phone_home',
							'address_1',
							'address_2',
							'city',
							'postcode',
							'newsletter',
							'newsletter_type',
							'created',
						);
						fputcsv($out, $headings);

						$customer_data = array(
							$customer->email,
							$customer->first_name,
							$customer->last_name,
							$customer->birthday,
							$customer->phone_mobile,
							$customer->phone_home,
							$customer->address_1,
							$customer->address_2,
							$customer->city,
							$customer->postcode,
							$customer->newsletter,
							$customer->newsletter_type,
							$customer->created
						);

						fputcsv($out, $customer_data);
						fclose($out);
					} else if ($type = 'json') {
						$customer_data = array(
							'email' => $customer->email,
							'first_name' => $customer->first_name,
							'last_name' => $customer->last_name,
							'birthday' => $customer->birthday,
							'phone_mobile' => $customer->phone_mobile,
							'phone_home' => $customer->phone_home,
							'address_1' => $customer->address_1,
							'address_2' => $customer->address_2,
							'city' => $customer->city,
							'postcode' => $customer->postcode,
							'newsletter' => $customer->newsletter,
							'newsletter_type' => $customer->newsletter_type,
							'created' => $customer->created,
						);
						header('Content-Type: application/json');
						echo json_encode($customer_data);
					}

					exit;
				} else {
					die('Invalid Request');
				}
			} else {
				die('Invalid Request');
			}
		} else {
			die('Invalid Request');
		}
	}

	public function bookings()
	{
		if ($this->request->is('get') && !empty($this->request->query['h'])) {
			$this->loadModel('GdprRequests');
			$this->loadModel('Bookings');

			// Check if the request is valid and is not expired
			$request = $this->checkIfValidRequest($this->request->query['h']);

			$data['error'] = false;
			if (!empty($request)) {
				$user = $this->MyUsers->get($request['user_id']);

				$data['hash'] = $this->request->query['h'];
				$data['request'] = $request;
				$data['download_csv'] = $this->generateLink('gdpr_requests', 'getBookings', ['h' => $data['hash'], 'type' => 'csv']);
				$data['download_json'] = $this->generateLink('gdpr_requests', 'getBookings', ['h' => $data['hash'], 'type' => 'json']);
			} else {
				$data['error'] = 'Invalid or expired GDPR request!';
			}

			$this->set(compact('data'));
		}
	}

	public function getBookings()
	{
		if ($this->request->is('get') && !empty($this->request->query['h'])) {
			$this->loadModel('GdprRequests');
			$type = $this->request->query['type'];

			$request = $this->checkIfValidRequest($this->request->query['h']);

			if (!empty($request)) {
				$customer = $this->MyUsers->find('all', array(
					'conditions' => array(
						'id' => $request['user_id']
					),
					'contain' => array('Bookings.Sales', 'Bookings.SaleOffers')
				))->first();


				if (!empty($customer)) {

					if ($type == 'csv') {
						$out = fopen('php://output', 'w');
						header("Content-Type:application/csv");
						header("Content-Disposition:attachment;filename=bookings.csv");

						$headings = array(
							'email',
							'sale_name',
							'sale_offer_name',
							'check_in',
							'check_out',
							'first_name',
							'last_name',
							'phone',
							'notes',
							'adults',
							'children',
							'infants',
							'rooms',
							'sale_total'
						);
						fputcsv($out, $headings);

						foreach ($customer->bookings as $booking) {
							$bookings_data = array(
								$customer->email,
								$booking->sale->title,
								$booking->sale_offer->name,
								$booking->check_in->nice(),
								$booking->check_out->nice(),
								$booking->first_name,
								$booking->last_name,
								$booking->phone,
								$booking->notes,
								$booking->adults,
								$booking->children,
								$booking->infants,
								$booking->rooms,
								$booking->sale_total
							);
							fputcsv($out, $bookings_data);
						}


						fclose($out);
					} else if ($type = 'json') {
						$bookings_data = [];
						foreach ($customer->bookings as $booking) {
							$bookings_data[] = array(
								'email' => $customer->email,
								'sale_name' => $booking->sale->title,
								'sale_offer_name' => $booking->sale_offer->name,
								'check_in' => $booking->check_in->nice(),
								'check_out' => $booking->check_out->nice(),
								'first_name' => $booking->first_name,
								'last_name' => $booking->last_name,
								'phone' => $booking->phone,
								'notes' => $booking->notes,
								'adults' => $booking->adults,
								'children' => $booking->children,
								'infants' => $booking->infants,
								'rooms' => $booking->rooms,
								'sale_total' => $booking->sale_total
							);
						}

						header('Content-Type: application/json');
						echo json_encode($bookings_data);
					}

					exit;
				} else {
					die('Invalid Request');
				}
			} else {
				die('Invalid Request');
			}
		} else {
			die('Invalid Request');
		}
	}

	public function logDeletionRequest($customer = array())
	{
		if (!empty($customer)) {
			$this->loadModel('DeleteRequests');

			$delete_request = array(
				'user_id' => $customer->id,
				'email' => $customer->email,
				'status' => 'pending',
				'created' => date('Y-m-d H:i:s'),
				'modified' => date('Y-m-d H:i:s')
			);
			$new_delete_request = $this->DeleteRequests->newEntity($delete_request);
			if ($this->DeleteRequests->save($new_delete_request)) {
				$subject = 'New deletion request received!';
				$message = 'The customer ' .  $customer->first_name . ' ' . $customer->last_name . ' has requested account deletion.';

				$email_vars = [
					'text' => $message,
					'button_text' => 'View more',
					'button_link' => 'https://cms.luxury-discounts.com/admin/users/delete-requests'
				];

				$this->sendEmail('company@luxury-discounts.com', $subject, $message, $email_vars);

				return true;
			} else {
				return false;
			}
		}
	}

	public function delete()
	{
		$this->loadModel('GdprRequests');
		$data = [];
		$data['error'] = false;
		if ($this->request->is('get') && !empty($this->request->query['h'])) {
			// Check if the request is valid and is not expired
			$request = $this->checkIfValidRequest($this->request->query['h']);

			$data['error'] = false;
			if (!empty($request)) {
				$user = $this->MyUsers->get($request['user_id']);

				$data['hash'] = $this->request->query['h'];
				$data['request'] = $request;
				$data['user'] = $user;
			} else {
				$data['error'] = 'Invalid or expired GDPR request!';
			}
		} else if ($this->request->is('post') && !empty($this->request->data)) {
			$post = $this->request->data;
			$request = $this->checkIfValidRequest($post['h']);
			if ($request) {
				$customer = $this->MyUsers->get($request['user_id']);

				$data['hash'] = !empty($post['h']) ?  $post['h'] : '';
				$data['request'] = !empty($request) ? $request : array();

				if (!empty($customer)) {
					$deleted = $this->logDeletionRequest($customer);

					if ($deleted) {
						return $this->setAction('success');
					} else {
						$this->Flash->error('Unable to delete record!', [
							'params' => array()
						]);
					}
				} else {
					$this->Flash->error('We could not find an account assotiated with this email!', [
						'params' => array()
					]);
				}
			} else {
				$user = [];
				$data['store_info'] = [];
				$data['store'] = [];
				$data['hash'] = '';
				$data['request'] = [];
				$this->Flash->error('Invalid or expired request!', [
					'params' => array()
				]);
			}
		}

		$this->set(compact('data'));
	}

	public function success()
	{ }
}
