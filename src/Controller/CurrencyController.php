<?php
namespace App\Controller;

use Cake\Event\Event;
use App\Controller\AppController;
use Cake\ORM\TableRegistry;

class CurrencyController extends AppController
{
	public function beforeFilter(Event $event)
	{
		parent::beforeFilter($event);
		$this->Auth->allow(['index', 'updateCurrencyRates']);
	}

	public function beforeRender(Event $event)
	{
		parent::beforeRender($event);
		$this->viewBuilder()->setLayout(false);
	}

	public function index($params = [])
	{
		$currency = $this->request->getSession()->read('Config.currency');

		$data = [];

		if (!empty($this->request->data['currency'])) {
			$data['currency'] = strtoupper($this->request->data['currency']);
		} elseif (!empty($params['currency'])) {
			$data['currency'] = strtoupper($params['currency']);
		} elseif (!empty($currency)) {
			$data['currency'] = strtoupper($currency);
		}

		setcookie("currency", $data['currency'], time() + 3600 * 24 * 365, '/');

		if (!empty($this->request->data['redirect'])) {
			$data['redirect'] = $this->request->data['redirect'];
		} else {
			$data['redirect'] = '/';
		}

		if (!empty($data['currency'])) {
			$this->request->getSession()->write('Config.currency', $data['currency']);
		}

		$routePrefix = substr($data['redirect'], 0, 3);

		if (!in_array($routePrefix, array('/en', '/bg', '/tr'))) {
			if (isset($_COOKIE['language']) && in_array($_COOKIE['language'], array('en', 'bg', 'tr'))) {
				$data['redirect'] = '/' . $_COOKIE['language'] . $data['redirect'];
			} else if (isset($_COOKIE['autolanguage']) && in_array($_COOKIE['autolanguage'], array('en', 'bg', 'tr'))) {
				$data['redirect'] = '/' . $_COOKIE['autolanguage'] . $data['redirect'];
			}
		}

		return $this->redirect($data['redirect']);
	}

	public function updateCurrencyRates()
	{
		$this->CurrencyRates = TableRegistry::get('CurrencyRates');
		$query = $this->CurrencyRates->find();
		$XML = simplexml_load_file("http://www.ecb.europa.eu/stats/eurofxref/eurofxref-daily.xml");
		$eur_try_rate = null;
		$eur_bgn_rate = null;

		foreach ($XML->Cube->Cube->Cube as $rate) {
			//Output the value of 1EUR for a currency code
			if ($rate["currency"] == 'TRY') {
				$eur_try_rate = (float)$rate["rate"];
			}
			if ($rate["currency"] == 'BGN') {
				$eur_bgn_rate = (float)$rate["rate"];
			}
		}

		foreach ($query as $row) {
			if ($row->fromCurrency == 'EUR' && $row->toCurrency == 'BGN') {
				$changed_rates = $this->CurrencyRates->patchEntity($row, ['rates' => $eur_bgn_rate]);
				$this->CurrencyRates->save($changed_rates);
			}
			if ($row->fromCurrency == 'EUR' && $row->toCurrency == 'TRY') {
				$changed_rates = $this->CurrencyRates->patchEntity($row, ['rates' => $eur_try_rate]);
				$this->CurrencyRates->save($changed_rates);
			}
			if ($row->fromCurrency == 'TRY' && $row->toCurrency == 'EUR') {
				$changed_rates = $this->CurrencyRates->patchEntity($row, ['rates' => 1 / $eur_try_rate]);
				$this->CurrencyRates->save($changed_rates);
			}
			if ($row->fromCurrency == 'BGN' && $row->toCurrency == 'EUR') {
				$changed_rates = $this->CurrencyRates->patchEntity($row, ['rates' => 1 / $eur_bgn_rate]);
				$this->CurrencyRates->save($changed_rates);
			}
			if ($row->fromCurrency == 'BGN' && $row->toCurrency == 'TRY') {
				$changed_rates = $this->CurrencyRates->patchEntity($row, ['rates' => $eur_try_rate / $eur_bgn_rate]);
				$this->CurrencyRates->save($changed_rates);
			}
		}

		exit;
	}
}
