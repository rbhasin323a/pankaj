<?php
namespace App\Controller;

use Cake\Controller\Controller;
use Cake\Event\Event;
use Cake\I18n\I18n;
use Cake\Routing\Router;
/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @link http://book.cakephp.org/3.0/en/controllers.html#the-app-controller
 */
class AppController extends Controller
{
	public function initialize()
	{
		parent::initialize();

		$this->loadComponent('RequestHandler');
		$this->loadComponent('Flash');
		$this->loadComponent('Cookie');
		$this->loadComponent('CakeDC/Users.UsersAuth');
		$this->loadComponent('Currency');
		 $this->loadComponent('AkkaFacebook.Graph', [
	    'app_id' => '416086055915522',
	    'app_secret' =>'bbe37c22825ba9ce808261a3e4e700ed',
	    'app_scope' => 'email,public_profile', // https://developers.facebook.com/docs/facebook-login/permissions/v2.4
	   // 'redirect_url' => Router::url(['controller' => 'Users', 'action' => 'login'], TRUE), // This should be enabled by default
	    'post_login_redirect' =>'/',
	   // 'enable_graph_helper' => false,
	    
	  
    ]);

		$this->Cookie->config('path', '/');

		$this->Cookie->config([
			'expires' => '+15 days',
			'httpOnly' => true
		]);

		$this->Auth->allow(['formatPrice', 'getCountry']);

		$this->Auth->config('authError', __("Please login to access this page"));

		$this->response->header('Access-Control-Allow-Origin', '*');
		$this->response->header('Access-Control-Allow-Methods', '*');
		$this->response->header('Access-Control-Allow-Headers', 'X-Requested-With');
		$this->response->header('Access-Control-Allow-Headers', 'Content-Type, x-xsrf-token');
		$this->response->header('Access-Control-Max-Age', '172800');

		$currency = $this->request->getSession()->read('Config.currency');

		if (empty($currency) && isset($_COOKIE['currency'])) {
			$currency = $_COOKIE['currency'];
			$this->request->getSession()->write('Config.currency', $currency);
		}

		$this->set('custom_currency', strtoupper($currency));
		$this->set('currency', $this->Currency);

		$lang = $this->request->getSession()->read('Config.language');
		$routePrefix = substr($this->request->getAttribute('here'), 0, 3);

		if (
			strpos($_SERVER['SERVER_NAME'], 'cms.') === false && (!isset($_SERVER['HTTP_X_REQUESTED_WITH']) || strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) !== 'xmlhttprequest') &&
			isset($_SERVER['REQUEST_METHOD']) && $_SERVER['REQUEST_METHOD'] == 'GET' &&
			$this->request->getAttribute('here') !== '/logout' &&
			strpos($_SERVER['REQUEST_URI'], '/validate') === false &&
			strpos($_SERVER['REQUEST_URI'], '/book') === false &&
			strpos($_SERVER['REQUEST_URI'], '/sales/success') === false &&
			strpos($_SERVER['REQUEST_URI'], '/reset-password') === false
		) {
			if (!in_array($routePrefix, array('/en', '/bg', '/tr'))) {
				// not a prefixed URL
				if (empty($_COOKIE['autolanguage'])) {
					// first time visit to https://luxury-discounts.com/, detect language by country
					$lang = $this->getCountry();
					$this->request->getSession()->write('Config.language', $lang);
					setcookie('autolanguage', $lang, time() + 3600 * 24 * 365, '/');
					setcookie('language', $lang, time() + 3600 * 24 * 365, '/');
					$_COOKIE['language'] = $lang;
					$_COOKIE['autolanguage'] = $lang;
				} else {
					// subsequent visit to https://luxury-discounts.com/, use cookie value for language
					$this->request->getSession()->write('Config.language', $_COOKIE['autolanguage']);

					$languageRedirect = '/' . $_COOKIE['autolanguage'] . $_SERVER['REQUEST_URI'];
					header('Location: ' . $languageRedirect, 302);
					exit;
				}
			} else {
				// prefixed URL
				$routePrefix = substr($this->request->getAttribute('here'), 1, 2);
				if (!isset($_COOKIE['autolanguage'])) {
					// prefixed url, no cookie => set cookie to prefix, use prefix for language
					$this->request->getSession()->write('Config.language', $routePrefix);
					setcookie('autolanguage', $routePrefix, time() + 3600 * 24 * 365, '/');
					setcookie('language', $routePrefix, time() + 3600 * 24 * 365, '/');
					$_COOKIE['language'] = $routePrefix;
					$lang = $routePrefix;
					if ($lang == 'en') $lang = 'gb';
					$_COOKIE['autolanguage'] = $routePrefix;
				} else {
					if ($routePrefix != $_COOKIE['autolanguage']) {
						// prefixed url, but prefix doesn't match cookie => set cookie to prefix, use prefix for language
						$this->request->getSession()->write('Config.language', $routePrefix);
						setcookie('autolanguage', $routePrefix, time() + 3600 * 24 * 365, '/');
						setcookie('language', $routePrefix, time() + 3600 * 24 * 365, '/');
						$_COOKIE['language'] = $routePrefix;
						$lang = $routePrefix;
						$_COOKIE['autolanguage'] = $routePrefix;
						if ($lang == 'en') $lang = 'gb';
					} else {
						// prefix and cookie match, all is fine with the world
						$lang = $routePrefix;
					}
				}
			}
		}

		switch ($lang) {
			case "gb":
				I18n::locale('en_US');
				break;
			case "bg":
				I18n::locale('bg_BG');
				\Cake\I18n\FrozenTime::setDefaultLocale('en_US');
				break;
			case "tr":
				I18n::locale('tr_TR');
				break;
			default:
				I18n::locale('en_US');
		}

		$this->checkAdminUserPermissions();
	}

	private function checkAdminUserPermissions()
	{
		if (!empty($this->request->getAttribute('params')['prefix']) && $this->request->getAttribute('params')['prefix'] == 'admin') {
			$user_role = [];
			$user = $this->Auth->user();

			if ($user) {
				$this->loadModel('UserRoles');
				$this->loadModel('MyUsers');

				$db_user = $this->MyUsers->find('all', [
					'conditions' => ['id LIKE' => $user['id']]
				])->first()->toArray();

				if (!empty($db_user['role'])) {
					$user_role =  $this->UserRoles->find('all', [
						'conditions' => ['UserRoles.name LIKE' => trim($db_user['role'])]
					])->first()->toArray();
				}

				$role_permissions = !empty($user_role['permissions']) ? json_decode($user_role['permissions'], true) : array();

				$access = !empty($role_permissions['access']) ? $role_permissions['access'] : array();

				if (empty($access)) {
					$this->redirectToHomePage();
				}

				if (!$this->authorize($user, $access)) {
					$this->Flash->error(__('You don\'t have permission to access this route!'));
				}

				$this->set('access', $access);
			}

			$this->viewBuilder()->setLayout('Admin/main_fluid');
		}
	}

	/**
	 * Before render callback.
	 *
	 * @param \Cake\Event\Event $event The beforeRender event.
	 * @return void
	 */
	public function beforeRender(Event $event)
	{
		parent::beforeRender($event);
		$this->loadModel('TranslationFooter');
		$this->loadModel('Footer');
		//load footer
		$query_footer = $this->Footer->find()->where(['Footer.id' => 'XANQW4QFAODS']);
		$footer_data = $query_footer->first();
		$footer_menu = $footer_data['menu_content'];
		$footer_copyright_text = $footer_data['copyright_text'];

		$this->set('footer_menu', $footer_menu);
		$this->set('footer_copyright_text', $footer_copyright_text);
	}

	public function authorize($user, $role_permissions)
	{
		if (!empty($role_permissions)) {
			$access_permissions = $role_permissions;
			if (!empty($access_permissions)) {
				$request_route = strtolower($this->request->controller) . '/' . $this->request->action;
				return in_array($request_route, $access_permissions);
			}
		}
		return false;
	}

	public function forceSSL()
	{
		return $this->redirect('https://' . env('SERVER_NAME') . $this->request->getAttribute('here'));
	}


	public function formatPrice()
	{
		$this->autoRender = false;

		$result = [];

		$currency_id = !empty($this->request->query['currency_id']) ? $this->request->query['currency_id'] : 1;
		$amount = !empty($this->request->query['amount']) ? $this->request->query['amount'] : 0;
		$enable_format = isset($this->request->query['enable_format']) && $this->request->query['enable_format'] == 'false' ? false : true;

		$result = $this->Currency->convert($currency_id, null, $amount, $enable_format);
		$this->response->body(json_encode($result));
	}

	public function getCountry()
	{
		$country = 'en';
		$ip = false;

		if (!empty($_SERVER["HTTP_CF_CONNECTING_IP"])) {
			$ip = $_SERVER['HTTP_CF_CONNECTING_IP'];
		} else if (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
			$ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
		} else if (!empty($_SERVER['REMOTE_ADDR'])) {
			$ip = $_SERVER['REMOTE_ADDR'];
		}

		if (!empty($ip)) {
			$this->loadModel("Ip2Nation");

			$result = $this->Ip2Nation->find('all', array(
				'conditions' => array(
					'ip < INET_ATON("' . $ip . '")'
				),
				'order' => ['ip' => 'DESC'],
				'limit' => 1
			))->hydrate(false)->first();

			$country = $result['country'];

			if (!in_array($country, array('bg', 'tr'))) {
				$country = 'en';
			}
		}

		return $country;
	}

	private function redirectToHomePage()
	{
		if (isset($_COOKIE['language']) && in_array($_COOKIE['language'], array('en', 'bg', 'tr'))) {
			$target = '/' . $_COOKIE['language'];
		} else if (isset($_COOKIE['autolanguage']) && in_array($_COOKIE['autolanguage'], array('en', 'bg', 'tr'))) {
			$target = '/' . $_COOKIE['autolanguage'];
		} else {
			$target = '/';
		}

		if ($target != $this->request->getAttribute('here') && ($target . '/') != $this->request->getAttribute('here')) {
			return $this->redirect($target);
		}
	}
}
