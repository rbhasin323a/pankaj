<?php
namespace App\Controller\Api;

use App\Controller\Traits\Api\Account\LoginTrait;
use App\Controller\Traits\Api\Account\RegisterTrait;
use App\Controller\Traits\Api\Account\CompleteDetailsTrait;

use CakeDC\Users\Controller\Traits\CustomUsersTableTrait;

/**
 * AccountController for API
 *
 */
class AccountController extends AppController
{
	use CustomUsersTableTrait;
	use LoginTrait;
	use RegisterTrait;
	use CompleteDetailsTrait;
}
