<?php
namespace App\Controller\Api;

use App\Controller\AppController as BaseAppController;
use App\Controller\Traits\Api\BaseTrait;

/**
 * AppController for API
 *
 */
class AppController extends BaseAppController
{
	use BaseTrait;
}
