<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Event\Event;

require_once(MALDOPAY_PATH . 'maldopay.php');

/**
 * Sales Controller
 *
 * @property \App\Model\Table\SalesTable $Sales
 */
class LandingController extends AppController
{
	public function initialize()
	{
		parent::initialize();
	}

	public function beforeFilter(Event $event)
	{
		parent::beforeFilter($event);
		$this->Auth->allow(['pending', 'breturn']);
		$this->Auth->allow(['canceled', 'breturn']);
		$this->Auth->allow(['declined', 'breturn']);
		$this->Auth->allow(['failed', 'breturn']);
	}

	/**
	 * Index method
	 *
	 * @return void
	 */
	public function index()
	{ }
	public function pending()
	{ }
	public function canceled()
	{ }
	public function declined()
	{ }
	public function failed()
	{
		//$this->set('codeMessage', $message);
	}
}
