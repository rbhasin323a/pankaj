<?php
namespace App\Controller;

use App\Controller\PagesController;
use Cake\Event\Event;
use App\Controller\AppController;
use Cake\Core\Configure;
use Cake\ORM\TableRegistry;
use Cake\I18n\I18n;
use Cake\Network\Response;

class LanguageController extends AppController
{
	public function beforeFilter(Event $event)
	{
		parent::beforeFilter($event);

		$this->Auth->allow(['index']);
	}

	public function beforeRender(Event $event)
	{
		parent::beforeRender($event);

		$this->viewBuilder()->setLayout(false);
	}

	public function index($params = [])
	{

		$languages = TableRegistry::get('Languages')->find('all')->where(['status' => 1])->order(['sort_order' => 'asc']);
		$lang = $this->request->getSession()->read('Config.language');

		$data = [];


		if (!empty($this->request->data['lang'])) {
			$data['lang'] = strtolower($this->request->data['lang']);
		} else if (!empty($params['lang'])) {
			$data['lang'] = strtolower($params['lang']);
		} else if (!empty($lang)) {
			$data['lang'] = strtolower($lang);
		}

		setcookie("language", $data['lang'], time() + 3600 * 24 * 365, '/');

		if (!empty($this->request->data['redirect'])) {
			$data['redirect'] = $this->request->data['redirect'];
		} else {
			$data['redirect'] = 'homepage';
		}

		foreach ($languages as $language) {
			if ($language->code == $data['lang']) {
				$lang = $data['lang'];
				$locale = $language->locale;
			}
		}

		if (!empty($lang) && !empty($locale)) {
			$this->request->getSession()->write('Config.language', $lang);

			switch ($lang) {
				case "bg":
					$this->request->getSession()->write('Config.currency', 'BGN');
					break;
				case "tr":
					$this->request->getSession()->write('Config.currency', 'TRY');
					break;
				default:
					$this->request->getSession()->write('Config.currency', 'EUR');
			}

			I18n::locale($locale);
		}

		if ($data['redirect'] == 'homepage') {
			$this->loadModel('TranslationFooter');
			$this->loadModel('Footer');
			//load footer
			$query_footer = $this->Footer->find()->where(['Footer.id' => 'XANQW4QFAODS']);
			$footer_data = $query_footer->first();
			$footer_menu = $footer_data['menu_content'];
			$footer_copyright_text = $footer_data['copyright_text'];

			$pages_controller = new PagesController();
			$pages_controller->beforeFilter(new Event('dummy_filter'));
			$pages_controller->beforeRender(new Event('dummy_render'));

			$this->autoRender = false;

			$view =  $this->viewBuilder()->build(array('footer_menu' => $footer_menu, 'footer_copyright_text' => $footer_copyright_text))->render('Pages/home', 'default');
			$response = new Response(array('body' => $view));
			return $response;
		} else {
			$routePrefix = substr($this->request->data['referrer'], 0, 3);

			if (!in_array($lang, array('bg', 'tr'))) {
				$lang = 'en';
			}
			if (in_array($routePrefix, array('/en', '/bg', '/tr'))) {
				// change lang prefix
				$newUrl = '/' . $lang . substr($this->request->data['referrer'], 3);
			} else {
				// need to add lang prefix
				$newUrl = '/' . $lang . $this->request->data['referrer'];
			}
			return $this->redirect($newUrl);
		}
	}
}
