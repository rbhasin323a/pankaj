<?php
namespace App\Controller;

use Cake\Event\Event;
use App\Controller\AppController;
use App\Form\ContactForm;
use Cake\I18n\I18n;

class ContactController extends AppController
{
	public function beforeFilter(Event $event) {
		parent::beforeFilter($event);

		$this->Auth->allow();
	}

	public function index() {
		$this->loadModel('TranslationSeo');
		$this->loadModel('TranslationFooter');
		$this->loadModel('Seo');
		$this->loadModel('Footer');

		$contact = new ContactForm();

		 //load the SEO titles
		$url = explode('/', $_SERVER['REQUEST_URI']);

 		$query = $this->Seo->find()->where(['Seo.page' => $url[1]]);
 		$seo_data = $query->first();

 		$seo_title = isset($seo_data['seo_title']) ? $seo_data['seo_title'] : '';
		$seo_description = isset($seo_data['seo_description']) ? $seo_data['seo_description'] : '';

		//load footer
		$query_footer = $this->Footer->find()->where(['Footer.id' => 'XANQW4QFAODS']);
		$footer_data = $query_footer->first();
		$footer_menu = $footer_data['menu_content'];
		$footer_copyright_text = $footer_data['copyright_text'];

		if ($this->request->is(['patch','post','put'])) {
			if ($contact->execute($this->request->data)) {
				$this->Flash->success(__('Thank you for reaching us. We will get back to you soon.'));
			} else {
				$this->Flash->error(__('There was a problem submitting your form. Please try again later.'));
			}
		}

		$this->set('contact', $contact);
		$this->set('seo_title', $seo_title);
		$this->set('seo_description', $seo_description);
		$this->set('footer_menu', $footer_menu);
		$this->set('footer_copyright_text', $footer_copyright_text);
		try {
			if (file_exists(APP . 'Template' . DS . $this->_viewPath() . DS . I18n::locale() . DS . 'index.ctp')) {
				$template =  I18n::locale() . '/index';
				$this->render($template);
			}
		} catch (MissingTemplateException $e) {
			if (Configure::read('debug')) {
				throw $e;
			}
			throw new NotFoundException();
		}
	}
}
