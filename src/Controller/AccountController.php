<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Event\Event;
use Cake\ORM\TableRegistry;

/**
 * Account Controller
 *
 */
class AccountController extends AppController
{
	public function beforeFilter(Event $event)
	{
		parent::beforeFilter($event);
	}

	public function index()
	{
	    
		$this->loadModel('TranslationSeo');
		$this->loadModel('TranslationFooter');
		$this->loadModel('Seo');
		$this->loadModel('Footer');
		$this->loadModel('MyUsers');
		$this->loadModel('Countries');

		$id = $this->Auth->user('id');
		$user = $this->MyUsers->get($id);


		if ($this->request->is(['patch', 'post', 'put'])) {
		   // $user->password =$this->request->data['password'];
		     // echo "<pre>";print_r($user);die;
		     
		     $user->active='1';
		     if(empty($this->request->data['password'])){
		         
		         unset($this->request->data['password']);
		         unset($this->request->data['password_confirm']);
		         
		     }
			 $user = $this->MyUsers->patchEntity($user, $this->request->data);
          
			if ($this->MyUsers->save($user)) {
			     /*$usersData= TableRegistry::get('MyUsers');
                 $usersSinglefileds = $usersData->get($id); // Return article with id = $id (primary_key of row which need to get updated)
                 $usersSinglefileds->password  =$this->request->data['password'];
                 $usersSinglefileds->active=1;
                 $usersData->save($usersSinglefileds);*/
                
			    
				$this->Flash->success(__('Your details have been saved.'));
				return $this->redirect(['action' => 'index']);
			} else {
				$this->Flash->error(__('Your details could not be saved. Please check your form for errors and try again.'));
			}
		}

		$countries = TableRegistry::get('Countries')->find('list')->order(['name' => 'asc']);

		// Load the SEO titles
		$query = $this->Seo->find()->where(['Seo.page' => 'account']);
		$seo_data = $query->first();

		$seo_title = isset($seo_data['seo_title']) ? $seo_data['seo_title'] : '';
		$seo_description = isset($seo_data['seo_description']) ? $seo_data['seo_description'] : '';

		// Load footer
		$query_footer = $this->Footer->find()->where(['Footer.id' => 'XANQW4QFAODS']);
		$footer_data = $query_footer->first();
		$footer_menu = $footer_data['menu_content'];
		$footer_copyright_text = $footer_data['copyright_text'];

		$this->set('countries', $countries);
		$this->set('seo_title', $seo_title);
		$this->set('seo_description', $seo_description);
		$this->set('footer_menu', $footer_menu);
		$this->set('footer_copyright_text', $footer_copyright_text);
		$this->set('user', $user);
	}

	public function bookings()
	{
		$this->loadModel('TranslationSeo');
		$this->loadModel('Seo');
		$this->loadModel('Bookings');

		// Load the SEO titles
		$query = $this->Seo->find()->where(['Seo.page' => 'bookings']);
		$seo_data = $query->first();

		$seo_title = isset($seo_data['seo_title']) ? $seo_data['seo_title'] : '';

		$this->paginate = [
			'limit' => 24,
			'order' => [
				'created' => 'desc'
			],
			'contain' => []
		];

		$query = $this->Bookings->find('all')->where([
			'user_id' => $this->Auth->user('id'),
		])->contain(['Sales']);

		$this->set('bookings', $this->paginate($query));
		$this->set('seo_title', $seo_title);
	}
}
