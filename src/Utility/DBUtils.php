<?php

namespace App\Utility;

class DBUtils
{
	public static function secure_random_string($length = 16)
	{
		if (function_exists('openssl_random_pseudo_bytes')) {
			$bytes = openssl_random_pseudo_bytes($length * 2);

			if ($bytes === false) {
				throw new \LengthException('$length is not accurate, unable to generate random string');
			}

			return substr(str_replace(array('/', '+', '='), '', base64_encode($bytes)), 0, $length);
		}

		return self::random_string($length);
	}

	public static function random_string($length = 16, $human_friendly = true, $include_symbols = false, $no_duplicate_chars = false)
	{
		$nice_chars = 'ABCDEFGHJKLMNPQRSTUVWXYZabcdefhjkmnprstuvwxyz23456789';
		$all_an     = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890';
		$symbols    = '!@#$%^&*()~_-=+{}[]|:;<>,.?/"\'\\`';
		$string     = '';

		// Determine the pool of available characters based on the given parameters
		if ($human_friendly) {
			$pool = $nice_chars;
		} else {
			$pool = $all_an;

			if ($include_symbols) {
				$pool .= $symbols;
			}
		}

		if (!$no_duplicate_chars) {
			return substr(str_shuffle(str_repeat($pool, $length)), 0, $length);
		}

		// Don't allow duplicate letters to be disabled if the length is
		// longer than the available characters
		if ($no_duplicate_chars && strlen($pool) < $length) {
			throw new \LengthException('$length exceeds the size of the pool and $no_duplicate_chars is enabled');
		}

		// Convert the pool of characters into an array of characters and
		// shuffle the array
		$pool       = str_split($pool);
		$poolLength = count($pool);
		$rand       = mt_rand(0, $poolLength - 1);

		// Generate our string
		for ($i = 0; $i < $length; $i++) {
			$string .= $pool[$rand];

			// Remove the character from the array to avoid duplicates
			array_splice($pool, $rand, 1);

			// Generate a new number
			if (($poolLength - 2 - $i) > 0) {
				$rand = mt_rand(0, $poolLength - 2 - $i);
			} else {
				$rand = 0;
			}
		}

		return $string;
	}

	public static function is_serialized($data)
	{
		// If it isn't a string, it isn't serialized
		if (!is_string($data)) {
			return false;
		}

		$data = trim($data);

		// Is it the serialized NULL value?
		if ($data === 'N;') {
			return true;
		}
		// Is it a serialized boolean?
		elseif ($data === 'b:0;' || $data === 'b:1;') {
			return true;
		}

		$length = strlen($data);

		// Check some basic requirements of all serialized strings
		if ($length < 4 || $data[1] !== ':' || ($data[$length - 1] !== ';' && $data[$length - 1] !== '}')) {
			return false;
		}

		return @unserialize($data) !== false;
	}

	/**
	 * Unserializes partially-corrupted arrays that occur sometimes. Addresses
	 * specifically the `unserialize(): Error at offset xxx of yyy bytes` error.
	 *
	 * NOTE: This error can *frequently* occur with mismatched character sets
	 * and higher-than-ASCII characters.
	 *
	 * Contributed by Theodore R. Smith of PHP Experts, Inc. <http://www.phpexperts.pro/>
	 *
	 * @param  string $brokenSerializedData
	 * @return string
	 */
	public static function fix_broken_serialization($brokenSerializedData)
	{
		$fixdSerializedData = preg_replace_callback('!s:(\d+):"(.*?)";!', function ($matches) {
			$snip = $matches[2];
			return 's:' . strlen($snip) . ':"' . $snip . '";';
		}, $brokenSerializedData);

		return $fixdSerializedData;
	}
}
