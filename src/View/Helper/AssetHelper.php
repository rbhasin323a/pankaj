<?php
namespace App\View\Helper;

use Cake\View\Helper;

class AssetHelper extends Helper {
	public function getFileRev($asset = false, $manifest = false) {
		if (empty($asset) || empty($manifest)) {
			return false;
		}

		if (file_exists($manifest)) {
			$json = json_decode(file_get_contents($manifest), true);

			if (!empty($json[$asset])) {
				return $json[$asset];
			}
		}

		return false;
	}
}
