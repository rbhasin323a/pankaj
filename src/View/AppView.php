<?php
namespace App\View;

use Cake\View\View;
use Cake\Core\Configure;
use Cake\ORM\TableRegistry;
use Cake\I18n\I18n;

/**
 * Application View
 *
 * Your application’s default view class
 *
 * @link http://book.cakephp.org/3.0/en/views.html#the-app-view
 */
class AppView extends View
{
	public $vd = [];

	/**
	 * Initialization hook method.
	 *
	 * Use this method to add common initialization code like loading helpers.
	 *
	 * e.g. `$this->loadHelper('Html');`
	 *
	 * @return void
	 */
	public function initialize()
	{
		$this->loadHelper('Html');
		$this->loadHelper('HtmlBetter');

		if (empty($this->request->getParam('prefix'))) {
			$this->loadHelper('Paginator', ['templates' => 'paginator-templates']);
		}

		$this->loadLanguage();
		$this->loadUser();

		$this->set('vd', $this->vd);
	}

	protected function loadUser()
	{
		$session_user = $this->request->getSession()->read('Auth.User');

		if (!empty($session_user)) {
			$this->vd['user'] = $session_user;
		}

		if (!empty($this->vd['user']['id'])) {
			$db_user = TableRegistry::get('MyUsers')->find('all', [
				'conditions' => ['id LIKE' => $this->vd['user']['id']]
			])->first();

			if (!empty($db_user)) {
				$this->vd['user'] = $db_user->toArray();
			}
		}
	}

	protected function loadLanguage()
	{
		$languages = TableRegistry::get('Languages')->find('all')->where(['status' => 1])->order(['sort_order' => 'asc']);

		$lang = $this->request->getSession()->read('Config.language');
		$locale = Configure::read('App.defaultLocale');

		if (empty($lang)) {
			foreach ($languages as $language) {
				if ($language->locale == $locale) {
					$lang = $language->code;
				}
			}
		}

		foreach ($languages as $language) {
			if ($language->code == $lang) {
				$locale = $language->locale;
			}
		}

		if (!empty($lang) && !empty($locale) && is_dir(APP . 'Locale' . DS . $locale)) {
			I18n::setLocale($locale);
		}

		$this->vd['languages'] = $languages;
		$this->vd['lang'] = $lang;
	}
}
