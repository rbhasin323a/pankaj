<?php
namespace App\View\Cell;

use Cake\View\Cell;

/**
 * HomeSearch cell
 */
class HomeSearchCell extends Cell {
	/**
	 * List of valid options that can be passed into this
	 * cell's constructor.
	 *
	 * @var array
	 */
	protected $_validCellOptions = [
		'special_sales_limit'
	];

	protected $special_sales_limit = 5;

	/**
	 * Default display method.
	 *
	 * @return void
	 */
	public function display() {
		$this->loadModel('Sales');

		/**
		 * Find active sales limited by $this->special_sales_limit
		 */
		$special_sales_query = $this->Sales->find()
		->contain([
			'SaleOffers' => function($q) {
				return $q->where([
					'SaleOffers.active' => 1
				])->contain([
					'SaleAllocations' => function($q) {
						return $q->where([
							'SaleAllocations.date_start >= ' => date('Y-m-d')
						]);
					}
				]);
			}
		])
		->where([
			'OR' => [
				['Sales.active' => 1],
				['Sales.active' => 2]
			],
			'Sales.date_open <= ' => date('Y-m-d H:i'),
			'Sales.date_close >= ' => date('Y-m-d H:i')
		])
		->order([
			'promoted' => 'DESC'
		])
		->limit($this->special_sales_limit);

		$this->set('special_sales', $special_sales_query->toArray());

		/**
		 * Itterate $special_sales_query and count SaleOffers
		 * @todo Replace with a better ORM count()
		 */
		$special_sale_offers_count = 0;

		foreach ($special_sales_query as $sale) {
			if (!empty($sale->sale_offers)) {
				foreach ($sale->sale_offers as $sale_offer) {
					$special_sale_offers_count++;
				}
			}
		}

		$this->set('special_sale_offers_count', $special_sale_offers_count);

		/**
		 * Find all active sales
		 */
		$total_offers_query = $this->Sales->find()
		->contain([
			'SaleOffers' => function($q) {
				return $q->where([
					'SaleOffers.active' => 1
				])->contain([
					'SaleAllocations' => function($q) {
						return $q->where([
							'SaleAllocations.date_start >= ' => date('Y-m-d')
						]);
					}
				]);
			}
		])
		->where([
			'OR' => [
				['Sales.active' => 1],
				['Sales.active' => 2]
			],
			'Sales.date_open <= ' => date('Y-m-d H:i'),
			'Sales.date_close >= ' => date('Y-m-d H:i')
		]);

		/**
		 * Itterate $total_offers_query and count SaleOffers
		 * @todo Replace with a better ORM count()
		 */
		$total_offers_count = 0;

		foreach ($total_offers_query as $sale) {
			if (!empty($sale->sale_offers)) {
				foreach ($sale->sale_offers as $sale_offer) {
					$total_offers_count++;
				}
			}
		}

		$this->set('total_offers_count', $total_offers_count);
	}
}
