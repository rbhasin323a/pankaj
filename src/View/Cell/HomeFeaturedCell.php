<?php
namespace App\View\Cell;

use Cake\View\Cell;
use Cake\I18n\I18n;

/**
 * HomeFeatured cell
 */
class HomeFeaturedCell extends Cell
{
	/**
	 * List of valid options that can be passed into this
	 * cell's constructor.
	 *
	 * @var array
	 */
	protected $_validCellOptions = [
		'categories_limit'
	];

	protected $categories_limit = 4;

	/**
	 * Default display method.
	 *
	 * @return void
	 */
	public function display()
	{
		$this->loadModel('Sales');
		$this->loadModel('Categories');

		/**
		 * Find active sales limited by $this->categories_limit
		 */
		$categories_query = $this->Categories->find()
			->limit($this->categories_limit);

		/**
		 * Find active sales in $categories_query
		 */
		$category_ids = [];

		foreach ($categories_query as $category) {
			if (!empty($category->id) && !in_array($category->id, $category_ids)) {
				$category_ids[] = $category->id;
			}
		}

		$category_sales_query = $this->Sales->find()
			->contain([
				'SaleOffers' => function ($q) {
					return $q->where([
						'SaleOffers.active' => 1
					])->contain([
						'SaleAllocations' => function ($q) {
							return $q->where([
								'SaleAllocations.date_start >= ' => date('Y-m-d')
							]);
						}
					]);
				},
				'SaleToCategory'
			])
			->join([
				'table' => 'sale_to_category',
				'alias' => 'stc',
				'type' => 'LEFT',
				'conditions' => 'stc.sale_id = Sales.id'
			])
			->where([
				'OR' => [
					['Sales.active' => 1],
					['Sales.active' => 2]
				],
				'AND' => [
					'stc.category_id IN' => $category_ids,
					'Sales.date_open <= ' => date('Y-m-d H:i'),
					'Sales.date_close >= ' => date('Y-m-d H:i')
				]
			]);

		/**
		 * Itterate $categories_query and count Sales and SaleOffers
		 * @todo Replace with a better ORM count()
		 */

		foreach ($categories_query as $category) {
			if (!isset($category->sale_count)) {
				$category->sale_count = 0;
			}

			if (!isset($category->sale_offers_count)) {
				$category->sale_offers_count = 0;
			}

			foreach ($category_sales_query as $sale) {

				if (is_array($sale->sale_to_category)) {
					foreach ($sale->sale_to_category as $sale_to_category) {
						if ($category->id === $sale_to_category->category_id) {
							$category->sale_count++;
							if (!empty($sale->sale_offers)) {
								foreach ($sale->sale_offers as $sale_offer) {
									$category->sale_offers_count++;
								}
							}
						}
					}
				}
			}
		}

		$categories_array = $categories_query->toArray();

		usort($categories_array, function ($a, $b) {
			return $a->priority < $b->priority;
		});

		$this->set('categories', $categories_array);

		/**
		 * Itterate $category_sales_query and count SaleOffers
		 * @todo Replace with a better ORM count()
		 */
		$total_category_sale_offers_count = 0;

		foreach ($category_sales_query as $sale) {
			if (!empty($sale->sale_offers)) {
				foreach ($sale->sale_offers as $sale_offer) {
					$total_category_sale_offers_count++;
				}
			}
		}

		$this->set('total_category_sale_offers_count', $total_category_sale_offers_count);
	}
}
