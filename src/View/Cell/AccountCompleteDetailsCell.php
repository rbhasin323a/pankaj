<?php
namespace App\View\Cell;

use Cake\View\Cell;

/**
 * AccountCompleteDetails cell
 */
class AccountCompleteDetailsCell extends Cell
{
	/**
	 * List of valid options that can be passed into this
	 * cell's constructor.
	 *
	 * @var array
	 */
	protected $_validCellOptions = [];

	/**
	 * Default display method.
	 *
	 * @return void
	 */
	public function display()
	{
		$this->set('account_logged_in', $this->request->getSession()->consume('API.Account.LoggedIn'));
		$this->set('account_registered', $this->request->getSession()->consume('API.Account.Registered'));
	}
}
