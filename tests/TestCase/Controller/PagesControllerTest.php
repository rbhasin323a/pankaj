<?php
namespace App\Test\TestCase\Controller;

use Cake\Core\Configure;
use Cake\TestSuite\IntegrationTestCase;

/**
 * PagesControllerTest class
 */
class PagesControllerTest extends IntegrationTestCase
{

	/**
	 * testDisplay method
	 *
	 * @return void
	 */
	public function testDisplay()
	{
		$this->get('/pages/home');
		$this->assertResponseOk();
		$this->assertResponseContains('CakePHP');
		$this->assertResponseContains('<html>');
	}

	/**
	 * Test that missing template renders 404 page in production
	 *
	 * @return void
	 */
	public function testMissingTemplate()
	{
		Configure::write('debug', false);
		$this->get('/pages/not_existing');

		$this->assertResponseError();
		$this->assertResponseContains('Error');
	}

	/**
	 * Test that missing template in debug mode renders missing_template error page
	 *
	 * @return void
	 */
	public function testMissingTemplateInDebug()
	{
		Configure::write('debug', true);
		$this->get('/pages/not_existing');

		$this->assertResponseFailure();
		$this->assertResponseContains('Missing Template');
		$this->assertResponseContains('Stacktrace');
		$this->assertResponseContains('not_existing.ctp');
	}
}
