<?php
namespace App\Test\TestCase\Controller\Admin;

use App\Controller\Admin\BookingsController;
use Cake\TestSuite\IntegrationTestCase;

/**
 * App\Controller\Admin\BookingsController Test Case
 */
class BookingsControllerTest extends IntegrationTestCase
{

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.bookings',
        'app.transactions',
        'app.booking_summaries',
        'app.sales',
        'app.contractors',
        'app.suppliers',
        'app.companies',
        'app.contacts',
        'app.currencies',
        'app.countries',
        'app.country_zones',
        'app.taxes',
        'app.sale_offers',
        'app.sale_offers_name_translation',
        'app.sale_offers_child_age_description_translation',
        'app.sale_offers_infant_age_description_translation',
        'app.sale_offers_single_policy_translation',
        'app.sale_offers_child_policy_translation',
        'app.sale_offers_summary_description_translation',
        'app.sale_offers_main_description_translation',
        'app.translation_sale_offers',
        'app.sale_allocations',
        'app.scyscanner_airports',
        'app.sales_title_translation',
        'app.sales_sub_title_translation',
        'app.sales_destination_translation',
        'app.sales_slug_translation',
        'app.sales_short_description_translation',
        'app.sales_main_description_translation',
        'app.sales_staff_description_translation',
        'app.sales_hotel_description_translation',
        'app.sales_travel_description_translation',
        'app.sales_review_description_translation',
        'app.sales_location_title_translation',
        'app.translation_sales',
        'app.departures',
        'app.users',
        'app.acl_groups',
        'app.payments'
    ];

    /**
     * Test index method
     *
     * @return void
     */
    public function testIndex()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test view method
     *
     * @return void
     */
    public function testView()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test add method
     *
     * @return void
     */
    public function testAdd()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test edit method
     *
     * @return void
     */
    public function testEdit()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test delete method
     *
     * @return void
     */
    public function testDelete()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
