<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\SaleDescriptionsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\SaleDescriptionsTable Test Case
 */
class SaleDescriptionsTableTest extends TestCase
{

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.sale_descriptions',
        'app.sales',
        'app.contractors',
        'app.suppliers',
        'app.companies',
        'app.contacts',
        'app.currencies',
        'app.languages'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('SaleDescriptions') ? [] : ['className' => 'App\Model\Table\SaleDescriptionsTable'];
        $this->SaleDescriptions = TableRegistry::get('SaleDescriptions', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->SaleDescriptions);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test beforeSave method
     *
     * @return void
     */
    public function testBeforeSave()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test secure_random_string method
     *
     * @return void
     */
    public function testSecureRandomString()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test random_string method
     *
     * @return void
     */
    public function testRandomString()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
