<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\SaleDetailsI18nTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\SaleDetailsI18nTable Test Case
 */
class SaleDetailsI18nTableTest extends TestCase
{

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.sale_details_i18n'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('SaleDetailsI18n') ? [] : ['className' => 'App\Model\Table\SaleDetailsI18nTable'];
        $this->SaleDetailsI18n = TableRegistry::get('SaleDetailsI18n', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->SaleDetailsI18n);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test beforeSave method
     *
     * @return void
     */
    public function testBeforeSave()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test generate_id method
     *
     * @return void
     */
    public function testGenerateId()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
