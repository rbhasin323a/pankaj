<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\BookingsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\BookingsTable Test Case
 */
class BookingsTableTest extends TestCase
{

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.bookings',
        'app.transactions',
        'app.booking_summaries',
        'app.sales',
        'app.contractors',
        'app.suppliers',
        'app.companies',
        'app.contacts',
        'app.currencies',
        'app.countries',
        'app.country_zones',
        'app.taxes',
        'app.sale_offers',
        'app.sale_offers_name_translation',
        'app.sale_offers_child_age_description_translation',
        'app.sale_offers_infant_age_description_translation',
        'app.sale_offers_single_policy_translation',
        'app.sale_offers_child_policy_translation',
        'app.sale_offers_summary_description_translation',
        'app.sale_offers_main_description_translation',
        'app.translation_sale_offers',
        'app.sale_allocations',
        'app.scyscanner_airports',
        'app.sales_title_translation',
        'app.sales_sub_title_translation',
        'app.sales_destination_translation',
        'app.sales_slug_translation',
        'app.sales_short_description_translation',
        'app.sales_main_description_translation',
        'app.sales_staff_description_translation',
        'app.sales_hotel_description_translation',
        'app.sales_travel_description_translation',
        'app.sales_review_description_translation',
        'app.sales_location_title_translation',
        'app.translation_sales',
        'app.departures',
        'app.users',
        'app.acl_groups',
        'app.payments'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('Bookings') ? [] : ['className' => 'App\Model\Table\BookingsTable'];
        $this->Bookings = TableRegistry::get('Bookings', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Bookings);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
