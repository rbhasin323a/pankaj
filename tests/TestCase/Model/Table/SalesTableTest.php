<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\SalesTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\SalesTable Test Case
 */
class SalesTableTest extends TestCase
{

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.sales',
        'app.contractors',
        'app.suppliers',
        'app.companies',
        'app.contacts',
        'app.currencies',
        'app.countries',
        'app.country_zones',
        'app.taxes',
        'app.sale_offers',
        'app.sale_offers_name_translation',
        'app.sale_offers_child_age_description_translation',
        'app.sale_offers_infant_age_description_translation',
        'app.sale_offers_single_policy_translation',
        'app.sale_offers_child_policy_translation',
        'app.sale_offers_summary_description_translation',
        'app.sale_offers_main_description_translation',
        'app.translation_sale_offers',
        'app.sale_allocations',
        'app.sales_title_translation',
        'app.sales_sub_title_translation',
        'app.sales_destination_translation',
        'app.sales_slug_translation',
        'app.sales_short_description_translation',
        'app.sales_main_description_translation',
        'app.sales_staff_description_translation',
        'app.sales_hotel_description_translation',
        'app.sales_travel_description_translation',
        'app.sales_review_description_translation',
        'app.sales_location_title_translation',
        'app.translation_sales'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('Sales') ? [] : ['className' => 'App\Model\Table\SalesTable'];
        $this->Sales = TableRegistry::get('Sales', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Sales);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test beforeSave method
     *
     * @return void
     */
    public function testBeforeSave()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test generate_id method
     *
     * @return void
     */
    public function testGenerateId()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test searchConfiguration method
     *
     * @return void
     */
    public function testSearchConfiguration()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
