<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\SaleDetailsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\SaleDetailsTable Test Case
 */
class SaleDetailsTableTest extends TestCase
{

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.sale_details',
        'app.sale_galleries',
        'app.sales',
        'app.contractors',
        'app.suppliers',
        'app.companies',
        'app.contacts',
        'app.currencies',
        'app.countries',
        'app.country_zones',
        'app.taxes',
        'app.sale_offers',
        'app.scyscanner_airports',
        'app.i18n',
        'app.sale_details_i18n'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('SaleDetails') ? [] : ['className' => 'App\Model\Table\SaleDetailsTable'];
        $this->SaleDetails = TableRegistry::get('SaleDetails', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->SaleDetails);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test beforeSave method
     *
     * @return void
     */
    public function testBeforeSave()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test generate_id method
     *
     * @return void
     */
    public function testGenerateId()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
