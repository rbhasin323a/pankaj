<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\SaleAllocationsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\SaleAllocationsTable Test Case
 */
class SaleAllocationsTableTest extends TestCase
{

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.sale_allocations',
        'app.sale_offers',
        'app.taxes',
        'app.sales',
        'app.contractors',
        'app.suppliers',
        'app.companies',
        'app.contacts',
        'app.currencies',
        'app.countries',
        'app.country_zones',
        'app.sales_title_translation',
        'app.sales_sub_title_translation',
        'app.sales_destination_translation',
        'app.sales_slug_translation',
        'app.sales_short_description_translation',
        'app.sales_main_description_translation',
        'app.sales_staff_description_translation',
        'app.sales_hotel_description_translation',
        'app.sales_travel_description_translation',
        'app.sales_review_description_translation',
        'app.sales_location_title_translation',
        'app.translation_sales',
        'app.sale_offers_name_translation',
        'app.sale_offers_child_age_description_translation',
        'app.sale_offers_infant_age_description_translation',
        'app.sale_offers_single_policy_translation',
        'app.sale_offers_child_policy_translation',
        'app.sale_offers_summary_description_translation',
        'app.sale_offers_main_description_translation',
        'app.translation_sale_offers'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('SaleAllocations') ? [] : ['className' => 'App\Model\Table\SaleAllocationsTable'];
        $this->SaleAllocations = TableRegistry::get('SaleAllocations', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->SaleAllocations);

        parent::tearDown();
    }

    /**
     * Test beforeSave method
     *
     * @return void
     */
    public function testBeforeSave()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test searchConfiguration method
     *
     * @return void
     */
    public function testSearchConfiguration()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test generate_id method
     *
     * @return void
     */
    public function testGenerateId()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
