<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\TransactionsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\TransactionsTable Test Case
 */
class TransactionsTableTest extends TestCase
{

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.transactions',
        'app.booking_summaries',
        'app.sales',
        'app.contractors',
        'app.suppliers',
        'app.companies',
        'app.contacts',
        'app.currencies',
        'app.countries',
        'app.country_zones',
        'app.taxes',
        'app.sale_offers',
        'app.sale_allocations',
        'app.scyscanner_airports',
        'app.sales_title_translation',
        'app.sales_sub_title_translation',
        'app.sales_destination_translation',
        'app.sales_slug_translation',
        'app.sales_short_description_translation',
        'app.sales_main_description_translation',
        'app.sales_staff_description_translation',
        'app.sales_hotel_description_translation',
        'app.sales_travel_description_translation',
        'app.sales_review_description_translation',
        'app.sales_location_title_translation',
        'app.translation_sales',
        'app.offers',
        'app.departures',
        'app.users',
        'app.acl_groups',
        'app.bookings',
        'app.payments'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('Transactions') ? [] : ['className' => 'App\Model\Table\TransactionsTable'];
        $this->Transactions = TableRegistry::get('Transactions', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Transactions);

        parent::tearDown();
    }

    /**
     * Test beforeSave method
     *
     * @return void
     */
    public function testBeforeSave()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test searchConfiguration method
     *
     * @return void
     */
    public function testSearchConfiguration()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test generate_id method
     *
     * @return void
     */
    public function testGenerateId()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
