<?php
namespace App\Test\TestCase\Model\Behavior;

use App\Model\Behavior\SaleDetailsBehavior;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Behavior\SaleDetailsBehavior Test Case
 */
class SaleDetailsBehaviorTest extends TestCase
{

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $this->SaleDetails = new SaleDetailsBehavior();
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->SaleDetails);

        parent::tearDown();
    }

    /**
     * Test initial setup
     *
     * @return void
     */
    public function testInitialization()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
