// Labels for the days of the week
var cal_days_labels = ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'];

// Human-readable month name labels, in order
var cal_months_labels = [
	'January',
	'February',
	'March',
	'April',
	'May',
	'June',
	'July',
	'August',
	'September',
	'October',
	'November',
	'December',
];

// Days of the month for each month, in order
var cal_days_in_month = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];

// Calendar class constructor
function Calendar(
	selector,
	start_calendar_date,
	end_calendar_date,
	sale_offer,
	deposit,
	currency,
	currency_rate,
	preselectedStartDay,
	preselectedEndDay,
	saleOfferId
) {
	if (typeof saleOfferId !== 'undefined') {
		this.sale_offer_id = saleOfferId;
	} else {
		this.sale_offer_id = null;
	}

	//Check if the Calendar class has predefined start dates
	if (typeof start_calendar_date !== 'undefined') {
		this.start_calendar_date = new Date(start_calendar_date);
	} else {
		this.start_calendar_date = new Date();
	}

	this.end_calendar_date = new Date(end_calendar_date);

	this.sale_offer = sale_offer;
	this.deposit = deposit;

	this.month = this.start_calendar_date.getMonth();
	this.year = this.start_calendar_date.getFullYear();

	// Calculate how many calendar months will be visible
	this.months_to_render =
		monthDiff(this.start_calendar_date, this.end_calendar_date) + 2;

	this.selector = selector;
	this.startDay = preselectedStartDay;
	this.endDay = preselectedEndDay;

	this.visibleMonth = undefined;

	this.fromNavigation = false;

	this.start_flexible_date = undefined;
	this.end_flexible_date = undefined;

	this.currency = currency;
	this.currency_rate = currency_rate;

	// Remove event handlers for the calendar events if they exist
	$(document).off(
		'mouseover',
		'.isl-calendar-picker .calendar-days li:not(".inactive")'
	);
	$(document).off(
		'mouseout',
		'.isl-calendar-picker .calendar-days li:not(".inactive")'
	);
	$(document).off(
		'click',
		'.isl-calendar-picker .calendar-days li:not(".inactive")'
	);

	// Set event handlers for the calendar events
	$(document).on(
		'mouseover',
		'.isl-calendar-picker .calendar-days li:not(".inactive")',
		this.dayHover.bind(this, $(this))
	);
	$(document).on(
		'mouseout',
		'.isl-calendar-picker .calendar-days li:not(".inactive")',
		this.clearHoverCalendar.bind(this, $(this))
	);
	$(document).on(
		'click',
		'.isl-calendar-picker .calendar-days li:not(".inactive")',
		this.dayClick.bind(this, $(this))
	);

	this.updateHTML();
}

// This function is used to generate the HTML for the calendar based on the properties in the constructor
Calendar.prototype.updateHTML = function() {
	$(this.selector)
		.find('.calendar-body .calendar-months')
		.html(this.generateMonths());
	this.showActiveMonth();
	$('#isl-calendar-previous').on('click', this.previousMonth.bind(this));
	$('#isl-calendar-next').on('click', this.nextMonth.bind(this));
};

Calendar.prototype.isDateChosen = function(year, month, day) {
	return (
		(year > this.startDay.year ||
			(year == this.startDay.year &&
				(month > this.startDay.month ||
					(month == this.startDay.month &&
						day >= this.startDay.day)))) &&
		(year < this.endDay.year ||
			(year == this.endDay.year &&
				(month < this.endDay.month ||
					(month == this.endDay.month && day <= this.endDay.day))))
	);
};

Calendar.prototype.dateClass = function(year, month, day) {
	year = parseInt(year);
	month = parseInt(month);
	day = parseInt(day);
	if (
		this.startDay &&
		this.startDay.year == year &&
		this.startDay.month == month &&
		this.startDay.day == day
	) {
		return 'first-selected chosen';
	}

	if (
		this.endDay &&
		this.endDay.year == year &&
		this.endDay.month == month &&
		this.endDay.day == day
	) {
		return 'last-selected chosen';
	}

	if (this.startDay && this.endDay) {
		var res = this.isDateChosen(year, month, day) ? 'chosen' : '';
		return res;
	}

	return '';
};

// This function is used to generate the months of the calendar
Calendar.prototype.generateMonths = function() {
	var start_month = this.start_calendar_date.getMonth();
	var start_year = this.start_calendar_date.getFullYear();
	var today = new Date();

	var html = '';

	// Iterate over all months to generate the HTML for each one of them
	for (var i = 0; i < this.months_to_render; i++) {
		// Every month starts at day 1
		var day = 1;

		var month = start_month;
		var year = start_year;

		/**28
		 * If the start month is bigger than 11, this means that we should switch the year.
		 * This is why we make the month to 0 (months starts from 0) and increment the year with 1.
		 */
		if (start_month > 11) {
			month = 0;
			start_month = 0;
			start_year++;
			year++;
		}

		// Define the first day of the month as JavaScript Date object
		var firstDay = new Date(year, month, 1);
		var startingDay = firstDay.getDay();

		// Find number of days in the current month
		var monthLength = cal_days_in_month[month];

		// Calculate if the current month is in a leap year
		if (month == 1) {
			// February only!
			if ((year % 4 == 0 && year % 100 != 0) || year % 400 == 0) {
				monthLength = 29;
			}
		}

		// HTML of the month START
		html +=
			'<div class="month" data-month="' +
			month +
			'" style="display:none;">';
		html +=
			'    <p class="month-label" style="display:none;">' +
			cal_months_labels[month] +
			' ' +
			year +
			'</p>';
		html += '    <ul class="calendar-days-of-week">';

		// Iterate through the Labels of the days of week
		for (var k = 0; k <= 6; k++) {
			html += '        <li>' + cal_days_labels[k] + '</li>';
		}

		html += '    </ul>';

		// Calendar days START
		html += '    <ul class="calendar-days">';

		// Initialize variables needed for generating days
		var firstMondayFilled = false;
		var lastSundayFilled = false;
		var now = Date.parse(new Date());
		var comparisonDate = new Date(year, month, 1);
		var isInactive = false;

		// Filling days START
		for (var j = 0; j <= 60; j++) {
			var li_chosen_class = '';
			// Check if the first Monday of the Month is filled, so we can put the last days of the previous month
			if (!firstMondayFilled && startingDay > 0) {
				// Calculate which is the previous month
				var previousMonth;
				if (month == 0) {
					previousMonth = 11;
				} else {
					previousMonth = month - 1;
				}

				// Fill the empty days of week with the ones of the previous month
				var previousMonthLength = cal_days_in_month[previousMonth];
				for (
					var pm = previousMonthLength - startingDay + 1;
					pm <= previousMonthLength;
					pm++
				) {
					li_chosen_class = this.dateClass(year, previousMonth, pm);
					html +=
						'          <li class="inactive ' +
						li_chosen_class +
						'" data-day="' +
						pm +
						'" data-month="' +
						previousMonth +
						'" data-year="' +
						year +
						'">';
					html += '<span style="display: block">' + pm + '</span>';
					html += '          </li>';
				}
				firstMondayFilled = true;
			} else {
				// Check if we have days to fill in the calendar until we reach the month lenght
				if (day <= monthLength) {
					isInactive = false;
					comparisonDate.setDate(day);

					// Days which are >= today are inactive
					if (
						Date.parse(comparisonDate) <
						now - 1000 * 60 * 60 * 24
					) {
						isInactive = true;
					}
					li_chosen_class = this.dateClass(year, month, day);
					html +=
						'          <li class="' +
						(isInactive ? 'inactive' : '') +
						' ' +
						li_chosen_class +
						'" data-day="' +
						day +
						'" data-month="' +
						month +
						'" data-year="' +
						year +
						'" data-full-date="' +
						year +
						'-' +
						padNumber(month + 1, 2) +
						'-' +
						padNumber(day, 2) +
						'">';
					html += '<span style="display: block">' + day + '</span>';
					html += '              <span class="rate"></span>';
					html += '          </li>';

					day++;
				} else {
					// We are here when we reach the last day of the month
					var lastDay = new Date(year, month, monthLength);
					var endingDay = lastDay.getDay();

					// Check if the last sunday of the current month view is filled
					if (!lastSundayFilled && endingDay < 6) {
						var nextDay = 1;
						// Fill the missing days with days of the next month
						for (var nm = 0; nm <= 6 - endingDay - 1; nm++) {
							li_chosen_class = this.dateClass(
								year,
								month + 1,
								nextDay
							);
							html +=
								'          <li class="inactive ' +
								li_chosen_class +
								'" data-day="' +
								nextDay +
								'" data-month="' +
								(month + 1) +
								'" data-year="' +
								year +
								'">';
							html += nextDay;
							html += '          </li>';
							nextDay++;
						}
						lastSundayFilled = true;
					}
				}
			}
		}
		/* Filling days END */

		html += '        </ul>';
		/* Calendar days END */
		html += '</div>';
		/* HTML of the month END */

		// Go to next month
		start_month++;
	}

	// Return generated HTML
	return html;
};

// Show the active month by displaying month name and updating navigation
Calendar.prototype.showActiveMonth = function() {
	var monthToShow = this.month;
	var fromNavigation = this.fromNavigation;
	if (!fromNavigation) {
		if (this.startDay !== false) {
			monthToShow = this.startDay.month;
		}
		this.fromNavigation = true;
	}

	$(this.selector)
		.find(
			'.calendar-body .calendar-months div.month[data-month="' +
				monthToShow +
				'"]'
		)
		.show(0);
	var monthName = cal_months_labels[monthToShow];
	$(this.selector)
		.find('.calendar-header .header-title')
		.html(monthName + ' ' + this.year);
	this.updateNavigation();
};

// In this function we decide if we have next and previous months, so we can hide/show the navigation arrows
Calendar.prototype.updateNavigation = function() {
	var i = 0;
	calendar = this;
	$(this.selector)
		.find('.calendar-body .calendar-months div.month')
		.each(function() {
			if ($(this).css('display') != 'none') {
				calendar.visibleMonth = i;
			}

			i++;
		});

	// When the first month is visible we are checking if there are more than 1 months, so we can show/hide the `next` arrow
	if (this.visibleMonth == 0) {
		$(this.selector)
			.find('#isl-calendar-previous')
			.css('visibility', 'hidden');
		if (this.months_to_render < 2) {
			$(this.selector)
				.find('#isl-calendar-next')
				.css('visibility', 'hidden');
		} else {
			$(this.selector)
				.find('#isl-calendar-next')
				.css('visibility', 'visible');
		}
	} else if (this.months_to_render > 1) {
		// When we reach the last available month, we hide the `next` arrow
		if (this.visibleMonth == this.months_to_render - 1) {
			$(this.selector)
				.find('#isl-calendar-next')
				.css('visibility', 'hidden');
			$(this.selector)
				.find('#isl-calendar-previous')
				.css('visibility', 'visible');
		} else {
			// Previous and Next arrows are visible
			$(this.selector)
				.find('#isl-calendar-previous')
				.css('visibility', 'visible');
			$(this.selector)
				.find('#isl-calendar-next')
				.css('visibility', 'visible');
		}
	}
};

// Go to the previous available month
Calendar.prototype.previousMonth = function() {
	// Decrement the month value
	this.month--;

	// If the month value is lower than 0, this means that we have to decrement also the year
	if (this.month < 0) {
		this.month = 11;
		this.year--;
	}

	// Hide all inactive months
	$(this.selector)
		.find('.calendar-body .calendar-months div.month')
		.each(function() {
			$(this).hide(0);
		});

	this.fromNavigation = true;

	// Show only the active month
	this.showActiveMonth();
};

// Go to the next available month
Calendar.prototype.nextMonth = function() {
	// Increment the month (and the year, if needed)
	this.year += parseInt((this.month + 1) / 12);
	this.month = (this.month + 1) % 12;

	// Hide all inactive months
	$(this.selector)
		.find('.calendar-body .calendar-months div.month')
		.each(function() {
			$(this).hide(0);
		});

	this.fromNavigation = true;

	// Show only the active month
	this.showActiveMonth();
};

// Based on the Sale Allocations provided in the Calendar constructor, we are adding the prices to each day
Calendar.prototype.addPrices = function(sale_allocations, sale_type) {
	// Iterating through each Sale Allocation
	for (var i = 0; i < sale_allocations.length; i++) {
		// Define where the current Sale Allocation starts from
		var date_start = new Date(sale_allocations[i].date_start);

		// Define where the current Sale Allocation ends
		var date_end = new Date(sale_allocations[i].date_end);

		// Add 1 day to the end day if the Sale Allocation has outbound overnight
		if (sale_allocations[i].outbound_overnight) {
			//date_start.setDate(date_start.getDate() - 1);
			date_end.setDate(date_end.getDate() + 1);
		}

		// Add 1 day from the end day if the Sale Allocation has outbound overnight
		if (sale_allocations[i].inbound_overnight) {
			date_end.setDate(date_end.getDate() + 1);
		}

		// Define the sale allocation start position in the calendar
		var start_allocation = $(this.selector)
			.find(
				'.calendar-body .calendar-months div.month[data-month="' +
					date_start.getMonth() +
					'"]'
			)
			.find(
				'li[data-day="' +
					date_start.getDate() +
					'"][data-year="' +
					date_start.getFullYear() +
					'"]'
			);
		var add_allocation = false;
		var only_checkout_date = false;

		// Check if the start allocation day is valid and available on the calendar
		if (start_allocation) {
			// Check if the allocation has min nights more than 1 and if the sale offer is flexible
			if (
				sale_allocations[i].min_nights > 1 &&
				(this.sale_offer.offer_type == 'flexible' ||
					this.sale_offer.offer_type == 1)
			) {
				// Iterate through the sale allocations and decide to add the allocation if there are minimum 1 day gap between the allocations
				for (var j = 0; j < sale_allocations.length; j++) {
					if (i != j) {
						if (
							sale_allocations[j] &&
							daysDiff(
								date_start,
								new Date(sale_allocations[j].date_start)
							) < 2
						) {
							add_allocation = true;
						}
					}
				}
				// If the sale allocations min nights are bigger than the available sale allocations, we won't be adding the allocation

				if (sale_allocations[i].min_nights > sale_allocations.length) {
					add_allocation = false;
				}
			} else {
				add_allocation = true;
			}

			if (sale_allocations[i].available_rooms == 0) {
				add_allocation = false;
			}

			if (add_allocation) {
				// Check if this is only checkout date
				if (only_checkout_date) {
					start_allocation.addClass('grayed');
				}

				// Assigning sale allocation and price for the current day
				if (sale_type == 1) {
					start_allocation
						.find('.rate')
						.html(
							formatCurrency(sale_allocations[i].rate / 2, this)
						);
				} else {
					if (this.sale_offer.offer_type == 1) {
						start_allocation
							.find('.rate')
							.html(
								formatCurrency(sale_allocations[i].rate, this)
							);
					} else {
						//start_allocation.find('.rate').html(formatCurrency((sale_allocations[i].rate * (sale_allocations[i].hasOwnProperty('discount') ? 1 - (sale_allocations[i].discount/100) : 1)), this));
						start_allocation
							.find('.rate')
							.html(
								formatCurrency(sale_allocations[i].rate, this)
							);
					}
				}
				start_allocation.attr(
					'data-sale-offer-id',
					sale_allocations[i].sale_offer_id
				);
				start_allocation.attr(
					'data-sale-allocation-id',
					sale_allocations[i].id
				);
				start_allocation.attr(
					'data-min-nights',
					sale_allocations[i].min_nights
				);
			}
		}

		var end_allocation = $(this.selector)
			.find(
				'.calendar-body .calendar-months div.month[data-month="' +
					date_end.getMonth() +
					'"]'
			)
			.find(
				'li[data-day="' +
					date_end.getDate() +
					'"][data-year="' +
					date_end.getFullYear() +
					'"]'
			);
		if (end_allocation) {
			if (this.sale_offer.offer_type == 1) {
				end_allocation.attr(
					'data-sale-offer-id',
					sale_allocations[i].sale_offer_id
				);
				end_allocation.attr(
					'data-sale-allocation-id',
					sale_allocations[i].id
				);
			}
		}
	}

	$(
		'.isl-calendar-picker .calendar-days li[data-sale-allocation-id]:not(".inactive")'
	).each(function() {
		var min_nights = $(this).attr('data-min-nights');
		var current_iteration_date = $(this);

		for (var i = 0; i < min_nights - 1; i++) {
			// Check if there are more days for this months
			if (
				current_iteration_date.next().length < 1 ||
				current_iteration_date.next().attr('data-day') == '1'
			) {
				// Get month lenght of the current month
				var monthLength =
					cal_days_in_month[
						current_iteration_date.attr('data-month')
					];

				// Check if we reached the end of the month
				if (current_iteration_date.attr('data-day') == monthLength) {
					var next_month =
						parseInt(current_iteration_date.attr('data-month')) + 1;
					var next_year = parseInt(
						current_iteration_date.attr('data-year')
					);
					if (next_month == 12) {
						next_month = 0;
						next_year++;
					}

					// Get the next active sale allocation from the next month
					current_iteration_date = $(
						'.isl-calendar-picker .calendar-days li[data-sale-allocation-id][data-day=1][data-month=' +
							next_month +
							'][data-year=' +
							next_year +
							']:not(".inactive")'
					);
				}
			} else {
				// Get the next date
				current_iteration_date = current_iteration_date.next();
			}

			// If Sale Type is different than package and the sale allocation is invalid, we have to make it inactive
			if (
				sale_type != 1 &&
				this.start_flexible_date == undefined &&
				current_iteration_date.attr('data-sale-allocation-id') == null
			) {
				$(this).addClass('inactive checkout-available');
			}
		}
	});
};

// This function is used to remove the arrows to the next/previous month if the sale allocations continues
Calendar.prototype.removeArrows = function() {
	$(document)
		.find('.calendar-body .calendar-months div.arrow:not(.temp)')
		.remove();
};

// This function is used to remove the arrows to the next/previous month if the sale allocations continues
Calendar.prototype.removeTempArrows = function() {
	$(document)
		.find('.calendar-body .calendar-months div.temp')
		.remove();
};

// This function is used to add the arrows to the next/previous month if the sale allocations continues
Calendar.prototype.addArrows = function(month, year, hover) {
	var arrowTriangle;
	var monthLength = cal_days_in_month[month];

	//First inactive day of next month
	arrowTriangle = $('<div class="arrow' + (hover ? ' temp' : '') + '">');

	arrowTriangle.css({
		height: '0',
		width: '0',
		'border-left': '25px solid #ff8f00',
		'border-top': '22px solid transparent',
		'border-bottom': '22px solid transparent',
		position: 'relative',
		top: '-25px',
	});

	$(document)
		.find(
			'li[data-day="1"][data-month="' +
				(month != 11 ? parseInt(month) + 1 : 1) +
				'"][data-year="' +
				(month != 11 ? year : parseInt(year) + 1) +
				'"].inactive'
		)
		.append(arrowTriangle);

	//Last inactive day of current month (in next month's calendar)
	arrowTriangle = $('<div class="arrow' + (hover ? ' temp' : '') + '">');

	arrowTriangle.css({
		height: '0',
		width: '0',
		'border-right': '25px solid #ff8f00',
		'border-top': '22px solid transparent',
		'border-bottom': '22px solid transparent',
		float: 'right',
		position: 'relative',
		top: '-25px',
	});

	$(document)
		.find(
			'li[data-day="' +
				monthLength +
				'"][data-month="' +
				month +
				'"][data-year="' +
				year +
				'"].inactive'
		)
		.append(arrowTriangle);
};

// This function is used to mark the sale allocation starts and ends when the customer hovers on specific sale allocation
Calendar.prototype.dayHover = function(calendar, event) {
	// If the sale allocation is selected, the hovering will be disabled
	if (
		$(document).find('.calendar-body .calendar-months li.last-selected')
			.length > 0
	) {
		return false;
	}

	// Get the current hovered day
	var clicked_day = $(event.target);

	// Get the parent if the current hovered element is a text (e.g. price of the allocation)
	if (clicked_day.is('span')) {
		clicked_day = clicked_day.parent();
	}

	// Get data attributes from the hovered day
	var full_date = clicked_day.attr('data-full-date');
	var day = clicked_day.attr('data-day');
	var month = clicked_day.attr('data-month');
	var year = clicked_day.attr('data-year');
	var allocation_id = clicked_day.attr('data-sale-allocation-id');
	var min_nights;

	// Check if the current sale offer is from type 'flexible'
	if (
		this.sale_offer.offer_type == 'flexible' ||
		this.sale_offer.offer_type == 1
	) {
		var select = true;
		min_nights = clicked_day.attr('data-min-nights');

		// Disable selecting if the start date is undefined and the allocation id is not present
		if (
			this.start_flexible_date == undefined &&
			allocation_id == undefined
		) {
			select = false;
		}

		if (this.start_flexible_date != undefined) {
			var fullDateObj = new Date(full_date);
			fullDateObj.setDate(fullDateObj.getDate() - 1);

			var previousDay = $(document)
				.find(
					'.calendar-body .calendar-months div.month[data-month="' +
						fullDateObj.getMonth() +
						'"]'
				)
				.find(
					'li[data-day="' +
						fullDateObj.getDate() +
						'"][data-month="' +
						fullDateObj.getMonth() +
						'"][data-year="' +
						fullDateObj.getFullYear() +
						'"]'
				);

			// Disable selecting if the previous day allocation is undefined and the hovered date is before the set start flexible date.
			if (
				previousDay.attr('data-sale-allocation-id') == undefined &&
				this.start_flexible_date < clicked_day.attr('data-full-date')
			) {
				select = false;
			}

			// Disable selecting the start flexible date is after the hovered date
			if (
				this.start_flexible_date > clicked_day.attr('data-full-date') &&
				clicked_day.attr('data-sale-allocation-id') == undefined
			) {
				select = false;
			}

			// Get the days difference between the set flexible date start and the hovered date
			var start_date = new Date(this.start_flexible_date);
			var days_diff = daysDiff(start_date, new Date(full_date));
			// When the hovered date is after the hovered date
			if (clicked_day.attr('data-full-date') > this.start_flexible_date) {
				// Iterate through all selected dates and if one of them has invalid allocation, the selection will be disabled
				for (var i = 0; i < days_diff - 1; i++) {
					var iteration_day = $(document)
						.find(
							'.calendar-body .calendar-months div.month[data-month="' +
								start_date.getMonth() +
								'"]'
						)
						.find(
							'li[data-day="' +
								start_date.getDate() +
								'"][data-month="' +
								start_date.getMonth() +
								'"][data-year="' +
								start_date.getFullYear() +
								'"]'
						);
					if (
						iteration_day.attr('data-sale-allocation-id') ==
						undefined
					) {
						select = false;
					}
					start_date.setDate(start_date.getDate() + 1);
				}
			}
		}

		// Disable selecting when the start and end are undefined
		if (
			this.start_flexible_date != undefined &&
			this.end_flexible_date != undefined
		) {
			select = false;
		}

		// If the selected day meets the requirements above
		if (select) {
			// If the start date is not yet chosen and there is a requirement for the min nights
			if (this.start_flexible_date == undefined && min_nights > 1) {
				var monthLength = cal_days_in_month[month];

				// Iterate through dates which match to the sale allocation criteria
				for (var i = 0; i < min_nights; i++) {
					if (month == 1) {
						// February only!
						if (
							(year % 4 == 0 && year % 100 != 0) ||
							year % 400 == 0
						) {
							monthLength = 29;
						}
					}

					if (day > monthLength) {
						day = 1;
						month++;
					}

					if (month > 11) {
						month = 0;
						year++;
					}

					// Selecting the current date by adding CSS class
					$(document)
						.find(
							'.calendar-body .calendar-months div.month[data-month="' +
								month +
								'"]'
						)
						.find(
							'li[data-day="' +
								day +
								'"][data-month="' +
								month +
								'"][data-year="' +
								year +
								'"]'
						)
						.addClass('selected');

					// Add class first-selected to the first date of the sale allocation
					if (i == 0) {
						$(document)
							.find(
								'.calendar-body .calendar-months div.month[data-month="' +
									month +
									'"]'
							)
							.find(
								'li[data-day="' +
									day +
									'"][data-month="' +
									month +
									'"][data-year="' +
									year +
									'"]:not(.chosen)'
							)
							.addClass('first-selected');
					}

					var first_li = $(document)
						.find(
							'.calendar-body .calendar-months div.month[data-month="' +
								month +
								'"]'
						)
						.find('li.selected.first-selected');
					if (first_li.attr('data-min-nights') == 2) {
						first_li.next().addClass('selected');
					}

					day++;
				}
			} else {
				// When the start date is set and we have to decide if the current hovered day should be colored
				if (
					this.start_flexible_date != undefined &&
					clicked_day.attr('data-full-date') <
						this.start_flexible_date
				) {
					$(document)
						.find(
							'.calendar-body .calendar-months div.month[data-month="' +
								month +
								'"]'
						)
						.find(
							'li[data-day="' +
								day +
								'"][data-month="' +
								month +
								'"][data-year="' +
								year +
								'"]'
						)
						.addClass('selected');
					$(document)
						.find(
							'.calendar-body .calendar-months div.month[data-month="' +
								month +
								'"]'
						)
						.find(
							'li[data-day="' +
								day +
								'"][data-month="' +
								month +
								'"][data-year="' +
								year +
								'"]:not(.chosen)'
						)
						.addClass('first-selected');
				} else {
					$(document)
						.find(
							'#full-availability-popup .modal-content .calendar-body .calendar-months div.month[data-month="' +
								month +
								'"]'
						)
						.find(
							'li[data-day="' +
								day +
								'"][data-month="' +
								month +
								'"][data-year="' +
								year +
								'"]'
						)
						.addClass('selected');

					$(document)
						.find(
							'.calendar-body .calendar-months div.month[data-month="' +
								month +
								'"]'
						)
						.find(
							'li[data-day="' +
								day +
								'"][data-month="' +
								month +
								'"][data-year="' +
								year +
								'"]'
						)
						.addClass('selected');
					if (this.start_flexible_date == undefined) {
						$(document)
							.find(
								'.calendar-body .calendar-months div.month[data-month="' +
									month +
									'"]'
							)
							.find(
								'li[data-day="' +
									day +
									'"][data-month="' +
									month +
									'"][data-year="' +
									year +
									'"]:not(.chosen)'
							)
							.addClass('first-selected');
					} else {
						var last_li = $(document)
							.find(
								'.calendar-body .calendar-months div.month[data-month="' +
									month +
									'"]'
							)
							.find(
								'li[data-day="' +
									day +
									'"][data-month="' +
									month +
									'"][data-year="' +
									year +
									'"]:not(.first-selected)'
							);

						var last_li_full_calendar = $(document)
							.find(
								'#full-availability-popup .calendar-body .calendar-months div.month[data-month="' +
									month +
									'"]'
							)
							.find(
								'li[data-day="' +
									day +
									'"][data-month="' +
									month +
									'"][data-year="' +
									year +
									'"]:not(.first-selected)'
							);

						last_li
							.parent()
							.children()
							.removeClass('chosen');
						var first_li = last_li.parent().find('.first-selected');
						last_li
							.parent()
							.children()
							.slice(first_li.index(), last_li.index())
							.addClass('chosen');

						last_li_full_calendar
							.parent()
							.children()
							.removeClass('chosen');
						var first_li_full_calendar = last_li_full_calendar
							.parent()
							.find('.first-selected');
						last_li_full_calendar
							.parent()
							.children()
							.slice(
								first_li_full_calendar.index(),
								last_li_full_calendar.index()
							)
							.addClass('chosen');

						last_li.addClass('last-selected');
						last_li_full_calendar.addClass('last-selected');
					}
				}
			}
		}
	} else {
		// Fixed sale

		var date_start;
		var date_end;

		if (sale.sale_type == 1) {
			// Package
			// Iterating through sale allocations and find the one we have hovered on, so we can mark the start and end date of the sale allocation
			for (i in sale_allocations) {
				if (
					sale_allocations[i].id ==
					clicked_day.attr('data-sale-allocation-id')
				) {
					date_start = new Date(sale_allocations[i].date_start);
					date_end = new Date(sale_allocations[i].date_end);

					if (sale_allocations[i].outbound_overnight) {
						date_end.setDate(date_end.getDate() + 1);
					}
					if (sale_allocations[i].inbound_overnight) {
						date_end.setDate(date_end.getDate() + 1);
					}
					min_nights = Math.round(
						(date_end - date_start) / (1000 * 60 * 60 * 24)
					);
					clicked_day.attr('data-min-nights', min_nights);
				}
			}
		} else {
			min_nights = clicked_day.attr('data-min-nights');
		}

		// If the min nights setting is set properly for the set allocation we are coloring all days for the fixed sale
		if (min_nights != null) {
			var monthLength = cal_days_in_month[month];
			if (
				parseInt(monthLength) < parseInt(day) + parseInt(min_nights) &&
				!$('div.arrow').length
			) {
				//this.addArrows(this.month, this.year, true);
			}

			for (var i = 0; i <= min_nights; i++) {
				if (month == 1) {
					// February only!
					if ((year % 4 == 0 && year % 100 != 0) || year % 400 == 0) {
						monthLength = 29;
					}
				}

				if (day > monthLength) {
					day = 1;
					month++;
				}

				if (month > 11) {
					month = 0;
					year++;
				}

				if (i == 0) {
					$(document)
						.find(
							'.calendar-body .calendar-months div.month[data-month="' +
								month +
								'"]'
						)
						.find(
							'li[data-day="' +
								day +
								'"][data-month="' +
								month +
								'"][data-year="' +
								year +
								'"]:not(.chosen)'
						)
						.addClass('first-selected');
				}

				if (i == min_nights) {
					$(document)
						.find(
							'.calendar-body .calendar-months div.month[data-month="' +
								month +
								'"]'
						)
						.find(
							'li[data-day="' +
								day +
								'"][data-month="' +
								month +
								'"][data-year="' +
								year +
								'"]:not(.chosen)'
						)
						.addClass('last-selected');
				}

				$(document)
					.find(
						'.calendar-body .calendar-months div.month[data-month="' +
							month +
							'"]'
					)
					.find(
						'li[data-day="' +
							day +
							'"][data-month="' +
							month +
							'"][data-year="' +
							year +
							'"]'
					)
					.addClass('selected');

				day++;
			}
		}
	}
};

// This function is used when the customer clicks on a day in the calendar
Calendar.prototype.dayClick = function(calendar, event) {
	// Get clicked day as a jQuery object
	var clicked_day = $(event.target);

	// Make sure that the clicked element is 'li'
	if (!$(event.target).is('li')) {
		clicked_day = clicked_day.parent();
	}

	if (this.sale_offer.offer_type != 'flexible' && sale.sale_type == 1) {
		// SALE_OFFER != FLEXIBLE AND SALE_TYPE = PACKAGE
		this.clearCalendar();
		var sale_allocation = $(document)
			.find('.calendar-body .calendar-months li.selected')
			.first();

		// If the selected day has valid sale allocation, we are making all hovered elements as chosen, making them constantly colored
		if (sale_allocation.attr('data-sale-allocation-id') != null) {
			$(document)
				.find('.calendar-body .calendar-months li.selected')
				.addClass('chosen');
			$('#SalesView')
				.find('[name="book[sale_allocation_id]"]')
				.val(sale_allocation.attr('data-sale-allocation-id'));
			this.addDateRange();
			this.addSelectionSummary();

			//Check if the date span is outside of current month's range
			if ($('div.arrow.temp').length) {
				this.clearHoverCalendar();
				//this.addArrows(this.month, this.year, false);
			}
		}
	} else {
		// SALE OFFER = FLEXIBLE

		// Checking if the start and end are set properly and clear them if there is any previous selection
		if (
			this.start_flexible_date != undefined &&
			this.end_flexible_date != undefined
		) {
			this.clearCalendar();
			this.start_flexible_date = undefined;
			this.end_flexible_date = undefined;
		} else if (
			$(document)
				.find('.calendar-body .calendar-months li.selected')
				.prev()
				.attr('data-sale-allocation-id') ||
			$(document)
				.find('.calendar-body .calendar-months li.selected')
				.first()
				.attr('data-sale-allocation-id')
		) {
			// When we will set start date of the flexible sale offer

			if (this.start_flexible_date == null) {
				// Setting the start date from the hovered day
				this.start_flexible_date = $(document)
					.find('.calendar-body .calendar-months li.selected')
					.attr('data-full-date');

				// Make marking checkout date available
				$(document)
					.find(
						'.calendar-body .calendar-months li.inactive.checkout-available'
					)
					.removeClass('inactive');

				// Add the start date in the form
				$('#SalesView')
					.find('[name="book[start_flexible_date]"]')
					.val(this.start_flexible_date);

				// Iterate through all hovered days if they are more than 1
				if (
					$(document).find(
						'.calendar-body .calendar-months li.selected'
					).length > 1
				) {
					var day = $(document)
						.find('.calendar-body .calendar-months li.selected')
						.first()
						.attr('data-day');
					var month = $(document)
						.find('.calendar-body .calendar-months li.selected')
						.first()
						.attr('data-month');
					var year = $(document)
						.find('.calendar-body .calendar-months li.selected')
						.first()
						.attr('data-year');

					var monthLength = cal_days_in_month[month];

					var days_diff = daysDiff(
						this.start_flexible_date,
						$(document)
							.find('.calendar-body .calendar-months li.selected')
							.last()
							.attr('data-full-date')
					);

					if (
						parseInt(monthLength) <
							parseInt(day) + parseInt(days_diff) &&
						!$('div.arrow').length
					) {
						//this.addArrows(this.month, this.year, true);
					}

					// This is the place where the iteration happens
					for (var i = 0; i <= days_diff - 1; i++) {
						if (month == 1) {
							// February only!
							if (
								(year % 4 == 0 && year % 100 != 0) ||
								year % 400 == 0
							) {
								monthLength = 29;
							}
						}

						if (day > monthLength) {
							day = 1;
							month++;
						}

						if (month > 11) {
							month = 0;
							year++;
						}

						var dayToSelect = $(document)
							.find(
								'.calendar-body .calendar-months div.month[data-month="' +
									month +
									'"]'
							)
							.find(
								'li[data-day="' +
									day +
									'"][data-month="' +
									month +
									'"][data-year="' +
									year +
									'"]'
							);
						var sale_allocation_id_form_input = $(
							'#SalesView'
						).find('[name="book[sale_allocation_id]"]');

						// Check if the current day to select is valid, so we can mark it as chosen
						if (dayToSelect.attr('data-sale-allocation-id') != '') {
							if (sale_allocation_id_form_input.val() == '') {
								sale_allocation_id_form_input.val(
									dayToSelect.attr('data-sale-allocation-id')
								);
							} else {
								sale_allocation_id_form_input.val(
									sale_allocation_id_form_input.val() +
										',' +
										dayToSelect.attr(
											'data-sale-allocation-id'
										)
								);
							}
						}

						// Marking the selected day as chosen
						dayToSelect.addClass('chosen');

						// Add class first-selected to the first day of the selection
						if (i == 0) {
							$(document)
								.find(
									'.calendar-body .calendar-months div.month[data-month="' +
										month +
										'"]'
								)
								.find(
									'li[data-day="' +
										day +
										'"][data-month="' +
										month +
										'"][data-year="' +
										year +
										'"]'
								)
								.addClass('first-selected');
						}

						day++;
					}

					// This will select 2 nights when the selected day is Friday and the offer has a condition to choose min 2 nights when selectiing Friday
					min_nights = clicked_day.attr('data-min-nights');

					$('#SalesView')
						.find('[name="book[end_flexible_date]"]')
						.val(
							$(document)
								.find(
									'.calendar-body .calendar-months li.selected'
								)
								.last()
								.attr('data-full-date')
						);

					// Set end date for the flexible sale offer, if not set
					if (
						this.end_flexible_date == null &&
						$(document).find(
							'.calendar-body .calendar-months li.last-selected'
						).length > 0
					) {
						this.end_flexible_date = $(document)
							.find('.calendar-body .calendar-months li.selected')
							.last()
							.attr('data-full-date');
					}
				}
			} else if (
				this.end_flexible_date == null &&
				this.start_flexible_date <
					$(document)
						.find('.calendar-body .calendar-months li.selected')
						.attr('data-full-date')
			) {
				// Set end flexible date
				this.end_flexible_date = $(document)
					.find('.calendar-body .calendar-months li.selected')
					.attr('data-full-date');

				var day = $(document)
					.find('.calendar-body .calendar-months li.chosen')
					.first()
					.attr('data-day');
				var month = $(document)
					.find('.calendar-body .calendar-months li.chosen')
					.first()
					.attr('data-month');
				var year = $(document)
					.find('.calendar-body .calendar-months li.chosen')
					.first()
					.attr('data-year');

				var monthLength = cal_days_in_month[month];

				var days_diff = daysDiff(
					this.start_flexible_date,
					this.end_flexible_date
				);

				if (
					parseInt(monthLength) <
						parseInt(day) + parseInt(days_diff) &&
					!$('div.arrow').length
				) {
					//this.addArrows(this.month, this.year, true);
				}

				// This is the place where the iteration happens
				for (var i = 0; i <= days_diff - 1; i++) {
					if (month == 1) {
						// February only!
						if (
							(year % 4 == 0 && year % 100 != 0) ||
							year % 400 == 0
						) {
							monthLength = 29;
						}
					}

					if (day > monthLength) {
						day = 1;
						month++;
					}

					if (month > 11) {
						month = 0;
						year++;
					}

					var dayToSelect = $(document)
						.find(
							'.calendar-body .calendar-months div.month[data-month="' +
								month +
								'"]'
						)
						.find(
							'li[data-day="' +
								day +
								'"][data-month="' +
								month +
								'"][data-year="' +
								year +
								'"]'
						);
					var sale_allocation_id_form_input = $('#SalesView').find(
						'[name="book[sale_allocation_id]"]'
					);

					// Check if the current day to select is valid, so we can mark it as chosen
					if (dayToSelect.attr('data-sale-allocation-id') != '') {
						if (sale_allocation_id_form_input.val() == '') {
							sale_allocation_id_form_input.val(
								dayToSelect.attr('data-sale-allocation-id')
							);
						} else {
							sale_allocation_id_form_input.val(
								sale_allocation_id_form_input.val() +
									',' +
									dayToSelect.attr('data-sale-allocation-id')
							);
						}
					}

					// Marking the selected day as chosen
					dayToSelect.addClass('chosen');

					day++;
				}

				// Set start and end date in the form
				$('#SalesView')
					.find('[name="book[start_flexible_date]"]')
					.val(this.start_flexible_date);
				$('#SalesView')
					.find('[name="book[end_flexible_date]"]')
					.val(this.end_flexible_date);

				// // Close the Full Availability popup, if opened
				// $('#full-availability-popup').closeModal();
			} else {
				// Set start date if not set
				this.clearCalendar();
				this.start_flexible_date = $(document)
					.find('.calendar-body .calendar-months li.selected')
					.attr('data-full-date');
				$(document)
					.find(
						'.calendar-body .calendar-months li.inactive.checkout-available'
					)
					.removeClass('inactive');
				$('#SalesView')
					.find('[name="book[start_flexible_date]"]')
					.val(this.start_flexible_date);
				this.end_flexible_date = null;
			}

			// Mark all `selected` days as `chosen`
			$(document)
				.find('.calendar-body .calendar-months li.selected')
				.addClass('chosen');
			this.addDateRange();
			this.addSelectionSummary();
		}
	}

	if (
		this.start_flexible_date != undefined &&
		clicked_day.attr('data-full-date') < this.start_flexible_date &&
		clicked_day.attr('data-sale-allocation-id') == undefined
	) {
		this.clearCalendar();
	}

	// $('#full-availability-popup').closeModal();
};
function createCookie(name, value, days) {
	var expires;
	if (days) {
		var date = new Date();
		date.setTime(date.getTime() + days * 24 * 60 * 60 * 1000);
		expires = '; expires=' + date.toGMTString();
	} else {
		expires = '';
	}
	document.cookie = name + '=' + value + expires + '; path=/';
}
// Shows selected date range below the calendar
Calendar.prototype.addDateRange = function() {
	var element = $('.date-range');
	var html = '';
	var startDate = $('.calendar-body .calendar-months li.chosen')
		.first()
		.attr('data-full-date');
	var endDate = $('.calendar-body .calendar-months li.chosen')
		.last()
		.attr('data-full-date');
	var startDateAllocation = $(
		'.calendar-body .calendar-months li.chosen'
	).first();
	var endDateAllocation = $(
		'.calendar-body .calendar-months li.chosen'
	).last();
	var clickedOfferId = $('.card-panel.opened')
		.parent()
		.attr('data-offer-id');
	var currentSaleId = $('input[name="book[sale_id]"]').val();
	var saleAlocationIds = [];
	$(document)
		.find('.booking-calendar .calendar-body .calendar-months li.chosen')
		.each(function(index, value) {
			var sale_allocation_id = $(this).attr('data-sale-allocation-id');

			for (i in sale_allocations) {
				if (
					sale_allocations[i].id == sale_allocation_id &&
					sale_allocations[i].id !==
						endDateAllocation.attr('data-sale-allocation-id')
				) {
					saleAlocationIds.push(sale_allocations[i].id);
				}
			}
		});

	booking_dates = {
		date_start: startDate,
		date_end: endDate,
		sale_allocation_ids: saleAlocationIds,
		offer_id: clickedOfferId,
		sale_id: currentSaleId,
	};
	startDate = new Date(startDate);
	endDate = new Date(endDate);

	this.startDay = {
		year: 1900 + startDate.getYear(),
		month: startDate.getMonth(),
		day: startDate.getDate(),
	};

	this.endDay = {
		year: 1900 + endDate.getYear(),
		month: endDate.getMonth(),
		day: endDate.getDate(),
	};

	createCookie('booking_dates', JSON.stringify(booking_dates), 1);
	html += '<i class="material-icons">date_range</i>';
	html +=
		'&nbsp;<span class="range-date-start">' +
		startDate.toDateString() +
		'</span>';
	html += '&nbsp;<i class="material-icons">trending_flat</i>';
	html += '&nbsp;<i class="material-icons">date_range</i>';
	html +=
		'&nbsp;<span class="range-date-end">' +
		endDate.toDateString() +
		'</span>';

	element.html(html);
};

// Show summary of the selection, marked nights and total price
Calendar.prototype.addSelectionSummary = function() {
	var element = $('.calendar-body .summary');
	var startDate = $('.calendar-body .calendar-months li.chosen').first();
	var rooms = $(
		'#offers .offer[data-offer-id="' +
			this.sale_offer.id +
			'"] select[name="rooms"]'
	).val();
	var html = '';

	if (
		(this.sale_offer.offer_type != 'flexible' ||
			this.sale_offer.offer_type != '1') &&
		sale.sale_type == 1
	) {
		for (i in sale_allocations) {
			if (
				sale_allocations[i].id ==
				startDate.attr('data-sale-allocation-id')
			) {
				if (
					startDate.attr('data-min-nights') !=
					sale_allocations[i].min_nights
				) {
					element
						.find('.nights')
						.html(startDate.attr('data-min-nights'));
				} else {
					element
						.find('.nights')
						.html(sale_allocations[i].min_nights);
				}

				if (sale.sale_type == 1) {
					element
						.find('.total-price')
						.html(
							formatCurrency(
								sale_allocations[i].rate * parseInt(rooms),
								this
							)
						);
				} else {
					element
						.find('.total-price')
						.html(
							formatCurrency(
								sale_allocations[i].rate *
									sale_allocations[i].min_nights *
									parseInt(rooms),
								this
							)
						);
				}

				if (
					this.sale_offer.deposit &&
					sale_allocations[i].total_deposit
				) {
					$('.summary-price')
						.removeClass('s6')
						.addClass('s4');
					$('.summary-nights')
						.removeClass('s6')
						.addClass('s4');
					$('.summary-deposit').removeClass('hide');

					$('.total-deposit').html(
						formatCurrency(
							sale_allocations[i].total_deposit * parseInt(rooms),
							this
						)
					);
				} else {
					$('.summary-price')
						.removeClass('s4')
						.addClass('s6');
					$('.summary-nights')
						.removeClass('s4')
						.addClass('s6');
					$('.summary-deposit').addClass('hide');
				}

				element.show(0);
				break;
			}
		}

		$('#SalesView')
			.find('[name="book[min_nights_validate]"]')
			.val(0);
	} else {
		var nights =
			$(document).find(
				'.booking-calendar .calendar-body .calendar-months li.chosen'
			).length - 1;
		if (
			$(document).find(
				'.booking-calendar .calendar-body .calendar-months li.chosen'
			).length == 0
		) {
			nights = 0;
		}
		var min_nights = 1;
		element.find('.nights').html(nights);
		var price = 0;

		$(document)
			.find('.booking-calendar .calendar-body .calendar-months li.chosen')
			.each(function(index, value) {
				if (index == nights) return;

				var sale_allocation_id = $(this).attr(
					'data-sale-allocation-id'
				);

				for (i in sale_allocations) {
					if (sale_allocations[i].id == sale_allocation_id) {
						if (index == 0) {
							min_nights = sale_allocations[i].min_nights;
						}

						price += parseInt(sale_allocations[i].rate);
						return;
					}
				}
			});

		if (min_nights > nights) {
			$('#SalesView')
				.find('[name="book[min_nights_validate]"]')
				.val(min_nights);
		} else {
			$('#SalesView')
				.find('[name="book[min_nights_validate]"]')
				.val(0);
		}

		element
			.find('.total-price')
			.html(formatCurrency(price * parseInt(rooms), this));
		element.show(0);

		if (
			nights !== 0 &&
			($('.calendar-body .calendar-months li.chosen').attr(
				'data-min-nights'
			) < 2 ||
				$('.calendar-body .calendar-months li.last-selected').attr(
					'data-min-nights'
				) >= 1)
		) {
			$('#full-availability-popup').hide(0);
			$('.modal-overlay').click();
		}
		//$('.calendar-body .clear-range').show(0);
	}
};

// Removes all `selected` classes and prepares the calendar of further selections
Calendar.prototype.clearHoverCalendar = function() {
	$(document)
		.find(
			'.calendar-body .calendar-months li.selected.first-selected:not(.chosen)'
		)
		.removeClass('first-selected');
	$(document)
		.find(
			'.calendar-body .calendar-months li.selected.last-selected:not(.chosen)'
		)
		.removeClass('last-selected');
	$(document)
		.find('.calendar-body .calendar-months li.selected')
		.removeClass('selected');
	$(document)
		.find('div.arrow.temp')
		.remove();
};

// Removes all `selected`, `chosen` classes, summaries, date ranges and prepares the calendar of further selections
Calendar.prototype.clearCalendar = function() {
	this.start_flexible_date = undefined;
	this.end_flexible_date = undefined;
	$(document)
		.find('.calendar-body .calendar-months li.checkout-available')
		.addClass('inactive');
	$(document)
		.find('.calendar-body .calendar-months li.chosen')
		.removeClass('selected')
		.removeClass('chosen')
		.removeClass('first-selected')
		.removeClass('last-selected');
	$('#SalesView')
		.find('[name="book[sale_allocation_id]"]')
		.val('');
	$('#SalesView')
		.find('[name="book[start_flexible_date]"]')
		.val('');
	$('#SalesView')
		.find('[name="book[end_flexible_date]"]')
		.val('');
	$('div.arrow:not(.temp)').remove();
	$('.date-range').html('');
	$('.calendar-body .summary').hide(0);
	//$('.calendar-body .clear-range').hide(0);
};

// Adds leading zero to a number
function padNumber(number_, width) {
	var num = number_.toString();

	if (num.length >= width) {
		return num;
	} else {
		var newNum = '';

		for (var i = 0; i < width - num.length; i++) {
			newNum += '0';
		}

		newNum += num;

		return newNum;
	}
}

// Months difference between 2 dates
function monthDiff(d1, d2) {
	var months;
	months = (d2.getFullYear() - d1.getFullYear()) * 12;
	months -= d1.getMonth() + 1;
	months += d2.getMonth();
	return months <= 0 ? 0 : months;
}

// Days difference between 2 dates
function daysDiff(d1, d2) {
	var oneDay = 24 * 60 * 60 * 1000; // hours*minutes*seconds*milliseconds
	var firstDate = new Date(d1);
	var secondDate = new Date(d2);
	return Math.round(
		Math.abs((firstDate.getTime() - secondDate.getTime()) / oneDay)
	);
}

// Format price based on the currency rate
function formatCurrency(value, calendar) {
	var result = value;
	switch (calendar.currency) {
		case 'EUR':
			result =
				'€' + Math.ceil((value * calendar.currency_rate).toFixed(2));
			break;
		case 'BGN':
			result =
				Math.ceil((value * calendar.currency_rate).toFixed(2)) + ' лв';
			break;
		case 'TRY':
			result =
				Math.ceil((value * calendar.currency_rate).toFixed(2)) + ' ₺';
			break;
	}

	return result;
}
