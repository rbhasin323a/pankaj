function formatPrice(currency_id, amount, enable_format, response_callback) {
	$.ajax({
		url: '/format-price',
		type: 'GET',
		data: {
			amount: amount,
			currency_id: currency_id,
			enable_format: enable_format,
		},
		dataType: 'json',
		success: function(response) {
			if (response_callback) response_callback(response);
		},
	});
}

function AJAXForm(form_id) {
	if (typeof form_id !== 'string') {
		return false;
	}

	$(document).on('submit', form_id, function(event) {
		event.preventDefault();

		var $form = $(this);

		if (!$form.hasClass('locked')) {
			$.ajax({
				url: $form.attr('action'),
				type: 'POST',
				data: $form.serializeArray(),
			}).done(function(response) {
				if (
					typeof response !== 'undefined' &&
					typeof response === 'object'
				) {
					for (var prop in response) {
						if (
							response.hasOwnProperty(prop) &&
							response[prop] !== null
						) {
							if (prop === 'data') {
								if (
									typeof response[prop].redirect === 'string'
								) {
									window.location = response[prop].redirect;
								}
							}

							if (prop === 'message') {
								if (typeof response[prop] === 'object') {
									var message = response[prop];

									for (var type in message) {
										if (
											message.hasOwnProperty(type) &&
											message[type] !== null
										) {
											if (
												typeof message[type] ===
												'string'
											) {
												alert(message[type]);
											}
										}
									}
								}
							}
						}
					}
				}
			});
		}
	});
}

(function($) {
	$('select')
		.not('.disabled')
		.material_select();

	$(document).on('click', '.alert-dismissable .close', function() {
		$(this)
			.parents('.alert')
			.remove();
	});

	$(document).on('ready', function(event) {
		$('ul.tabs').tabs();
	});

	$(window).on('load', function(event) {
		$page = $('.page');
		$page_form = $('form', $page);
		$page_heading = $('.page-heading', $page);

		if (
			typeof $page_form !== 'undefined' &&
			typeof $page_heading !== 'undefined'
		) {
			if ($page_form.length == 1 && $page_heading.length == 1) {
				if ($(window).width() <= 768) {
					$('html, body').animate(
						{
							scrollTop: $page_heading.offset().top - 50,
						},
						1000
					);
				}
			}
		}
	});
})(jQuery);
