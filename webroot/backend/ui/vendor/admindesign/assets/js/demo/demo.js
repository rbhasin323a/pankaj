"use strict";
var Demo = function() {
    var a = function() {
            $(".themed-form").on("submit", function(a) {
                return $("body.timeline-page").length || $("body.admin-validation-page").length ? void 0 : (a.preventDefault, alert("Your form has submitted!"), !1)
            });
            var a = $(".fileupload-preview");
            a.length && a.each(function(a, b) {
                var c = $(b).parents(".fileupload").find(".btn-file > input");
                $(b).on("click", function() {
                    c.click()
                })
            })
        },
        b = function() {
            $("#topbar-multiple").length && $("#topbar-multiple").multiselect({
                buttonClass: "btn btn-default btn-sm ph15",
                dropRight: !0
            })
        },
        c = function() {
            var a = $(".bs-component");
            if (a.length) {
                $.ajaxSetup({
                    cache: !0
                }), $("<link/>", {
                    rel: "stylesheet",
                    type: "text/css",
                    href: "/backend/ui/vendor/admindesign/vendor/plugins/highlight/styles/github.css"
                }).appendTo("head"), $.getScript("/backend/ui/vendor/admindesign/vendor/plugins/highlight/highlight.pack.js");
                var b = '<div class="modal fade" id="source-modal" tabindex="-1" role="dialog">  <div class="modal-dialog modal-lg"> <div class="modal-content"> <div class="modal-header"> <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button> <h4 class="modal-title" id="myModalLabel">Source Code HTML</h4> </div> <div class="modal-body"> <div class="highlight"> <pre> <code class="language-html" data-lang="html"></code> </pre> </div> </div> <div class="modal-footer"> <button type="button" class="btn btn-primary btn-clipboard">Highlight Source</button> </div> </div> </div> </div> </div>';
                $(b).appendTo("body");
                var c = $("<div id='source-button' class='btn btn-primary btn-xs'>&lt; &gt;</div>");
                c.click(function() {
                    var a = $(this).parent().html();
                    a = d(a), $("#source-modal pre").text(a), $("#source-modal").modal();
                    var b = $("#source-modal").find("pre");
                    setTimeout(function() {
                        b.each(function(a, b) {
                            hljs.highlightBlock(b)
                        })
                    }, 250), $(".btn-clipboard").on("click", function() {
                        var a = $(this).parents(".modal-dialog").find("pre");
                        a.selectText()
                    }), $(document).keypress(function(a) {
                        99 == a.which && (console.log("go"), $(".btn-clipboard").click())
                    })
                }), a.hover(function() {
                    $(this).append(c), c.show()
                }, function() {
                    c.hide()
                });
                var d = function(a) {
                    var b = a.split(/\n/);
                    b.shift(), b.splice(-1, 1);
                    var c = b[0].length - b[0].trim().length,
                        d = new RegExp(" {" + c + "}");
                    return b = b.map(function(a) {
                        return a.match(d) && (a = a.substring(c)), a
                    }), b = b.join("\n")
                };
                jQuery.fn.selectText = function() {
                    var a, b, c = document,
                        d = this[0];
                    c.body.createTextRange ? (a = document.body.createTextRange(), a.moveToElementText(d), a.select()) : window.getSelection && (b = window.getSelection(), a = document.createRange(), a.selectNodeContents(d), b.removeAllRanges(), b.addRange(a))
                }
            }
        },
        d = function() {
            if ($("#skin-toolbox").length) {
                $("#skin-toolbox .panel-heading").on("click", function() {
                    $("#skin-toolbox").toggleClass("toolbox-open")
                }), $("#skin-toolbox .panel-heading").disableSelection();
                var a = $("body"),
                    b = $("#topbar"),
                    c = $("#sidebar_left"),
                    d = $(".navbar"),
                    e = d.children(".navbar-branding"),
                    f = "bg-primary bg-success bg-info bg-warning bg-danger bg-alert bg-system bg-dark",
                    g = "sidebar-light light dark",
                    h = {
                        headerSkin: "",
                        sidebarSkin: "sidebar-default",
                        headerState: "navbar-fixed-top",
                        sidebarState: "affix",
                        sidebarAlign: "",
                        breadcrumbState: "relative",
                        breadcrumbHidden: "visible"
                    },
                    i = "admin-settings1",
                    j = localStorage.getItem(i);
                null === j && (localStorage.setItem(i, JSON.stringify(h)), j = localStorage.getItem(i)),
                    function() {
                        var i = JSON.parse(j);
                        h = i, $.each(i, function(h, i) {
                            switch (h) {
                                case "headerSkin":
                                    d.removeClass(f).addClass(i), e.removeClass(f).addClass(i + " dark"), "bg-light" === i ? e.removeClass(f) : e.removeClass(f).addClass(i), $('#toolbox-header-skin input[value="bg-light"]').prop("checked", !1), $('#toolbox-header-skin input[value="' + i + '"]').prop("checked", !0);
                                    break;
                                case "sidebarSkin":
                                    c.removeClass(g).addClass(i), $('#toolbox-sidebar-skin input[value="bg-light"]').prop("checked", !1), $('#toolbox-sidebar-skin input[value="' + i + '"]').prop("checked", !0);
                                    break;
                                case "headerState":
                                    "navbar-fixed-top" === i ? (d.addClass("navbar-fixed-top"), $("#header-option").prop("checked", !0)) : (d.removeClass("navbar-fixed-top"), $("#header-option").prop("checked", !1), c.nanoScroller({
                                        destroy: !0
                                    }), c.find(".nano-content").attr("style", ""), c.removeClass("affix"), $("#sidebar-option").prop("checked", !1));
                                    break;
                                case "sidebarState":
                                    "affix" === i ? (c.addClass("affix"), $("#sidebar-option").prop("checked", !0)) : (c.nanoScroller({
                                        destroy: !0
                                    }), c.find(".nano-content").attr("style", ""), c.removeClass("affix"), $("#sidebar-option").prop("checked", !1));
                                    break;
                                case "sidebarAlign":
                                    "sb-top" === i ? (a.addClass("sb-top"), $("#sidebar-align").prop("checked", !0)) : (a.removeClass("sb-top"), $("#sidebar-align").prop("checked", !1));
                                    break;
                                case "breadcrumbState":
                                    "affix" === i ? (b.addClass("affix"), $("#breadcrumb-option").prop("checked", !0)) : (b.removeClass("affix"), $("#breadcrumb-option").prop("checked", !1));
                                    break;
                                case "breadcrumbHidden":
                                    b.hasClass("hidden") ? $("#breadcrumb-hidden").prop("checked", !0) : "hidden" === i ? (b.addClass("hidden"), $("#breadcrumb-hidden").prop("checked", !0)) : (b.removeClass("hidden"), $("#breadcrumb-hidden").prop("checked", !1))
                            }
                        })
                    }(), $("#toolbox-header-skin input").on("click", function() {
                        var a = $(this),
                            b = a.val();
                        a.attr("id"), d.removeClass(f).addClass(b), e.removeClass(f).addClass(b + " dark"), h.headerSkin = b, localStorage.setItem(i, JSON.stringify(h))
                    }), $("#toolbox-sidebar-skin input").on("click", function() {
                        var a = $(this).val();
                        c.removeClass(g).addClass(a), h.sidebarSkin = a, localStorage.setItem(i, JSON.stringify(h))
                    }), $("#header-option").on("click", function() {
                        var a = "navbar-fixed-top";
                        d.hasClass("navbar-fixed-top") ? (d.removeClass("navbar-fixed-top"), a = "relative", c.removeClass("affix"), c.nanoScroller({
                            destroy: !0
                        }), c.find(".nano-content").attr("style", ""), c.removeClass("affix"), $("#sidebar-option").prop("checked", !1), $("#sidebar-option").parent(".checkbox-custom").addClass("checkbox-disabled").end().prop("checked", !1).attr("disabled", !0), h.sidebarState = "", localStorage.setItem(i, JSON.stringify(h)), b.removeClass("affix"), $("#breadcrumb-option").parent(".checkbox-custom").addClass("checkbox-disabled").end().prop("checked", !1).attr("disabled", !0), h.breadcrumbState = "", localStorage.setItem(i, JSON.stringify(h))) : (d.addClass("navbar-fixed-top"), a = "navbar-fixed-top", $("#sidebar-option").parent(".checkbox-custom").removeClass("checkbox-disabled").end().attr("disabled", !1), $("#breadcrumb-option").parent(".checkbox-custom").removeClass("checkbox-disabled").end().attr("disabled", !1)), h.headerState = a, localStorage.setItem(i, JSON.stringify(h))
                    }), $("#sidebar-option").on("click", function() {
                        var a = "";
                        c.hasClass("affix") ? (c.nanoScroller({
                            destroy: !0
                        }), c.find(".nano-content").attr("style", ""), c.removeClass("affix"), a = "") : (c.addClass("affix"), $(".nano.affix").length && $(".nano.affix").nanoScroller({
                            preventPageScrolling: !0
                        }), a = "affix"), $(window).trigger("resize"), h.sidebarState = a, localStorage.setItem(i, JSON.stringify(h))
                    }), $("#sidebar-align").on("click", function() {
                        var b = "";
                        a.hasClass("sb-top") ? (a.removeClass("sb-top"), b = "") : (a.removeClass("sb-top"), b = "sb-top"), h.sidebarAlign = b, localStorage.setItem(i, JSON.stringify(h))
                    }), $("#breadcrumb-option").on("click", function() {
                        var a = "";
                        b.hasClass("affix") ? (b.removeClass("affix"), a = "") : (b.addClass("affix"), a = "affix"), h.breadcrumbState = a, localStorage.setItem(i, JSON.stringify(h))
                    }), $("#breadcrumb-hidden").on("click", function() {
                        var a = "";
                        b.hasClass("hidden") ? (b.removeClass("hidden"), a = "") : (b.addClass("hidden"), a = "hidden"), h.breadcrumbHidden = a, localStorage.setItem(i, JSON.stringify(h))
                    }), $("#clearLocalStorage").on("click", function() {
                        bootbox.confirm && bootbox.confirm("Are You Sure?!", function(a) {
                            a && setTimeout(function() {
                                localStorage.clear(), location.reload()
                            }, 200)
                        })
                    })
            }
        },
        e = function() {
            var a = $("html"),
                b = window.navigator.userAgent,
                c = b.indexOf("MSIE "),
                d = b.indexOf("Trident/");
            (c > -1 || d > -1) && (a = $("body"));
            var e = $.fullscreen.isNativelySupported();
            $(".request-fullscreen").on("click", function() {
                e ? $.fullscreen.isFullScreen() ? $.fullscreen.exit() : a.fullscreen({
                    overflow: "auto"
                }) : alert("Your browser does not support fullscreen mode.")
            })
        };
    return {
        init: function() {
            a(), b(), c(), d(), e()
        }
    }
}();