$(document).ready(function() {
	$('form').on('focus', 'input[type="number"]', function(e) {
		$(this).on('mousewheel.disableScroll', function(e) {
			e.preventDefault();
		});
	});

	$('form').on('blur', 'input[type="number"]', function(e) {
		$(this).off('mousewheel.disableScroll');
	});

	$('input[name=deposit]').on('change', function() {
		if ($(this).is(':checked')) {
			$('.total_deposit, .single_deposit, .infant_deposit, .child_deposit').parent().show();
		} else {
			$('.total_deposit, .single_deposit, .infant_deposit, .child_deposit').parent().hide();
		}
	});


	
	$("#toggle_sidemenu_l, .sidebar-toggle-mini").on('click', function() {
		if (localStorage.getItem('showSidebar')) {
			localStorage.removeItem('showSidebar');
		}
		if ($('body').hasClass('sb-l-m')) {
			$('.birthday-notifications').css('display', 'inline-block');
			document.cookie = 'showSidebar=true';
		} else {
			$('.birthday-notifications').css('display', 'none');
			document.cookie = 'showSidebar=false';
		}
	});

	if (localStorage.getItem('showSidebar') == "false") {
		$('body').addClass('sb-l-m sb-l-disable-animation');
		$('.birthday-notifications').hide();
	}
});