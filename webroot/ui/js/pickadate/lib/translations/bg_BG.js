// Bulgarian

jQuery.extend( jQuery.fn.pickadate.defaults, {
    monthsFull: [ 'януари','февруари','март','април','май','юни','юли','август','септември','октомври','ноември','декември' ],
    monthsShort: [ 'янр','фев','мар','апр','май','юни','юли','авг','сеп','окт','ное','дек' ],
    weekdaysFull: [ 'неделя', 'понеделник', 'вторник', 'сряда', 'четвъртък', 'петък', 'събота' ],
    weekdaysShort: [ 'нд', 'пн', 'вт', 'ср', 'чт', 'пт', 'сб' ],
    today: 'днес',
    clear: 'изчисти',
	close: 'затвори',
    firstDay: 1,
    format: 'd mmmm yyyy г.',
    formatSubmit: 'yyyy/mm/dd'
});

if (typeof jQuery.fn.pickatime !== "undefined") {
	jQuery.extend( jQuery.fn.pickatime.defaults, {
	    clear: 'изчисти'
	});
}
