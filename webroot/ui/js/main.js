function formatPrice(currency_id, amount, enable_format, response_callback) {
	var result = false;

	$.ajax({
		url: '/format-price',
		type: 'GET',
		data: {
			amount: amount,
			currency_id: currency_id,
			enable_format: enable_format
		},
		dataType: 'json',
		success: function(response) {
			if (response_callback) response_callback(response);
		}
	});
}

(function($) {
	/* Font Loading */
	var font = new FontFaceObserver('Roboto');

	font.load().then(
		function() {
			$('html').addClass('roboto-loaded');
		},
		function() {
			console.log('Roboto is not available');
		}
	);

	/* Materialize */
	$('select')
		.not('.disabled')
		.material_select();

	$(document).on('click', '.alert-dismissable .close', function() {
		$(this)
			.parents('.alert')
			.remove();
	});

	$(document).ready(function() {
		$('ul.tabs').tabs();
	});

	$(window).on('load', function() {
		retinajs($('img[data-rjs]'));
	});
})(jQuery);
