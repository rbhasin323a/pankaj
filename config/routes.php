<?php

use Cake\Routing\RouteBuilder;
use Cake\Routing\Router;

/**
 * The default class to use for all routes
 *
 * The following route classes are supplied with CakePHP and are appropriate
 * to set as the default:
 *
 * - Route
 * - InflectedRoute
 * - DashedRoute
 *
 * If no call is made to `Router::defaultRouteClass()`, the class used is
 * `Route` (`Cake\Routing\Route\Route`)
 *
 * Note that `Route` does not do any inflections on URLs which will result in
 * inconsistently cased URLs when used with `:plugin`, `:controller` and
 * `:action` markers.
 *
 */
Router::defaultRouteClass('DashedRoute');

Router::addUrlFilter(function ($params, $request) {

	if (isset($request->params['language']) && !isset($params['language'])) {
		$params['lang'] = $request->params['lang'];
	} elseif (!isset($params['lang'])) {
		$params['lang'] = 'en'; // set your default language here
	}
	if (isset($_COOKIE['language'])) {
		$params['lang'] = $_COOKIE['language'] == 'gb' ? 'en' : $_COOKIE['language'];
	} else {
		$params['lang'] = 'en';
		if (isset($_COOKIE['autolanguage'])) {
			$params['lang'] = $_COOKIE['autolanguage'] == 'gb' ? 'en' : $_COOKIE['autolanguage'];
		}
	}

	return $params;
});

/**
 * Main Scope
 */
Router::scope('/', function (RouteBuilder $routes) {
	/**
	 * API
	 */
	Router::prefix('api', function ($routes) {
		$routes->connect('/*', ['prefix' => 'api', 'controller' => 'Api', 'action' => 'index']);
		$routes->fallbacks('DashedRoute');
	});

	/**
	 * CMS
	 */
	if (!empty($_SERVER['SERVER_NAME']) && strpos($_SERVER['SERVER_NAME'], 'cms.') !== false) {
		$routes->connect('/', ['prefix' => 'admin', 'controller' => 'Admin', 'action' => 'index']);
		$routes->connect('/booking_travellers/edit/:booking_id', ['prefix' => 'admin', 'controller' => 'BookingTravellers', 'action' => 'edit']);

		Router::prefix('admin', function ($routes) {
			$routes->connect('/', ['controller' => 'Admin', 'action' => 'index']);
			$routes->connect('/booking_travellers/edit/:booking_id', ['controller' => 'BookingTravellers', 'action' => 'edit']);
			$routes->fallbacks('DashedRoute');
		});
	}

	/**
	 * Language scope /:lang
	 */
	$languagePrefixes = array('/en', '/bg', '/tr', '');

	foreach ($languagePrefixes as $prefix) {
		Router::scope($prefix, function ($routes) {
			$routes->connect('/', ['controller' => 'Sales', 'action' => 'index']);

			/**
			 * Main Routes
			 */
			$routes->connect('/account', ['controller' => 'Account', 'action' => 'index']);
			$routes->connect('/change-password', ['plugin' => 'CakeDC/Users', 'controller' => 'Users', 'action' => 'changePassword']);
			$routes->connect('/forgotten', ['plugin' => 'CakeDC/Users', 'controller' => 'Users', 'action' => 'requestResetPassword']);
			$routes->connect('/login', ['plugin' => 'CakeDC/Users', 'controller' => 'Users', 'action' => 'login']);
			$routes->connect('/logout', ['plugin' => 'CakeDC/Users', 'controller' => 'Users', 'action' => 'logout']);
			$routes->connect('/pages/*', ['controller' => 'Pages', 'action' => 'display']);
			$routes->connect('/register', ['plugin' => 'CakeDC/Users', 'controller' => 'Users', 'action' => 'register']);
			$routes->connect('/resend-validation', ['plugin' => 'CakeDC/Users', 'controller' => 'Users', 'action' => 'resendTokenValidation']);
			$routes->connect('/reset-password', ['plugin' => 'CakeDC/Users', 'controller' => 'Users', 'action' => 'resetPassword']);
			$routes->connect('/validate', ['plugin' => 'CakeDC/Users', 'controller' => 'Users', 'action' => 'validateEmail']);

			/**
			 * Sales
			 */
			$routes->connect('/latest-sales', ['controller' => 'Sales', 'action' => 'index']);
			$routes->connect('/sales/success', ['controller' => 'Sales', 'action' => 'success']);
			$routes->connect('/sales/getSaleAllocations', ['controller' => 'Sales', 'action' => 'getSaleAllocations']);
			$routes->connect('/sales/:slug', ['controller' => 'Sales', 'action' => 'view']);
			$routes->connect('/sale/:slug', ['controller' => 'Sales', 'action' => 'view']);
			$routes->connect('/bansko', ['controller' => 'Sales', 'action' => 'view', 'id' => '0JGCYCLIHP0K']);

			/**
			 * Maldopay
			 */
			$routes->connect('/landing/pending', ['controller' => 'Landing', 'action' => 'pending']);
			$routes->connect('/landing/success', ['controller' => 'Landing', 'action' => 'success']);
			$routes->connect('/landing/canceled', ['controller' => 'Landing', 'action' => 'canceled']);
			$routes->connect('/landing/declined', ['controller' => 'Landing', 'action' => 'declined']);
			$routes->connect('/maldocallback', ['controller' => 'Maldopay', 'action' => 'index']);

			/**
			 * i18n
			 */
			$routes->connect('/en', ['controller' => 'Language', 'action' => 'index', ['lang' => 'gb']]);
			$routes->connect('/bg', ['controller' => 'Language', 'action' => 'index', ['lang' => 'bg']]);
			$routes->connect('/tr', ['controller' => 'Language', 'action' => 'index', ['lang' => 'tr']]);
			$routes->connect('/bgn', ['controller' => 'Currency', 'action' => 'index', ['currency' => 'bgn']]);
			$routes->connect('/eur', ['controller' => 'Currency', 'action' => 'index', ['currency' => 'eur']]);
			$routes->connect('/try', ['controller' => 'Currency', 'action' => 'index', ['currency' => 'try']]);

			/**
			 * Tooling
			 */
			$routes->connect('/language', ['controller' => 'Language', 'action' => 'index']);
			$routes->connect('/format-price', ['controller' => 'App', 'action' => 'formatPrice']);
			$routes->connect('/currency', ['controller' => 'Currency', 'action' => 'index']);
		});
	}

	$languagePrefixes = array_reverse($languagePrefixes);

	foreach ($languagePrefixes as $prefix) {
		Router::scope($prefix, function ($routes) {
			$routes->fallbacks('DashedRoute');
		});
	}
});
