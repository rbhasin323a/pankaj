var concat = require('gulp-concat');
var csso = require('gulp-csso');
var fs = require('fs');
var gulp = require('gulp');
var imagemin = require('gulp-imagemin');
var imageResize = require('gulp-image-resize');
var less = require('gulp-less');
var realFavicon = require('gulp-real-favicon');
var rev = require('gulp-rev');
var sass = require('gulp-sass');
var svgmin = require('gulp-svgmin');
var uglify = require('gulp-uglify');

var Brand = {
	name: 'Luxury Discounts',
	url: 'https://www.luxury-discounts.com/',
	colors: {
		bg_dark: '#030408'
	}
};

var AssetPath = './webroot/assets';
var FilesPath = './webroot/files';
var UiPath = './webroot/ui';

var Source = {
	watch: {
		less: [AssetPath + '/src/less/**/*.less'],
		sass: [AssetPath + '/src/sass/**/*.scss'],
		js: [AssetPath + '/src/js/**/*.js']
	},
	compile: {
		less: [AssetPath + '/src/less/website-less.less'],
		sass: [AssetPath + '/src/sass/website-sass.scss']
	},
	min: {
		img: [
			AssetPath + '/src/img/**/*.png',
			AssetPath + '/src/img/**/*.gif',
			AssetPath + '/src/img/**/*.jpeg',
			AssetPath + '/src/img/**/*.jpg'
		],
		img_sales: [
			FilesPath + '/images/sales/*.png',
			FilesPath + '/images/sales/*.gif',
			FilesPath + '/images/sales/*.jpeg',
			FilesPath + '/images/sales/*.jpg'
		],
		img_ui: [
			UiPath + '/img/*.png',
			UiPath + '/img/*.gif',
			UiPath + '/img/*.jpeg',
			UiPath + '/img/*.jpg'
		],
		svg: [AssetPath + '/src/img/**/*.svg'],
		css: [AssetPath + '/src/css/**/*.css'],
		js: [AssetPath + '/src/js/**/*.js']
	},
	bundle: {
		css: [
			AssetPath + '/dist/css/website-sass.css',
			AssetPath + '/dist/css/website-less.css'
		],
		js: [AssetPath + '/dist/js/website.js']
	},
	rev: {
		css: [
			AssetPath + '/dist/css/**/*.css',
			AssetPath + '/src/vendor/**/*.css'
		],
		js: [AssetPath + '/dist/js/**/*.js', AssetPath + '/src/vendor/**/*.js']
	}
};

var Output = {
	compile: {
		less: AssetPath + '/src/css',
		sass: AssetPath + '/src/css'
	},
	min: {
		img: AssetPath + '/dist/img',
		img_sales: FilesPath + '/images/sales',
		img_ui: UiPath + '/img',
		svg: AssetPath + '/dist/img',
		css: AssetPath + '/dist/css',
		js: AssetPath + '/dist/js'
	},
	bundle: {
		css: AssetPath + '/dist/css',
		js: AssetPath + '/dist/js'
	},
	rev: {
		css: AssetPath + '/rev/css',
		js: AssetPath + '/rev/js'
	}
};

function errorHandler(error) {
	console.log(error.toString());
	this.emit('end');
}

/** Min IMG **/
gulp.task('img_min', function() {
	gulp.src(AssetPath + '/src/img/favicon/*').pipe(
		gulp.dest(AssetPath + '/dist/img/favicon/')
	);

	return gulp
		.src(Source.min.img)
		.pipe(
			imagemin(
				[
					imagemin.gifsicle({
						interlaced: true
					}),
					imagemin.jpegtran({
						progressive: true
					}),
					imagemin.optipng({
						optimizationLevel: 5
					})
				],
				{
					verbose: true
				}
			)
		)
		.on('error', errorHandler)
		.pipe(gulp.dest(Output.min.img));
});

gulp.task('img_sales_min', function() {
	return gulp
		.src(Source.min.img_sales)
		.pipe(
			imagemin(
				[
					imagemin.gifsicle({
						interlaced: true
					}),
					imagemin.jpegtran({
						progressive: true
					}),
					imagemin.optipng({
						optimizationLevel: 5
					})
				],
				{
					verbose: true
				}
			)
		)
		.on('error', errorHandler)
		.pipe(gulp.dest(Output.min.img_sales));
});

gulp.task('img_sales_resize', function() {
	return gulp
		.src(Source.min.img_sales)
		.pipe(
			imageResize({
				width: 800,
				height: 600,
				cover: true,
				crop: true,
				upscale: false
			})
		)
		.on('error', errorHandler)
		.pipe(gulp.dest(Output.min.img_sales));
});

gulp.task('img_ui', function() {
	return gulp
		.src(Source.min.img_ui)
		.pipe(
			imagemin(
				[
					imagemin.gifsicle({
						interlaced: true
					}),
					imagemin.jpegtran({
						progressive: true
					}),
					imagemin.optipng({
						optimizationLevel: 5
					})
				],
				{
					verbose: true
				}
			)
		)
		.on('error', errorHandler)
		.pipe(gulp.dest(Output.min.img_ui));
});

/** Min SVG **/
gulp.task('svg_min', function() {
	return gulp
		.src(Source.min.svg)
		.pipe(svgmin())
		.on('error', errorHandler)
		.pipe(gulp.dest(Output.min.svg));
});

/** Compile LESS **/
gulp.task('less_compile', function() {
	return gulp
		.src(Source.compile.less)
		.pipe(less())
		.on('error', errorHandler)
		.pipe(gulp.dest(Output.compile.less));
});

/** Compile SASS **/
gulp.task('sass_compile', function() {
	return gulp
		.src(Source.compile.sass)
		.pipe(sass())
		.on('error', errorHandler)
		.pipe(gulp.dest(Output.compile.sass));
});

/** Min CSS **/
gulp.task('css_min', function() {
	return gulp
		.src(Source.min.css)
		.pipe(
			csso({
				restructure: true,
				comments: false,
				forceMediaMerge: true
			})
		)
		.on('error', errorHandler)
		.pipe(gulp.dest(Output.min.css));
});

/** Min JS **/
gulp.task('js_min', function() {
	return gulp
		.src(Source.min.js)
		.pipe(uglify())
		.on('error', errorHandler)
		.pipe(gulp.dest(Output.min.js));
});

/** Bundle CSS **/
gulp.task('css_bundle', function() {
	return gulp
		.src(Source.bundle.css)
		.pipe(concat('bundle.min.css'))

		.pipe(gulp.dest(Output.bundle.css));
});

/** Bundle JS **/
gulp.task('js_bundle', function() {
	return gulp
		.src(Source.bundle.js)
		.pipe(concat('bundle.min.js'))

		.pipe(gulp.dest(Output.bundle.js));
});

/** Rev CSS **/
gulp.task('css_rev', function() {
	return gulp
		.src(Source.rev.css)

		.pipe(rev())
		.pipe(gulp.dest(Output.rev.css))

		.pipe(rev.manifest())
		.pipe(gulp.dest(Output.rev.css));
});

/** Rev JS **/
gulp.task('js_rev', function() {
	return gulp
		.src(Source.rev.js)

		.pipe(rev())
		.pipe(gulp.dest(Output.rev.js))

		.pipe(rev.manifest())
		.pipe(gulp.dest(Output.rev.js));
});

/** Real Favicon **/
var FAVICON_DATA_FILE = AssetPath + '/src/img/favicon/faviconData.json';

gulp.task('generate-favicon', function(done) {
	return realFavicon.generateFavicon(
		{
			masterPicture: AssetPath + '/src/img/favicon/favicon.png',
			dest: AssetPath + '/src/img/favicon/',
			iconsPath: '/assets/dist/img/favicon/',
			design: {
				ios: {
					pictureAspect: 'backgroundAndMargin',
					backgroundColor: Brand.colors.bg_dark,
					margin: '14%',
					assets: {
						ios6AndPriorIcons: false,
						ios7AndLaterIcons: false,
						precomposedIcons: false,
						declareOnlyDefaultIcon: true
					},
					appName: Brand.name
				},
				desktopBrowser: {},
				windows: {
					pictureAspect: 'noChange',
					backgroundColor: Brand.colors.bg_dark,
					onConflict: 'override',
					assets: {
						windows80Ie10Tile: true,
						windows10Ie11EdgeTiles: {
							small: true,
							medium: true,
							big: true,
							rectangle: true
						}
					},
					appName: Brand.name
				},
				androidChrome: {
					pictureAspect: 'backgroundAndMargin',
					margin: '8%',
					backgroundColor: Brand.colors.bg_dark,
					themeColor: Brand.colors.bg_dark,
					manifest: {
						name: Brand.name,
						startUrl: Brand.url,
						display: 'standalone',
						orientation: 'notSet',
						onConflict: 'override',
						declared: true
					},
					assets: {
						legacyIcon: false,
						lowResolutionIcons: false
					}
				}
			},
			settings: {
				scalingAlgorithm: 'Lanczos',
				errorOnImageTooSmall: false,
				readmeFile: true,
				htmlCodeFile: true,
				usePathAsIs: false
			},
			versioning: {
				paramName: 'v',
				paramValue: Math.random()
					.toString(36)
					.substr(2, 9)
			},
			markupFile: FAVICON_DATA_FILE
		},
		function(err) {
			done();
		}
	);
});

// Check for updates on RealFaviconGenerator (think: Apple has just
// released a new Touch icon along with the latest version of iOS).
// Run this task from time to time. Ideally, make it part of your
// continuous integration system.
gulp.task('check-for-favicon-update', function(done) {
	var currentVersion = JSON.parse(fs.readFileSync(FAVICON_DATA_FILE)).version;
	realFavicon.checkForUpdates(currentVersion, function(err) {
		if (err) {
			throw err;
		} else {
			done();
		}
	});
});

/** Tasks **/
gulp.task('watch', function() {
	gulp.watch(
		Source.watch.less,
		gulp.series('less_compile', 'css_min', 'css_bundle', 'css_rev')
	);
	gulp.watch(
		Source.watch.sass,
		gulp.series('sass_compile', 'css_min', 'css_bundle', 'css_rev')
	);
	gulp.watch(Source.watch.js, gulp.series('js_min', 'js_bundle', 'js_rev'));
});

gulp.task(
	'min',
	gulp.series(
		'js_min',
		'less_compile',
		'sass_compile',
		'css_min'
	)
);
gulp.task('bundle', gulp.series('js_bundle', 'css_bundle'));
gulp.task('rev', gulp.series('js_rev', 'css_rev'));

gulp.task('img', gulp.series('img_min', 'img_ui', 'svg_min'));
gulp.task(
	'css',
	gulp.series(
		'less_compile',
		'sass_compile',
		'css_min',
		'css_bundle',
		'css_rev'
	)
);
gulp.task('js', gulp.series('js_min', 'js_bundle', 'js_rev'));
gulp.task('default', gulp.series('min', 'bundle', 'rev'));
