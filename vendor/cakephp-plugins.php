<?php
$baseDir = dirname(dirname(__FILE__));
return [
    'plugins' => [
        'Bake' => $baseDir . '/vendor/cakephp/bake/',
        'Cache' => $baseDir . '/vendor/dereuromark/cakephp-cache/',
        'CakeDC/Users' => $baseDir . '/vendor/cakedc/users/',
        'Cake/Localized' => $baseDir . '/vendor/cakephp/localized/',
         'AkkaFacebook' => $baseDir . '/vendor/akkaweb/cakephp-facebook/',
        'DebugKit' => $baseDir . '/vendor/cakephp/debug_kit/',
        'Migrations' => $baseDir . '/vendor/cakephp/migrations/',
        'Search' => $baseDir . '/vendor/friendsofcake/search/',
        'WyriHaximus/TwigView' => $baseDir . '/vendor/wyrihaximus/twig-view/'
    ]
];